﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Input;
using System.CodeDom.Compiler;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Interactions.Internal;
using OpenQA.Selenium.Support.UI;
using SecureRx.Page_Elements;


namespace SecureRx.Page_Elements.Users
{
    public class New_User
    {
        public string canceBtn = "/html/body/div/main/div/div/form/div[2]/a";
        public string email = "//*[@id='Email']";
        public string firstName = "//*[@id='FirstName']";
        public string header = "/html/body/div/main/div/div/div/h4";
        //private By headerLocator = null;
        public string lastName = "//*[@id='LastName']";
        public string pswd = "//*[@id='Password']";
        public string submitBtn = "/html/body/div/main/div/div/form/div[2]/button";

        public New_User(IWebDriver driver)
        {
        }

    }
}
