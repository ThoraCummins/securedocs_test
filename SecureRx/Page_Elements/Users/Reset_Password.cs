﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Input;
using System.CodeDom.Compiler;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Interactions.Internal;
using OpenQA.Selenium.Support.UI;
using SecureRx.Page_Elements;

namespace SecureRx.Page_Elements.Users
{
    public class Reset_Password
    {
        public string cancelBtn = "/html/body/div/main/div/div/form/div[2]/a";
        public string email = "//*[@id='Email']";
        public string header = "/html/body/div/main/div/div/div/h4";
        public string pswd = "//*[@id='Password']";
        public string submitBtn = "/html/body/div/main/div/div/form/div[2]/button";

        public Reset_Password(IWebDriver driver)
        {
        }
    }
}
