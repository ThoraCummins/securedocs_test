﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Input;
using System.CodeDom.Compiler;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Interactions.Internal; //html/body/div/main/div/div/div[2]/table/tbody/tr[1]/td[1]
using OpenQA.Selenium.Support.UI;
using SecureRx.Page_Elements;
using SecureRx.Common;

namespace SecureRx.Page_Elements.Users
{
    public class Company_Details
    {
        SecureRx.Common.Common_Code cc = null;

        public string editUser = "/html/body/div/main/div/div/div[2]/table/tbody/tr[1]/td[5]/form/a[1]";
        public string header = "/html/body/div/main/div/div/div[1]/h4";
        private string newUser = " / html / body / div / main / div / div / div[1] / a";
        public string resetPassword = "/html/body/div/main/div/div/div[2]/table/tbody/tr[1]/td[5]/form/a[2]";
        private By UserLocator = null;
        public string userTable = "/html/body/div/main/div/div/div[2]/table/tbody";
        public string userTable2 = "/html/body/div/main/div/div[2]/div[2]/table/tbody";

        public Company_Details(IWebDriver driver)
        {
            cc = new Common_Code(driver);
        }

        public SecureRx.Page_Elements.Users.Edit_User OpenEditUser(IWebDriver driver)
        {
            cc.ClickButton(driver, editUser);

            //Is this the SecureDocs app?
            if (driver.Title != "Edit User - SecureRx")
            {
                throw new NoSuchWindowException(string.Concat("This is not the Edit User page - ", driver.Title));
            }

            return new SecureRx.Page_Elements.Users.Edit_User(driver);
        }

        public SecureRx.Page_Elements.Users.Edit_User OpenEditUser(IWebDriver driver, string ele)
        {
            //UserLocator = By.XPath(ele);
            cc.ClickButton(driver, ele);
            //driver.FindElement(UserLocator).Click();

            //Is this the SecureDocs app?
            if (driver.Title != "Edit User - SecureRx")
            {
                throw new NoSuchWindowException(string.Concat("This is not the Edit User page - ", driver.Title));
            }

            return new Edit_User(driver);
        }

        public SecureRx.Page_Elements.Users.New_User OpenNewUser(IWebDriver driver)
        {
            cc.ClickButton(driver, newUser);

            //Is this the SecureDocs app?
            if (driver.Title != "Create User - SecureRx")
            {
                throw new NoSuchWindowException(string.Concat("This is not the Create User page - ", driver.Title));
            }

            return new New_User(driver);
        }

        public SecureRx.Page_Elements.Users.Reset_Password OpenResetPassword(IWebDriver driver)
        {
            cc.ClickButton(driver, resetPassword);

            //Is this the SecureDocs app?
            if (driver.Title != "Reset Password - SecureRx")
            {
                throw new NoSuchWindowException(string.Concat("This is not the Reset Password page - ", driver.Title));
            }

            return new Reset_Password(driver);
        }

        public SecureRx.Page_Elements.Users.Reset_Password OpenResetPassword(IWebDriver driver, string ele)
        {
            //UserLocator = By.XPath(ele);
            cc.ClickButton(driver, ele);
            //driver.FindElement(UserLocator).Click();

            //Is this the SecureDocs app?
            if (driver.Title != "Reset Password - SecureRx")
            {
                throw new NoSuchWindowException(string.Concat("This is not the Reset Password page - ", driver.Title));
            }

            return new Reset_Password(driver);
            //}
        }
    }
}
