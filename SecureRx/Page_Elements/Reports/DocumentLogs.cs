﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Input;
using System.CodeDom.Compiler;
using System.IO;
using System.Text.RegularExpressions;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Interactions.Internal;
using OpenQA.Selenium.Support.UI;
using SecureRx.Page_Elements;
using SecureRx.Common;

namespace SecureRx.Page_Elements.Reports
{
    public class DocumentLogs
    {
        SecureRx.Common.Common_Code cc = null;

        public string applyBtn = "/html/body/div[2]/div[3]/div/button[1]";
        public string calendarElement = "//*[@id='reportrange']";
        public string customRange = "/html/body/div[2]/div[3]/ul/li[7]";
        public string endDate = "/html/body/div[2]/div[2]/div[1]/input";
        public string exportBtn = "/html/body/div[1]/main/div/div/div[1]/a";
        public string header = "/html/body/div[1]/main/div/div/div[1]/h4";
        public By locator = null;
        public string pageNumbers = "/html/body/div[1]/main/div/div/div[2]/div[3]/div[1]/p";
        public string searchBtn = "//*[@id='print-log-search']/div[1]/div/span/button";
        public string searchText = "//*[@id='searchTextInput']";
        public string startDate = "/html/body/div[2]/div[1]/div[1]/input";
        public string tablegrid = "//*[@id='logsListTable']/tbody";
        public string toDay = "/html/body/div[2]/div[3]/ul/li[1]";
        public string yesterDay = "/html/body/div[2]/div[3]/ul/li[2]";

        public DocumentLogs(IWebDriver driver)
        {
            cc = new Common_Code(driver);
        }

        public SecureRx.Page_Elements.Reports.Document_Logs_Details OpenDocumentLogDetails(IWebDriver driver)
        {
            cc.ClickButton(driver, string.Concat(tablegrid, "/tr[1]/td[1]"));

            //Is this the SecureDocs app?
            if (driver.Title != "Document Details - SecureRx")
            {
                throw new NoSuchWindowException(string.Concat("This is not the Document Log Detail page - ", driver.Title));
            }
            return new Document_Logs_Details(driver);
        }
    }
}
