﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Drawing;
using System.Threading;
using System.Windows.Input;
using System.CodeDom.Compiler;
using System.IO;
using System.Text.RegularExpressions;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Interactions.Internal;
using OpenQA.Selenium.Support.UI;

namespace SecureRx.Page_Elements
{
    public class LoginPage
    {
        public LoginPage(IWebDriver driver)
        {
        }

        //variables
        private string buildVersion = "/html/body/div[2]";
        private By buildVersionLocator = null;
        private string logOn = "/html/body/div[1]/div/div/div/div/div/form/div[6]/div[1]/button";
        private By logOnButton = null;
        private string pswd = "//*[@id='Password']";
        private By pswdLocator = null;
        private string username = "//*[@id='Email']";
        private By usernameLocator = null;
        //private By vpaHeaderLocator = null;

        public string FetchBuildVersion(IWebDriver driver)
        {
            buildVersionLocator = By.XPath(buildVersion);
            return driver.FindElement(buildVersionLocator).GetAttribute("innerHTML");
        }

        public LoginPage(IWebDriver driver, string url)
        {
            driver.Navigate().GoToUrl(url);
            driver.Manage().Window.Maximize();
            Thread.Sleep(15000);

        }

        public MainPage SDLogin(string user, string paswd, IWebDriver driver)
        {
            logOnButton = By.XPath(logOn);
            pswdLocator = By.XPath(pswd);
            usernameLocator = By.XPath(username);
            IWebElement userEle = null;

            userEle = driver.FindElement(usernameLocator);
            userEle.SendKeys(user);
            driver.FindElement(pswdLocator).Clear();
            driver.FindElement(pswdLocator).SendKeys(paswd);
            driver.FindElement(logOnButton).Click();

            //Is this the SecureDocs app?
            //if (driver.Title != "Home Page - SecureRx")
            //{
            //    throw new NoSuchWindowException(string.Concat("This is not the Home Page : ", driver.Title));
            //}

            return new MainPage(driver);
        }
    }
}
