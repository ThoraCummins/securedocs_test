﻿using System.Configuration;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Input;
using System.CodeDom.Compiler;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Interactions.Internal;
using OpenQA.Selenium.Support.UI;
using SecureRx.Page_Elements;

namespace SecureRx.Page_Elements.API
{
    public class API_Page
    {
        public string header = "/html/body/div/main/div/div/div[1]/h4";
        public string clientID = "/html/body/div/main/div/div/div[2]/div/div[1]/div";
        public string clientSecret = "//*[@id='clientSecret']";
        public string showSecret = "//*[@id='showSecretButton']";

        public API_Page(IWebDriver driver)
        {
        }
    }
}
