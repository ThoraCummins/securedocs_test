﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Input;
using System.CodeDom.Compiler;
using System.IO;
using System.Text.RegularExpressions;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Interactions.Internal;
using OpenQA.Selenium.Support.UI;
using SecureRx.Page_Elements;

namespace  SecureRx.Page_Elements.PrintAgents
{
    public class Edit_Print_Agent
    {
        public string addMetadata = "//*[@id='app']/form/div[1]/fieldset/div[9]/div[2]/select";
        public string assignedTemplates = "//*[@id='app']/form/div[1]/fieldset/div[4]/div[1]/ol";
        public string availableTemplates = "//*[@id='app']/form/div[1]/fieldset/div[3]/div[1]/ol";///*[@id="app"]/form/div[1]/fieldset/div[3]/div[1]/ol/li[2]
        public string backgroundValue = "//*[@id='app']/form/div[1]/fieldset/div[6]/div/input";
        public string cancelBtn = "//*[@id='app']/form/div[2]/button[2]";
        public string computerName = "//*[@id='ClientId']";
        public string enableDisable = "/html/body/div/main/div/div[2]/div/div/div[2]/table/tbody/tr/td[5]/form/a[2]";
        public string enableFileOutput = "//*[@id='EnablePrinterOutput']";
        public string fileNameFormat = "//*[@id='app']/form/div[1]/fieldset/div[9]/div[1]/input";
        public string fileOutputTab = "//*[@id='file-output-tab']";
        public string foregroundValue = "//*[@id='app']/form/div[1]/fieldset/div[7]/div/input";
        public string Header = "/html/body/div/main/div/div/div/h4";
        //private By HeaderLocator = null;
        public string overridePantograph = "//*[@id='app']/form/div[1]/fieldset/div[5]/div/label/input";
        public string pantographBackground = "//*[@id='app']/form/div[1]/fieldset/div[6]/div/input";
        public string pantographForeground = "//*[@id='app']/form/div[1]/fieldset/div[7]/div/input";
        public string preprocessBackup = "//div[@id='app']/form/div/fieldset/div[6]/div/label/input";
        public string printer = "//*[@id='app']/form/div[1]/fieldset/div[7]/div/select";
        public string printerName = "//*[@id='SourcePrintQueueName']";
        public string saveBtn = "//*[@id='app']/form/div[2]/button[1]";
        public string saveBtn2 = "/html/body/div/main/div/div/form/div[3]/button";
        public string xpsPath = "//*[@id='OutputFilePath']";

        public Edit_Print_Agent(IWebDriver driver)
        {
        }

    }
}
