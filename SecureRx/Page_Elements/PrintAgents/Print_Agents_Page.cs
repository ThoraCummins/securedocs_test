﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Input;
using System.CodeDom.Compiler;
using System.IO;
using System.Text.RegularExpressions;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Interactions.Internal;
using OpenQA.Selenium.Support.UI;
using SecureRx.Page_Elements;
using SecureRx.Common;

namespace SecureRx.Page_Elements.PrintAgents
{
    public class Print_Agents_Page
    {
        SecureRx.Common.Common_Code cc = null;

        public string agentCount = "/html/body/div/main/div/div[1]/div";
        public string agentCount2 = "/html/body/div/main/div/div[2]/div";
        public string agentTable = "/html/body/div/main/div/div[2]/div/div/div[2]/table/tbody";///html/body/div/main/div/div[2]/div/div/div[2]/table/tbody/tr[1]/td[1]
        public string agentTable2 = "/html/body/div/main/div/div[3]/div/div/div[2]/table/tbody";
        public string agentTable3 = "/html/body/div/main/div/div[2]/div/div/div[2]/table/tbody";
        public string bulkAdd = "/html/body/div/main/div/div[2]/div/div/div[1]/a[1]";
        private string create = "/html/body/div/main/div/div[2]/div/div/div[1]/a[2]";
        //private By createLocator = null;
        public string deleteButton = "/html/body/div/main/div/div[2]/div/div/div[2]/table/tbody/tr[1]/td[7]/form/button";
        private string edit = "/html/body/div/main/div/div[2]/div/div/div[2]/table/tbody/tr[1]/td[5]/form/a[1]";
        //private By editLocator = null;
        public string enableDisable = "/html/body/div/main/div/div[2]/div/div/div[2]/table/tbody/tr/td[5]/form/a[2]";
        public string Header = "/html/body/div/main/div/div[2]/div/div/div[1]/h4";
        public string Header2 = "/html/body/div/main/div/div[2]/div/div/div[1]/h4";
        //private By locator = null;
        public string status = "/html/body/div/main/div/div[2]/div/div/div[2]/table/tbody/tr[3]/td[6]/span";
        public string statusBadge = "/html/body/div/main/div/div[2]/div/div/div[2]/table/tbody/tr/td[1]/span";
        private By tableLocator = null;

        public Print_Agents_Page(IWebDriver driver)
        {
            cc = new Common_Code(driver);
        }

        public SecureRx.Page_Elements.PrintAgents.Bulk_Add BulkAdd(IWebDriver driver)
        {
            cc.ClickButton(driver, bulkAdd);

            //Is this the correct page?
            if (driver.Title != "Bulk Add Print Agents - SecureRx")
            {
                throw new NoSuchWindowException(string.Concat("This is not the bulk add print agents page - ", driver.Title));
            }

            return new PrintAgents.Bulk_Add(driver);
        }

        public SecureRx.Page_Elements.PrintAgents.Create_Print_Agent CreatePrintAgent(IWebDriver driver)
        {
            cc.ClickButton(driver, create);

            //Is this the correct page?
            if (driver.Title != "Create Print Agent - SecureRx")
            {
                throw new NoSuchWindowException(string.Concat("This is not the create print agent page - ", driver.Title));
            }

            return new PrintAgents.Create_Print_Agent(driver);
        }

        public SecureRx.Page_Elements.PrintAgents.Edit_Print_Agent EditPrintAgent(IWebDriver driver)
        {
            cc.ClickButton(driver,edit);

            //Is this the correct page?
            if (driver.Title != "Edit Print Agent - SecureRx")
            {
                throw new NoSuchWindowException(string.Concat("This is not the Edit print agent page - ", driver.Title));
            }

            return new SecureRx.Page_Elements.PrintAgents.Edit_Print_Agent(driver);
        }

        public SecureRx.Page_Elements.PrintAgents.Edit_Print_Agent EditPrintAgent(IWebDriver driver, string ele)
        {
            //locator = By.XPath(ele);

            //driver.FindElement(locator).Click();
            cc.ClickButton(driver, ele);

            //Is this the SecureDocs app?
            if (driver.Title != "Edit Print Agent - SecureRx")
            {
                throw new NoSuchWindowException(string.Concat("This is not the Edit print agent page - ", driver.Title));
            }

            return new Edit_Print_Agent(driver);
        }

        public SecureRx.Page_Elements.PrintAgents.Edit_Print_Agent PrintAgentDetail(IWebDriver driver)
        {
            cc.ClickButton(driver, string.Concat(agentTable, "/tr[1]/td[1]"));

            //Is this the correct page?
            if (driver.Title != "Edit Print Agent - SecureRx")
            {
                throw new NoSuchWindowException(string.Concat("This is not the Edit print agent page - ", driver.Title));
            }

            return new PrintAgents.Edit_Print_Agent(driver);
        }

        public SecureRx.Page_Elements.PrintAgents.Edit_Print_Agent PrintAgentDetail(IWebDriver driver, string ele)
        {
            tableLocator = By.XPath(ele);

            driver.FindElement(tableLocator).Click();

            //Is this the correct page?
            if (driver.Title != "Edit Print Agent - SecureRx")
            {
                throw new NoSuchWindowException(string.Concat("This is not the edit print agent page - ", driver.Title));
            }

            return new PrintAgents.Edit_Print_Agent(driver);
        }

    }
}
