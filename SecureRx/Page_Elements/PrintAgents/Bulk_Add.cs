﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Input;
using System.CodeDom.Compiler;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Interactions.Internal;
using OpenQA.Selenium.Support.UI;
using SecureRx.Common;

namespace SecureRx.Page_Elements.PrintAgents
{
    public class Bulk_Add
    {
        public Bulk_Add(IWebDriver driver)
        {
        }

        public string chooseFileBtn = "//*[@id='bulkadd-form']/div[1]/div/input";
        public string header = "/html/body/div/div[2]/form/div[1]/div/p";
        //public string selectedFile = "//*[@id='bulkadd-form']/div[1]/div/input";
        public string tableGrid = "/html/body/div/div[2]/div/div/div/div[2]/table/tbody";
        public string upload = "//form[@id='bulkadd-form']/div[2]/div/input";
    }
}
