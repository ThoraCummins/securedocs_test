﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Input;
using System.CodeDom.Compiler;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Interactions.Internal;
using OpenQA.Selenium.Support.UI;
using SecureRx.Common;

namespace SecureRx.Page_Elements.PrintAgents
{
    public class Create_Print_Agent
    {
        //SecureRx.Common.Common_Code cc = null;

        public string appDiv = "//*[@id='app']";
        public string backupOutput = "//*[@id='app']/form/div[1]/fieldset/div[6]/div/input";
        public string cancelBtn = "//*[@id='app']/form/div[2]/button[2]";
        public string enableFileOutput = "//*[@id='EnableFileOutput']";
        public string fileOutputTab = "//*[@id='file-output-tab']";
        public string Header = "/html/body/div/main/div/div/div/h4";
        public string preProcessing = "//*[@id='app']/form/div[1]/fieldset/div[6]/div/label/input";
        public string printClient = "//*[@id='ClientId']";
        public string saveBtn = "/html/body/div/main/div/div/form/div[3]/button";
        public string VPN = "//*[@id='SourcePrintQueueName']";
        public string xpsFile = "//*[@id='OutputFilePath']";
        public string xpsFile2 = "//*[@id='app']/form/div[1]/fieldset/div[8]/div/input";

        public Create_Print_Agent(IWebDriver driver)
        {
        }

    }
}
