﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Input;
using System.CodeDom.Compiler;
using System.IO;
using System.Text.RegularExpressions;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Interactions.Internal;
using OpenQA.Selenium.Support.UI;
using SecureRx.Page_Elements;
using SecureRx.Common;

namespace SecureRx.Page_Elements.PrintAgents
{
    public class Print_Agent_Details
    {
        SecureRx.Common.Common_Code cc = null;

        public string computer = "/html/body/div/main/div/div/div/div/div[2]/div[3]/span[2]";
        private string edit = "/html/body/div/main/div/div/div/div/div[3]/a[2]";
        private By editLocator = null;
        public string fileOutput = "/html/body/div/main/div/div/div[2]/div[6]/span[2]";
        public string Header = "/html/body/div/main/div/div/div/div/div[1]/h4";
        public string printer = "/html/body/div/main/div/div/div/div/div[2]/div[6]/span[2]";
        public string printerName = "/html/body/div/main/div/div/div/div/div[2]/div[2]/span[2]";
        public string returnButton = "/html/body/div/main/div/div/div/div/div[3]/a[1]";
        public string serverName = "/html/body/div/main/div/div/div[2]/div[2]/span[2]";
        public string status = "/html/body/div/main/div/div/div/div/div[2]/div[1]/span[2]/span";

        public Print_Agent_Details(IWebDriver driver)
        {
            cc = new Common_Code(driver);
        }

        public SecureRx.Page_Elements.PrintAgents.Edit_Print_Agent EditPrintAgent(IWebDriver driver)
        {
            cc.ClickButton(driver, edit);

            //Is this the SecureDocs app?
            if (driver.Title != "Edit Print Agent - SecureRx")
            {
                throw new NoSuchWindowException(string.Concat("This is not the Edit Print Agent page - ", driver.Title));
            }

            return new PrintAgents.Edit_Print_Agent(driver);
        }

    }
}
