﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Input;
using System.CodeDom.Compiler;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Interactions.Internal;
using OpenQA.Selenium.Support.UI;
using SecureRx.Common;
using SecureRx.Page_Elements;

namespace SecureRx.Page_Elements
{
    public class MainPage
    {
        SecureRx.Common.Common_Code cc = null;

        //variables
        //private string accountSettings = "/html/body/div/div/nav/ul/li[7]/ul/li[2]/a";
        private string authActivityLink = "/html/body/div/div/nav/ul/li[3]/a";
        private By authActivityLocator = null;
        //private By allActivityLocator = null;
        private string adminLink = "/html/body/div/div/nav/ul/li[7]/a";
        //private By adminLocator = null;
        public string agentsCount = "/html/body/div/main/div/div[1]/div[3]/div/div/div[1]";
        //private string apiLink = "/html/body/div/div[1]/nav/ul/li[6]/ul/li[5]/a";
        //private By apiLocator = null;
        private string apiReferenceLink = "/html/body/div/div/nav/ul/li[8]/ul/li[2]/a";
        private string authPortalSettings = "/html/body/div/div/nav/ul/li[6]/ul/li[3]/a";
        //private By authPortalLinkLocator = null;
        //private string dashboardLink = "/html/body/div/div/nav/ul/li[1]/a";
        //private By dashboardLocator = null;
        private string documentLogs = "/html/body/div/div/nav/ul/li[5]/ul/li[1]/a";
        //private By documentLogsLocator = null;
        private string documentationLink = "/html/body/div/div/nav/ul/li[8]/ul/li[1]/a";
        public string documentsSecuredCount = "/html/body/div/main/div/div[1]/div[1]/div/div/div[1]";
        //private By elementLocator = null;
        private string generalLink = "/html/body/div/div/nav/ul/li[6]/ul/li[1]/a";
        //private By generalLinkLocator = null;
        private string logout = "/html/body/div/div/div[1]/span/a";
        //private By logoutLocator = null;
        public string pagesSecured = "/html/body/div/main/div/div[1]/div[2]/div/div/div[1]";
        public string portalEvents = "/html/body/div/div/nav/ul/li[5]/ul/li[2]/a";
        public By portalEventsLocator = null;
        public string prescriptions = "/html/body/div/div/nav/ul/li[2]/a/span";
        public string printAgentCount = "/html/body/div/main/div/div[1]/div[3]/div/div/div[1]";
        private string printAgentLink = "/html/body/div/div/nav/ul/li[6]/ul/li[4]/a";
        //private By printAgentLinkLocator = null;
        public string printLogTable = "//*[@id='logsListTable']/tbody";
        public string reportsLink = "/html/body/div/div/nav/ul/li[5]/a";
        public By reportsLinkLocator = null;
        private string resourcesLink = "/html/body/div/div/nav/ul/li[8]/a";
        //private By resourcesLinkLocator = null;
        private string serializationLink = "/html/body/div/div/nav/ul/li[6]/ul/li[2]/a";
        //private By serializationLinkLocator = null;
        public string securedDocumentsCount = "/html/body/div/main/div/div[1]/div[1]/div/div/div[1]";
        //private string securityKeyLink = "/html/body/div/div/nav/ul/li[6]/ul/li[4]/a";
        //private By securityKeyLinkLocator = null;
        public string securityTemplateCount = "/html/body/div/main/div/div[1]/div[4]/div/div/div[1]";
        private string securityTemplateLink = "/html/body/div/div/nav/ul/li[4]/a";
        //private By securityTemplateLinkLocator = null;
        private string settingsLink = "/html/body/div/div/nav/ul/li[6]/a";
        //private By settingsLocator = null;
        private string softwareLink = "/html/body/div/div/nav/ul/li[8]/ul/li[3]/a";
        private By tableLocator = null;
        private string username = "/html/body/div/div/div[1]/a";
        //private By usernameLocator = null;
        //private By userProfileLinkLocator = null;
        private string usersLink = "/html/body/div/div/nav/ul/li[7]/ul/li[1]/a";
        //private By usersLinkLocator = null;
        private string userProfileLink = "/html/body/div/div/div[1]/a";
        public string viewAllActivity = "/html/body/div/main/div/div[3]/div/div/div/a";

        public MainPage(IWebDriver driver)
        {
            cc = new Common_Code(driver);
        }

        public string FetchSerialNumber(IWebDriver driver, string col, string row)
        {
            tableLocator = By.XPath(string.Concat(printLogTable, "/tr[", row, "]/td[", col, "]/a"));
            return driver.FindElement(tableLocator).GetAttribute("innerHTML");
        }

        public void LogoutandCloseBrowser(IWebDriver driver)
        {
            cc.ClickButton(driver, logout);

            driver.Close();
            driver.Quit();
        }

        //--------------------------------------- Asserts -----------------------------------------
        public bool Assert_Negative_Value(string testValu, string expectValu)
        {
            if (testValu.Replace("-", "") == expectValu)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool Assert_No_Alphabetic_Characters(string testValu, string expectValu)
        {
            string invalidMatch = "";
            string pattern = @"[^1-9]";

            foreach (Match match in Regex.Matches(testValu, pattern))
                invalidMatch = "0";

            if (invalidMatch == "0") // any non numeric will be converted to 0, which is what is expected
            {
                return true;
            }
            else
            {
                if (testValu == expectValu) //if test value is numeric it should be same as expected value
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        //--------------------------------------- Open Pages -----------------------------------------
        //public SecureRx.Page_Elements.Users.Admin_Settings OpenAccountSettings(IWebDriver driver)
        //{
        //    adminLocator = By.XPath(adminLink);
        //    usersLinkLocator = By.XPath(accountSettings);

        //    driver.FindElement(adminLocator).Click();
        //    //Thread.Sleep(15000);
        //    driver.FindElement(usersLinkLocator).Click();
        //    //Thread.Sleep(30000);

        //    //Is this the SecureDocs app?
        //    if (driver.Title != "Account Settings - SecureRx")
        //    {
        //        throw new NoSuchWindowException(string.Concat("This is not the Account Settings page - ", driver.Title));
        //    }

        //    return new SecureRx.Page_Elements.Users.Admin_Settings(driver);
        //}

        public SecureRx.Page_Elements.AuthPortalActivity.AuthPortal_Activity OpenAuthenticationPortAct(IWebDriver driver)
        {
            authActivityLocator = By.XPath(authActivityLink);

            driver.FindElement(authActivityLocator).Click();
            //Thread.Sleep(10000);

            //Is this the SecureRx app?
            if (driver.Title != "Activity - SecureRx")
            {
                throw new NoSuchWindowException(string.Concat("This is not the Authentication Portal Activity page - ", driver.Title));
            }

            return new Page_Elements.AuthPortalActivity.AuthPortal_Activity(driver);
        }

        public SecureRx.Page_Elements.Activity.Activity OpenActivityFromDashboard(IWebDriver driver)
        {
            cc.ClickButton(driver, viewAllActivity);
            Thread.Sleep(5000);

            //Is this the SecureDocs app?
            if (driver.Title != "- SecureRx")
            {
                throw new NoSuchWindowException(string.Concat("This is not the Activity page - ", driver.Title));
            }

            return new SecureRx.Page_Elements.Activity.Activity(driver);
        }

        //public SecureRx.Page_Elements.API.API_Page OpenAPI(IWebDriver driver)
        //{
        //    settingsLocator = By.XPath(settingsLink);
        //    apiLocator = By.XPath(apiLink);

        //    driver.FindElement(settingsLocator).Click();
        //    //Thread.Sleep(15000);
        //    driver.FindElement(apiLocator).Click();
        //    //Thread.Sleep(30000);

        //    //Is this the SecureDocs app ?
        //    if (driver.Title != "API Keys - SecureRx")
        //    {
        //        throw new NoSuchWindowException(string.Concat("This is not the API page - ", driver.Title));
        //    }

        //    return new SecureRx.Page_Elements.API.API_Page(driver);
        //}

        public SecureRx.Page_Elements.Resources.API_References OpenAPIReference(IWebDriver driver)
        {
            cc.ClickButton(driver, resourcesLink);
            Thread.Sleep(500);
            cc.ClickButton(driver, apiReferenceLink);
            Thread.Sleep(500);

            driver.SwitchTo().Window(driver.CurrentWindowHandle);

            return new SecureRx.Page_Elements.Resources.API_References(driver);
        }

        public SecureRx.Page_Elements.Authentication.Authentication_Portal OpenAuthPort(IWebDriver driver)
        {
            cc.ClickButton(driver, settingsLink);
            Thread.Sleep(500);
            cc.ClickButton(driver, authPortalSettings);

            if (driver.Title != "Settings - SecureRx")
            {
                throw new NoSuchWindowException(string.Concat("This is not the Authentication Portal Settings page - ", driver.Title));
            }
            return new SecureRx.Page_Elements.Authentication.Authentication_Portal(driver);
        }

        public SecureRx.Page_Elements.Users.Company_Details OpenCompanyDetail(IWebDriver driver)
        {
            cc.ClickButton(driver, adminLink);
            Thread.Sleep(500);
            cc.ClickButton(driver, usersLink);
            Thread.Sleep(500);

            //Is this the SecureDocs app?
            if (driver.Title != "Company Details - SecureRx")
            {
                throw new NoSuchWindowException(string.Concat("This is not the Company Details page - ", driver.Title));
            }

            return new SecureRx.Page_Elements.Users.Company_Details(driver);
        }

        public SecureRx.Page_Elements.Resources.Documentation OpenDocumentation(IWebDriver driver)
        {
            cc.ClickButton(driver, resourcesLink);
            Thread.Sleep(500);
            cc.ClickButton(driver, documentationLink);
            Thread.Sleep(1000);

            //Is this the SecureDocs app?
            if (driver.Title != "Home Page - SecureRx")
            {
                throw new NoSuchWindowException(string.Concat("This is not the documentation page - ", driver.Title));
            }

            return new SecureRx.Page_Elements.Resources.Documentation(driver);
        }

        public SecureRx.Page_Elements.Reports.DocumentLogs OpenDocumentLogs(IWebDriver driver)
        {
            cc.ClickButton(driver, reportsLink);
            Thread.Sleep(500);
            cc.ClickButton(driver, documentLogs);

            if (driver.Title != "- SecureRx")
            {
                throw new NoSuchWindowException(string.Concat("This is not the Document Logs page - ", driver.Title));
            }

            return new Reports.DocumentLogs(driver);
        }

        public SecureRx.Page_Elements.Resources.Software_Downloads OpenDownloads(IWebDriver driver)
        {
            cc.ClickButton(driver, resourcesLink);
            Thread.Sleep(500);
            cc.ClickButton(driver, softwareLink);
            Thread.Sleep(500);

            //Is this the SecureDocs app?
            if (driver.Title != "Software Downloads - SecureRx")
            {
                throw new NoSuchWindowException(string.Concat("This is not the downloads page - ", driver.Title));
            }

            return new SecureRx.Page_Elements.Resources.Software_Downloads(driver);
        }

        public SecureRx.Page_Elements.Prescriptions.Prescriptions OpenPrescriptions(IWebDriver driver)
        {
            cc.ClickButton(driver, prescriptions);
            Thread.Sleep(500);

            //Is this the SecureRx app?
            if (driver.Title != "Prescriptions - SecureRx")
            {
                throw new NoSuchWindowException(string.Concat("This is not the Prescriptions page - ", driver.Title));
            }

            return new Prescriptions.Prescriptions(driver);
        }

        public SecureRx.Page_Elements.PrintAgents.Print_Agents_Page OpenPrintAgent(IWebDriver driver)
        {
            cc.ClickButton(driver, settingsLink);
            Thread.Sleep(500);
            cc.ClickButton(driver, printAgentLink);

            //Is this the SecureDocs app?
            if (driver.Title != "- SecureRx")
            {
                throw new NoSuchWindowException(string.Concat("This is not the Print Agent page - ", driver.Title));
            }

            return new SecureRx.Page_Elements.PrintAgents.Print_Agents_Page(driver);
        }

        public SecureRx.Page_Elements.Reports.PortalEvents OpenPortalEvents(IWebDriver driver)
        {
            cc.ClickButton(driver, reportsLink);
            Thread.Sleep(500);
            cc.ClickButton(driver, portalEvents);
            Thread.Sleep(500);

            if (driver.Title != "- SecureRx")
            {
                throw new NoSuchWindowException(string.Concat("This is not the Portal Events page - ", driver.Title));
            }

            return new Reports.PortalEvents(driver);
        }

        //public SecureRx.Page_Elements.SecurityKey.Security_Key OpenSecurityKey(IWebDriver driver)
        //{
        //    settingsLocator = By.XPath(settingsLink);
        //    securityKeyLinkLocator = By.XPath(securityKeyLink);

        //    driver.FindElement(settingsLocator).Click();
        //    //Thread.Sleep(5000);
        //    driver.FindElement(securityKeyLinkLocator).Click();
        //    //Thread.Sleep(5000);

        //    //Is this the SecureDocs app?
        //    if (driver.Title != "Security Key - SecureRx")
        //    {
        //        throw new NoSuchWindowException(string.Concat("This is not the Security Key page - ", driver.Title));
        //    }

        //    return new SecurityKey.Security_Key(driver);
        //}

        public SecureRx.Page_Elements.Serialization.Serialization OpenSerialization(IWebDriver driver)
        {
            cc.ClickButton(driver, settingsLink);
            Thread.Sleep(500);
            cc.ClickButton(driver, serializationLink);
            Thread.Sleep(500);

            //Is this the SecureDocs app?
            if (driver.Title != "Index - SecureRx")
            {
                throw new NoSuchWindowException(string.Concat("This is not the Serialization page - ", driver.Title));
            }

            return new Serialization.Serialization(driver);
        }

        public SecureRx.Page_Elements.Templates.SecurityTemplates OpenSecurityTemplates(IWebDriver driver)
        {
            cc.ClickButton(driver, securityTemplateLink);

            //Is this the SecureDocs app?
            if (driver.Title != "Security Templates - SecureRx")
            {
                throw new NoSuchWindowException(string.Concat("This is not the Security Templates page - ", driver.Title));
            }

            return new Templates.SecurityTemplates(driver);
        }

        public SecureRx.Page_Elements.Settings.Settings OpenSettings(IWebDriver driver)
        {
            cc.ClickButton(driver, settingsLink);
            Thread.Sleep(500);
            cc.ClickButton(driver, generalLink);
            Thread.Sleep(500);

            //Is this the SecureDocs app?
            if (driver.Title != "Settings - SecureRx")
            {
                throw new NoSuchWindowException(string.Concat("This is not the Settings page - ", driver.Title));
            }

            return new Settings.Settings(driver);
        }

        public SecureRx.Page_Elements.Activity.Activity_Detail OpenSpecificDocumentLog(IWebDriver driver, string row)
        {
            cc.ClickButton(driver, string.Concat(printLogTable, "/tr[", row, "]/td[1]"));

            //Is this the SecureRx app ?
            if (driver.Title != "Document Details - SecureRx")
            {
                throw new NoSuchWindowException(string.Concat("This is not the Document Log Detail page - ", driver.Title));
            }

            return new SecureRx.Page_Elements.Activity.Activity_Detail(driver);
        }

        public SecureRx.Page_Elements.Users.User_Profile OpenUserProfile(IWebDriver driver)
        {
            cc.ClickButton(driver, username);
            Thread.Sleep(500);
            cc.ClickButton(driver, userProfileLink);
            Thread.Sleep(500);

            //Is this the SecureDocs app?
            if (driver.Title != "Index - SecureRx")
            {
                throw new NoSuchWindowException(string.Concat("This is not a Docrity page - ", driver.Title));
            }

            return new SecureRx.Page_Elements.Users.User_Profile(driver);
        }

    }
}
