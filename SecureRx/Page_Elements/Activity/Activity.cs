﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Input;
using System.CodeDom.Compiler;
using System.IO;
using System.Text.RegularExpressions;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Interactions.Internal;
using OpenQA.Selenium.Support.UI;
using SecureRx.Common;

namespace SecureRx.Page_Elements.Activity
{
    public class Activity
    {
        public Activity (IWebDriver driver)
        {
        }

        public string applyButton = "/html/body/div[2]/div[3]/div/button[1]";
        public string calendarElement = "//*[@id='reportrange']";
        public string customRange = "/html/body/div[2]/div[3]/ul/li[7]";
        public string endDate = "/html/body/div[2]/div[2]/div[1]/input";
        public string exportBtn = "/html/body/div[1]/main/div/div/div[1]/a";
        public string Header = "/html/body/div[1]/main/div/div/div[1]/h4";
        public string last30Days = "/html/body/div[2]/div[3]/ul/li[4]";
        public string last7Days = "/html/body/div[2]/div[3]/ul/li[3]";
        public string lastMonth = "/html/body/div[2]/div[3]/ul/li[6]";
        public string nextBtn = "/html/body/div[1]/main/div/div/div[2]/div[3]/div[2]/nav/ul/li[2]/a";
        public string pageNumbers = "/html/body/div[1]/main/div/div/div[2]/div[3]/div[1]";
        public string prevBtn = "/html/body/div[1]/main/div/div/div[2]/div[3]/div[2]/nav/ul/li[1]/a";
        public string printLogTable = "//*[@id='logsListTable']/tbody";
        public string searchBtn = "//*[@id='print-log-search']/div[1]/div/span/button";
        public string searchText = "//*[@id='searchTextInput']";
        public string startDate = "/html/body/div[2]/div[1]/div[1]/input";
        public string table = "//*[@id='logsListTable']/tbody"; // used for common code
        private By tableLocator = null;
        public string thisMonth = "/html/body/div[2]/div[3]/ul/li[5]";
        public string toDay = "/html/body/div[2]/div[3]/ul/li[1]";
        public string yesterDay = "/html/body/div[2]/div[3]/ul/li[2]";

        // ---------------------------------- Open -----------------------------------
        public SecureRx.Page_Elements.Activity.Activity_Detail OpenActivityDetail(IWebDriver driver)
        {
            tableLocator = By.XPath(string.Concat(printLogTable, "/tr[1]"));

            driver.FindElement(tableLocator).Click();

            //Is this the SecureDocs app?
            if (driver.Title != "Print Job By Serial Number - SecureRx")
            {
                throw new NoSuchWindowException(string.Concat("This is not the Activity Detail page - ", driver.Title));
            }

            return new Activity_Detail(driver);
        }

        public SecureRx.Page_Elements.Activity.Activity_Detail OpenSpecificPrintLog(IWebDriver driver, string row)
        {
            tableLocator = By.XPath(string.Concat(printLogTable, "/tr[", row, "]/td[1]"));

            driver.FindElement(tableLocator).Click();

            return new Activity_Detail(driver);
        }

    }
}
