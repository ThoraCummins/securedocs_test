﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Input;
using System.CodeDom.Compiler;
using System.IO;
using System.Text.RegularExpressions;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Interactions.Internal;
using OpenQA.Selenium.Support.UI;
using SecureRx.Page_Elements;

namespace SecureRx.Page_Elements.Activity
{
    public class Activity_Detail
    {
        public Activity_Detail(IWebDriver driver)
        {
        }

        public string backToDashboard = "/html/body/div/main/div[1]/div/div[2]/ul/li[2]/a";
        public string copies = "/html/body/div/main/div[2]/div/div[3]/dl[1]/dd[3]";
        public string createdDate = "/html/body/div/main/div[2]/div/div[3]/dl[1]/dd[7]";
        public string dashboardLink = "/html/body/div/div[1]/nav/ul/li[1]/a";
        public string documentName = "/html/body/div/main/div[1]/div/div[1]";
        public string downloadDocument = "/html/body/div/main/div/div[2]/div/div/div[1]/a";
        public string Header = "/html/body/div/main/div/div/div/div/div/h4";
        public string pages = "/html/body/div/main/div[2]/div/div[3]/dl[1]/dd[2]";
        //public string printAgent = "/html/body/div/main/div[2]/div/div[3]/dl/dd[1]";
        public string PrintLogsPathLink = "/html/body/div/main/ol/li[2]/a";
        public string processTime = "/html/body/div/main/div[2]/div/div[3]/dl[1]/dd[5]";
        public string serialNumber = "/html/body/div/main/div[2]/div/div[3]/dl[1]/dd[1]";
        public string userName = "/html/body/div/main/div[2]/div/div[3]/dl[1]/dd[6]";
    }

}
