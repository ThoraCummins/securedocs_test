﻿using System.Configuration;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Input;
using System.CodeDom.Compiler;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Interactions.Internal;
using OpenQA.Selenium.Support.UI;
using SecureRx.Page_Elements;

namespace SecureRx.Page_Elements.Authentication
{
    public class Authentication_Portal
    {
        //Customa Messages Tab
        public string customMsgTab = "//*[@id='message-tab']";
        public string failedMessage = "//*[@id='ErrorMessage']";
        public string failedOptHTML = "//*[@id='ErrorHtml']";
        public string successMess = "//*[@id='SuccessMessage']";
        //Display Tab
        public string blockChain = "//*[@id='Blockchain']";
        public string displayPage = "//*[@id='display-tab']";
        public string showActivity = "//*[@id='EnableActivity']";
        public string showData = "//*[@id='EnableDataCapture']";
        public string showDocument = "//*[@id='EnableShowDocument']";
        public string showIssuerInfo = "//*[@id='EnableIssuerInformation']";
        public string optHTML = "//*[@id='DisplayHtml']";
        //General Tab
        public string accessibility = "//*[@id='Accessibility']";
        public string chooseFile = "//*[@id='general']/div[2]/input";
        public string companyEmail = "//*[@id='Email']";
        public string companyName = "//*[@id='CompanyName']";
        public string companyPhone = "//*[@id='Phone']";
        public string generalPage = "//*[@id='general-tab']";
        public string header = "/html/body/div/main/div/div[2]/form/div[1]/p";
        public string logoPreview = "//*[@id='general']/div[2]/div";
        //Misc
        public string saveBtn = "/html/body/div/main/div";
        //public string allowUserRegistration = "//*[@id='AllowRegistration']";
        //public string requireSignIn = "//*[@id='RequireSignIn']";

        public Authentication_Portal(IWebDriver driver)
        {
        }
    }
}
