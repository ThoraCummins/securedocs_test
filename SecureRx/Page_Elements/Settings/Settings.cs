﻿using System.Configuration;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Input;
using System.CodeDom.Compiler;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Interactions.Internal;
using OpenQA.Selenium.Support.UI;
using SecureRx.Page_Elements;

namespace SecureRx.Page_Elements.Settings
{
    public class Settings
    {
        public string auditLog = "//*[@id='ServerPrintLog']";
        public string auditLog2 = "//*[@id='print']/fieldset/div[2]/div[1]/label/span[2]";
        //public string auditLog3 = "/html/body/div/main/div/div[2]/form/div[2]/fieldset/div[1]/div[1]/label/span[1]";
        public string clientID = "//*[@id='keys']/fieldset/div[1]/div/div[1]/div";
        public string clientSecret = "//*[@id='clientSecret']";
        public string document_DocumentCapture = "//*[@id='CaptureDocuments']";
        public string document_DocumentCapture2 = "//*[@id='print']/fieldset/div[1]/div[1]/label/span[2]";
        public string document_saveTextCaptureData2 = "//*[@id='document']/fieldset/div/div[1]/label/span[1]";
        public string document_submit = "/html/body/div/main/div/div[2]/form/div[2]/button";
        public string document_submit2 = "/html/body/div/main/div/div[3]/form/div[2]/button";
        public string enableBlockchain = "//*[@id='AnchorToBlockchain']";
        public string enableBlockchain2 = "/html/body/div/main/div/div[2]/form/div[2]/fieldset/div[3]/div[1]/label/span[1]";
        public string enableBlockchain3 = "/html/body/div/main/div/div[2]/form/div[2]/fieldset/div[3]/div[1]/label/span[2]";
        public string header = "/html/body/div/main/div/div[2]/form/div[1]/h4";
        public string keysSettingsLink = "//*[@id='keys-tab']";
        public string locale = "//*[@id='Locale']";
        public string printAgentSettingsLink = "//*[@id='print-tab']";
        public string printAgentSettingsSaved = "/html/body/div/main/div/div[2]/form/div[2]/button";
        public string printAgentSettingsSaved2 = "/html/body/div/main/div/div[3]/form/div[2]/button";
        public string saveTextCaptureData = "//*[@id='SaveTextCaptureFields']";
        public string saveTextCaptureData3 = "//*[@id='document']/fieldset/div[2]/div[1]/label/span[2]";
        public string settingsSaved = "/html/body/div/div[2]";
        public string showSecret = "//*[@id='showSecretButton']";
        public string siteSave = "/html/body/div/main/div/div[2]/form/div[2]/button";
        public string siteSave2 = "/html/body/div/main/div/div[3]/form/div[2]/button";
        public string siteSettingsLink = "//*[@id='site-tab']";
        public string successUpdated = "/html/body/div/div[2]";

        public string syncInterval = "//*[@id='Interval']";
        public string timeoutSession = "//*[@id='SessionTimeout']";
        public string timeZone = "//*[@id='TimeZoneId']";

        public Settings(IWebDriver driver)
        {
        }
    }
}
