﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Input;
using System.CodeDom.Compiler;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Interactions.Internal;
using OpenQA.Selenium.Support.UI;
using SecureRx.Page_Elements;
using SecureRx.Common;

namespace SecureRx.Page_Elements.Prescriptions
{
    public class Prescriptions
    {
        SecureRx.Common.Common_Code cc = null;

        //private By elementLocator = null;
        public string header = "/html/body/div/main/div/div/div[1]/h4";
        public string script = "/html/body/div/main/div/div/div[2]/div[2]/div/table/tbody/tr[1]/td[2]/a";

        public Prescriptions(IWebDriver driver)
        {
            cc = new Common_Code(driver);
        }

        //-------------------------------------------------------------------------------------
        public SecureRx.Page_Elements.Prescriptions.Prescription_Details OpenScriptDetails(IWebDriver driver)
        {
            cc.ClickButton(driver, script);
            Thread.Sleep(500);

            //Is this the SecureRx app?
            if (driver.Title != "Prescription Details - SecureRx")
            {
                throw new NoSuchWindowException(string.Concat("This is not the Prescriptions Details page - ", driver.Title));
            }

            return new Prescription_Details(driver);
        }

    }
}
