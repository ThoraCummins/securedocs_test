﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Input;
using System.CodeDom.Compiler;
using System.IO;
using System.Text.RegularExpressions;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Interactions.Internal;
using OpenQA.Selenium.Support.UI;
using SecureRx.Page_Elements;

namespace SecureRx.Page_Elements.Serialization
{
    public class Serialization
    {
        public string addRegionBtn = "//*[@id='app']/div/div[2]/div[2]/button";
        public string addVisualRegion = "//*[@id='app']/div/div[2]/div/label/span[1]";
        public string addVisualRegion2 = "//*[@id='app']/div/div[2]/div/label/span[1]";
        public string barcode = "//*[@id='app']/div/div[2]/div[3]/div/div[2]/div[2]/div[2]/div/div[1]/label/input";
        public string barcode2 = "//*[@id='app']/div/div[2]/div[3]/div/div[2]/div[2]/div[2]/div/div[1]/label/span[1]";
        public string barcodeSize = "//*[@id='app']/div/div[2]/div[2]/div/div[2]/div[2]/div[2]/div/div[2]/div/div[1]/div/select";
        public string barcodeType = "//*[@id='app']/div/div[2]/div[2]/div/div[2]/div[2]/div[2]/div/div[2]/div/div[2]/div/select";
        public string beforeText = "//*[@id='app']/div/div[2]/div[2]/div/div[2]/div[2]/div[2]/div/div[2]/div/div[4]/div[1]/label/input";
        public string beforeText2 = "//*[@id='app']/div/div[2]/div[2]/div/div[2]/div[2]/div[2]/div/div[2]/div/div[4]/div[1]/label/span[1]";
        public string disableEnableBarCode = "//*[@id='app']/div/div[2]/div[2]/div/div[2]/div[2]/div[2]/div/div[1]/label/input";
        public string disableEnableBarCode2 = "//*[@id='app']/div/div[2]/div[2]/div/div[2]/div[2]/div[2]/div/div[1]/label/span[1]";
        public string disableEnableSerialization = "//*[@id='app']/div/div[2]/div[2]/div/div[2]/div[2]/div[1]/div/div[1]/label/input";
        public string disableEnableSerialization2 = "//*[@id='app']/div/div[2]/div[2]/div/div[2]/div[2]/div[1]/div/div[1]/label/span[1]";
        public string documentCapture = "//*[@id='app']/div/div[2]/div[1]/label/input";
        public string documentCapture2 = "//*[@id='app']/div/div[2]/div[1]/label/span[1]";
        public string fontList = "//*[@id='app']/div/div[2]/div[2]/div/div[2]/div[2]/div[1]/div/div[2]/div/div[2]/div/select";
        public string fontSize = "//*[@id='app']/div/div[2]/div[2]/div/div[2]/div[2]/div[1]/div/div[2]/div/div[3]/div/select";
        public string Header = "//*[@id='app']/div/div[1]/h4";
        public string includePageNumber = "//*[@id='app']/div/div[2]/div[2]/div/div[2]/div[2]/div[2]/div/div[2]/div/div[3]/div[1]/label/input";
        public string includePageNumber2 = "//*[@id='app']/div/div[2]/div[2]/div/div[2]/div[2]/div[2]/div/div[2]/div/div[3]/div[1]/label/span[1]";
        public string padding = "//*[@id='app']/div/div[2]/div[2]/div/div[2]/div[1]/div[2]/div[1]/div/input";
        public string jobMetadata = "//*[@id='app']/div/div[2]/div[2]/div/div[2]/div[2]/div[1]/div/div[2]/div/div[1]/div/select";
        public string position = "//*[@id='app']/div/div[2]/div[2]/div/div[2]/div[1]/div[1]/div/select";
        public string removeButton = "//*[@id='app']/div/div[2]/div[2]/div/div[1]/button";
        public string saveBtn = "//*[@id='app']/div/div[3]/form/button";
        public string serialization = "//*[@id='app']/div/div[2]/div[3]/div/div[2]/div[2]/div[1]/div/div[1]/label/input";
        public string serialization2 = "//*[@id='app']/div/div[2]/div[3]/div/div[2]/div[2]/div[1]/div/div[1]/label/span[1]";
        public string textArea = "//*[@id='app']/div/div[2]/div[2]/div/div[2]/div[2]/div[1]/div/div[2]/div/div[1]/div/input";
        public string transform = "//*[@id='app']/div/div[2]/div[2]/div/div[2]/div[2]/div[1]/div/div[2]/div/div[4]/div/select";

        public Serialization(IWebDriver driver)
        {
        }
    }
}
