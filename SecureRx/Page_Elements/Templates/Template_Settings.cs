﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Input;
using System.CodeDom.Compiler;
using System.IO;
using System.Text.RegularExpressions;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Interactions.Internal;
using OpenQA.Selenium.Support.UI;
using SecureRx.Page_Elements;

namespace SecureRx.Page_Elements.Templates
{
    public class Template_Settings
    {
        public Template_Settings(IWebDriver driver)
        {
        }

        private string cancelBtn = "/html/body/div[2]/div/div[1]/div/div/div[3]/button[2]";
        public string header = "/html/body/div[2]/div/div[1]/div/div/div[1]/h4";
        private By locator = null;

        //---------------------------------------------------------------------------------------------------
        public void CancelTemplateSettings(IWebDriver driver)
        {
            locator = By.XPath(cancelBtn);

            driver.FindElement(locator).Click();

        }

    }
}
