﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Input;
using System.CodeDom.Compiler;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Interactions.Internal;
using OpenQA.Selenium.Support.UI;
using SecureRx.Page_Elements;
using SecureRx.Common;

namespace SecureRx.Page_Elements.Templates
{
    public class SecurityTemplates
    {
        SecureRx.Common.Common_Code cc = null;

        public string breadcrumb = "/html/body/div/main/ol";
        //private By breadcrumbLocator = null;
        public string Header = "/html/body/div/main/div/div[1]/div[1]/h4";
        //private By HeaderLocator = null;
        private By locator = null;
        private string newTemplate = "/html/body/div/main/div/div[1]/div[1]/button";
        //private By newTemplateLocator = null;
        private string showUpload = "/html/body/div/main/div/div[2]/div/div/div[1]/div[1]/div/a[2]";
        public string templates = "/html/body/div/main/div/div[2]/div/div";
        public string templateGrid = "/html/body/div/main/div/div[2]/div/div";
        public string templateGrid2 = "/html/body/div/main/div/div[2]/div[2]/div";
        private string templateEdit = "/html/body/div/main/div/div[2]/div/div/div[1]/div[1]/div/a[1]";
        private By templateLocator = null;
        public string templateName = "/html/body/div/main/div/div[2]/div/div/div[1]/div[2]/a";
        private string templateRemove = "/html/body/div/main/div/div[2]/div/div/div[1]/div[1]/div/a[3]";

        public SecurityTemplates(IWebDriver driver)
        {
            cc = new Common_Code(driver);
        }

        public SecureRx.Page_Elements.Templates.Remove_Security_Template RemoveTemplate(IWebDriver driver)
        {
            cc.ClickButton(driver, templateRemove);
            Thread.Sleep(500);

            //Is this the SecureDocs app?
            if (driver.Title != "- SecureRx")
            {
                throw new NoSuchWindowException(string.Concat("This is not the Delete Security Template page - ", driver.Title));
            }

            return new Remove_Security_Template(driver);
        }

        public SecureRx.Page_Elements.Templates.Remove_Security_Template RemoveTemplate(IWebDriver driver, string element)
        {
            templateLocator = By.XPath(element);

            driver.FindElement(templateLocator).Click();
            Thread.Sleep(3000);

            //Is this the SecureDocs app?
            if (driver.Title != "- SecureRx")
            {
                throw new NoSuchWindowException(string.Concat("This is not the Delete Security Template page - ", driver.Title));
            }

            return new Remove_Security_Template(driver);
        }

        public SecureRx.Page_Elements.Templates.Create_Security_Templates OpenCreateTemplate(IWebDriver driver)
        {
            cc.ClickButton(driver, newTemplate);

            System.Collections.Generic.List<string> previousHandles = new System.Collections.Generic.List<string>();
            System.Collections.Generic.List<string> currentHandles = new System.Collections.Generic.List<string>();
            previousHandles.AddRange(driver.WindowHandles);

            for (int i = 0; i < 20; i++)
            {
                currentHandles.Clear();
                currentHandles.AddRange(driver.WindowHandles);
                foreach (string s in previousHandles)
                {
                    currentHandles.RemoveAll(p => p == s);
                }
                if (currentHandles.Count == 1)
                {
                    driver.SwitchTo().Window(currentHandles[0]);
                    Thread.Sleep(100);
                }
                else
                {
                    Thread.Sleep(500);
                }
            }

            return new Create_Security_Templates(driver);
        }

        public SecureRx.Page_Elements.Templates.Show_Upload OpenShowUpload(IWebDriver driver)
        {
            locator = By.XPath(showUpload);
            driver.FindElement(locator).Click();

            System.Collections.Generic.List<string> previousHandles = new System.Collections.Generic.List<string>();
            System.Collections.Generic.List<string> currentHandles = new System.Collections.Generic.List<string>();
            previousHandles.AddRange(driver.WindowHandles);

            for (int i = 0; i < 20; i++)
            {
                currentHandles.Clear();
                currentHandles.AddRange(driver.WindowHandles);
                foreach (string s in previousHandles)
                {
                    currentHandles.RemoveAll(p => p == s);
                }
                if (currentHandles.Count == 1)
                {
                    driver.SwitchTo().Window(currentHandles[0]);
                    Thread.Sleep(100);
                }
                else
                {
                    Thread.Sleep(500);
                }
            }

            return new Show_Upload(driver);
        }

        public SecureRx.Page_Elements.Templates.Show_Upload OpenShowUpload(IWebDriver driver,string loc)
        {
            locator = By.XPath(loc);
            driver.FindElement(locator).Click();

            System.Collections.Generic.List<string> previousHandles = new System.Collections.Generic.List<string>();
            System.Collections.Generic.List<string> currentHandles = new System.Collections.Generic.List<string>();
            previousHandles.AddRange(driver.WindowHandles);

            for (int i = 0; i < 20; i++)
            {
                currentHandles.Clear();
                currentHandles.AddRange(driver.WindowHandles);
                foreach (string s in previousHandles)
                {
                    currentHandles.RemoveAll(p => p == s);
                }
                if (currentHandles.Count == 1)
                {
                    driver.SwitchTo().Window(currentHandles[0]);
                    Thread.Sleep(100);
                }
                else
                {
                    Thread.Sleep(500);
                }
            }

            return new Show_Upload(driver);
        }

        public SecureRx.Page_Elements.Templates.Template OpenTemplate(IWebDriver driver)
        {
            templateLocator = By.XPath(templateEdit);

            driver.FindElement(templateLocator).Click();

            //Is this the SecureDocs app?
            if (driver.Title != "Security Template Designer - SecureRx")
            {
                throw new NoSuchWindowException(string.Concat("This is not the Security Template Designer page - ", driver.Title));
            }

            return new Template(driver);
        }

    }
}