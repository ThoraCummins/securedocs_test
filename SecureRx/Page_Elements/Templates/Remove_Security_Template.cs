﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Input;
using System.CodeDom.Compiler;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Interactions.Internal;
using OpenQA.Selenium.Support.UI;
using SecureRx.Page_Elements;

namespace SecureRx.Page_Elements.Templates
{
    public class Remove_Security_Template
    {
        public Remove_Security_Template(IWebDriver driver)
        {
        }

        public string header = "//*[@id='newTemplate']";
        private By locator = null;
        public string noBtn = "/html/body/div/div[2]/div/form/div[3]/a";
        public string yesBtn = "/html/body/div/div[2]/div/form/div[3]/button";

        public SecureRx.Page_Elements.Templates.SecurityTemplates SecurityTemplateYES(IWebDriver driver, string btn)
        {
            if (btn == "Yes")
            {
                locator = By.XPath(yesBtn);
            }
            else
            {
                locator = By.XPath(noBtn);
            }

            driver.FindElement(locator).Click();

            //Is this the SecureDocs app?
            if (driver.Title != "Security Templates - SecureRx")
            {
                throw new NoSuchWindowException(string.Concat("This is not the Security Templates page - ", driver.Title));
            }

            return new SecurityTemplates(driver);
        }
    }
}
