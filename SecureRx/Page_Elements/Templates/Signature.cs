﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Input;
using System.CodeDom.Compiler;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Interactions.Internal;
using OpenQA.Selenium.Support.UI;
using SecureRx.Page_Elements;
using SecureRx.Common;

namespace SecureRx.Page_Elements.Templates
{
    public class Signature
    {
        SecureRx.Common.Common_Code cc = null;

        public Signature(IWebDriver driver)
        {
            cc = new SecureRx.Common.Common_Code(driver);
        }

        private string cancelButton = "//*[@id='signature-ui']/div/div[3]/button[3]";
        private By elementLocator = null;
        public string newSignature = "//*[@id='signature-ui']/div/div[3]/button[1]";
        public string signatureArea = "//*[@id='signature-ui']/div/div[2]/div[2]/span";
        public string signButton = "//*[@id='signature-ui']/div/div[3]/button[2]";
        private int timeoutInMilliseconds = 500;

        public void CancelSignature(IWebDriver driver, string text)
        {
            Actions actions = new Actions(driver);
            IWebElement ele = null;
            elementLocator = By.XPath(signatureArea);

            cc.ClickButton(driver, newSignature);
            Thread.Sleep(500);
            ele = WebDriverExtensions.FindElementInMilliseconds(driver, elementLocator, timeoutInMilliseconds);

            actions.MoveToElement(ele);
            actions.Click();
            actions.SendKeys(text);
            actions.Build().Perform();

            cc.ClickButton(driver, cancelButton);
        }

        public void EnterSignature(IWebDriver driver, string text)
        {
            Actions actions = new Actions(driver);
            IWebElement ele = null;
            elementLocator = By.XPath(signatureArea);

            cc.ClickButton(driver, newSignature);
            Thread.Sleep(500);
            ele = WebDriverExtensions.FindElementInMilliseconds(driver, elementLocator, timeoutInMilliseconds);

            actions.MoveToElement(ele);
            actions.Click();
            actions.SendKeys(text);
            actions.Build().Perform();

            cc.ClickButton(driver, signButton);
        }
    }
}
