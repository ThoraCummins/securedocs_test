﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Input;
using System.CodeDom.Compiler;
using System.IO;
using System.Text.RegularExpressions;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Interactions.Internal;
using OpenQA.Selenium.Support.UI;
using SecureRx.Page_Elements;

namespace SecureRx.Page_Elements.Templates
{
    public class Protect_Settings
    {
        public Protect_Settings(IWebDriver driver)
        {
        }

        public string cancelBtn = "//*[@id='modal-protect']/div/div[3]/button[3]";
        public string header = "//*[@id='modal-protect']/div/div[1]/h4";
        public By locator = null;

        //------------------------------------------------------------------------------------------------
        public void CancelProtectSettings(IWebDriver driver)
        {
            locator = By.XPath(cancelBtn);

            driver.FindElement(locator).Click();
            
        }
    }
}
