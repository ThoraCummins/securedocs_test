﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Input;
using System.CodeDom.Compiler;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Interactions.Internal;
using OpenQA.Selenium.Support.UI;
using SecureRx.Page_Elements;
using SecureRx.Common;

namespace SecureRx.Page_Elements.Templates
{
    public class Template
    {
        SecureRx.Common.Common_Code cc = null;

        //AuthMark
        public string authMark_BackgroundColor = "//*[@id='root']/div/aside/div/div[2]/div[7]/div[1]/div";//*[@id="root"]/div/aside/div/div[2]/div[7]/div/div
        public string authMark_BackgroundColor2 = "//*[@id='root']/div/aside/div/div[2]/div[7]/div[2]/div[1]";
        public string authMark_Background_A = "//*[@id='root']/div/aside/div/div[2]/div[7]/div[2]/div[2]/div[3]/div[5]/div/input";
        public string authMark_Background_B = "//*[@id='root']/div/aside/div/div[2]/div[7]/div[2]/div[2]/div[3]/div[4]/div/input";
        public string authMark_Background_G = "//*[@id='root']/div/aside/div/div[2]/div[7]/div[2]/div[2]/div[3]/div[3]/div/input";
        public string authMark_Background_Hex = "//*[@id='root']/div/aside/div/div[2]/div[7]/div[2]/div[2]/div[3]/div[1]/div/input";
        public string authMark_Background_R = "//*[@id='root']/div/aside/div/div[2]/div[7]/div[2]/div[2]/div[3]/div[2]/div/input";
        public string authMark_ConfigureBtn = "//*[@id='root']/div/aside/div/div[2]/div[2]/div[2]/button";
        public string authMark_ConfigureCancel = "/html/body/div[2]/div/div[1]/div/div/div[3]/button[2]";
        public string authMark_ConfigureSave = "/html/body/div[2]/div/div[1]/div/div/div[3]/button[1]";
        public string authMarkFont = "//*[@id='root']/div/aside/div/div[2]/div[3]/div[2]/div/select";
        public string authMarkFontSize = "//*[@id='root']/div/aside/div/div[2]/div[3]/div[3]/div/input";
        public string authMark_ForegroundColor = "//*[@id='root']/div/aside/div/div[2]/div[5]/div/div";
        public string authMark_ForegroundColor2 = "//*[@id='root']/div/aside/div/div[2]/div[5]";
        public string authMark_Foreground_A = "//*[@id='root']/div/aside/div/div[2]/div[5]/div[2]/div[2]/div[3]/div[5]/div/input";
        public string authMark_Foreground_B = "//*[@id='root']/div/aside/div/div[2]/div[5]/div[2]/div[2]/div[3]/div[4]/div/input";
        public string authMark_Foreground_G = "//*[@id='root']/div/aside/div/div[2]/div[5]/div[2]/div[2]/div[3]/div[3]/div/input";
        public string authMark_Foreground_Hex = "//*[@id='root']/div/aside/div/div[2]/div[5]/div[2]/div[2]/div[3]/div[1]/div/input";
        public string authMark_Foreground_R = "//*[@id='root']/div/aside/div/div[2]/div[5]/div[2]/div[2]/div[3]/div[2]/div/input";
        public string authMarkHashDataFields = "/html/body/div[2]/div/div[1]/div/div/div[2]/div/div/div/label/input";
        public string authMarkImage = "//*[@id='root']/div/div/div/div[1]/div[2]/div[1]/div/div[4]/div/div/div/button[1]";
        public string authMarkShowAuthCode = "//*[@id='root']/div/aside/div/div[2]/div[2]/div[2]/label/input";
        //Capture
        public string captureAvailableDataList = "/html/body/div[2]/div/div[1]/div/div/div[2]/div[2]/div";
        public string captureName = "//*[@id='root']/div/aside/div/div[2]/div[2]/div[2]/div/input";
        public string captureCancel = "/html/body/div[2]/div/div[1]/div/div/div[3]/button[2]";
        public string captureSave = "/html/body/div[2]/div/div[1]/div/div/div[3]/button[1]";
        public string captureText = "//*[@id='root']/div/nav/div/div[1]/div/div[2]/div[1]/img";
        #region Common Element Properties
        //Common Element Properties
        public string element_AlignCenter = "//*[@id='root']/div/aside/div/div[2]/div[2]/div[3]/button[2]";
        public string element_AlignLeft = "//*[@id='root']/div/aside/div/div[2]/div[2]/div[3]/button[1]";
        public string element_AlignRight = "//*[@id='root']/div/aside/div/div[2]/div[2]/div[3]/button[3]";
        public string elementBackgroundColor = "//*[@id='root']/div/aside/div/div[2]/div[6]/div";
        public string elementBackgroundColor2 = "//*[@id='root']/div/aside/div/div[2]/div[6]";
        public string element_Background_A = "//*[@id='root']/div/aside/div/div[2]/div[6]/div[2]/div[2]/div[3]/div[5]/div/input";
        public string element_Background_B = "//*[@id='root']/div/aside/div/div[2]/div[6]/div[2]/div[2]/div[3]/div[4]/div/input";
        public string element_Background_G = "//*[@id='root']/div/aside/div/div[2]/div[6]/div[2]/div[2]/div[3]/div[3]/div/input";
        public string element_Background_Hex = "//*[@id='root']/div/aside/div/div[2]/div[6]/div[2]/div[2]/div[3]/div[1]/div/input";
        public string element_Background_R = "//*[@id='root']/div/aside/div/div[2]/div[6]/div[2]/div[2]/div[3]/div[2]/div/input";
        public string element_EditText = "//*[@id='root']/div/div/div/div[2]/div/div[1]/div/div/div/div[3]/div[2]/button";
        public string elementForeground = "//*[@id='root']/div/aside/div/div[2]/div[4]/div/div";
        public string elementForeground2 = "//*[@id='root']/div/aside/div/div[2]/div[4]";
        public string element_Foreground_A = "//*[@id='root']/div/aside/div/div[2]/div[4]/div[2]/div[2]/div[3]/div[5]/div/input";
        public string element_Foreground_B = "//*[@id='root']/div/aside/div/div[2]/div[4]/div[2]/div[2]/div[3]/div[4]/div/input";
        public string element_Foreground_G = "//*[@id='root']/div/aside/div/div[2]/div[4]/div[2]/div[2]/div[3]/div[3]/div/input";
        public string element_Foreground_Hex = "//*[@id='root']/div/aside/div/div[2]/div[4]/div[2]/div[2]/div[3]/div[1]/div/input";
        public string element_Foreground_R = "//*[@id='root']/div/aside/div/div[2]/div[4]/div[2]/div[2]/div[3]/div[2]/div/input";
        public string elementHeight = "//*[@id='root']/div/aside/div/div[2]/div[1]/div[5]/div/input";
        public string elementWidth = "//*[@id='root']/div/aside/div/div[2]/div[1]/div[4]/div/input";
        public string elementX = "//*[@id='root']/div/aside/div/div[2]/div[1]/div[3]/div/input";
        public string elementY = "//*[@id='root']/div/aside/div/div[2]/div[1]/div[2]/div/input";
        #endregion
        //Designer
        public string canvasID = "designer";
        public string canvasIframe = "/html/body/div/main/iframe";
        public string elementTab = "//*[@id='root']/div/aside/div/div/div[1]/a/img";
        //Exclusion
        public string exclusionImg = "//*[@id='root']/div/nav/div/div[3]/div/div[7]/div[1]/img";
        public string exclusion_Height = "//*[@id='root']/div/aside/div/div[2]/div[1]/div[5]/div/input";
        public string exclusion_Pantograph = "//*[@id='root']/div/div/div/div[2]/div/div[1]/div/div/div/div[2]/label[1]/input";
        public string exclusion_TROYMark = "//*[@id='root']/div/div/div/div[2]/div/div[1]/div/div/div/div[2]/label[2]/input";
        public string exclusion_Width = "//*[@id='root']/div/aside/div/div[2]/div[1]/div[4]/div/input";
        public string exclusion_X = "//*[@id='root']/div/aside/div/div[2]/div[1]/div[2]/div/input";
        public string exclusion_Y = "//*[@id='root']/div/aside/div/div[2]/div[1]/div[3]/div/input";
        #region Image Watermark
        public string imageWatermarkGrayscale = "//*[@id='root']/div/div/div/div[2]/div/div[1]/div/div/div/label[2]/input";
        public string imageWatermarkMaintainAspectRatio = "//*[@id='root']/div/div/div/div[2]/div/div[1]/div/div/div/label[1]/input";
        public string imageWatermarkNativeSize = "//*[@id='root']/div/div/div/div[2]/div/div[1]/div/div/div/div[5]/button";
        public string imageWatermarkOpacity = "//*[@id='root']/div/div/div/div[2]/div/div[1]/div/div/div/div[7]/div";
        public string imageWatermarkPosition = "//*[@id='root']/div/div/div/div[2]/div/div[1]/div/div/div/div[1]/div[6]/div/select";
        public string imageWatermarkRotation = "//*[@id='root']/div/div/div/div[2]/div/div[1]/div/div/div/div[9]/div";
        public string imageWatermarkUploadImage = "//*[@id='root']/div/div/div/div[2]/div/div[1]/div/div/div/div[4]/button";
        public string selectImageArea = "/html/body/div[2]/div/div[1]/div/div/div[2]/div";
        #endregion
        //Layers
        public string featureList = "//*[@id='root']/div/aside/div/ul";
        public string layersTab = "//*[@id='root']/div/aside/div/div/div[2]/a";
        public string layersUL = "//*[@id='root']/div/aside/div/ul";//*[@id="root"]/div/aside/div/ul/li[1]
        //Pantograph
        public string pantographBackground = "//*[@id='root']/div/aside/div/div[2]/div[5]/div[1]/div/input";
        public string pantographDownloadTestPatterns = "//*[@id='root']/div/aside/div/div[2]/div[7]/div/a";
        public string pantographEnableMicroprintBorder = "//*[@id='root']/div/aside/div/div[2]/div[9]/label/input";
        public string pantographFont = "//*[@id='root']/div/aside/div/div[2]/div[3]/div[2]/div/select";
        public string pantographFontSize = "//*[@id='root']/div/aside/div/div[2]/div[3]/div[3]/div/input";
        public string pantographForeground = "//*[@id='root']/div/aside/div/div[2]/div[5]/div[2]/div/input";
        public string pantographFullPage = "//*[@id='root']/div/aside/div/div[2]/div[1]/label/input";
        public string pantographImg = "//*[@id='root']/div/nav/div/div[3]/div/div[5]/div[1]/img";
        public string pantographIncludeTag = "//*[@id='root']/div/aside/div/div[2]/div[9]/div/div/div[8]/label/input";
        public string pantographIncludeTagFont = "//*[@id='root']/div/aside/div/div[2]/div[9]/div/div/div[11]/div/select";
        public string pantographIncludeTagFontSize = "//*[@id='root']/div/aside/div/div[2]/div[9]/div/div/div[12]/div/select";
        public string pantographIncludeTagText = "//*[@id='root']/div/aside/div/div[2]/div[9]/div/div/div[10]/div/input";
        public string pantographInterferencePattern = "//*[@id='root']/div/aside/div/div[2]/div[6]/div/div/input";
        public string pantographLineSpacing = "//*[@id='root']/div/aside/div/div[2]/div[3]/div[4]/div/input";
        public string pantographMPBorderColor = "//*[@id='root']/div/aside/div/div[2]/div[9]/div/div/div[5]/div";
        public string pantographMPBorderColor2 = "//*[@id='root']/div/aside/div/div[2]/div[9]/div/div/div[5]/div[2]/div[1]";
        public string pantographMPBorderFont = "//*[@id='root']/div/aside/div/div[2]/div[9]/div/div/div[3]/div/select";
        public string pantographMPBorderFontSize = "//*[@id='root']/div/aside/div/div[2]/div[9]/div/div/div[4]/div/select";
        public string pantographMPBorderMargin = "//*[@id='root']/div/aside/div/div[2]/div[9]/div/div/div[6]/div/input";
        public string pantographMPBorderPadding = "//*[@id='root']/div/aside/div/div[2]/div[9]/div/div/div[7]/div/input";
        public string pantographMPEditText = "//*[@id='root']/div/aside/div/div[2]/div[9]/div/div/div[1]/div/button";
        public string pantographMPText_A = "//*[@id='root']/div/aside/div/div[2]/div[9]/div/div/div[5]/div[2]/div[2]/div[3]/div[5]/div/input";
        public string pantographMPText_B = "//*[@id='root']/div/aside/div/div[2]/div[9]/div/div/div[5]/div[2]/div[2]/div[3]/div[4]/div/input";
        public string pantographMPText_G = "//*[@id='root']/div/aside/div/div[2]/div[9]/div/div/div[5]/div[2]/div[2]/div[3]/div[3]/div/input";
        public string pantographMPText_Hex = "//*[@id='root']/div/aside/div/div[2]/div[9]/div/div/div[5]/div[2]/div[2]/div[3]/div[1]/div/input";
        public string pantographMPText_R = "//*[@id='root']/div/aside/div/div[2]/div[9]/div/div/div[5]/div[2]/div[2]/div[3]/div[2]/div/input";
        public string pantographPosition = "//*[@id='root']/div/aside/div/div[2]/div[1]/div[6]/div/div/select";
        public string pantographRemoveAllSpaces = "//*[@id='root']/div/aside/div/div[2]/div[9]/div/div/div[2]/label/input";
        public string pantographTransform = "//*[@id='root']/div/aside/div/div[2]/div[3]/div[5]/div/select";
        //Protect Settings
        public string protectSettings = "//*[@id='menu-protect']";
        //QR Code
        public string qrCodeImage = "//*[@id='root']/div/nav/div/div[3]/div/div[3]/div[3]/img";
        //Redaction
        public string redactionImg = "//*[@id='root']/div/nav/div/div[3]/div/div[6]/div[2]/img";
        public string redact_Height = "//*[@id='root']/div/aside/div/div[2]/div/div[5]/div/input";
        public string redact_Width = "//*[@id='root']/div/aside/div/div[2]/div[1]/div[4]/div/input";
        public string redact_X = "//*[@id='root']/div/aside/div/div[2]/div/div[2]/div/input";
        public string redact_Y = "//*[@id='root']/div/aside/div/div[2]/div/div[3]/div/input";
        //SecureMark
        public string secureMarkImage = "//*[@id='root']/div/nav/div/div[3]/div/div[3]/div[1]/img";
        //*
        public string captureElements = "//*[@id='root']/div/nav/div/div[1]/a/img";
        private string ddlMenu = "//*[@id='dropdownMenuButton']";
        public By eleLocator = null;
        private string exitBtn = "//*[@id='root']/div/div/div/div[1]/div[1]/div/div[1]/div/div/button[9]";
        public By locator = null;
        private string return2Temp = "//*[@id='root']/div/main/header/div[1]/a";
        private By return2TempLocator = null;
        public string saveButton = "//*[@id='root']/div/div/div/div[1]/div[1]/div/div[2]/div/button";
        public string saveButton2 = "//*[@id='root']/div/aside/div/div[2]/div[7]/div[2]/div[1]";
        public string securityElements = "//*[@id='root']/div/nav/div/div[3]/a/img";
        private string showAdvancedCode = "//*[@id='root']/div/div/div/div[1]/div[1]/div/div[1]/div/div/div[3]/button";
        private string templateSettings = "//*[@id='root']/div/div/div/div[1]/div[1]/div/div[1]/div/div/div[4]/button";
        //Template
        private string authenticationMenu = "//*[@id='menu-auth']";
        private By authenticationLocator = null;
        private string imageWatermark = "//*[@id='root']/div/div/div/div[1]/div[2]/div[1]/div/div[2]/div/div/div/button[2]/img";
        private string initial = "//*[@id='root']/div/div/div/div[1]/div[2]/div[1]/div/div[3]/div/div/div/button[2]";
        public string signatureMenu = "//*[@id='menu-signature']/img";
        private string signature = "//*[@id='root']/div/div/div/div[1]/div[2]/div[1]/div/div[3]/div/div/div/button[1]";
        private string stripedWatermark = "//*[@id='root']/div/div/div/div[1]/div[2]/div[1]/div/div[2]/div/div/div/button[3]/img";
        public string templateHomeBtn = "//*[@id='root']/div/nav/div/a";
        public string templateMetaData = "/html/body/div[2]/div/div[1]/div/div/div[2]/div[1]/div[2]/div/button";
        public string templateMetadataList = "/html/body/div[2]/div/div[1]/div/div/div[2]/div[1]/div[2]/div/div";
        public string templateName = "//*[@id='root']/div/main/header/div[1]/div/input";
        public string templateText = "/html/body/div[2]/div/div[1]/div/div/div[2]/div[2]/div/div/textarea";
        public string templateTextInput = "/html/body/div[2]/div/div[1]/div/div/div[2]/div[2]/div/div/input";
        public string templateTextCancel = "/html/body/div[2]/div/div[1]/div/div/div[3]/button[2]";
        public string templateTextSave = "/html/body/div[2]/div/div[1]/div/div/div[3]/button[1]";
        public string template_X = "//*[@id='root']/div/aside/div/div[2]/div[1]/div[2]/div/input";
        public string template_Y = "//*[@id='root']/div/aside/div/div[2]/div[1]/div[3]/div/input";
        private string watermark = "//*[@id='root']/div/div/div/div[1]/div[2]/div[1]/div/div[2]/div/div/div/button[1]/img";
        private string watermarks = "//*[@id='menu-watermark']";
        private By watermarksLocator = null;
        //Enter Text Area
        public string textAreaImage = "//*[@id='root']/div/nav/div/div[2]/div/div[6]/div[1]/img";
        public string textAreaBorder = "//*[@id='root']/div/aside/div/div[2]/div[7]/div[3]/div/input";
        public string textAreaFontSize = "//*[@id='root']/div/aside/div/div[2]/div[3]/div[4]/div/input";
        public string textArea_X = "//*[@id='root']/div/aside/div/div[2]/div[1]/div[2]/div/input";
        public string textArea_Y = "//*[@id='root']/div/aside/div/div[2]/div[1]/div[3]/div/input";
        //TextBox
        public string textboxImage = "//*[@id='root']/div/nav/div/div[2]/div/div[6]/div[1]/img";
        #region Striped Watermark
        public string stripedWatermarkExclusionButton = "//*[@id='root']/div/div/div/div[2]/div/div[1]/div/div/div/div[2]/button";
        public string troyMarkEditButton = "//*[@id='root']/div/div/div/div[2]/div/div[1]/div/div/div/div[3]/div[2]/button";
        public string troyMarkFont = "//*[@id='root']/div/div/div/div[2]/div/div[1]/div/div/div/div[4]/div[2]/div/select";
        public string troyMarkFontSize = "//*[@id='root']/div/div/div/div[2]/div/div[1]/div/div/div/div[4]/div[4]/div/input";
        public string troymarkFullPage = "//*[@id='root']/div/div/div/div[2]/div/div[1]/div/div/div/div[1]/div[6]/label[1]/input";
        public string troymarkImg = "//*[@id='root']/div/nav/div/div[3]/div/div[2]/div[2]/img";
        public string troyMark_Height = "//*[@id='root']/div/div/div/div[2]/div/div[1]/div/div/div/div[1]/div[5]/div/input";
        public string troyMark_Location = "//*[@id='root']/div/div/div/div[2]/div/div[1]/div/div/div/div[1]/div[7]/div/select";
        public string troyMark_Spacing = "//*[@id='root']/div/div/div/div[2]/div/div[1]/div/div/div/div[4]/div[5]/div/input";
        public string troyMarkTextColor = "//*[@id='root']/div/div/div/div[2]/div/div[1]/div/div/div/div[4]/div[3]/div";
        public string troyMarkTextColor2 = "//*[@id='root']/div/div/div/div[2]/div/div[1]/div/div/div/div[4]/div[3]/div[2]/div[1]";
        public string troyMark_Text_A = "//*[@id='root']/div/div/div/div[2]/div/div[1]/div/div/div/div[4]/div[3]/div[2]/div[2]/div[3]/div[5]/div/input";
        public string troyMark_Text_B = "//*[@id='root']/div/div/div/div[2]/div/div[1]/div/div/div/div[4]/div[3]/div[2]/div[2]/div[3]/div[4]/div/input";
        public string troyMark_Text_G = "//*[@id='root']/div/div/div/div[2]/div/div[1]/div/div/div/div[4]/div[3]/div[2]/div[2]/div[3]/div[3]/div/input";
        public string troyMark_Text_Hex = "//*[@id='root']/div/div/div/div[2]/div/div[1]/div/div/div/div[4]/div[3]/div[2]/div[2]/div[3]/div[1]/div/input";
        public string troyMark_Text_R = "//*[@id='root']/div/div/div/div[2]/div/div[1]/div/div/div/div[4]/div[3]/div[2]/div[2]/div[3]/div[2]/div/input";
        public string troyMark_Transform = "//*[@id='root']/div/div/div/div[2]/div/div[1]/div/div/div/div[4]/div[6]/div/select";
        public string troyMark_Width = "//*[@id='root']/div/div/div/div[2]/div/div[1]/div/div/div/div[1]/div[4]/div/input";
        public string troyMark_X = "//*[@id='root']/div/div/div/div[2]/div/div[1]/div/div/div/div[1]/div[2]/div/input";
        public string troyMark_Y = "//*[@id='root']/div/div/div/div[2]/div/div[1]/div/div/div/div[1]/div[3]/div/input";
        #endregion
        #region Watermark
        public string watermarkAlignCenter = "//*[@id='root']/div/div/div/div[2]/div/div[1]/div/div/div/div[2]/div[3]/div/button[2]";
        public string watermarkAlignLeft = "//*[@id='root']/div/div/div/div[2]/div/div[1]/div/div/div/div[2]/div[3]/div/button[1]";
        public string watermarkAlignRight = "//*[@id='root']/div/div/div/div[2]/div/div[1]/div/div/div/div[2]/div[3]/div/button[3]";
        public string watermarkEditButton = "//*[@id='root']/div/div/div/div[2]/div/div[1]/div/div/div/div[2]/div[2]/button";
        public string watermarkFont = "//*[@id='root']/div/div/div/div[2]/div/div[1]/div/div/div/div[3]/div[2]/div/select";
        public string watermarkFontSize = "//*[@id='root']/div/div/div/div[2]/div/div[1]/div/div/div/div[3]/div[4]/div/input";
        public string watermark_InputText = "/html/body/div[2]/div/div[1]/div/div/div[2]/div[2]/div/div/textarea";
        public string watermarkProportionalSize = "//*[@id='root']/div/div/div/div[2]/div/div[1]/div/div/div/div[6]/label[1]/input";
        public string watermarkRotateCenter = "//*[@id='root']/div/div/div/div[2]/div/div[1]/div/div/div/div[5]/div[2]/button[2]";
        public string watermarkRotateLeft = "//*[@id='root']/div/div/div/div[2]/div/div[1]/div/div/div/div[5]/div[2]/button[1]";
        public string watermarkRotateRight = "//*[@id='root']/div/div/div/div[2]/div/div[1]/div/div/div/div[5]/div[2]/button[3]";
        public string watermarkRotation = "//*[@id='root']/div/div/div/div[2]/div/div[1]/div/div/div/div[5]/div[1]";
        public string watermark_Spacing = "//*[@id='root']/div/div/div/div[2]/div/div[1]/div/div/div/div[3]/div[5]/div/input";
        public string watermarkTextColor = "//*[@id='root']/div/div/div/div[2]/div/div[1]/div/div/div/div[3]/div[3]/div/div";
        public string watermarkTextColor2 = "//*[@id='root']/div/div/div/div[2]/div/div[1]/div/div/div/div[3]/div[3]/div[2]/div[1]";
        public string watermark_Text_A = "//*[@id='root']/div/div/div/div[2]/div/div[1]/div/div/div/div[3]/div[3]/div[2]/div[2]/div[3]/div[5]/div/input";
        public string watermark_Text_B = "//*[@id='root']/div/div/div/div[2]/div/div[1]/div/div/div/div[3]/div[3]/div[2]/div[2]/div[3]/div[4]/div/input";
        public string watermark_Text_G = "//*[@id='root']/div/div/div/div[2]/div/div[1]/div/div/div/div[3]/div[3]/div[2]/div[2]/div[3]/div[3]/div/input";
        public string watermark_Text_Hex = "//*[@id='root']/div/div/div/div[2]/div/div[1]/div/div/div/div[3]/div[3]/div[2]/div[2]/div[3]/div[1]/div/input";
        public string watermark_Text_R = "//*[@id='root']/div/div/div/div[2]/div/div[1]/div/div/div/div[3]/div[3]/div[2]/div[2]/div[3]/div[2]/div/input";
        public string watermark_Transform = "//*[@id='root']/div/div/div/div[2]/div/div[1]/div/div/div/div[3]/div[6]/div/select";
        #endregion 
        //*
        public string upToDate = "//*[@id='root']/div/main/header/div[3]/button";

        public Template(IWebDriver driver)
        {
            cc = new Common_Code(driver);
        }

        //------------------------------------------------------ Elements --------------------------------------------------------
        public void AddAuthmark(IWebDriver driver)
        {
            authenticationLocator = By.XPath(authenticationMenu);
            eleLocator = By.XPath(authMarkImage);

            driver.FindElement(authenticationLocator).Click();
            //Thread.Sleep(3000);

            driver.FindElement(eleLocator).Click();
            Thread.Sleep(3000);
        }

        public void AddImageWatermark(IWebDriver driver)
        {
            watermarksLocator = By.XPath(watermarks);
            eleLocator = By.XPath(imageWatermark);

            driver.FindElement(watermarksLocator).Click();
            driver.FindElement(eleLocator).Click();
            Thread.Sleep(3000);
        }

        public void AddInitial(IWebDriver driver)
        {
            cc.ClickButton(driver, signatureMenu);
            Thread.Sleep(500);
            cc.ClickButton(driver, initial);
            Thread.Sleep(500);
        }

        public void AddSignature(IWebDriver driver)
        {
            cc.ClickButton(driver, signatureMenu);
            Thread.Sleep(500);
            cc.ClickButton(driver, signature);
            Thread.Sleep(500);
        }

        public void AddStripedWatermark(IWebDriver driver)
        {
            watermarksLocator = By.XPath(watermarks);
            eleLocator = By.XPath(stripedWatermark);

            driver.FindElement(watermarksLocator).Click();
            //Thread.Sleep(3000);

            driver.FindElement(eleLocator).Click();
            Thread.Sleep(3000);
        }

        public void AddStripedWatermarkExclusion(IWebDriver driver)
        {
            watermarksLocator = By.XPath(watermarks);
            eleLocator = By.XPath(stripedWatermark);

            driver.FindElement(watermarksLocator).Click();
            driver.FindElement(eleLocator).Click();
            Thread.Sleep(3000);

            eleLocator = By.XPath(troymarkFullPage);
            driver.FindElement(eleLocator).Click();
            Thread.Sleep(3000);

            eleLocator = By.XPath(stripedWatermarkExclusionButton);
            driver.FindElement(eleLocator).Click();
            Thread.Sleep(3000);

        }

        public void AddWatermark(IWebDriver driver)
        {
            watermarksLocator = By.XPath(watermarks);
            eleLocator = By.XPath(watermark);

            driver.FindElement(watermarksLocator).Click();
            //Thread.Sleep(3000);

            driver.FindElement(eleLocator).Click();
            Thread.Sleep(3000);
        }
        //--------------------------------------------------------------------------------------------------------------------------
        public void ClickMetadataButton(IWebDriver driver, string element, string text)
        {
            locator = By.XPath(templateMetaData);
            driver.FindElement(locator).Click();
            Thread.Sleep(2000);

            locator = By.XPath(element);
            int eCount = Convert.ToInt16(driver.FindElement(locator).GetAttribute("childElementCount"));

            for (int i = 1; i <= eCount; i++)
            {
                locator = By.XPath(string.Concat(element, "/button[", i.ToString(), "]"));

                if (driver.FindElement(locator).GetAttribute("innerHTML") == text)
                {
                    driver.FindElement(locator).Click();
                }
            }
        }

        public void ExitDesigner (IWebDriver driver)
        {
            cc.ClickButton(driver, ddlMenu);
            Thread.Sleep(1000);

            cc.ClickButton(driver, exitBtn);
            Thread.Sleep(1000);
        }

        public void ExitDesigner2(IWebDriver driver)
        {

            //eleLocator = By.XPath(ddlMenu);
            //locator = By.XPath(exitBtn);

            driver.SwitchTo().Frame(driver.FindElement(By.Id("designer")));

            //driver.FindElement(eleLocator).Click();
            //Thread.Sleep(2000);
            //driver.FindElement(locator).Click();
            //Thread.Sleep(5000);
            cc.ClickButton(driver, ddlMenu);
            Thread.Sleep(500);

            cc.ClickButton(driver, exitBtn);
            Thread.Sleep(1000);
        }

        public string FetchHeader(IWebDriver driver)
        {
            return2TempLocator = By.XPath(return2Temp);
            return driver.FindElement(return2TempLocator).GetAttribute("innerHTML");
        }

        public void SelectLayerItem(IWebDriver driver, int valu)
        {
            locator = By.XPath(string.Concat(layersUL, "/li[", valu, "]"));

            driver.FindElement(locator).Click();
        }

        //------------------------------------------------------------------------------------------------------------------------------
        public SecureRx.Page_Elements.Templates.Security_Profile OpenAdvancedCode(IWebDriver driver)
        {
            eleLocator = By.XPath(ddlMenu);
            locator = By.XPath(showAdvancedCode);

            driver.SwitchTo().Frame(driver.FindElement(By.Id("designer")));

            driver.FindElement(eleLocator).Click();
            Thread.Sleep(2000);
            driver.FindElement(locator).Click();
            Thread.Sleep(5000);

            return new Security_Profile(driver);
        }

        public SecureRx.Page_Elements.Templates.Signature OpenInitial(IWebDriver driver)
        {
            cc.ClickButton(driver, signatureMenu);
            Thread.Sleep(500);
            cc.ClickButton(driver, initial);
            Thread.Sleep(500);

            return new Signature(driver);
        }

        public SecureRx.Page_Elements.Templates.Name_Template OpenNameTemplate(IWebDriver driver)
        {
            driver.SwitchTo().Frame(driver.FindElement(By.Id("designer")));
            locator = By.XPath(saveButton);

            driver.FindElement(locator).Click();

            return new Name_Template(driver);
        }

        public SecureRx.Page_Elements.Templates.Download_Pantograph_Test_Patterns OpenPatterns(IWebDriver driver)
        {
            string ele =pantographDownloadTestPatterns;
            eleLocator = By.XPath(ele);

            driver.FindElement(eleLocator).Click();

            cc.AcceptAssertOK(driver);

            //Is this the SecureDocs app?
            if (driver.Title != "Downloads - SecureRx")
            {
                throw new NoSuchWindowException(string.Concat("This is not the Download Test Patterns page - ", driver.Title));
            }

            return new Templates.Download_Pantograph_Test_Patterns(driver);
        }

        public SecureRx.Page_Elements.Templates.Protect_Settings OpenProtectSettings(IWebDriver driver)
        {
            driver.SwitchTo().Frame(driver.FindElement(By.Id("designer")));

            locator = By.XPath(protectSettings);
            driver.FindElement(locator).Click();
            Thread.Sleep(3000);

            return new Protect_Settings(driver);
        }

        public SecureRx.Page_Elements.Templates.Signature OpenSignature(IWebDriver driver)
        {
            cc.ClickButton(driver, signatureMenu);
            Thread.Sleep(500);
            cc.ClickButton(driver, signature);
            Thread.Sleep(500);

            return new Signature(driver);
        }

        public SecureRx.Page_Elements.Templates.Template_Settings OpenTemplateSettings(IWebDriver driver)
        {
            eleLocator = By.XPath(ddlMenu);
            locator = By.XPath(templateSettings);

            driver.SwitchTo().Frame(driver.FindElement(By.Id("designer")));

            driver.FindElement(eleLocator).Click();
            Thread.Sleep(2000);
            driver.FindElement(locator).Click();
            Thread.Sleep(5000);

            return new Template_Settings(driver);
        }

    }
}
