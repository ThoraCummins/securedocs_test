﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Input;
using System.CodeDom.Compiler;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Interactions.Internal;
using OpenQA.Selenium.Support.UI;
using SecureRx.Page_Elements;

namespace SecureRx.Page_Elements.Templates
{
    public class Name_Template
    {

        public Name_Template(IWebDriver driver)
        {
        }

        public string applyBtn = "/html/body/div[2]/div/div[1]/div/div/div[3]/button[1]";
        public string cancelBtn = "/html/body/div[2]/div/div[1]/div/div/div[3]/button[2]";
        public By eleLocator = null;
        public string header = "/html/body/div[2]/div/div[1]/div/div/div[1]/h4";
        public By locator = null;
        public string name = "/html/body/div[2]/div/div[1]/div/div/div[2]/fieldset/div/div/input";

        public void NameTemplate(IWebDriver driver, string text)
        {
            eleLocator = By.XPath(name);
            locator = By.XPath(applyBtn);

            driver.FindElement(eleLocator).Clear();
            driver.FindElement(eleLocator).SendKeys(text);
            Thread.Sleep(3000);

            driver.FindElement(locator).Click();
        }
    }
}
