﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Input;
using System.CodeDom.Compiler;
using System.IO;
using System.Text.RegularExpressions;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Interactions.Internal;
using OpenQA.Selenium.Support.UI;
using SecureRx.Page_Elements;
using SecureRx.Common;

namespace SecureRx.Page_Elements.Templates
{
    public class Create_Security_Templates
    {
        SecureRx.Common.Common_Code cc = null;

        private string close = "//*[@id='newSecurityTemplate']/div/div/div[1]/button";
        private By closeLocator = null;
        public string createBlank = "//*[@id='newSecurityTemplate']/div/div/div[3]/div/div[2]/form/div[1]/button";
        public string Header = "//*[@id='newTemplate']";
        private By locator = null;

        public Create_Security_Templates(IWebDriver driver)
        {
            cc = new Common_Code(driver);
        }

        public void CancelCreate(IWebDriver driver)
        {
            closeLocator = By.XPath(close);

            driver.FindElement(closeLocator).Click();
        }

        public SecureRx.Page_Elements.Templates.Template OpenTemplate(IWebDriver driver)
        {
            cc.ClickButton(driver, createBlank);

            //Is this the SecureDocs app?
            if (driver.Title != "Security Template Designer - SecureRx")
            {
                throw new NoSuchWindowException(string.Concat("This is not the Security Template Designer page - ", driver.Title));
            }

            return new SecureRx.Page_Elements.Templates.Template(driver);
        }

    }
}
