﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Input;
using System.CodeDom.Compiler;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Interactions.Internal;
using OpenQA.Selenium.Support.UI;
using SecureRx.Page_Elements;

namespace SecureRx.Page_Elements.Templates
{
    public class Download_Pantograph_Test_Patterns
    {

       public Download_Pantograph_Test_Patterns(IWebDriver driver)
        {
        }

        public By secTemplateLocator = null;
        public string testPattern_1 = "/html/body/div/main/div/div/div[2]/table/tbody/tr[1]/td[3]/a";

        public SecureRx.Page_Elements.Templates.SecurityTemplates OpenSecurityTemplates(IWebDriver driver)
        {
            secTemplateLocator = By.XPath("/html/body/div/div/nav/ul/li[3]/a");

            driver.FindElement(secTemplateLocator).Click();
            Thread.Sleep(15000);

            //Is this the SecureDocs app?
            if (driver.Title != "Security Templates - SecureRx")
            {
                throw new NoSuchWindowException(string.Concat("This is not the Security Templates page - ", driver.Title));
            }

            return new Templates.SecurityTemplates(driver);
        }


    }
}
