﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Input;
using System.CodeDom.Compiler;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Interactions.Internal;
using OpenQA.Selenium.Support.UI;
using SecureRx.Page_Elements;

namespace SecureRx.Page_Elements.Templates
{
    public class Show_Upload
    {
        public Show_Upload(IWebDriver driver)
        {
        }

        public string browseBtn = "//*[@id='uploadTemplateModal-browse-button']";
        public string cancelBtn = "//*[@id='uploadTemplateModal-cancel-button']";
        public string Header = "//*[@id='uploadTemplateModal']/div/div/div[1]/h5";
        public By locator = null;
        public string uploadDocument = "//*[@id='uploadTemplateModal-upload-button']";

        public void CancelUpload(IWebDriver driver)
        {
            locator = By.XPath(cancelBtn);

            driver.FindElement(locator).Click();
        }

        public SecureRx.Page_Elements.Activity.Activity_Detail OpenSecuredDocumentViewer(IWebDriver driver)
        {
           locator = By.XPath(uploadDocument);

            driver.FindElement(locator).Click();
            Thread.Sleep(30000);

            //Is this the SecureRx app ?
            if (driver.Title != "Document Details - SecureRx")
            {
                throw new NoSuchWindowException(string.Concat("This is not the Secured Document Viewer page - ", driver.Title));
            }

            return new SecureRx.Page_Elements.Activity.Activity_Detail(driver);
        }

    }
}
