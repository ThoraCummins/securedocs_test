﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;

namespace SecureRx.Tests
{
    [TestClass]
    public class Admin_Tests
    {
        #region libraries
        SecureRx.Page_Elements.MainPage mp = null;
        SecureRx.Page_Elements.LoginPage lp = null;

        //SecureRx.Page_Elements.API.API_Page api = null;
        SecureRx.Page_Elements.Users.Company_Details cd = null;
        SecureRx.Page_Elements.Users.Edit_User eu = null;
        SecureRx.Page_Elements.Users.New_User nu = null;
        SecureRx.Page_Elements.Users.Reset_Password rp = null;

        SecureRx.Common.Common_Code cc = null;
        SecureRx.Common.PT_Client ptc = new Common.PT_Client();
        SecureRx.Common.TGLogging tglog = new Common.TGLogging();
        #endregion

        //Vars
        private string baseURL = "";
        private string baseWindowHandle = "";
        //private string beta = "beta";
        private string brow = "";
        private string build = "";
        private string dev = "dev";
        private IWebDriver driver = null;
        private string email = "";
        private string filePath = "C:\\Test Logs\\SecureRx\\";
        private string firstname = "Stephanie";
        private string fullName = "";
        private int instance = 0;
        private string itemCount = "0";
        private string lastname = "Smith";
        private int passFail = 1;
        private string pswd = "Troypass1$";
        private string project = "5194";
        private string qaDev = "";
        private string resultMsg = "";
        private string staging = "stage";
        private string testName = "";
        private string user = "";

        //---------------------------------------------------------------------------------------------------------------
        [TestMethod, TestCategory("R1 - Admin")]
        public void R1_User_AddNewUser()
        {
            if (qaDev.ToLower() == dev)
            {
                //instance = 11117164;
                testName = "Admin_User_AddNewUser_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14575093;
                    testName = "Admin_User_AddNewUser_Stage";
                }
                else
                    {
                    instance = 14823102;
                    testName = "Admin_User_AddNewUser_Prod";
                    }
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");
            fullName = string.Concat(firstname, " ", lastname);
            email = string.Concat(firstname, lastname, "@troygroup.com");

            CreateLogFile();
            baseWindowHandle = driver.CurrentWindowHandle;

            try
            {
                //setup
                cd = mp.OpenCompanyDetail(driver);
                //tglog.AppendLogText(filePath, driver.Title);

                StringAssert.Contains(cc.FetchInnerHTML(driver, cd.header), "Users");
                tglog.AppendLogText(filePath, "Opened Company User page ..... OK");

                itemCount = cc.FetchChildCount(driver, cd.userTable);
                AddNewUser();

                //test
                StringAssert.Contains("False", (itemCount == cc.FetchChildCount(driver, cd.userTable)).ToString());
                tglog.AppendLogText(filePath, string.Concat("User Count = ", itemCount, " Original Count <> Current Count..... OK"));

                for (int x = 1; x <= Convert.ToInt16(cc.FetchChildCount(driver, cd.userTable)); x++)
                {
                    if (string.Concat(firstname, " ", lastname) == cc.FetchCellValue(driver, "1", x.ToString(), cd.userTable))
                    {
                        StringAssert.Contains(string.Concat(firstname, lastname, "@troygroup.com"), cc.FetchCellValue(driver, "2", x.ToString(), cd.userTable));
                        tglog.AppendLogText(filePath, "New user email displayed correctly on Users List..... OK");

                        StringAssert.Contains(string.Concat(firstname, " ", lastname), cc.FetchCellValue(driver, "1", x.ToString(), cd.userTable));
                        tglog.AppendLogText(filePath, "New user Full Name displayed correctly on Users List..... OK");
                    }
                }
                passFail = 0;

                //tear down
                TearDown();
            }
            catch (Exception e)
            {
                //tear down
                TearDown();

                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        [TestMethod, TestCategory("R2 - Admin")]
        public void Admin_User_CancelAddUser()
        {
            if (qaDev.ToLower() == dev)
            {
                //instance = 11117165;
                testName = "Admin_User_CancelAddUser_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14575097;
                    testName = "Admin_User_CancelAddUser_Stage";
                }
                else
                    {
                        instance = 12997559;
                        testName = "Admin_User_CancelAddUser_Prod";
                    }
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");
            fullName = string.Concat(firstname, " ", lastname);
            email = string.Concat(firstname, lastname, "@troygroup.com");

            CreateLogFile();
            baseWindowHandle = driver.CurrentWindowHandle;

            try
            {
                //setup
                cd = mp.OpenCompanyDetail(driver);
                //tglog.AppendLogText(filePath, driver.Title);

                StringAssert.Contains(cc.FetchInnerHTML(driver, cd.header), "Users");
                tglog.AppendLogText(filePath, "Opened Company User page ..... OK");

                itemCount = cc.FetchChildCount(driver, cd.userTable);

                //test
                nu = cd.OpenNewUser(driver);
                //tglog.AppendLogText(filePath, driver.Title);

                StringAssert.Contains(cc.FetchInnerHTML(driver, nu.header), "Create User");
                tglog.AppendLogText(filePath, "Opened New User page ..... OK");

                //test
                cc.EnterText(driver, firstname, nu.firstName);
                cc.EnterText(driver, lastname, nu.lastName);
                cc.EnterText(driver, string.Concat(firstname, lastname, "@troygroup.com"), nu.email);
                cc.EnterText(driver, pswd, nu.pswd);

                cc.ClickButton(driver, nu.canceBtn);

                StringAssert.Contains(itemCount, cc.FetchChildCount(driver, cd.userTable));
                tglog.AppendLogText(filePath, string.Concat("User Count = ", itemCount, " Original Count = Current Count..... OK"));

                passFail = 0;

                //tear down
                TearDown();
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        [TestMethod, TestCategory("R2 - Admin")]
        public void Admin_User_CancelDeleteUser()
        {
            if (qaDev.ToLower() == dev)
            {
                //instance = 11117172;
                testName = "Admin_User_CancelDeleteUser_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14575098;
                    testName = "Admin_User_CancelDeleteUser_Stage";
                }
                    else
                    {
                    instance = 12997560;
                    testName = "Admin_User_CancelDeleteUser_Prod";
                    }
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();
            baseWindowHandle = driver.CurrentWindowHandle;

            try
            {
                //set up
                cd = mp.OpenCompanyDetail(driver);
                //tglog.AppendLogText(filePath, driver.Title);

                StringAssert.Contains(cc.FetchInnerHTML(driver, cd.header), "Users");
                tglog.AppendLogText(filePath, "Opened Company User page ..... OK");

                AddNewUser();
                tglog.AppendLogText(filePath, "Added test user");

                //test
                itemCount = cc.FetchChildCount(driver, cd.userTable);
                for (int x = 1; x <= Convert.ToInt16(cc.FetchChildCount(driver, cd.userTable)); x++)
                {
                    if (string.Concat(firstname, " ", lastname) == cc.FetchCellValue(driver, "1", x.ToString(), cd.userTable))
                    {
                        StringAssert.Contains(string.Concat(firstname, " ", lastname), cc.FetchCellValue(driver, "1", x.ToString(), cd.userTable));
                        tglog.AppendLogText(filePath, "New user Full Name displayed correctly on Users List..... OK");
                        cc.ClickButton(driver, string.Concat(cd.userTable, "/tr[", x.ToString(), "]/td[5]/form/button"));

                        if (cc.CancelAssert(driver) == "")
                        {
                            driver.SwitchTo().Window(baseWindowHandle);
                            StringAssert.Contains(itemCount, cc.FetchChildCount(driver, cd.userTable));
                            tglog.AppendLogText(filePath, "Selected user was not removed.....OK");
                            passFail = 0;
                        }
                        else
                        {
                            passFail = 1;
                            tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Alert was not displayed!", Environment.NewLine));
                        }
                    }
                }
                //tear down
                TearDown();

            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        [TestMethod, TestCategory("R2 - Admin")]
        public void Admin_User_CancelEditUser()
        {
            if (qaDev.ToLower() == dev)
            {
                //instance = 11117169;
                testName = "Admin_User_CancelEditUser_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14575099;
                    testName = "Admin_User_CancelEditUser_Stage";
                }
                else
                    {
                    instance = 12997561;
                    testName = "Admin_User_CancelEditUser_Prod";
                    }
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");
            fullName = string.Concat(firstname, " ", lastname);

            CreateLogFile();
            baseWindowHandle = driver.CurrentWindowHandle;

            try
            {
                cd = mp.OpenCompanyDetail(driver);
                //tglog.AppendLogText(filePath, driver.Title);

                StringAssert.Contains(cc.FetchInnerHTML(driver, cd.header), "Users");
                tglog.AppendLogText(filePath, "Opened Company User page ..... OK");

                AddNewUser();
                tglog.AppendLogText(filePath, "Added test user");

                //test
                itemCount = cc.FetchChildCount(driver, cd.userTable);
                for (int x = 1; x <= Convert.ToInt16(cc.FetchChildCount(driver, cd.userTable)); x++)
                {
                    if (string.Concat(firstname, " ", lastname) == cc.FetchCellValue(driver, "1", x.ToString(), cd.userTable))
                    {

                        eu = cd.OpenEditUser(driver, string.Concat(cd.userTable, "/tr[", x.ToString(), "]/td[5]/form/a[1]"));
                        Thread.Sleep(3000);

                        cc.EnterText(driver, "Joyce", eu.firstName);
                        cc.ClickButton(driver, eu.cancelBtn);
                        Thread.Sleep(5000);

                        StringAssert.Contains(string.Concat(firstname, " ", lastname), cc.FetchCellValue(driver, "1", x.ToString(), cd.userTable));
                        tglog.AppendLogText(filePath, "New user Full Name is the original on Users List..... OK");

                        //reset test
                        eu = cd.OpenEditUser(driver, string.Concat(cd.userTable, "/tr[", x.ToString(), "]/td[5]/form/a[1]"));
                        Thread.Sleep(3000);

                        cc.EnterText(driver, firstname, eu.firstName);
                        cc.ClickButton(driver, eu.submitBtn);
                        Thread.Sleep(3000);

                        fullName = string.Concat(firstname, " ", lastname);
                        StringAssert.Contains(fullName, cc.FetchCellValue(driver, "1", x.ToString(), cd.userTable));
                        tglog.AppendLogText(filePath, "Restored user Full Name displayed correctly on Users List..... OK");

                        passFail = 0;
                        break;
                    }
                }

                //tear down
                TearDown();
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        [TestMethod, TestCategory("R2 - Admin")]
        public void Admin_User_CancelResetPassword()
        {
            if (qaDev.ToLower() == dev)
            {
                //instance = 11117170;
                testName = "Admin_User_CancelResetPassword_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14575100;
                    testName = "Admin_User_CancelResetPassword_Stage";
                }
                    else
                    {
                        instance = 12997562;
                        testName = "Admin_User_CancelResetPassword_Prod";
                    }
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();
            baseWindowHandle = driver.CurrentWindowHandle;

            try
            {
                //setup
                cd = mp.OpenCompanyDetail(driver);
                //tglog.AppendLogText(filePath, driver.Title);

                StringAssert.Contains(cc.FetchInnerHTML(driver, cd.header), "Users");
                tglog.AppendLogText(filePath, "Opened Company User page ..... OK");

                AddNewUser();
                tglog.AppendLogText(filePath, "Added test user");

                //test
                itemCount = cc.FetchChildCount(driver, cd.userTable);
                for (int x = 1; x <= Convert.ToInt16(cc.FetchChildCount(driver, cd.userTable)); x++)
                {
                    if (string.Concat(firstname, " ", lastname) == cc.FetchCellValue(driver, "1", x.ToString(), cd.userTable))
                    {
                        StringAssert.Contains(string.Concat(firstname, " ", lastname), cc.FetchCellValue(driver, "1", x.ToString(), cd.userTable));
                        tglog.AppendLogText(filePath, "New user Full Name displayed correctly on Users List..... OK");

                        rp = cd.OpenResetPassword(driver, string.Concat(cd.userTable, "/tr[", x.ToString(), "]/td[5]/form/a[2]"));
                        Thread.Sleep(3000);

                        cc.EnterText(driver, "something", rp.pswd);
                        cc.ClickButton(driver, rp.cancelBtn);
                        Thread.Sleep(3000);

                        StringAssert.Contains(string.Concat(firstname, " ", lastname), cc.FetchCellValue(driver, "1", x.ToString(), cd.userTable));
                        tglog.AppendLogText(filePath, "User Full Name is the original on Users List..... OK");

                        passFail = 0;
                        break;
                    }
                }
                //tear down
                TearDown();
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        [TestMethod, TestCategory("R1 - Admin")]
        public void R1_User_ChangeEmail()
        {
            if (qaDev.ToLower() == dev)
            {
                //instance = 11117168;
                testName = "Admin_User_ChangeEmail_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14575094;
                    testName = "Admin_User_ChangeEmail_Stage";
                }
                else
                    {
                    instance = 14823103;
                    testName = "Admin_User_ChangeEmail_Prod";
                    }
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");
            fullName = string.Concat(firstname, " ", lastname);
            email = string.Concat(firstname, lastname, "@troygroup.com");

            CreateLogFile();
            baseWindowHandle = driver.CurrentWindowHandle;

            try
            {
                //setup
                cd = mp.OpenCompanyDetail(driver);
                //tglog.AppendLogText(filePath, driver.Title);

                StringAssert.Contains(cc.FetchInnerHTML(driver, cd.header), "Users");
                tglog.AppendLogText(filePath, "Opened Company User page ..... OK");

                AddNewUser();
                tglog.AppendLogText(filePath, "Added test user");

                //test
                itemCount = cc.FetchChildCount(driver, cd.userTable);
                for (int x = 1; x <= Convert.ToInt16(cc.FetchChildCount(driver, cd.userTable)); x++)
                {
                    if (string.Concat(firstname, " ", lastname) == cc.FetchCellValue(driver, "1", x.ToString(), cd.userTable))
                    {
                        StringAssert.Contains(string.Concat(firstname, " ", lastname), cc.FetchCellValue(driver, "1", x.ToString(), cd.userTable));
                        tglog.AppendLogText(filePath, "New user Full Name displayed correctly on Users List..... OK");

                        eu = cd.OpenEditUser(driver, string.Concat(cd.userTable, "/tr[", x.ToString(), "]/td[5]/form/a[1]"));
                        Thread.Sleep(3000);

                        cc.EnterText(driver, "JoyceSmith@troy.com", eu.email);
                        cc.ClickButton(driver, eu.submitBtn);
                        Thread.Sleep(3000);

                        StringAssert.Contains("True", ("JoyceSmith@troy.com" == cc.FetchCellValue(driver, "2", x.ToString(), cd.userTable)).ToString());
                        tglog.AppendLogText(filePath, "New user email change is reflected on Users List..... OK");

                        //reset test
                        eu = cd.OpenEditUser(driver, string.Concat(cd.userTable, "/tr[", x.ToString(), "]/td[5]/form/a[1]"));
                        Thread.Sleep(3000);

                        cc.EnterText(driver, email, eu.email);
                        cc.ClickButton(driver, eu.submitBtn);
                        Thread.Sleep(3000);

                        fullName = string.Concat(firstname, " ", lastname);

                        passFail = 0;
                        break;
                    }
                }
                //tear down
                TearDown();
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        [TestMethod, TestCategory("R2 - Admin")]
        public void Admin_User_ChangeFirstName()
        {
            if (qaDev.ToLower() == dev)
            {
                //instance = 11117167;
                testName = "Admin_User_ChangeFirstName_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14575101;
                    testName = "Admin_User_ChangeFirstName_Stage";
                }
                else
                    {
                        instance = 12997564;
                        testName = "Admin_User_ChangeFirstName_Prod";
                    }
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");
            fullName = string.Concat(firstname, " ", lastname);

            CreateLogFile();
            baseWindowHandle = driver.CurrentWindowHandle;

            try
            {
                cd = mp.OpenCompanyDetail(driver);
                //tglog.AppendLogText(filePath, driver.Title);

                StringAssert.Contains(cc.FetchInnerHTML(driver, cd.header), "Users");
                tglog.AppendLogText(filePath, "Opened Company User page ..... OK");

                AddNewUser();
                tglog.AppendLogText(filePath, "Added test user");

                //test
                itemCount = cc.FetchChildCount(driver, cd.userTable);
                for (int x = 1; x <= Convert.ToInt16(cc.FetchChildCount(driver, cd.userTable)); x++)
                {
                    if (string.Concat(firstname, " ", lastname) == cc.FetchCellValue(driver, "1", x.ToString(), cd.userTable))
                    {

                        eu = cd.OpenEditUser(driver, string.Concat(cd.userTable, "/tr[", x.ToString(), "]/td[5]/form/a[1]"));
                        Thread.Sleep(3000);

                        cc.EnterText(driver, "Joyce", eu.firstName);
                        cc.ClickButton(driver, eu.submitBtn);
                        Thread.Sleep(3000);

                        fullName = string.Concat("Joyce ", lastname);
                        StringAssert.Contains("True", (fullName == cc.FetchCellValue(driver, "1", x.ToString(), cd.userTable)).ToString());
                        tglog.AppendLogText(filePath, "User First name change is reflected on Users List..... OK");

                        //reset test
                        eu = cd.OpenEditUser(driver, string.Concat(cd.userTable, "/tr[", x.ToString(), "]/td[5]/form/a[1]"));
                        Thread.Sleep(3000);

                        cc.EnterText(driver, firstname, eu.firstName);
                        cc.ClickButton(driver, eu.submitBtn);
                        Thread.Sleep(3000);

                        fullName = string.Concat(firstname, " ", lastname);
                        StringAssert.Contains(fullName, cc.FetchCellValue(driver, "1", x.ToString(), cd.userTable));
                        tglog.AppendLogText(filePath, "Restored user Full Name displayed correctly on Users List..... OK");

                        passFail = 0;
                        break;
                    }
                }

                //tear down
                TearDown();
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        [TestMethod, TestCategory("R2 - Admin")]
        public void Admin_User_ChangeLastName()
        {
            if (qaDev.ToLower() == dev)
            {
                //instance = 11117166;
                testName = "Admin_User_ChangeLastName_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14575102;
                    testName = "Admin_User_ChangeLastName_Stage";
                }
                else
                    {
                    instance = 12997565;
                    testName = "Admin_User_ChangeLastName_Prod";
                    }
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");
            fullName = string.Concat(firstname, " ", lastname);

            CreateLogFile();
            baseWindowHandle = driver.CurrentWindowHandle;

            try
            {
                cd = mp.OpenCompanyDetail(driver);
                //tglog.AppendLogText(filePath, driver.Title);

                StringAssert.Contains(cc.FetchInnerHTML(driver, cd.header), "Users");
                tglog.AppendLogText(filePath, "Opened Company User page ..... OK");

                AddNewUser();
                tglog.AppendLogText(filePath, "Added Test User");

                //test
                itemCount = cc.FetchChildCount(driver, cd.userTable);
                for (int x = 1; x <= Convert.ToInt16(cc.FetchChildCount(driver, cd.userTable)); x++)
                {
                    if (string.Concat(firstname, " ", lastname) == cc.FetchCellValue(driver, "1", x.ToString(), cd.userTable))
                    {
                        StringAssert.Contains(string.Concat(firstname, " ", lastname), cc.FetchCellValue(driver, "1", x.ToString(), cd.userTable));
                        tglog.AppendLogText(filePath, "New user Full Name displayed correctly on Users List..... OK");

                        eu = cd.OpenEditUser(driver, string.Concat(cd.userTable, "/tr[", x.ToString(), "]/td[5]/form/a[1]"));
                        Thread.Sleep(3000);

                        cc.EnterText(driver, "Miller", eu.lastName);
                        cc.ClickButton(driver, eu.submitBtn);
                        Thread.Sleep(3000);

                        fullName = string.Concat(firstname, " Miller");
                        StringAssert.Contains("True", (fullName == cc.FetchCellValue(driver, "1", x.ToString(), cd.userTable)).ToString());
                        tglog.AppendLogText(filePath, "New user Last name change is reflected on Users List..... OK");

                        //reset test
                        eu = cd.OpenEditUser(driver, string.Concat(cd.userTable, "/tr[", x.ToString(), "]/td[5]/form/a[1]"));
                        Thread.Sleep(3000);

                        cc.EnterText(driver, lastname, eu.lastName);
                        cc.ClickButton(driver, eu.submitBtn);
                        Thread.Sleep(3000);

                        fullName = string.Concat(firstname, " ", lastname);

                        passFail = 0;
                        break;
                    }
                }

                //tear down
                TearDown();
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        [TestMethod, TestCategory("R1 - Admin")]
        public void R1_User_DeleteUser()
        {
            if (qaDev.ToLower() == dev)
            {
                //instance = 11117173;
                testName = "Admin_User_DeleteUser_Dev";
            }
            else
            {
                if (qaDev.ToLower() ==staging)
                {
                    instance = 14575095;
                    testName = "Admin_User_DeleteUser_Stage";
                }
                    else
                    {
                    instance = 14823104;
                    testName = "Admin_User_DeleteUser_Prod";
                    }
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();
            baseWindowHandle = driver.CurrentWindowHandle;

            try
            {
                //set up
                cd = mp.OpenCompanyDetail(driver);
                //tglog.AppendLogText(filePath, driver.Title);

                StringAssert.Contains(cc.FetchInnerHTML(driver, cd.header), "Users");
                tglog.AppendLogText(filePath, "Opened Company User page ..... OK");

                AddNewUser();
                tglog.AppendLogText(filePath, "Added Test User");

                //test
                itemCount = cc.FetchChildCount(driver, cd.userTable);
                for (int x = 1; x <= Convert.ToInt16(cc.FetchChildCount(driver, cd.userTable)); x++)
                {
                    if (string.Concat(firstname, " ", lastname) == cc.FetchCellValue(driver, "1", x.ToString(), cd.userTable))
                    {
                        StringAssert.Contains(string.Concat(firstname, " ", lastname), cc.FetchCellValue(driver, "1", x.ToString(), cd.userTable));
                        tglog.AppendLogText(filePath, "New user Full Name displayed correctly on Users List..... OK");
                        cc.ClickButton(driver, string.Concat(cd.userTable, "/tr[", x.ToString(), "]/td[5]/form/button"));

                        if (cc.AcceptAssertOK(driver) == "")
                        {
                            driver.SwitchTo().Window(baseWindowHandle);
                            passFail = 0;
                            break;
                        }
                        else
                        {
                            passFail = 1;
                            tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Alert was not displayed!", Environment.NewLine));
                        }
                    }
                }

                for (int x = 1; x <= Convert.ToInt16(cc.FetchChildCount(driver, cd.userTable)); x++)
                {
                    if (string.Concat(firstname, " ", lastname) == cc.FetchCellValue(driver, "2", x.ToString(), cd.userTable))
                    {
                        passFail = 1;
                        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, string.Concat("User ", firstname, " ", lastname, " was not removed.....FAIL!"), Environment.NewLine));
                        break;
                    }
                }
                //tear down
                TearDown();
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        [TestMethod, TestCategory("R1 - Admin")]
        public void R1_User_ResetPassword()
        {
            if (qaDev.ToLower() == dev)
            {
                //instance = 11117171;
                testName = "Admin_User_ResetPassword_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14575096;
                    testName = "Admin_User_ResetPassword_Stage";
                }
                    else
                    {
                    instance = 14823105;
                    testName = "Admin_User_ResetPassword_Prod";
                    }
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();
            baseWindowHandle = driver.CurrentWindowHandle;

            try
            {
                cd = mp.OpenCompanyDetail(driver);
                //tglog.AppendLogText(filePath, driver.Title);

                StringAssert.Contains(cc.FetchInnerHTML(driver,cd.header), "Users");
                tglog.AppendLogText(filePath, "Opened Company User page ..... OK");

                AddNewUser();
                tglog.AppendLogText(filePath, "Added Test User");

                itemCount = cc.FetchChildCount(driver, cd.userTable);
                for (int x = 1; x <= Convert.ToInt16(cc.FetchChildCount(driver, cd.userTable)); x++)
                {
                    if (string.Concat(firstname, lastname, "@troygroup.com") == cc.FetchCellValue(driver, "2", x.ToString(), cd.userTable))
                    {
                        StringAssert.Contains(string.Concat(firstname, lastname, "@troygroup.com"), cc.FetchCellValue(driver, "2", x.ToString(), cd.userTable));
                        tglog.AppendLogText(filePath, "User Email displayed correctly on Users List..... OK");

                        rp = cd.OpenResetPassword(driver, string.Concat(cd.userTable, "/tr[", x.ToString(), "]/td[5]/form/a[2]"));
                        Thread.Sleep(3000);

                        cc.EnterText(driver, "Thunder01$", rp.pswd);
                        cc.ClickButton(driver, rp.submitBtn);
                        Thread.Sleep(3000);

                        tglog.AppendLogText(filePath, "Password Changed..... OK");

                        passFail = 0;
                        break;
                    }
                }
                TearDown();
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        //-------------------------------------------------------------------------------------------
        #region Methods
        private void AddNewUser()
        {
            nu = cd.OpenNewUser(driver);
            //tglog.AppendLogText(filePath, driver.Title);

            StringAssert.Contains(cc.FetchInnerHTML(driver, nu.header), "Create User");
            tglog.AppendLogText(filePath, "Opened New User page ..... OK");

            //test
            cc.EnterText(driver, firstname, nu.firstName);
            cc.EnterText(driver, lastname, nu.lastName);
            cc.EnterText(driver, string.Concat(firstname, lastname, "@troygroup.com"), nu.email);
            cc.EnterText(driver, pswd, nu.pswd);

            cc.ClickButton(driver, nu.submitBtn);
        }

        private void SetupDriver()
        {
            if ((brow.ToLower() == "ie"))
            {
                this.driver = new InternetExplorerDriver("C:\\Selenium");
            }

            if ((brow.ToLower() == "chrome"))
            {
                this.driver = new ChromeDriver(@"C:\Selenium");
            }

            //if ((brow.ToLower() == "firefox"))
            //{
            //    this.driver = new FirefoxDriver();
            //}

        }

        private void CreateLogFile()
        {
            tglog.CreateLogFile(filePath, string.Concat(Environment.NewLine, "----------- Start testing using ", brow, " -----------", Environment.NewLine));
            tglog.AppendLogText(filePath, string.Concat("Set up ", brow, " ..... OK"));
            tglog.AppendLogText(filePath, string.Concat("URL = ", baseURL));
            tglog.AppendLogText(filePath, string.Concat("Envionment = ", qaDev));
            tglog.AppendLogText(filePath, string.Concat("User = ", user));
            tglog.AppendLogText(filePath, "Opened SecureDocs in browser ..... OK");
            tglog.AppendLogText(filePath, string.Concat("Build Version: ", build));
            tglog.AppendLogText(filePath, "Logged into SecureDocs ..... OK");
            tglog.AppendLogText(filePath, string.Concat("Instance = ", instance));
            tglog.AppendLogText(filePath, string.Concat("FilePath = ", filePath));
            tglog.AppendLogText(filePath, "Opened SecureDocs Dashboard ..... OK");
        }

        private void SendToPractitest()
        {
            if (qaDev.ToUpper() == "DEV")
            {
                ptc.PT_Post_Specific_Run(filePath, passFail, resultMsg, instance, project, brow, "Development");
            }
            else
            {
                if (qaDev.ToUpper() == "STAGE")
                {
                    ptc.PT_Post_Specific_Run(filePath, passFail, resultMsg, instance, project, brow, "QA");
                }
                //else
                //{
                //    if (qaDev.ToUpper() == "BETA")
                //    {
                //        ptc.PT_Post_Specific_Run(filePath, passFail, resultMsg, instance, project, brow, "Beta");
                //    }
                    else
                    {
                        ptc.PT_Post_Specific_Run(filePath, passFail, resultMsg, instance, project, brow, "Production");
                    }
                //}
            }
        }

        private void SetUpEnvironVars()
        {
            if (qaDev.ToLower() == dev)
            {
                filePath = string.Concat(filePath, "Dev\\");
            }
            //else
            //{
            //    if (qaDev.ToLower() == qa)
            //    {
            //        filePath = string.Concat(filePath, "QA\\");
            //    }
                else
                {
                    if (qaDev.ToLower() == staging)
                    {
                        filePath = string.Concat(filePath, "Stage\\");
                    }
                    else
                    {
                        filePath = string.Concat(filePath, "Prod\\");
                    }
                //}
            }
        }

        private void TearDown()
        {
            Thread.Sleep(3000);

            //teardown
            itemCount = cc.FetchChildCount(driver, cd.userTable);
            for (int x = 1; x <= Convert.ToInt16(cc.FetchChildCount(driver, cd.userTable)); x++)
            {
                if (string.Concat(firstname, " ", lastname) == cc.FetchCellValue(driver, "1", x.ToString(), cd.userTable))
                {
                    cc.ClickButton(driver, string.Concat(cd.userTable, "/tr[", x.ToString(), "]/td[5]/form/button"));

                    if (cc.AcceptAssertOK(driver) == "")
                    {
                        driver.SwitchTo().Window(baseWindowHandle);
                        passFail = 0;
                        break;
                    }
                    else
                    {
                        passFail = 1;
                        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Alert was not displayed!", Environment.NewLine));
                    }
                }
            }

            tglog.AppendLogText(filePath, "Removed test user");

        }
        #endregion

        //--------------------------------------------------------------------------------------------
        #region Additional test attributes
        [TestInitialize()]
        public void MyTestInitialize()
        {
            cc = new Common.Common_Code(driver);

            brow = ConfigurationManager.AppSettings["browser"];
            baseURL = ConfigurationManager.AppSettings["DocrityURL"];
            project = ConfigurationManager.AppSettings["project"];
            qaDev = ConfigurationManager.AppSettings["testEnv"];
            user = ConfigurationManager.AppSettings["testUser"].ToUpper();
            string pswd = ConfigurationManager.AppSettings["TestPswd"];

            SetupDriver();
            SetUpEnvironVars();

            lp = new SecureRx.Page_Elements.LoginPage(driver, baseURL);
            build = lp.FetchBuildVersion(driver);
            mp = lp.SDLogin(user, pswd, driver);

            //Did Home Menu open
            if (mp == null)
            {
                throw new NoSuchWindowException("Docrity home page was not displayed!");
            }
            else
            {
                passFail = 0;
            }
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {

            driver.SwitchTo().Window(baseWindowHandle);
            Thread.Sleep(1000);

            try
            {
                mp.LogoutandCloseBrowser(driver);
                tglog.AppendLogText(filePath, "Logged out and Closed browser..... OK");
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }

            if (passFail != 0)
            {
                resultMsg = string.Concat(testName, " ..... FAILED!!!!! Please refer to log file");
                //driver.Close();
            }
            else
            {
                resultMsg = string.Concat(testName, " ..... PASSED!!!!!");
            }

            SendToPractitest();

            tglog.AppendLogText(filePath, "Close Docrity");
            tglog.AppendLogText(filePath, "\n Test Finished \n");
        }

        #endregion

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;
    }
}