﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;

namespace SecureRx.Tests
{
    [TestClass]
    public class Resource_Tests
    {
        #region libraries
        SecureRx.Page_Elements.MainPage mp = null;
        SecureRx.Page_Elements.LoginPage lp = null;

        SecureRx.Common.Common_Code cc = null;
        SecureRx.Common.PT_Client ptc = new Common.PT_Client();
        SecureRx.Common.TGLogging tglog = new Common.TGLogging();
        #endregion

        //Vars
        private string baseURL = "";
        private string baseWindowHandle = "";
        private string beta = "beta";
        private string brow = "";
        private string build = "";
        private string dev = "dev";
        private IWebDriver driver = null;
        //private string email = "";
        private string filePath = "C:\\Test Logs\\SecureDocs v2\\";
        //private string firstname = "Stephanie";
        //private string fullName = "";
        private int instance = 0;
        //private string itemCount = "0";
        //private string lastname = "Smith";
        private int passFail = 1;
        //private string pswd = "Troypass1$";
        //private string prod = "prod";
        private string project = "5194";
        private string qa = "qa";
        private string qaDev = "";
        private string resultMsg = "";
        private string testName = "";
        private string user = "";

        //-------------------------------------------------------------------------------------------
        //[TestMethod, TestCategory("Functional - Admin")]
        //public void Admin_API_ExposeClientSecret()
        //{
        //    string secret = "";

        //    if (qaDev.ToLower() == dev)
        //    {
        //        //instance = 4997503;
        //        testName = "Admin_API_ExposeClientSecret_Dev";
        //    }
        //    else
        //    {
        //        if (qaDev.ToLower() == qa)
        //        {
        //            instance = 9695512;
        //            testName = "Admin_API_ExposeClientSecret_QA";
        //        }
        //        else
        //        {
        //            if (qaDev.ToLower() == beta)
        //            {
        //                instance = 9720429;
        //                testName = "Admin_API_ExposeClientSecret_Beta";
        //            }
        //            else
        //            {
        //                if (qaDev.ToLower() == prod)
        //                {
        //                    instance = 9720451;
        //                    testName = "Admin_API_ExposeClientSecret_Prod";
        //                }
        //                else
        //                {
        //                    //instance = 6964431;
        //                    testName = "Admin_API_ExposeClientSecret_OP";
        //                }
        //            }
        //        }
        //    }
        //    passFail = 1;  //reset before test
        //    filePath = string.Concat(filePath, testName, ".txt");

        //    CreateLogFile();

        //    try
        //    {
        //        api = mp.OpenAPI(driver);

        //        StringAssert.Contains(cc.FetchInnerHTML(driver, api.header), "API Keys");
        //        tglog.AppendLogText(filePath, "Opened API Keys page ..... OK");

        //        tglog.AppendLogText(filePath, string.Concat("Client ID = ", cc.FetchInnerHTML(driver, api.clientID)));

        //        //test
        //        cc.ClickButton(driver, api.showSecret);
        //        secret = cc.FetchInnerHTML(driver, api.clientSecret);
        //        TestContext.WriteLine(string.Concat("secret = ", secret));
        //        StringAssert.Contains("True", (secret != "").ToString());
        //        passFail = 0;
        //    }
        //    catch (Exception e)
        //    {
        //        passFail = 1;
        //        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
        //    }
        //}

        //[TestMethod, TestCategory("NOT READY - Functional - Admin")]
        //public void Admin_Settings_ChangeEnableBlockchain()
        //{
        //    string submit = "";

        //    if (qaDev.ToLower() == dev)
        //    {
        //        //instance = 4997504;
        //        testName = "Admin_Settings_ChangeEnableBlockchain_Dev";
        //    }
        //    else
        //    {
        //        if (qaDev.ToLower() == qa)
        //        {
        //            instance = 9723697;
        //            testName = "Admin_Settings_ChangeEnableBlockchain_QA";
        //        }
        //        else
        //        {
        //            if (qaDev.ToLower() == beta)
        //            {
        //                instance = 9723720;
        //                testName = "Admin_Settings_ChangeEnableBlockchain_Beta";
        //            }
        //            else
        //            {
        //                if (qaDev.ToLower() == prod)
        //                {
        //                    instance = 9723722;
        //                    testName = "Admin_Settings_ChangeEnableBlockchain_Prod";
        //                }
        //                else
        //                {
        //                    //instance = 6964432;
        //                    testName = "Admin_Settings_ChangeEnableBlockchain_OP";
        //                }
        //            }
        //        }
        //    }
        //    passFail = 1;  //reset before test
        //    filePath = string.Concat(filePath, testName, ".txt");

        //    CreateLogFile();

        //    try
        //    {
        //        stt = mp.OpenSettings(driver);
        //        submit = string.Concat(stt.submit, "/div[2]/form/div[5]/button");

        //        StringAssert.Contains(cc.FetchInnerHTML(driver, stt.header), "General");
        //        tglog.AppendLogText(filePath, "Opened Company Settings page ..... OK");

        //        //tglog.AppendLogText(filePath, string.Concat("Original Set Print Log is ", cc.FetchChecked(driver, stt.printLog)));

        //        //test
        //        cc.ClickButton(driver, stt.enableBlockchain);
        //        cc.ClickButton(driver, submit);
        //        Thread.Sleep(3000);
        //        //StringAssert.Contains("True", (interval != cc.FetchTextValue(driver, stt.syncInterval)).ToString());
        //        tglog.AppendLogText(filePath, string.Concat("Clicked Enable Blockchaim", Environment.NewLine));

        //        Thread.Sleep(5000);
        //        passFail = 0;

        //        // reset original
        //        submit = string.Concat(stt.submit, "/div[3]/form/div[5]/button");
        //        cc.ClickButton(driver, stt.enableBlockchain2);
        //        cc.ClickButton(driver, submit);
        //        //StringAssert.Contains("True", (interval == cc.FetchTextValue(driver, stt.syncInterval)).ToString());
        //        tglog.AppendLogText(filePath, string.Concat("Reset Enable Blockchain", Environment.NewLine));
        //        Thread.Sleep(5000);

        //    }
        //    catch (Exception e)
        //    {
        //        passFail = 1;
        //        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
        //    }
        //}

        //[TestMethod, TestCategory("Functional - Admin")]
        //public void Admin_Settings_ChangePrintLog()
        //{
        //    string submit = "";

        //    if (qaDev.ToLower() == dev)
        //    {
        //        //instance = 4997504;
        //        testName = "Admin_Settings_ChangePrintLog_Dev";
        //    }
        //    else
        //    {
        //        if (qaDev.ToLower() == qa)
        //        {
        //            instance = 9721868;
        //            testName = "Admin_Settings_ChangePrintLog_QA";
        //        }
        //        else
        //        {
        //            if (qaDev.ToLower() == beta)
        //            {
        //                instance = 9721952;
        //                testName = "Admin_Settings_ChangePrintLog_Beta";
        //            }
        //            else
        //            {
        //                if (qaDev.ToLower() == prod)
        //                {
        //                    instance = 9721976;
        //                    testName = "Admin_Settings_ChangePrintLog_Prod";
        //                }
        //                else
        //                {
        //                    //instance = 6964432;
        //                    testName = "Admin_Settings_ChangePrintLog_OP";
        //                }
        //            }
        //        }
        //    }
        //    passFail = 1;  //reset before test
        //    filePath = string.Concat(filePath, testName, ".txt");

        //    CreateLogFile();

        //    try
        //    {
        //        stt = mp.OpenSettings(driver);
        //        submit = string.Concat(stt.submit, "/div[2]/form/div[5]/button");//

        //        StringAssert.Contains(cc.FetchInnerHTML(driver, stt.header), "General");
        //        tglog.AppendLogText(filePath, "Opened Company Settings page ..... OK");

        //        //tglog.AppendLogText(filePath, string.Concat("Original Set Print Log is ", cc.FetchChecked(driver, stt.printLog)));

        //        //test
        //        cc.ClickButton(driver, stt.printLog);

        //        Thread.Sleep(3000);
        //        cc.ClickButton(driver, submit);
        //        //StringAssert.Contains("True", (interval != cc.FetchTextValue(driver, stt.syncInterval)).ToString());
        //        tglog.AppendLogText(filePath, string.Concat("Clicked Set Print Log", Environment.NewLine));

        //        Thread.Sleep(5000);
        //        passFail = 0;

        //        // reset original
        //        submit = string.Concat(stt.submit, "/div[3]/form/div[5]/button");///html/body/div/main/div/div[3]/form/div[5]/button
        //        cc.ClickButton(driver, stt.printLog2);
        //        Thread.Sleep(5000);
        //        cc.ClickButton(driver, submit);
        //        //StringAssert.Contains("True", (interval == cc.FetchTextValue(driver, stt.syncInterval)).ToString());
        //        tglog.AppendLogText(filePath, string.Concat("Reset Set Print Log", Environment.NewLine));

        //    }
        //    catch (Exception e)
        //    {
        //        passFail = 1;
        //        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
        //    }
        //}

        //[TestMethod, TestCategory("Functional - Admin")]
        //public void Admin_Settings_EditInterval()
        //{
        //    string interval = "";
        //    string submit = "";

        //    if (qaDev.ToLower() == dev)
        //    {
        //        instance = 4997504;
        //        testName = "Admin_Settings_EditInterval_Dev";
        //    }
        //    else
        //    {
        //        if (qaDev.ToLower() == qa)
        //        {
        //            instance = 4998767;
        //            testName = "Admin_Settings_EditInterval_QA";
        //        }
        //        else
        //        {
        //            if (qaDev.ToLower() == beta)
        //            {
        //                instance = 4998769;
        //                testName = "Admin_Settings_EditInterval_Beta";
        //            }
        //            else
        //            {
        //                if (qaDev.ToLower() == prod)
        //                {
        //                    instance = 4998777;
        //                    testName = "Admin_Settings_EditInterval_Prod";
        //                }
        //                else
        //                {
        //                    instance = 6964432;
        //                    testName = "Admin_Settings_EditInterval_OP";
        //                }
        //            }
        //        }
        //    }
        //    passFail = 1;  //reset before test
        //    filePath = string.Concat(filePath, testName, ".txt");

        //    CreateLogFile();

        //    try
        //    {
        //        stt = mp.OpenSettings(driver);
        //        submit = string.Concat(stt.submit, "/div[2]/form/div[5]/button");
        //        //tglog.AppendLogText(filePath, driver.Title);

        //        StringAssert.Contains(cc.FetchInnerHTML(driver, stt.header), "General");
        //        tglog.AppendLogText(filePath, "Opened Company Settings page ..... OK");

        //        interval = cc.FetchTextValue(driver, stt.syncInterval);
        //        tglog.AppendLogText(filePath, string.Concat("Original Sync Interval Value is ", interval));

        //        //test
        //        cc.EnterText(driver, "45", stt.syncInterval);
        //        cc.ClickButton(driver, submit);

        //        Thread.Sleep(3000);
        //        StringAssert.Contains("True", (interval != cc.FetchTextValue(driver, stt.syncInterval)).ToString());
        //        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "New Sync Interval, ", cc.FetchTextValue(driver, stt.syncInterval), ", is not same as Orignal,", interval, ".....Passed", Environment.NewLine));

        //        Thread.Sleep(5000);
        //        passFail = 0;

        //        // reset original
        //        submit = string.Concat(stt.submit, "/div[3]/form/div[5]/button");
        //        cc.EnterText(driver, interval, stt.syncInterval);
        //        cc.ClickButton(driver, submit);
        //        StringAssert.Contains("True", (interval == cc.FetchTextValue(driver, stt.syncInterval)).ToString());
        //        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Original Sync Intereval, ", interval, ", has been reset", Environment.NewLine));
        //        Thread.Sleep(5000);

        //    }
        //    catch (Exception e)
        //    {
        //        passFail = 1;
        //        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
        //    }
        //}

        //[TestMethod, TestCategory("Functional - Admin")]
        //public void Admin_Settings_EditLocale()
        //{
        //    string locale = "";
        //    string submit = "";

        //    if (qaDev.ToLower() == dev)
        //    {
        //        instance = 4997503;
        //        testName = "Admin_Settings_EditLocale_Dev";
        //    }
        //    else
        //    {
        //        if (qaDev.ToLower() == qa)
        //        {
        //            instance = 4998766;
        //            testName = "Admin_Settings_EditLocale_QA";
        //        }
        //        else
        //        {
        //            if (qaDev.ToLower() == beta)
        //            {
        //                instance = 4998768;
        //                testName = "Admin_Settings_EditLocale_Beta";
        //            }
        //            else
        //            {
        //                if (qaDev.ToLower() == prod)
        //                {
        //                    instance = 4998776;
        //                    testName = "Admin_Settings_EditLocale_Prod";
        //                }
        //                else
        //                {
        //                    instance = 6964431;
        //                    testName = "Admin_Settings_EditLocale_OP";
        //                }
        //            }
        //        }
        //    }
        //    passFail = 1;  //reset before test
        //    filePath = string.Concat(filePath, testName, ".txt");

        //    CreateLogFile();

        //    try
        //    {
        //        stt = mp.OpenSettings(driver);
        //        submit = string.Concat(stt.submit, "/div[2]/form/div[5]/button");
        //        //tglog.AppendLogText(filePath, driver.Title);

        //        StringAssert.Contains(cc.FetchInnerHTML(driver, stt.header), "General");
        //        tglog.AppendLogText(filePath, "Opened Company Settings page ..... OK");

        //        locale = cc.FetchSelectedOption(driver, stt.locale);
        //        tglog.AppendLogText(filePath, string.Concat("Original Locale Value is ", locale));

        //        //test
        //        cc.SelectDropDownByElement(driver, stt.locale, "Dutch");
        //        cc.ClickButton(driver, submit);
        //        StringAssert.Contains("True", (locale != cc.FetchSelectedOption(driver, stt.locale)).ToString());
        //        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "New Locale, ", cc.FetchSelectedOption(driver, stt.locale), ", is not same as Orignal,", locale, ".....Passed", Environment.NewLine));

        //        StringAssert.Contains("Settings Saved", cc.FetchInnerHTML(driver, stt.settingsSaved));
        //        tglog.AppendLogText(filePath, cc.FetchInnerHTML(driver, stt.settingsSaved));
        //        passFail = 0;

        //        // reset original
        //        submit = string.Concat(stt.submit, "/div[3]/form/div[5]/button");
        //        Thread.Sleep(3000);
        //        cc.SelectDropDownByElement(driver, stt.locale, locale);
        //        cc.ClickButton(driver, submit);
        //        StringAssert.Contains("True", (locale == cc.FetchSelectedOption(driver, stt.locale)).ToString());
        //        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Original locale, ", locale, ", has been reset", Environment.NewLine));

        //    }
        //    catch (Exception e)
        //    {
        //        passFail = 1;
        //        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
        //    }
        //}

        //[TestMethod, TestCategory("Functional - Admin")]
        //public void Admin_Settings_EditSessionTimeout()
        //{
        //    string timeoutSetting = "";
        //    string submit = "";

        //    if (qaDev.ToLower() == dev)
        //    {
        //        //instance = 4997503;
        //        testName = "Admin_Settings_EditSessionTimeout";
        //    }
        //    else
        //    {
        //        if (qaDev.ToLower() == qa)
        //        {
        //            instance = 9695151;
        //            testName = "Admin_Settings_EditSessionTimeout_QA";
        //        }
        //        else
        //        {
        //            if (qaDev.ToLower() == beta)
        //            {
        //                instance = 9695201;
        //                testName = "Admin_Settings_EditSessionTimeout_Beta";
        //            }
        //            else
        //            {
        //                if (qaDev.ToLower() == prod)
        //                {
        //                    instance = 9695498;
        //                    testName = "Admin_Settings_EditSessionTimeout_Prod";
        //                }
        //                else
        //                {
        //                    //instance = 6964431;
        //                    testName = "Admin_Settings_EditSessionTimeout_OP";
        //                }
        //            }
        //        }
        //    }
        //    passFail = 1;  //reset before test
        //    filePath = string.Concat(filePath, testName, ".txt");

        //    CreateLogFile();

        //    try
        //    {
        //        stt = mp.OpenSettings(driver);
        //        submit = string.Concat(stt.submit, "/div[2]/form/div[5]/button");

        //        StringAssert.Contains(cc.FetchInnerHTML(driver, stt.header), "General");
        //        tglog.AppendLogText(filePath, "Opened Company Settings page ..... OK");

        //        timeoutSetting = cc.FetchTextValue(driver, stt.timeoutSession);
        //        tglog.AppendLogText(filePath, string.Concat("Original Timeout Value is ", timeoutSetting));

        //        //test
        //        cc.EnterText(driver, "5", stt.timeoutSession);
        //        cc.ClickButton(driver, submit);
        //        StringAssert.Contains("True", (timeoutSetting != cc.FetchTextValue(driver, stt.timeoutSession)).ToString());

        //        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "New Session Timeout, ", cc.FetchTextValue(driver, stt.timeoutSession), ", is not same as Orignal,", timeoutSetting, ".....Passed", Environment.NewLine));

        //        StringAssert.Contains("Settings Saved", cc.FetchInnerHTML(driver, stt.settingsSaved));
        //        tglog.AppendLogText(filePath, cc.FetchInnerHTML(driver, stt.settingsSaved));
        //        passFail = 0;

        //        // reset original
        //        submit = string.Concat(stt.submit, "/div[3]/form/div[5]/button");
        //        Thread.Sleep(3000);
        //        cc.EnterText(driver, timeoutSetting, stt.timeoutSession);
        //        cc.ClickButton(driver, submit);
        //        StringAssert.Contains("True", (timeoutSetting == cc.FetchTextValue(driver, stt.timeoutSession)).ToString());
        //        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Original Timeout, ", timeoutSetting, ", has been reset", Environment.NewLine));

        //    }
        //    catch (Exception e)
        //    {
        //        passFail = 1;
        //        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
        //    }
        //}

        //[TestMethod, TestCategory("Functional - Admin")]
        //public void Admin_Settings_ChangeSaveTextCaptureData()
        //{
        //    string submit = "";

        //    if (qaDev.ToLower() == dev)
        //    {
        //        //instance = 4997504;
        //        testName = "Admin_Settings_ChangeSaveTextCaptureData_Dev";
        //    }
        //    else
        //    {
        //        if (qaDev.ToLower() == qa)
        //        {
        //            instance = 9723089;
        //            testName = "Admin_Settings_ChangeSaveTextCaptureData_QA";
        //        }
        //        else
        //        {
        //            if (qaDev.ToLower() == beta)
        //            {
        //                instance = 9723669;
        //                testName = "Admin_Settings_ChangeSaveTextCaptureData_Beta";
        //            }
        //            else
        //            {
        //                if (qaDev.ToLower() == prod)
        //                {
        //                    instance = 9723670;
        //                    testName = "Admin_Settings_ChangeSaveTextCaptureData_Prod";
        //                }
        //                else
        //                {
        //                    //instance = 6964432;
        //                    testName = "Admin_Settings_ChangeSaveTextCaptureData_OP";
        //                }
        //            }
        //        }
        //    }
        //    passFail = 1;  //reset before test
        //    filePath = string.Concat(filePath, testName, ".txt");

        //    CreateLogFile();

        //    try
        //    {
        //        stt = mp.OpenSettings(driver);
        //        submit = string.Concat(stt.submit, "/div[2]/form/div[5]/button");

        //        StringAssert.Contains(cc.FetchInnerHTML(driver, stt.header), "General");
        //        tglog.AppendLogText(filePath, "Opened Company Settings page ..... OK");

        //        //tglog.AppendLogText(filePath, string.Concat("Original Set Print Log is ", cc.FetchChecked(driver, stt.printLog)));

        //        //test
        //        cc.ClickButton(driver, stt.saveTextCaptureData);
        //        cc.ClickButton(driver, submit);
        //        Thread.Sleep(3000);
        //        //StringAssert.Contains("True", (interval != cc.FetchTextValue(driver, stt.syncInterval)).ToString());
        //        tglog.AppendLogText(filePath, string.Concat("Clicked Save Text Capture Data", Environment.NewLine));

        //        Thread.Sleep(5000);
        //        passFail = 0;

        //        // reset original
        //        submit = string.Concat(stt.submit, "/div[3]/form/div[5]/button");
        //        cc.ClickButton(driver, stt.saveTextCaptureData2);
        //        cc.ClickButton(driver, submit);
        //        //StringAssert.Contains("True", (interval == cc.FetchTextValue(driver, stt.syncInterval)).ToString());
        //        tglog.AppendLogText(filePath, string.Concat("Reset Savge Text Capture Data", Environment.NewLine));
        //        Thread.Sleep(5000);

        //    }
        //    catch (Exception e)
        //    {
        //        passFail = 1;
        //        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
        //    }
        //}

        //[TestMethod, TestCategory("Functional - Admin")]
        //public void Admin_User_AddNewUser()
        //{
        //    if (qaDev.ToLower() == dev)
        //    {
        //        instance = 4953097;
        //        testName = "Admin_User_AddNewUser_Dev";
        //    }
        //    else
        //    {
        //        if (qaDev.ToLower() == qa)
        //        {
        //            instance = 4953146;
        //            testName = "Admin_User_AddNewUser_QA";
        //        }
        //        else
        //        {
        //            if (qaDev.ToLower() == beta)
        //            {
        //                instance = 4953188;
        //                testName = "Admin_User_AddNewUser_Beta";
        //            }
        //            else
        //            {
        //                if (qaDev.ToLower() == prod)
        //                {
        //                    instance = 4974335;
        //                    testName = "Admin_User_AddNewUser_Prod";
        //                }
        //                else
        //                {
        //                    instance = 6964422;
        //                    testName = "Admin_User_AddNewUser_OP";
        //                }
        //            }
        //        }
        //    }
        //    passFail = 1;  //reset before test
        //    filePath = string.Concat(filePath, testName, ".txt");

        //    CreateLogFile();

        //    try
        //    {

        //        cd = mp.OpenCompanyDetail(driver);
        //        //tglog.AppendLogText(filePath, driver.Title);

        //        StringAssert.Contains(cd.FetchHeader(driver), "Users");
        //        tglog.AppendLogText(filePath, "Opened Company User page ..... OK");

        //        itemCount = cc.FetchChildCount(driver, cd.userTable);

        //        nu = cd.OpenNewUser(driver);
        //        //tglog.AppendLogText(filePath, driver.Title);

        //        StringAssert.Contains(nu.FetchHeader(driver), "Create User");
        //        tglog.AppendLogText(filePath, "Opened New User page ..... OK");

        //        //test
        //        cc.EnterText(driver, firstname, nu.firstName);
        //        cc.EnterText(driver, lastname, nu.lastName);
        //        cc.EnterText(driver, string.Concat(firstname, lastname, "@troygroup.com"), nu.email);
        //        cc.EnterText(driver, pswd, nu.pswd);

        //        cc.ClickButton(driver, nu.submitBtn);

        //        StringAssert.Contains("False", (itemCount == cc.FetchChildCount(driver, cd.userTable)).ToString());
        //        tglog.AppendLogText(filePath, string.Concat("User Count = ", itemCount, " Original Count <> Current Count..... OK"));

        //        for (int x = 1; x <= Convert.ToInt16(cc.FetchChildCount(driver, cd.userTable)); x++)
        //        {
        //            if (string.Concat(firstname, " ", lastname) == cc.FetchCellValue(driver, "2", x.ToString(), cd.userTable))
        //            {
        //                StringAssert.Contains(string.Concat(firstname, lastname, "@troygroup.com"), cc.FetchCellValue(driver, "1", x.ToString(), cd.userTable));
        //                tglog.AppendLogText(filePath, "New user email displayed correctly on Users List..... OK");

        //                StringAssert.Contains(string.Concat(firstname, " ", lastname), cc.FetchCellValue(driver, "2", x.ToString(), cd.userTable));
        //                tglog.AppendLogText(filePath, "New user Full Name displayed correctly on Users List..... OK");

        //                StringAssert.Contains(string.Concat(firstname, lastname, "@troygroup.com"), cc.FetchCellValue(driver, "3", x.ToString(), cd.userTable));
        //                tglog.AppendLogText(filePath, "New user email displayed correctly on Users List..... OK");

        //                StringAssert.Contains(cc.FetchCellValue(driver, "1", x.ToString(), cd.userTable), cc.FetchCellValue(driver, "3", x.ToString(), cd.userTable));
        //                tglog.AppendLogText(filePath, "New user email is same as username on Users List..... OK");
        //            }
        //        }
        //        passFail = 0;
        //    }
        //    catch (Exception e)
        //    {
        //        passFail = 1;
        //        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
        //    }
        //}

        //[TestMethod, TestCategory("Functional - Admin")]
        //public void Admin_User_CancelNewUser()
        //{
        //    if (qaDev.ToLower() == dev)
        //    {
        //        instance = 4953098;
        //        testName = "Admin_User_CancelNewUser_Dev";
        //    }
        //    else
        //    {
        //        if (qaDev.ToLower() == qa)
        //        {
        //            instance = 4953145;
        //            testName = "Admin_User_CancelNewUser_QA";
        //        }
        //        else
        //        {
        //            if (qaDev.ToLower() == beta)
        //            {
        //                instance = 4953187;
        //                testName = "Admin_User_CancelNewUser_Beta";
        //            }
        //            else
        //            {
        //                if (qaDev.ToLower() == prod)
        //                {
        //                    instance = 4974334;
        //                    testName = "Admin_User_CancelNewUser_Prod";
        //                }
        //                else
        //                {
        //                    instance = 6964421;
        //                    testName = "Admin_User_CancelNewUser_OP";
        //                }
        //            }
        //        }
        //    }
        //    passFail = 1;  //reset before test
        //    filePath = string.Concat(filePath, testName, ".txt");

        //    CreateLogFile();

        //    try
        //    {
        //        cd = mp.OpenCompanyDetail(driver);
        //        //tglog.AppendLogText(filePath, driver.Title);

        //        StringAssert.Contains(cd.FetchHeader(driver), "Users");
        //        tglog.AppendLogText(filePath, "Opened Company User page ..... OK");

        //        itemCount = cc.FetchChildCount(driver, cd.userTable);

        //        nu = cd.OpenNewUser(driver);
        //        //tglog.AppendLogText(filePath, driver.Title);

        //        StringAssert.Contains(nu.FetchHeader(driver), "Create User");
        //        tglog.AppendLogText(filePath, "Opened New User page ..... OK");

        //        //test
        //        cc.EnterText(driver, firstname, nu.firstName);
        //        cc.EnterText(driver, lastname, nu.lastName);
        //        cc.EnterText(driver, string.Concat(firstname, lastname, "@troygroup.com"), nu.email);
        //        cc.EnterText(driver, pswd, nu.pswd);

        //        cc.ClickButton(driver, nu.canceBtn);

        //        StringAssert.Contains(itemCount, cc.FetchChildCount(driver, cd.userTable));
        //        tglog.AppendLogText(filePath, string.Concat("User Count = ", itemCount, " Original Count = Current Count..... OK"));
        //        passFail = 0;
        //    }
        //    catch (Exception e)
        //    {
        //        passFail = 1;
        //        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
        //    }
        //}

        //[TestMethod, TestCategory("Functional - Admin")]
        //public void Admin_User_CancelDeleteUser()
        //{
        //    if (qaDev.ToLower() == dev)
        //    {
        //        instance = 4953109;
        //        testName = "Admin_User_CancelDeleteUser_Dev";
        //    }
        //    else
        //    {
        //        if (qaDev.ToLower() == qa)
        //        {
        //            instance = 4953153;
        //            testName = "Admin_User_CancelDeleteUser_QA";
        //        }
        //        else
        //        {
        //            if (qaDev.ToLower() == beta)
        //            {
        //                instance = 4953195;
        //                testName = "Admin_User_CancelDeleteUser_Beta";
        //            }
        //            else
        //            {
        //                if (qaDev.ToLower() == prod)
        //                {
        //                    instance = 4974342;
        //                    testName = "Admin_User_CancelDeleteUser_Prod";
        //                }
        //                else
        //                {
        //                    instance = 6964429;
        //                    testName = "Admin_User_CancelDeleteUser_OP";
        //                }
        //            }
        //        }
        //    }
        //    passFail = 1;  //reset before test
        //    filePath = string.Concat(filePath, testName, ".txt");

        //    CreateLogFile();
        //    baseWindowHandle = driver.CurrentWindowHandle;

        //    try
        //    {
        //        cd = mp.OpenCompanyDetail(driver);
        //        //tglog.AppendLogText(filePath, driver.Title);

        //        StringAssert.Contains(cd.FetchHeader(driver), "Users");
        //        tglog.AppendLogText(filePath, "Opened Company User page ..... OK");

        //        itemCount = cc.FetchChildCount(driver, cd.userTable);
        //        for (int x = 1; x <= Convert.ToInt16(cc.FetchChildCount(driver, cd.userTable)); x++)
        //        {
        //            if (string.Concat(firstname, " ", lastname) == cc.FetchCellValue(driver, "2", x.ToString(), cd.userTable))
        //            {
        //                StringAssert.Contains(string.Concat(firstname, " ", lastname), cc.FetchCellValue(driver, "2", x.ToString(), cd.userTable));
        //                tglog.AppendLogText(filePath, "New user Full Name displayed correctly on Users List..... OK");
        //                cc.ClickButton(driver, string.Concat(cd.userTable, "/tr[", x.ToString(), "]/td[5]/form/button"));

        //                if (cc.CancelAssert(driver) == "")
        //                {
        //                    driver.SwitchTo().Window(baseWindowHandle);
        //                    StringAssert.Contains(itemCount, cc.FetchChildCount(driver, cd.userTable));
        //                    tglog.AppendLogText(filePath, "Selected user was not removed.....OK");
        //                    passFail = 0;
        //                }
        //                else
        //                {
        //                    passFail = 1;
        //                    tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Alert was not displayed!", Environment.NewLine));
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        passFail = 1;
        //        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
        //    }
        //}

        //[TestMethod, TestCategory("Functional - Admin")]
        //public void Admin_User_CancelEditUser()
        //{
        //    if (qaDev.ToLower() == dev)
        //    {
        //        instance = 4953102;
        //        testName = "Admin_User_CancelEditUser_Dev";
        //    }
        //    else
        //    {
        //        if (qaDev.ToLower() == qa)
        //        {
        //            instance = 4953147;
        //            testName = "Admin_User_CancelEditUser_QA";
        //        }
        //        else
        //        {
        //            if (qaDev.ToLower() == beta)
        //            {
        //                instance = 4953189;
        //                testName = "Admin_User_CancelEditUser_Beta";
        //            }
        //            else
        //            {
        //                if (qaDev.ToLower() == prod)
        //                {
        //                    instance = 4974336;
        //                    testName = "Admin_User_CancelEditUser_Prod";
        //                }
        //                else
        //                {
        //                    instance = 6964423;
        //                    testName = "Admin_User_CancelEditUser_OP";
        //                }
        //            }
        //        }
        //    }
        //    passFail = 1;  //reset before test
        //    filePath = string.Concat(filePath, testName, ".txt");

        //    CreateLogFile();
        //    baseWindowHandle = driver.CurrentWindowHandle;

        //    try
        //    {
        //        cd = mp.OpenCompanyDetail(driver);
        //        //tglog.AppendLogText(filePath, driver.Title);

        //        StringAssert.Contains(cd.FetchHeader(driver), "Users");
        //        tglog.AppendLogText(filePath, "Opened Company User page ..... OK");

        //        itemCount = cc.FetchChildCount(driver, cd.userTable);
        //        for (int x = 1; x <= Convert.ToInt16(cc.FetchChildCount(driver, cd.userTable)); x++)
        //        {
        //            if (string.Concat(firstname, " ", lastname) == cc.FetchCellValue(driver, "2", x.ToString(), cd.userTable))
        //            {
        //                StringAssert.Contains(string.Concat(firstname, " ", lastname), cc.FetchCellValue(driver, "2", x.ToString(), cd.userTable));
        //                tglog.AppendLogText(filePath, "New user Full Name displayed correctly on Users List..... OK");

        //                eu = cd.OpenEditUser(driver, string.Concat(cd.userTable, "/tr[", x.ToString(), "]/td[5]/form/a[1]"));
        //                Thread.Sleep(3000);

        //                cc.EnterText(driver, "Miller", eu.lastName);
        //                cc.ClickButton(driver, eu.cancelBtn);
        //                Thread.Sleep(3000);

        //                StringAssert.Contains(string.Concat(firstname, " ", lastname), cc.FetchCellValue(driver, "2", x.ToString(), cd.userTable));
        //                tglog.AppendLogText(filePath, "New user Full Name is the original on Users List..... OK");

        //                passFail = 0;
        //                break;
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        passFail = 1;
        //        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
        //    }
        //}

        //[TestMethod, TestCategory("Functional - Admin")]
        //public void Admin_User_CancelResetPassword()
        //{
        //    if (qaDev.ToLower() == dev)
        //    {
        //        instance = 4953107;
        //        testName = "Admin_User_CancelResetPassword_Dev";
        //    }
        //    else
        //    {
        //        if (qaDev.ToLower() == qa)
        //        {
        //            instance = 4953151;
        //            testName = "Admin_User_CancelResetPassword_QA";
        //        }
        //        else
        //        {
        //            if (qaDev.ToLower() == beta)
        //            {
        //                instance = 4953193;
        //                testName = "Admin_User_CancelResetPassword_Beta";
        //            }
        //            else
        //            {
        //                if (qaDev.ToLower() == prod)
        //                {
        //                    instance = 4974340;
        //                    testName = "Admin_User_CancelResetPassword_Prod";
        //                }
        //                else
        //                {
        //                    instance = 6964427;
        //                    testName = "Admin_User_CancelResetPassword_OP";
        //                }
        //            }
        //        }
        //    }
        //    passFail = 1;  //reset before test
        //    filePath = string.Concat(filePath, testName, ".txt");

        //    CreateLogFile();
        //    baseWindowHandle = driver.CurrentWindowHandle;

        //    try
        //    {
        //        cd = mp.OpenCompanyDetail(driver);
        //        //tglog.AppendLogText(filePath, driver.Title);

        //        StringAssert.Contains(cd.FetchHeader(driver), "Users");
        //        tglog.AppendLogText(filePath, "Opened Company User page ..... OK");

        //        itemCount = cc.FetchChildCount(driver, cd.userTable);
        //        for (int x = 1; x <= Convert.ToInt16(cc.FetchChildCount(driver, cd.userTable)); x++)
        //        {
        //            if (string.Concat(firstname, " ", lastname) == cc.FetchCellValue(driver, "2", x.ToString(), cd.userTable))
        //            {
        //                StringAssert.Contains(string.Concat(firstname, " ", lastname), cc.FetchCellValue(driver, "2", x.ToString(), cd.userTable));
        //                tglog.AppendLogText(filePath, "New user Full Name displayed correctly on Users List..... OK");

        //                rp = cd.OpenResetPassword(driver, string.Concat(cd.userTable, "/tr[", x.ToString(), "]/td[5]/form/a[2]"));
        //                Thread.Sleep(3000);

        //                cc.EnterText(driver, "something", rp.pswd);
        //                cc.ClickButton(driver, rp.cancelBtn);
        //                Thread.Sleep(3000);

        //                StringAssert.Contains(string.Concat(firstname, " ", lastname), cc.FetchCellValue(driver, "2", x.ToString(), cd.userTable));
        //                tglog.AppendLogText(filePath, "New user Full Name is the original on Users List..... OK");

        //                passFail = 0;
        //                break;
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        passFail = 1;
        //        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
        //    }
        //}

        //[TestMethod, TestCategory("Functional - Admin")]
        //public void Admin_User_DeleteUser()
        //{
        //    if (qaDev.ToLower() == dev)
        //    {
        //        instance = 4953110;
        //        testName = "Admin_User_DeleteUser_Dev";
        //    }
        //    else
        //    {
        //        if (qaDev.ToLower() == qa)
        //        {
        //            instance = 4953154;
        //            testName = "Admin_User_DeleteUser_QA";
        //        }
        //        else
        //        {
        //            if (qaDev.ToLower() == beta)
        //            {
        //                instance = 4953196;
        //                testName = "Admin_User_DeleteUser_Beta";
        //            }
        //            else
        //            {
        //                if (qaDev.ToLower() == prod)
        //                {
        //                    instance = 4974343;
        //                    testName = "Admin_User_DeleteUser_Prod";
        //                }
        //                else
        //                {
        //                    instance = 6964430;
        //                    testName = "Admin_User_DeleteUser_OP";
        //                }
        //            }
        //        }
        //    }
        //    passFail = 1;  //reset before test
        //    filePath = string.Concat(filePath, testName, ".txt");

        //    CreateLogFile();
        //    baseWindowHandle = driver.CurrentWindowHandle;

        //    try
        //    {
        //        cd = mp.OpenCompanyDetail(driver);
        //        //tglog.AppendLogText(filePath, driver.Title);

        //        StringAssert.Contains(cd.FetchHeader(driver), "Users");
        //        tglog.AppendLogText(filePath, "Opened Company User page ..... OK");

        //        itemCount = cc.FetchChildCount(driver, cd.userTable);
        //        for (int x = 1; x <= Convert.ToInt16(cc.FetchChildCount(driver, cd.userTable)); x++)
        //        {
        //            if (string.Concat(firstname, " ", lastname) == cc.FetchCellValue(driver, "2", x.ToString(), cd.userTable))
        //            {
        //                StringAssert.Contains(string.Concat(firstname, " ", lastname), cc.FetchCellValue(driver, "2", x.ToString(), cd.userTable));
        //                tglog.AppendLogText(filePath, "New user Full Name displayed correctly on Users List..... OK");
        //                cc.ClickButton(driver, string.Concat(cd.userTable, "/tr[", x.ToString(), "]/td[5]/form/button"));

        //                if (cc.AcceptAssertOK(driver) == "")
        //                {
        //                    driver.SwitchTo().Window(baseWindowHandle);
        //                    passFail = 0;
        //                    break;
        //                }
        //                else
        //                {
        //                    passFail = 1;
        //                    tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Alert was not displayed!", Environment.NewLine));
        //                }
        //            }
        //        }

        //        for (int x = 1; x <= Convert.ToInt16(cc.FetchChildCount(driver, cd.userTable)); x++)
        //        {
        //            if (string.Concat(firstname, " ", lastname) == cc.FetchCellValue(driver, "2", x.ToString(), cd.userTable))
        //            {
        //                passFail = 1;
        //                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, string.Concat("User ", firstname, " ", lastname, " was not removed.....FAIL!"), Environment.NewLine));
        //                break;
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        passFail = 1;
        //        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
        //    }
        //}

        //[TestMethod, TestCategory("Functional - Admin")]
        //public void Admin_User_EditEmail()
        //{
        //    if (qaDev.ToLower() == dev)
        //    {
        //        instance = 4953101;
        //        testName = "Admin_User_EditEmail_Dev";
        //    }
        //    else
        //    {
        //        if (qaDev.ToLower() == qa)
        //        {
        //            instance = 4953150;
        //            testName = "Admin_User_EditEmail_QA";
        //        }
        //        else
        //        {
        //            if (qaDev.ToLower() == beta)
        //            {
        //                instance = 4953192;
        //                testName = "Admin_User_EditEmail_Beta";
        //            }
        //            else
        //            {
        //                if (qaDev.ToLower() == prod)
        //                {
        //                    instance = 4974339;
        //                    testName = "Admin_User_EditEmail_Prod";
        //                }
        //                else
        //                {
        //                    instance = 6964426;
        //                    testName = "Admin_User_EditEmail_OP";
        //                }
        //            }
        //        }
        //    }
        //    passFail = 1;  //reset before test
        //    filePath = string.Concat(filePath, testName, ".txt");
        //    fullName = string.Concat(firstname, " ", lastname);
        //    email = string.Concat(firstname, lastname, "@troygroup.com");

        //    CreateLogFile();
        //    baseWindowHandle = driver.CurrentWindowHandle;

        //    try
        //    {
        //        cd = mp.OpenCompanyDetail(driver);
        //        //tglog.AppendLogText(filePath, driver.Title);

        //        StringAssert.Contains(cd.FetchHeader(driver), "Users");
        //        tglog.AppendLogText(filePath, "Opened Company User page ..... OK");

        //        itemCount = cc.FetchChildCount(driver, cd.userTable);
        //        for (int x = 1; x <= Convert.ToInt16(cc.FetchChildCount(driver, cd.userTable)); x++)
        //        {
        //            if (string.Concat(firstname, " ", lastname) == cc.FetchCellValue(driver, "2", x.ToString(), cd.userTable))
        //            {
        //                StringAssert.Contains(string.Concat(firstname, " ", lastname), cc.FetchCellValue(driver, "2", x.ToString(), cd.userTable));
        //                tglog.AppendLogText(filePath, "New user Full Name displayed correctly on Users List..... OK");

        //                eu = cd.OpenEditUser(driver, string.Concat(cd.userTable, "/tr[", x.ToString(), "]/td[5]/form/a[1]"));
        //                Thread.Sleep(3000);

        //                cc.EnterText(driver, "JoyceSmith@troy.com", eu.email);
        //                cc.ClickButton(driver, eu.submitBtn);
        //                Thread.Sleep(3000);

        //                StringAssert.Contains("True", ("JoyceSmith@troy.com" == cc.FetchCellValue(driver, "3", x.ToString(), cd.userTable)).ToString());
        //                tglog.AppendLogText(filePath, "New user email change is reflected on Users List..... OK");

        //                StringAssert.Contains(cc.FetchCellValue(driver, "1", x.ToString(), cd.userTable), cc.FetchCellValue(driver, "3", x.ToString(), cd.userTable));
        //                tglog.AppendLogText(filePath, "Changed user email is same as username on Users List..... OK");

        //                //reset test
        //                eu = cd.OpenEditUser(driver, string.Concat(cd.userTable, "/tr[", x.ToString(), "]/td[5]/form/a[1]"));
        //                Thread.Sleep(3000);

        //                cc.EnterText(driver, email, eu.email);
        //                cc.ClickButton(driver, eu.submitBtn);
        //                Thread.Sleep(3000);

        //                fullName = string.Concat(firstname, " ", lastname);

        //                passFail = 0;
        //                break;
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        passFail = 1;
        //        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
        //    }
        //}

        //[TestMethod, TestCategory("Functional - Admin")]
        //public void Admin_User_EditFirstName()
        //{
        //    if (qaDev.ToLower() == dev)
        //    {
        //        instance = 4953099;
        //        testName = "Admin_User_EditFirstName_Dev";
        //    }
        //    else
        //    {
        //        if (qaDev.ToLower() == qa)
        //        {
        //            instance = 4953148;
        //            testName = "Admin_User_EditFirstName_QA";
        //        }
        //        else
        //        {
        //            if (qaDev.ToLower() == beta)
        //            {
        //                instance = 4953190;
        //                testName = "Admin_User_EditFirstName_Beta";
        //            }
        //            else
        //            {
        //                if (qaDev.ToLower() == prod)
        //                {
        //                    instance = 4974337;
        //                    testName = "Admin_User_EditFirstName_Prod";
        //                }
        //                else
        //                {
        //                    instance = 6964424;
        //                    testName = "Admin_User_EditFirstName_OP";
        //                }
        //            }
        //        }
        //    }
        //    passFail = 1;  //reset before test
        //    filePath = string.Concat(filePath, testName, ".txt");
        //    fullName = string.Concat(firstname, " ", lastname);

        //    CreateLogFile();
        //    baseWindowHandle = driver.CurrentWindowHandle;

        //    try
        //    {
        //        cd = mp.OpenCompanyDetail(driver);
        //        //tglog.AppendLogText(filePath, driver.Title);

        //        StringAssert.Contains(cd.FetchHeader(driver), "Users");
        //        tglog.AppendLogText(filePath, "Opened Company User page ..... OK");

        //        itemCount = cc.FetchChildCount(driver, cd.userTable);
        //        for (int x = 1; x <= Convert.ToInt16(cc.FetchChildCount(driver, cd.userTable)); x++)
        //        {
        //            if (string.Concat(firstname, " ", lastname) == cc.FetchCellValue(driver, "2", x.ToString(), cd.userTable))
        //            {
        //                StringAssert.Contains(string.Concat(firstname, " ", lastname), cc.FetchCellValue(driver, "2", x.ToString(), cd.userTable));
        //                tglog.AppendLogText(filePath, "New user Full Name displayed correctly on Users List..... OK");

        //                eu = cd.OpenEditUser(driver, string.Concat(cd.userTable, "/tr[", x.ToString(), "]/td[5]/form/a[1]"));
        //                Thread.Sleep(3000);

        //                cc.EnterText(driver, "Joyce", eu.firstName);
        //                cc.ClickButton(driver, eu.submitBtn);
        //                Thread.Sleep(3000);

        //                fullName = string.Concat("Joyce ", lastname);
        //                StringAssert.Contains("True", (fullName == cc.FetchCellValue(driver, "2", x.ToString(), cd.userTable)).ToString());
        //                tglog.AppendLogText(filePath, "New user first name change is reflected on Users List..... OK");

        //                //reset test
        //                eu = cd.OpenEditUser(driver, string.Concat(cd.userTable, "/tr[", x.ToString(), "]/td[5]/form/a[1]"));
        //                Thread.Sleep(3000);

        //                cc.EnterText(driver, firstname, eu.firstName);
        //                cc.ClickButton(driver, eu.submitBtn);
        //                Thread.Sleep(3000);

        //                fullName = string.Concat(firstname, " ", lastname);

        //                passFail = 0;
        //                break;
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        passFail = 1;
        //        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
        //    }
        //}

        //[TestMethod, TestCategory("Functional - Admin")]
        //public void Admin_User_EditLastName()
        //{
        //    if (qaDev.ToLower() == dev)
        //    {
        //        instance = 4953100;
        //        testName = "Admin_User_EditLastName_Dev";
        //    }
        //    else
        //    {
        //        if (qaDev.ToLower() == qa)
        //        {
        //            instance = 4953149;
        //            testName = "Admin_User_EditLastName_QA";
        //        }
        //        else
        //        {
        //            if (qaDev.ToLower() == beta)
        //            {
        //                instance = 4953191;
        //                testName = "Admin_User_EditLastName_Beta";
        //            }
        //            else
        //            {
        //                if (qaDev.ToLower() == prod)
        //                {
        //                    instance = 4974338;
        //                    testName = "Admin_User_EditLastName_Prod";
        //                }
        //                else
        //                {
        //                    instance = 6964425;
        //                    testName = "Admin_User_EditLastName_OP";
        //                }
        //            }
        //        }
        //    }
        //    passFail = 1;  //reset before test
        //    filePath = string.Concat(filePath, testName, ".txt");
        //    fullName = string.Concat(firstname, " ", lastname);

        //    CreateLogFile();
        //    baseWindowHandle = driver.CurrentWindowHandle;

        //    try
        //    {
        //        cd = mp.OpenCompanyDetail(driver);
        //        //tglog.AppendLogText(filePath, driver.Title);

        //        StringAssert.Contains(cd.FetchHeader(driver), "Users");
        //        tglog.AppendLogText(filePath, "Opened Company User page ..... OK");

        //        itemCount = cc.FetchChildCount(driver, cd.userTable);
        //        for (int x = 1; x <= Convert.ToInt16(cc.FetchChildCount(driver, cd.userTable)); x++)
        //        {
        //            if (string.Concat(firstname, " ", lastname) == cc.FetchCellValue(driver, "2", x.ToString(), cd.userTable))
        //            {
        //                StringAssert.Contains(string.Concat(firstname, " ", lastname), cc.FetchCellValue(driver, "2", x.ToString(), cd.userTable));
        //                tglog.AppendLogText(filePath, "New user Full Name displayed correctly on Users List..... OK");

        //                eu = cd.OpenEditUser(driver, string.Concat(cd.userTable, "/tr[", x.ToString(), "]/td[5]/form/a[1]"));
        //                Thread.Sleep(3000);

        //                cc.EnterText(driver, "Miller", eu.lastName);
        //                cc.ClickButton(driver, eu.submitBtn);
        //                Thread.Sleep(3000);

        //                fullName = string.Concat(firstname, " Miller");
        //                StringAssert.Contains("True", (fullName == cc.FetchCellValue(driver, "2", x.ToString(), cd.userTable)).ToString());
        //                tglog.AppendLogText(filePath, "New user Last name change is reflected on Users List..... OK");

        //                //reset test
        //                eu = cd.OpenEditUser(driver, string.Concat(cd.userTable, "/tr[", x.ToString(), "]/td[5]/form/a[1]"));
        //                Thread.Sleep(3000);

        //                cc.EnterText(driver, lastname, eu.lastName);
        //                cc.ClickButton(driver, eu.submitBtn);
        //                Thread.Sleep(3000);

        //                fullName = string.Concat(firstname, " ", lastname);

        //                passFail = 0;
        //                break;
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        passFail = 1;
        //        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
        //    }
        //}

        //[TestMethod, TestCategory("Functional - Admin")]
        //public void Admin_User_ResetPassword()
        //{
        //    if (qaDev.ToLower() == dev)
        //    {
        //        instance = 4953108;
        //        testName = "Admin_User_ResetPassword_Dev";
        //    }
        //    else
        //    {
        //        if (qaDev.ToLower() == qa)
        //        {
        //            instance = 4953152;
        //            testName = "Admin_User_ResetPassword_QA";
        //        }
        //        else
        //        {
        //            if (qaDev.ToLower() == beta)
        //            {
        //                instance = 4953194;
        //                testName = "Admin_User_ResetPassword_Beta";
        //            }
        //            else
        //            {
        //                if (qaDev.ToLower() == prod)
        //                {
        //                    instance = 4974341;
        //                    testName = "Admin_User_ResetPassword_Prod";
        //                }
        //                else
        //                {
        //                    instance = 6964428;
        //                    testName = "Admin_User_ResetPassword_OP";
        //                }
        //            }
        //        }
        //    }
        //    passFail = 1;  //reset before test
        //    filePath = string.Concat(filePath, testName, ".txt");

        //    CreateLogFile();
        //    baseWindowHandle = driver.CurrentWindowHandle;

        //    try
        //    {
        //        cd = mp.OpenCompanyDetail(driver);
        //        //tglog.AppendLogText(filePath, driver.Title);

        //        StringAssert.Contains(cd.FetchHeader(driver), "Users");
        //        tglog.AppendLogText(filePath, "Opened Company User page ..... OK");

        //        itemCount = cc.FetchChildCount(driver, cd.userTable);
        //        for (int x = 1; x <= Convert.ToInt16(cc.FetchChildCount(driver, cd.userTable)); x++)
        //        {
        //            if (string.Concat(firstname, lastname, "@troygroup.com") == cc.FetchCellValue(driver, "1", x.ToString(), cd.userTable))
        //            {
        //                StringAssert.Contains(string.Concat(firstname, lastname, "@troygroup.com"), cc.FetchCellValue(driver, "1", x.ToString(), cd.userTable));
        //                tglog.AppendLogText(filePath, "User Email displayed correctly on Users List..... OK");

        //                rp = cd.OpenResetPassword(driver, string.Concat(cd.userTable, "/tr[", x.ToString(), "]/td[5]/form/a[2]"));
        //                Thread.Sleep(3000);

        //                cc.EnterText(driver, "Thunder01$", rp.pswd);
        //                cc.ClickButton(driver, rp.submitBtn);
        //                Thread.Sleep(3000);

        //                tglog.AppendLogText(filePath, "Password Changed..... OK");

        //                passFail = 0;
        //                break;
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        passFail = 1;
        //        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
        //    }
        //}

        //-------------------------------------------------------------------------------------------
        #region Methods
        private void SetupDriver()
        {
            if ((brow.ToLower() == "ie"))
            {
                this.driver = new InternetExplorerDriver("C:\\Selenium");
            }

            if ((brow.ToLower() == "chrome"))
            {
                this.driver = new ChromeDriver(@"C:\Selenium");
            }

            //if ((brow.ToLower() == "firefox"))
            //{
            //    this.driver = new FirefoxDriver();
            //}

        }

        private void CreateLogFile()
        {
            tglog.CreateLogFile(filePath, string.Concat(Environment.NewLine, "----------- Start testing using ", brow, " -----------", Environment.NewLine));
            tglog.AppendLogText(filePath, string.Concat("Set up ", brow, " ..... OK"));
            tglog.AppendLogText(filePath, string.Concat("URL = ", baseURL));
            tglog.AppendLogText(filePath, string.Concat("Envionment = ", qaDev));
            tglog.AppendLogText(filePath, string.Concat("User = ", user));
            tglog.AppendLogText(filePath, "Opened SecureDocs in browser ..... OK");
            tglog.AppendLogText(filePath, string.Concat("Build Version: ", build));
            tglog.AppendLogText(filePath, "Logged into SecureDocs ..... OK");
            tglog.AppendLogText(filePath, string.Concat("Instance = ", instance));
            tglog.AppendLogText(filePath, string.Concat("FilePath = ", filePath));
            tglog.AppendLogText(filePath, "Opened SecureDocs Dashboard ..... OK");
        }

        private void SendToPractitest()
        {
            if (qaDev.ToUpper() == "DEV")
            {
                ptc.PT_Post_Specific_Run(filePath, passFail, resultMsg, instance, project, brow, "Development");
            }
            else
            {
                if (qaDev.ToUpper() == "QA")
                {
                    ptc.PT_Post_Specific_Run(filePath, passFail, resultMsg, instance, project, brow, "QA");
                }
                else
                {
                    if (qaDev.ToUpper() == "BETA")
                    {
                        ptc.PT_Post_Specific_Run(filePath, passFail, resultMsg, instance, project, brow, "Beta");
                    }
                    else
                    {
                        ptc.PT_Post_Specific_Run(filePath, passFail, resultMsg, instance, project, brow, "Production");
                    }
                }
            }
        }

        private void SetUpEnvironVars()
        {
            if (qaDev.ToLower() == dev)
            {
                filePath = string.Concat(filePath, "Dev\\");
            }
            else
            {
                if (qaDev.ToLower() == qa)
                {
                    filePath = string.Concat(filePath, "QA\\");
                }
                else
                {
                    if (qaDev.ToLower() == beta)
                    {
                        filePath = string.Concat(filePath, "Beta\\");
                    }
                    else
                    {
                        filePath = string.Concat(filePath, "Prod\\");
                    }
                }
            }
        }
        #endregion

        //--------------------------------------------------------------------------------------------
        #region Additional test attributes
        [TestInitialize()]
        public void MyTestInitialize()
        {
            cc = new Common.Common_Code(driver);

            brow = ConfigurationManager.AppSettings["browser"];
            baseURL = ConfigurationManager.AppSettings["DocrityaURL"];
            qaDev = ConfigurationManager.AppSettings["testEnv"];
            user = ConfigurationManager.AppSettings["testUser"].ToUpper();
            string pswd = ConfigurationManager.AppSettings["TestPswd"];

            SetupDriver();

            SetUpEnvironVars();

            lp = new SecureRx.Page_Elements.LoginPage(driver, baseURL);
            build = lp.FetchBuildVersion(driver);
            mp = lp.SDLogin(user, pswd, driver);

            //Did Home Menu open
            if (mp == null)
            {
                throw new NoSuchWindowException("SecureDocs home page was not displayed!");
            }
            else
            {
                passFail = 0;
            }
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {

            driver.SwitchTo().Window(baseWindowHandle);
            Thread.Sleep(1000);

            try
            {
                mp.LogoutandCloseBrowser(driver);
                tglog.AppendLogText(filePath, "Logged out and Closed browser..... OK");
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }

            if (passFail != 0)
            {
                resultMsg = string.Concat(testName, " ..... FAILED!!!!! Please refer to log file");
                //driver.Close();
            }
            else
            {
                resultMsg = string.Concat(testName, " ..... PASSED!!!!!");
            }

            SendToPractitest();

            tglog.AppendLogText(filePath, "Close SecureDocs");
            tglog.AppendLogText(filePath, "\n Test Finished \n");
        }

        #endregion

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;
    }
}