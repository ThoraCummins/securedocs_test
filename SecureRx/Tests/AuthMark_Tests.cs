﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Data;
using System.IO;
using System.Net;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;

namespace Tests
{
    [TestClass]
    public class Authmark_Tests
    {
        #region libraries
        SecureRx.Page_Elements.MainPage mp = null;
        SecureRx.Page_Elements.LoginPage lp = null;

        SecureRx.Page_Elements.Templates.SecurityTemplates st = null;
        SecureRx.Page_Elements.Templates.Create_Security_Templates cst = null;
        SecureRx.Page_Elements.Templates.Remove_Security_Template rst = null;
        SecureRx.Page_Elements.Templates.Template tst = null;

        SecureRx.Common.Common_Code cc = null;
        SecureRx.Common.PT_Client ptc = new SecureRx.Common.PT_Client();
        SecureRx.Common.TGLogging tglog = new SecureRx.Common.TGLogging();
        #endregion

        //Vars
        private string baseURL = "";
        private string baseWindowHandle = "";
        private string beta = "beta";
        private string brow = "";
        private string build = "";
        private string dev = "dev";
        private IWebDriver driver = null;
        private string filePath = "C:\\Test Logs\\SecureRx\\";
        private string imagePath = "";
        private int instance = 0;
        private int passFail = 1;
        //private string prod = "prod";
        private string project = "4896";
        private string qa = "qa";
        private string qaDev = "";
        private string resultMsg = "";
        private string testName = "";
        private string user = "";

        //-------------------------------------------------------------------------------------------
        [TestMethod, TestCategory("Under Review - AuthMark Tests")]
        public void AuthMark_Position_MoveAuthMark()
        {
            string templateName = "";
            string testValu = "";
            string testValu2 = "";
            string imagePath2 = "";
            string imagePath1 = "";

            if (qaDev.ToLower() == dev)
            {
                instance = 11128357;
                testName = "AuthMark_Position_MoveAuthMark_Dev";
            }
            else
            {
                if (qaDev.ToLower() == qa)
                {
                    instance = 11507434;
                    testName = "AuthMark_Position_MoveAuthMark_QA";
                }
                //else
                //{
                //    if (qaDev.ToLower() == beta)
                //    {
                //        instance = 7225653;
                //        testName = "AuthMark_Position_MoveAuthMark_Beta";
                //    }
                else
                {
                        instance = 11311907;
                        testName = "AuthMark_Position_MoveAuthMark_Prod";
                    }
                //}
            }
            passFail = 1;  //reset before test
            testValu = "561";
            testValu2 = "486";
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "AuthMark-DO NOT USE";
            CreateLogFile();

            try
            {
                //setup
                CreateTemplateUnderTest(templateName);
                baseWindowHandle = driver.CurrentWindowHandle;

                //Test
                SetupAuthMark();

                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, string.Concat("AuthMark X and Y Coordinate Change ", testValu, " "), ScreenshotImageFormat.Png, 0);
                Thread.Sleep(3000);

                cc.EnterText(driver, testValu2, tst.elementX);
                tglog.AppendLogText(filePath, string.Concat("AuthMark X Coordinate = ", testValu2));

                cc.EnterText(driver, testValu, tst.elementY);
                tglog.AppendLogText(filePath, string.Concat("AuthMark Y Coordinate = ", testValu));

                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, string.Concat("AuthMark X and Y Coordinate Change ", testValu, " "), ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);

                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template ..... OK");
                Thread.Sleep(3000);

                cc.ClickButton(driver, tst.templateHomeBtn);
                tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                Thread.Sleep(3000);
                cc.ScrollPage(driver);

                //Tear Down
                RemoveTemplateUnderTest(templateName);

                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                passFail = 0;
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Tear Down
                cc.ClickButton(driver, tst.templateHomeBtn);
                tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                Thread.Sleep(5000);
                cc.ScrollPage(driver);
                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        [TestMethod, TestCategory("Under Review - AuthMark Tests")]
        public void AuthMark_Position_ResizeAuthMark()
        {
            string templateName = "";
            string testValu = "";
            string testValu2 = "";
            string imagePath2 = "";
            string imagePath1 = "";

            if (qaDev.ToLower() == dev)
            {
                instance = 11128358;
                testName = "AuthMark_Position_ResizeAuthMark_Dev";
            }
            else
            {
                if (qaDev.ToLower() == qa)
                {
                    instance = 11507435;
                    testName = "AuthMark_Position_ResizeAuthMark_QA";
                }
                //    else
                //    {
                //        if (qaDev.ToLower() == beta)
                //        {
                //            instance = 7225653;
                //            testName = "AuthMark_Position_ResizeAuthMark_Beta";
                //        }
                else
                        {
                            instance = 11311908;
                            testName = "AuthMark_Position_ResizeAuthMark_Prod";
                        }
                    }
            //    }
        //}
            passFail = 1;  //reset before test
            testValu = "300";
            testValu2 = "200";
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "AuthMark-DO NOT USE";
            CreateLogFile();

            try
            {
                //setup
                CreateTemplateUnderTest(templateName);
                baseWindowHandle = driver.CurrentWindowHandle;

                //Test
                SetupAuthMark();

                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, string.Concat("AuthMark Height and Width Change ", testValu, " "), ScreenshotImageFormat.Png, 0);
                Thread.Sleep(3000);

                cc.EnterText(driver, testValu2, tst.elementWidth);
                tglog.AppendLogText(filePath, string.Concat("AuthMark Width = ", testValu2));

                cc.EnterText(driver, testValu, tst.elementHeight);
                tglog.AppendLogText(filePath, string.Concat("AuthMark Height = ", testValu));

                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, string.Concat("AuthMark Height and Width Change ", testValu, " "), ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);

                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template ..... OK");
                Thread.Sleep(3000);

                cc.ClickButton(driver, tst.templateHomeBtn);
                tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                Thread.Sleep(3000);
                cc.ScrollPage(driver);

                //Tear Down
                RemoveTemplateUnderTest(templateName);

                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                passFail = 0;
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Tear Down
                cc.ClickButton(driver, tst.templateHomeBtn);
                tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                Thread.Sleep(5000);
                cc.ScrollPage(driver);
                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        [TestMethod, TestCategory("Under Review - AuthMark Tests")]
        public void AuthMark_Color_ChangeBackgroundColor()
        {
            string imagePath2 = "";
            string imagePath1 = "";
            string originalHex = "";
            string templateName = "";

            if (qaDev.ToLower() == dev)
            {
                instance = 11128365;
                testName = "AuthMark_TextProperties_ChangeBackgroundColor_Dev";
            }
            else
            {
                if (qaDev.ToLower() == qa)
                {
                    instance = 11507440;
                    testName = "AuthMark_TextProperties_ChangeBackgroundColor_QA";
                }
                //    else
                //    {
                //        if (qaDev.ToLower() == beta)
                //        {
                //            instance = 7225653;
                //            testName = "AuthMark_TextProperties_ChangeBackgroundColor_Beta";
                //        }
                else
                {
                    instance = 11311913;
                   testName = "AuthMark_TextProperties_ChangeBackgroundColor_Prod";
                }
                //        }
                //    }
            }
            passFail = 1;  //reset before test
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "AuthMark-DO NOT USE";
            CreateLogFile();

            try
            {
                //setup
                CreateTemplateUnderTest(templateName);
                baseWindowHandle = driver.CurrentWindowHandle;

                //Test
                SetupAuthMark();

                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "AuthMark Background Color ", ScreenshotImageFormat.Png, 0);
                Thread.Sleep(3000);

                cc.ClickButton(driver, tst.authMark_BackgroundColor);
                originalHex = cc.FetchIDefaultValue(driver, tst.authMark_Background_Hex);
                tglog.AppendLogText(filePath, string.Concat("Background R Value = ", cc.FetchIDefaultValue(driver, tst.authMark_Background_R)));
                tglog.AppendLogText(filePath, string.Concat("Background G Value = ", cc.FetchIDefaultValue(driver, tst.authMark_Background_G)));
                tglog.AppendLogText(filePath, string.Concat("Background B Value = ", cc.FetchIDefaultValue(driver, tst.authMark_Background_B)));
                tglog.AppendLogText(filePath, string.Concat("Background A Value = ", cc.FetchIDefaultValue(driver, tst.authMark_Background_A)));
                tglog.AppendLogText(filePath, string.Concat("Background Hex Value = ", originalHex));

                cc.EnterText(driver, "31", tst.authMark_Background_R);
                Thread.Sleep(1000);
                cc.EnterText(driver, "219", tst.authMark_Background_G);
                Thread.Sleep(1000);
                cc.EnterText(driver, "219", tst.authMark_Background_B);
                Thread.Sleep(1000);
                cc.EnterText(driver, "90", tst.authMark_Background_A);
                Thread.Sleep(1000);

                tglog.AppendLogText(filePath, string.Concat("Background R Value = ", cc.FetchIDefaultValue(driver, tst.authMark_Background_R)));
                tglog.AppendLogText(filePath, string.Concat("Background G Value = ", cc.FetchIDefaultValue(driver, tst.authMark_Background_G)));
                tglog.AppendLogText(filePath, string.Concat("Background B Value = ", cc.FetchIDefaultValue(driver, tst.authMark_Background_B)));
                tglog.AppendLogText(filePath, string.Concat("Background A Value = ", cc.FetchIDefaultValue(driver, tst.authMark_Background_A)));
                tglog.AppendLogText(filePath, string.Concat("Background Hex Value = ", cc.FetchIDefaultValue(driver, tst.authMark_Background_Hex)));

                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "AuthMark Background Color ", ScreenshotImageFormat.Png, 1);
                Thread.Sleep(5000);

                StringAssert.Contains(false.ToString(), (originalHex == cc.FetchIDefaultValue(driver, tst.authMark_Background_Hex)).ToString());
                tglog.AppendLogText(filePath, string.Concat("Original Hex, ", originalHex, " does not equal ", cc.FetchIDefaultValue(driver, tst.authMark_Background_Hex)));

                cc.ClickButton(driver, tst.authMark_BackgroundColor2);
                cc.ScrollUP(driver);

                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template ..... OK");
                Thread.Sleep(5000);

                cc.ClickButton(driver, tst.templateHomeBtn);
                tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                Thread.Sleep(3000);
                //cc.ScrollPage(driver);

                //Tear Down
                RemoveTemplateUnderTest(templateName);

                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));

                passFail = 0;
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Tear Down
                cc.ClickButton(driver, tst.templateHomeBtn);
                tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                Thread.Sleep(5000);
                cc.ScrollPage(driver);
                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        [TestMethod, TestCategory("Under Review - AuthMark Tests")]
        public void AuthMark_Color_ChangeForegroundColor()
        {
            string imagePath2 = "";
            string imagePath1 = "";
            string originalHex = "";
            string templateName = "";

            if (qaDev.ToLower() == dev)
            {
                instance = 11128364;
                testName = "AuthMark_TextProperties_ChangeForegroundColor_Dev";
            }
            else
            {
                if (qaDev.ToLower() == qa)
                {
                    instance = 11507439;
                    testName = "AuthMark_TextProperties_ChangeForegroundColor_QA";
                }
                //    else
                //    {
                //        if (qaDev.ToLower() == beta)
                //        {
                //            instance = 7225653;
                //            testName = "AuthMark_TextProperties_ChangeForegroundColor_Beta";
                //        }
                else
                {
                    instance = 11311912;
                    testName = "AuthMark_TextProperties_ChangeForegroundColor_Prod";
                }
                //        }
                //    }
            }
            passFail = 1;  //reset before test
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "AuthMark-DO NOT USE";
            CreateLogFile();

            try
            {
                //setup
                CreateTemplateUnderTest(templateName);
                baseWindowHandle = driver.CurrentWindowHandle;

                //Test
                SetupAuthMark();

                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "AuthMark Foreground Color ", ScreenshotImageFormat.Png, 0);
                Thread.Sleep(3000);

                cc.ClickButton(driver, tst.authMark_ForegroundColor);
                originalHex = cc.FetchIDefaultValue(driver, tst.authMark_Foreground_Hex);
                tglog.AppendLogText(filePath, string.Concat("Foreground R Value = ", cc.FetchIDefaultValue(driver, tst.authMark_Foreground_R)));
                tglog.AppendLogText(filePath, string.Concat("Foreground G Value = ", cc.FetchIDefaultValue(driver, tst.authMark_Foreground_G)));
                tglog.AppendLogText(filePath, string.Concat("Foreground B Value = ", cc.FetchIDefaultValue(driver, tst.authMark_Foreground_B)));
                tglog.AppendLogText(filePath, string.Concat("Foreground A Value = ", cc.FetchIDefaultValue(driver, tst.authMark_Foreground_A)));
                tglog.AppendLogText(filePath, string.Concat("Foreground Hex Value = ", originalHex));

                cc.EnterText(driver, "31", tst.authMark_Foreground_R);
                Thread.Sleep(1000);
                cc.EnterText(driver, "219", tst.authMark_Foreground_G);
                Thread.Sleep(1000);
                cc.EnterText(driver, "219", tst.authMark_Foreground_B);
                Thread.Sleep(1000);
                cc.EnterText(driver, "90", tst.authMark_Foreground_A);
                Thread.Sleep(1000);

                tglog.AppendLogText(filePath, string.Concat("Foreground R Value = ", cc.FetchIDefaultValue(driver, tst.authMark_Foreground_R)));
                tglog.AppendLogText(filePath, string.Concat("Foreground G Value = ", cc.FetchIDefaultValue(driver, tst.authMark_Foreground_G)));
                tglog.AppendLogText(filePath, string.Concat("Foreground B Value = ", cc.FetchIDefaultValue(driver, tst.authMark_Foreground_B)));
                tglog.AppendLogText(filePath, string.Concat("Foreground A Value = ", cc.FetchIDefaultValue(driver, tst.authMark_Foreground_A)));
                tglog.AppendLogText(filePath, string.Concat("Background Hex Value = ", cc.FetchIDefaultValue(driver, tst.authMark_Foreground_Hex)));

                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "AuthMark Foreground Color ", ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);

                StringAssert.Contains(false.ToString(), (originalHex == cc.FetchIDefaultValue(driver, tst.authMark_Foreground_Hex)).ToString());
                tglog.AppendLogText(filePath, string.Concat("Original Hex, ", originalHex, " does not equal ", cc.FetchIDefaultValue(driver, tst.authMark_Foreground_Hex)));

                cc.ClickButton(driver, tst.authMark_ForegroundColor2);
                //cc.ScrollUP(driver);

                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template ..... OK");
                Thread.Sleep(5000);

                cc.ClickButton(driver, tst.templateHomeBtn);
                tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                Thread.Sleep(3000);
                //cc.ScrollPage(driver);

                //Tear Down
                RemoveTemplateUnderTest(templateName);

                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));

                passFail = 0;
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Tear Down
                cc.ClickButton(driver, tst.templateHomeBtn);
                tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                Thread.Sleep(5000);
                cc.ScrollPage(driver);
                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        [TestMethod, TestCategory("Under Review - AuthMark Tests")]
        public void AuthMark_TextProperties_ChangeTextFont()
        {
            string templateName = "";
            string imagePath2 = "";
            string imagePath1 = "";

            if (qaDev.ToLower() == dev)
            {
                instance = 11128362;
                testName = "AuthMark_TextProperties_ChangeTextFont_Dev";
            }
            else
            {
                if (qaDev.ToLower() == qa)
                {
                    instance = 11507437;
                    testName = "AuthMark_TextProperties_ChangeTextFont_QA";
                }
                //    else
                //    {
                //        if (qaDev.ToLower() == beta)
                //        {
                //            instance = 7225653;
                //            testName = "AuthMark_TextProperties_ChangeTextFont_Beta";
                //        }
                else
                {
                    instance = 11311910;
                    testName = "AuthMark_TextProperties_ChangeTextFont_Prod";
                }
                //        }
                //    }
            }
            passFail = 1;  //reset before test
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "AuthMark-DO NOT USE";
            CreateLogFile();

            try
            {
                //setup
                CreateTemplateUnderTest(templateName);
                baseWindowHandle = driver.CurrentWindowHandle;

                //Test
                SetupAuthMark();

                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "AuthMark Text Font ", ScreenshotImageFormat.Png, 0);
                Thread.Sleep(3000);

                tglog.AppendLogText(filePath, string.Concat("Text Font Selected = ", cc.FetchSelectedOption(driver, tst.authMarkFont)));

                cc.SelectDropDownByElement(driver, tst.authMarkFont, "Courier");
                tglog.AppendLogText(filePath, string.Concat("Text Font Selected = ", cc.FetchChecked(driver, tst.authMarkShowAuthCode)));

                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "AuthMark Text Font ", ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);

                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template ..... OK");
                Thread.Sleep(3000);

                cc.ClickButton(driver, tst.templateHomeBtn);
                tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                Thread.Sleep(3000);
                cc.ScrollPage(driver);

                //Tear Down
                RemoveTemplateUnderTest(templateName);

                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                passFail = 0;
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Tear Down
                cc.ClickButton(driver, tst.templateHomeBtn);
                tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                Thread.Sleep(5000);
                cc.ScrollPage(driver);
                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        [TestMethod, TestCategory("Under Review - AuthMark Tests")]
        public void AuthMark_TextProperties_ChangeTextFontSize()
        {
            string templateName = "";
            string imagePath2 = "";
            string imagePath1 = "";

            if (qaDev.ToLower() == dev)
            {
                instance = 11128363;
                testName = "AuthMark_TextProperties_ChangeTextFontSize_Dev";
            }
            else
            {
                if (qaDev.ToLower() == qa)
                {
                    instance = 11507438;
                    testName = "AuthMark_TextProperties_ChangeTextFontSize_QA";
                }
                //    else
                //    {
                //        if (qaDev.ToLower() == beta)
                //        {
                //            instance = 7225653;
                //            testName = "AuthMark_TextProperties_ChangeTextFontSize_Beta";
                //        }
                else
                {
                    instance = 11311911;
                    testName = "AuthMark_TextProperties_ChangeTextFontSize_Prod";
                }
                //        }
                //    }
            }
            passFail = 1;  //reset before test
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "AuthMark-DO NOT USE";
            CreateLogFile();

            try
            {
                //setup
                CreateTemplateUnderTest(templateName);
                baseWindowHandle = driver.CurrentWindowHandle;

                //Test
                SetupAuthMark();

                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "AuthMark Text Font Size ", ScreenshotImageFormat.Png, 0);
                Thread.Sleep(3000);

                tglog.AppendLogText(filePath, string.Concat("Text Font Size = ", cc.FetchIDefaultValue(driver, tst.authMarkFontSize)));

                cc.EnterText(driver,"12", tst.authMarkFontSize);
                tglog.AppendLogText(filePath, string.Concat("Text Font Size = ", cc.FetchIDefaultValue(driver, tst.authMarkFontSize)));

                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "AuthMark Text Font Size ", ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);

                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template ..... OK");
                Thread.Sleep(3000);

                cc.ClickButton(driver, tst.templateHomeBtn);
                tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                Thread.Sleep(3000);
                cc.ScrollPage(driver);

                //Tear Down
                RemoveTemplateUnderTest(templateName);

                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                passFail = 0;
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Tear Down
                cc.ClickButton(driver, tst.templateHomeBtn);
                tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                Thread.Sleep(5000);
                cc.ScrollPage(driver);
                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        [TestMethod, TestCategory("Under Review - AuthMark Tests")]
        public void AuthMark_TextProperties_ShowAuthCode()
        {
            string templateName = "";
            string imagePath2 = "";
            string imagePath1 = "";

            if (qaDev.ToLower() == dev)
            {
                instance = 11128361;
                testName = "AuthMark_TextProperties_ShowAuthCode_Dev";
            }
            else
            {
                if (qaDev.ToLower() == qa)
                {
                    instance = 11507436;
                    testName = "AuthMark_TextProperties_ShowAuthCode_QA";
                }
                else
                {
                    //        if (qaDev.ToLower() == beta)
                    //        {
                    //            instance = 7225653;
                    //            testName = "AuthMark_TextProperties_ShowAuthCode_Beta";
                    //        }
                    //        else
                    //        {
                    instance = 11311909;
                    testName = "AuthMark_TextProperties_ShowAuthCode_Prod";
                }
                //        }
                //    }
            }
            passFail = 1;  //reset before test
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "AuthMark-DO NOT USE";
            CreateLogFile();

            try
            {
                //setup
                CreateTemplateUnderTest(templateName);
                baseWindowHandle = driver.CurrentWindowHandle;

                //Test
                SetupAuthMark();

                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "AuthMark Show AuthMark ", ScreenshotImageFormat.Png, 0);
                Thread.Sleep(3000);

                tglog.AppendLogText(filePath, string.Concat("AuthMark Checked = ", cc.FetchChecked(driver,tst.authMarkShowAuthCode)));

                cc.ClickButton(driver, tst.authMarkShowAuthCode);
                tglog.AppendLogText(filePath, string.Concat("AuthMark Checked = ", cc.FetchChecked(driver, tst.authMarkShowAuthCode)));

                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "AuthMark Show AuthMark ", ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);

                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template ..... OK");
                Thread.Sleep(3000);

                cc.ClickButton(driver, tst.templateHomeBtn);
                tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                Thread.Sleep(3000);
                cc.ScrollPage(driver);

                //Tear Down
                RemoveTemplateUnderTest(templateName);

                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                passFail = 0;
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Tear Down
                cc.ClickButton(driver, tst.templateHomeBtn);
                tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                Thread.Sleep(5000);
                cc.ScrollPage(driver);
                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        #region Methods
        private void CreateLogFile()
        {
            tglog.CreateLogFile(filePath, string.Concat(Environment.NewLine, "----------- Start testing using ", brow, " -----------", Environment.NewLine));
            tglog.AppendLogText(filePath, string.Concat("Set up ", brow, " ..... OK"));
            tglog.AppendLogText(filePath, string.Concat("URL = ", baseURL));
            tglog.AppendLogText(filePath, string.Concat("Envionment = ", qaDev));
            tglog.AppendLogText(filePath, string.Concat("User = ", user));
            tglog.AppendLogText(filePath, "Opened SecureDocs in browser ..... OK");
            tglog.AppendLogText(filePath, string.Concat("Build Version: ", build));
            tglog.AppendLogText(filePath, "Logged into SecureDocs ..... OK");
            tglog.AppendLogText(filePath, string.Concat("Instance = ", instance));
            tglog.AppendLogText(filePath, string.Concat("FilePath = ", filePath));
            tglog.AppendLogText(filePath, "Opened SecureDocs Dashboard ..... OK");
        }

        private void CreateTemplateUnderTest(string text)
        {
            OpenSecurityTemplates();
            cst = st.OpenCreateTemplate(driver);

            tglog.AppendLogText(filePath, "Opened New Security Template popup ..... OK");

            tst = cst.OpenTemplate(driver);
            Thread.Sleep(5000);

            tglog.AppendLogText(filePath, "Created Blank Template ..... OK");

            cc.SwitchToiFrame(driver); // switch to iFrame containing templates
            cc.EnterText(driver, text, tst.templateName);

            cc.ClickButton(driver, tst.saveButton);
            Thread.Sleep(5000);

        }

        private void OpenSecurityTemplates()
        {
            try
            {
                st = mp.OpenSecurityTemplates(driver);
                //tglog.AppendLogText(filePath, driver.Title);

                passFail = 0;
                tglog.AppendLogText(filePath, "Opened Security Templates ..... OK");

            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        private void RemoveTemplateUnderTest(string text)
        {
            string ele = "";
            string element = "";
            int itemCount = 0;
            string templateName = "";

            itemCount = Convert.ToInt16(cc.FetchChildCount(driver, st.templateGrid));

            for (int x = 1; x <= itemCount; x++)
            {
                element = string.Concat(st.templateGrid, "/div[", x.ToString(), "]/div[3]/a");
                ele = string.Concat(st.templateGrid, "/div[", x.ToString(), "]/div[1]/div/a[2]");
                tglog.AppendLogText(filePath, string.Concat("Name: ",element, " / remove: ", ele));
                templateName = cc.FetchInnerHTML(driver, element);

                tglog.AppendLogText(filePath, string.Concat("text = ", text, " / template name = ", templateName));
                if (templateName == text)
                {
                    rst = st.RemoveTemplate(driver, ele);

                    StringAssert.Contains(string.Concat("Delete Template '", templateName, "'"), cc.FetchInnerHTML(driver, rst.header));
                    tglog.AppendLogText(filePath, "Opened Remove Security Template..... OK");

                    st = rst.SecurityTemplateYES(driver, "Yes");
                    tglog.AppendLogText(filePath, string.Concat("Removed template", templateName));
                    break;
                }
            }
        }

        private void SendToPractitest()
        {
            if (qaDev.ToUpper() == "DEV")
            {
                ptc.PT_Post_Specific_Run(filePath, passFail, resultMsg, instance, project, brow, "Development");
            }
            else
            {
                if (qaDev.ToUpper() == "QA")
                {
                    ptc.PT_Post_Specific_Run(filePath, passFail, resultMsg, instance, project, brow, "QA");
                }
                else
                {
                    if (qaDev.ToUpper() == "BETA")
                    {
                        ptc.PT_Post_Specific_Run(filePath, passFail, resultMsg, instance, project, brow, "Beta");
                    }
                    else
                    {
                        ptc.PT_Post_Specific_Run(filePath, passFail, resultMsg, instance, project, brow, "Production");
                    }
                }
            }
        }

        private void SetupDriver()
        {
            if ((brow.ToLower() == "ie"))
            {
                this.driver = new InternetExplorerDriver("C:\\Selenium");
            }

            if ((brow.ToLower() == "chrome"))
            {
                this.driver = new ChromeDriver(@"C:\Selenium");
            }

            //if ((brow.ToLower() == "firefox"))
            //{
            //    this.driver = new FirefoxDriver();
            //}

        }

        private void SetupAuthMark()
        {
            cc.ClickButton(driver, tst.securityElements);
            tglog.AppendLogText(filePath, "Exposed Security Elements ..... OK");

            cc.ClickButton(driver, tst.authMarkImage);
            tglog.AppendLogText(filePath, "Added Authmark To Template ..... OK");
        }

        private void SetupCaptureText()
        {
            cc.ClickButton(driver, tst.captureElements);
            tglog.AppendLogText(filePath, "Exposed Security Elements ..... OK");

            cc.ClickButton(driver, tst.captureText);
            tglog.AppendLogText(filePath, "Added Data Capture To Template ..... OK");
        }

        private void SetUpEnvironVars()
        {
            if (qaDev.ToLower() == dev)
            {
                filePath = string.Concat(filePath, "Dev\\");
            }
            else
            {
                if (qaDev.ToLower() == qa)
                {
                    filePath = string.Concat(filePath, "QA\\");
                }
                else
                {
                    if (qaDev.ToLower() == beta)
                    {
                        filePath = string.Concat(filePath, "Beta\\");
                    }
                    else
                    {
                        filePath = string.Concat(filePath, "Prod\\");
                    }
                }
            }
        }
        #endregion

        //--------------------------------------------------------------------------------------------
        #region Additional test attributes
        [TestInitialize()]
        public void MyTestInitialize()
        {
            cc = new SecureRx.Common.Common_Code(driver);

            brow = ConfigurationManager.AppSettings["browser"];
            baseURL = ConfigurationManager.AppSettings["DocrityURL"];
            project = ConfigurationManager.AppSettings["project"];
            qaDev = ConfigurationManager.AppSettings["testEnv"];
            user = ConfigurationManager.AppSettings["testUser"].ToUpper();
            string pswd = ConfigurationManager.AppSettings["TestPswd"];

            SetupDriver();
            SetUpEnvironVars();

            lp = new SecureRx.Page_Elements.LoginPage(driver, baseURL);
            build = lp.FetchBuildVersion(driver);
            mp = lp.SDLogin(user, pswd, driver);

            //Did Home Menu open
            if (mp == null)
            {
                throw new NoSuchWindowException("Docrity home page was not displayed!");
            }
            else
            {
                passFail = 0;
            }
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {

            driver.SwitchTo().Window(baseWindowHandle);
            Thread.Sleep(1000);

            try
            {
                mp.LogoutandCloseBrowser(driver);
                tglog.AppendLogText(filePath, "Logged out and Closed browser..... OK");
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }

            if (passFail != 0)
            {
                resultMsg = string.Concat(testName, " ..... FAILED!!!!! Please refer to log file");
                //driver.Close();
            }
            else
            {
                resultMsg = string.Concat(testName, " ..... PASSED!!!!!");
            }

            SendToPractitest();

            tglog.AppendLogText(filePath, "Close Docrity");
            tglog.AppendLogText(filePath, "\n Test Finished \n");
        }

        #endregion

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;
    }
}
