﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Interactions.Internal;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;


namespace SecureRx.Tests
{
    [TestClass]
    public class Settings_Tests
    {
        #region libraries
        SecureRx.Page_Elements.MainPage mp = null;
        SecureRx.Page_Elements.LoginPage lp = null;

        SecureRx.Page_Elements.Activity.Activity_Detail actd = null;
        //SecureRx.Page_Elements.API.API_Page api = null;
        SecureRx.Page_Elements.Authentication.Authentication_Portal auth = null;
        SecureRx.Page_Elements.PrintAgents.Create_Print_Agent cspa = null;
        SecureRx.Page_Elements.PrintAgents.Print_Agent_Details dspa = null;
        SecureRx.Page_Elements.PrintAgents.Edit_Print_Agent espa = null;
        SecureRx.Page_Elements.PrintAgents.Print_Agents_Page spa = null;
        SecureRx.Page_Elements.Serialization.Serialization ser = null;
        SecureRx.Page_Elements.Settings.Settings stt = null;
        SecureRx.Page_Elements.Templates.SecurityTemplates st = null;
        SecureRx.Page_Elements.Templates.Show_Upload sst = null;

        SecureRx.Common.Common_Code cc = null;
        SecureRx.Common.PT_Client ptc = new Common.PT_Client();
        SecureRx.Common.TGLogging tglog = new Common.TGLogging();
        #endregion

        //Vars
        private string baseURL = "";
        private string baseWindowHandle = "";
        //private string beta = "beta";
        private string brow = "";
        private string build = "";
        private string dev = "dev";
        private IWebDriver driver = null;
        private string filename = null;
        private string filePath = "C:\\Test Logs\\SecureRx\\";
        private string imagePath = "";
        private int instance = 0;
        private int itemCnt = 0;
        //private string itemCount = "0";
        private string outputFilePath = "C:\\PDF Files\\";
        private int passFail = 1;
        private string printClient = "";
        private string printerName = "";
        //private string prod = "prod";
        private string project = "";
        private string qa = "qa";
        private string qaDev = "";
        private int rows = 0;
        private string resultMsg = "";
        private string staging = "stage";
        private string templateName = "";
        private string testName = "";
        public string today = DateTime.Now.ToString("d");
        private string user = "";

        //-------------------------------------------------------------------------------------------
        #region API
        [TestMethod, TestCategory("R1 - General")]
        public void R1_General_ExposeClientSecret()
        {
            string secret = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 11117492;
                testName = "Settings_APIKeys_ExposeClientSecret_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14575086;
                    testName = "Settings_APIKeys_ExposeClientSecret_Stage";
                }
                    else
                    {
                    instance = 14823101;
                    testName = "Settings_APIKeys_ExposeClientSecret_Prod";
                    }
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                stt = mp.OpenSettings(driver);
                cc.ClickButton(driver, stt.keysSettingsLink);
                tglog.AppendLogText(filePath, "Opened Keys Settings page");
                Thread.Sleep(3000);
                tglog.AppendLogText(filePath, string.Concat("Client Secret ID is ", cc.FetchInnerHTML(driver, stt.clientID)));

                //test
                cc.ClickButton(driver, stt.showSecret);
                Thread.Sleep(2000);
                secret = cc.FetchInnerHTML(driver, stt.clientSecret);
                tglog.AppendLogText(filePath, string.Concat("Client Secret Value is ", secret));
                StringAssert.Contains("True", (secret.Trim() != "").ToString());

                passFail = 0;
                Thread.Sleep(5000);

            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        #endregion

        #region Authentication Portal
        [TestMethod, TestCategory("R2 - Authentication Portal")]
        public void Settings_AuthenticationPortal_ChangeAccessibility()
        {
            string selected = "";
            string submit = "";

            if (qaDev.ToLower() == dev)
            {
                instance = 11250716;
                testName = "Settings_AuthenticationPortal_ChangeAccessibility_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14691495;
                    testName = "Settings_AuthenticationPortal_ChangeAccessibility_Stage";
                }
                //else
                //{
                //    if (qaDev.ToLower() == beta)
                //    {
                //        //instance = 4998768;
                //        testName = "Settings_AuthenticationPortal_ChangeAccessibility_Beta";
                //    }
                    else
                    {
                        instance = 11263425;
                        testName = "Settings_AuthenticationPortal_ChangeAccessibility_Prod";
                    }
                //}
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                //Setup
                auth = mp.OpenAuthPort(driver);
                submit = string.Concat(auth.saveBtn, "/div[2]/form/div[2]/button");

                StringAssert.Contains(cc.FetchInnerHTML(driver, auth.header), "Auth Portal settings applies to all AuthMarks.");
                tglog.AppendLogText(filePath, "Opened Authentication Portal page ..... OK");

                //SetupAuthPortalSettings();
                Thread.Sleep(2000);

                selected = cc.FetchSelectedOption(driver, auth.accessibility);
                tglog.AppendLogText(filePath, string.Concat("Original Selected Accessibility Value is ", selected));

                //test
                cc.SelectDropDownByElement(driver, auth.accessibility,"Private");
                cc.ClickButton(driver, submit);
                StringAssert.Contains("True", (selected != cc.FetchSelectedOption(driver, auth.accessibility)).ToString());

                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "New Accessibility, ", cc.FetchSelectedOption(driver, auth.accessibility), ", is not same as Orignal.....Passed", Environment.NewLine));

                tglog.AppendLogText(filePath, cc.FetchSelectedOption(driver, auth.accessibility));
                passFail = 0;

                // reset original
                submit = string.Concat(auth.saveBtn, "/div[3]/form/div[2]/button");
                Thread.Sleep(3000);
                cc.SelectDropDownByElement(driver, auth.accessibility,"Public");
                cc.ClickButton(driver, submit);

            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        //[TestMethod, TestCategory("Settings - Authentication Portal")]
        //public void Settings_AuthenticationPortal_ChangeActivity()
        //{
        //    string showActivity = "";
        //    string submit = "";

        //    if (qaDev.ToLower() == dev)
        //    {
        //        instance = 11117520;
        //        testName = "Settings_AuthenticationPortal_ChangeActivity_Dev";
        //    }
        //    else
        //    {
        //        if (qaDev.ToLower() == qa)
        //        {
        //            //instance = 4998766;
        //            testName = "Settings_AuthenticationPortal_ChangeActivity_QA";
        //        }
        //        else
        //        {
        //            if (qaDev.ToLower() == beta)
        //            {
        //                //instance = 4998768;
        //                testName = "Settings_AuthenticationPortal_ChangeActivity_Beta";
        //            }
        //            else
        //            {
        //                instance = 11263394;
        //                testName = "Settings_AuthenticationPortal_ChangeActivity_Prod";
        //            }
        //        }
        //    }
        //    passFail = 1;  //reset before test
        //    filePath = string.Concat(filePath, testName, ".txt");

        //    CreateLogFile();

        //    try
        //    {
        //        //Setup
        //        auth = mp.OpenAuthPort(driver);
        //        submit = string.Concat(auth.saveBtn, "/div[2]/form/div[2]/button");
        //        //tglog.AppendLogText(filePath, driver.Title);

        //        StringAssert.Contains(cc.FetchInnerHTML(driver, auth.header), "Auth Portal settings applies to all AuthMarks.");
        //        tglog.AppendLogText(filePath, "Opened Authentication Portal page ..... OK");

        //        SetupAuthPortalSettings();
        //        Thread.Sleep(2000);
        //        tglog.AppendLogText(filePath, "Entered Company Name, Phone and Email ..... OK");

        //        cc.ClickButton(driver, auth.displayPage);
        //        tglog.AppendLogText(filePath, "Clicked Display Tab ..... OK");
        //        Thread.Sleep(3000);
        //        showActivity = cc.FetchChecked(driver, auth.showActivity).ToString();
        //        tglog.AppendLogText(filePath, string.Concat("Original Show Activity setting is ", showActivity));

        //        //test
        //        cc.ClickButton(driver, auth.showActivity);
        //        cc.ClickButton(driver, submit);
        //        Thread.Sleep(3000);
        //        StringAssert.Contains("True", (showActivity!= cc.FetchChecked(driver, auth.showActivity).ToString()).ToString());
        //        tglog.AppendLogText(filePath, string.Concat("Clicked Show Activity", Environment.NewLine));

        //        Thread.Sleep(5000);
        //        passFail = 0;

        //        // reset original
        //        cc.ClickButton(driver, auth.displayPage);
        //        tglog.AppendLogText(filePath, "Clicked Display Tab ..... OK");
        //        Thread.Sleep(3000);

        //        submit = string.Concat(auth.saveBtn, "/div[3]/form/div[2]/button");
        //        cc.ClickButton(driver, auth.showActivity);
        //        cc.ClickButton(driver, submit);
        //        Thread.Sleep(3000);
        //        StringAssert.Contains("True", (showActivity == cc.FetchChecked(driver, auth.showActivity).ToString()).ToString());
        //        tglog.AppendLogText(filePath, string.Concat("Reset Show Activity Setting", Environment.NewLine));
        //        Thread.Sleep(5000);

        //    }
        //    catch (Exception e)
        //    {
        //        passFail = 1;
        //        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
        //    }
        //}



        //[TestMethod, TestCategory("Settings - Authentication Portal")]
        //public void Settings_AuthenticationPortal_ChangeLogo()
        //{
        //    string message = "";
        //    string submit = "";

        //    if (qaDev.ToLower() == dev)
        //    {
        //        //instance = 10033816;
        //        testName = "Settings_AuthenticationPortal_ChangeLogo_Dev";
        //    }
        //    else
        //    {
        //        if (qaDev.ToLower() == qa)
        //        {
        //            //instance = 4998766;
        //            testName = "Settings_AuthenticationPortal_ChangeLogo_QA";
        //        }
        //        else
        //        {
        //            if (qaDev.ToLower() == beta)
        //            {
        //                //instance = 4998768;
        //                testName = "Settings_AuthenticationPortal_ChangeLogo_Beta";
        //            }
        //            else
        //            {
        //                //if (qaDev.ToLower() == prod)
        //                //{
        //                //instance = 4998776;
        //                testName = "Settings_AuthenticationPortal_ChangeLogo_Prod";
        //                //}
        //            }
        //        }
        //    }
        //    passFail = 1;  //reset before test
        //    filePath = string.Concat(filePath, testName, ".txt");

        //    CreateLogFile();

        //    try
        //    {
        //        //Setup
        //        auth = mp.OpenAuthPort(driver);
        //        submit = string.Concat(auth.submit, "/div[2]/form/div[3]/button");
        //        //tglog.AppendLogText(filePath, driver.Title);

        //        StringAssert.Contains(cc.FetchInnerHTML(driver, auth.header), "Authentication Portal");
        //        tglog.AppendLogText(filePath, "Opened Authentication Portal page ..... OK");

        //        //message = cc.FetchPlaceholder(driver, auth.message);
        //        //tglog.AppendLogText(filePath, string.Concat("Original Message is ", message));

        //        ////test
        //        //cc.EnterText(driver, "This is a test", auth.message);
        //        //cc.ClickButton(driver, submit);
        //        //StringAssert.Contains("True", (message != cc.FetchTextValue(driver, auth.message)).ToString());

        //        //tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "New Message, ", cc.FetchPlaceholder(driver, auth.message), ", is not same as Orignal,", message, ".....Passed", Environment.NewLine));

        //        ////StringAssert.Contains("Message Saved", cc.FetchPlaceholder(driver, auth.message));
        //        //tglog.AppendLogText(filePath, cc.FetchPlaceholder(driver, auth.message));
        //        //passFail = 0;

        //        //// reset original
        //        //submit = string.Concat(auth.submit, "/div[3]/form/div[3]/button");
        //        //Thread.Sleep(3000);
        //        //cc.EnterText(driver, "", auth.message);
        //        //cc.ClickButton(driver, submit);
        //        ////StringAssert.Contains("True", (message == cc.FetchPlaceholder(driver, auth.message)).ToString());
        //        ////tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Original Message, ", message, ", has been reset", Environment.NewLine));

        //    }
        //    catch (Exception e)
        //    {
        //        passFail = 1;
        //        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
        //    }
        //}

        //[TestMethod, TestCategory("Settings - Authentication Portal")]
        //public void Settings_AuthenticationPortal_ChangeCompanyEmail()
        //{
        //    string email = "";
        //    string submit = "";

        //    if (qaDev.ToLower() == dev)
        //    {
        //        instance = 11250719;
        //        testName = "Settings_AuthenticationPortal_ChangeCompanyEmail_Dev";
        //    }
        //    else
        //    {
        //        if (qaDev.ToLower() == qa)
        //        {
        //            //instance = 4998766;
        //            testName = "Settings_AuthenticationPortal_ChangeCompanyEmail_QA";
        //        }
        //        else
        //        {
        //            if (qaDev.ToLower() == beta)
        //            {
        //                //instance = 4998768;
        //                testName = "Settings_AuthenticationPortal_ChangeCompanyEmail_Beta";
        //            }
        //            else
        //            {
        //                instance = 11263428;
        //                testName = "Settings_AuthenticationPortal_ChangeCompanyEmail_Prod";
        //            }
        //        }
        //    }
        //    passFail = 1;  //reset before test
        //    filePath = string.Concat(filePath, testName, ".txt");

        //    CreateLogFile();

        //    try
        //    {
        //        //Setup
        //        auth = mp.OpenAuthPort(driver);
        //        submit = string.Concat(auth.saveBtn, "/div[2]/form/div[2]/button");
        //        //tglog.AppendLogText(filePath, driver.Title);

        //        StringAssert.Contains(cc.FetchInnerHTML(driver, auth.header), "Auth Portal settings applies to all AuthMarks.");
        //        tglog.AppendLogText(filePath, "Opened Authentication Portal page ..... OK");

        //        email = cc.FetchTextValue(driver, auth.companyEmail);
        //        tglog.AppendLogText(filePath, string.Concat("Original Company Email is ", email));

        //        //test
        //        cc.EnterText(driver, "Test Company Name@TROYGroup.com", auth.companyEmail);
        //        cc.ClickButton(driver, submit);
        //        StringAssert.Contains("True", (email != cc.FetchTextValue(driver, auth.companyEmail)).ToString());

        //        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "New Company Email, ", cc.FetchTextValue(driver, auth.companyEmail), ", is not same as Orignal,", email, ".....Passed", Environment.NewLine));

        //        tglog.AppendLogText(filePath, cc.FetchTextValue(driver, auth.companyEmail));
        //        passFail = 0;

        //        // reset original
        //        submit = string.Concat(auth.saveBtn, "/div[3]/form/div[2]/button");
        //        Thread.Sleep(3000);
        //        cc.EnterText(driver, email, auth.companyEmail);
        //        cc.ClickButton(driver, submit);

        //    }
        //    catch (Exception e)
        //    {
        //        passFail = 1;
        //        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
        //    }
        //}

        //[TestMethod, TestCategory("Settings - Authentication Portal")]
        //public void Settings_AuthenticationPortal_ChangeCompanyName()
        //{
        //    string name = "";
        //    string submit = "";

        //    if (qaDev.ToLower() == dev)
        //    {
        //        instance = 11250717;
        //        testName = "Settings_AuthenticationPortal_ChangeCompanyName_Dev";
        //    }
        //    else
        //    {
        //        if (qaDev.ToLower() == qa)
        //        {
        //            //instance = 4998766;
        //            testName = "Settings_AuthenticationPortal_ChangeCompanyName_QA";
        //        }
        //        else
        //        {
        //            if (qaDev.ToLower() == beta)
        //            {
        //                //instance = 4998768;
        //                testName = "Settings_AuthenticationPortal_ChangeCompanyName_Beta";
        //            }
        //            else
        //            {
        //                instance = 11263426;
        //                testName = "Settings_AuthenticationPortal_ChangeCompanyName_Prod";
        //            }
        //        }
        //    }
        //    passFail = 1;  //reset before test
        //    filePath = string.Concat(filePath, testName, ".txt");

        //    CreateLogFile();

        //    try
        //    {
        //        //Setup
        //        auth = mp.OpenAuthPort(driver);
        //        submit = string.Concat(auth.saveBtn, "/div[2]/form/div[2]/button");
        //        //tglog.AppendLogText(filePath, driver.Title);

        //        StringAssert.Contains(cc.FetchInnerHTML(driver, auth.header), "Auth Portal settings applies to all AuthMarks.");
        //        tglog.AppendLogText(filePath, "Opened Authentication Portal page ..... OK");

        //        name = cc.FetchTextValue(driver, auth.companyName);
        //        tglog.AppendLogText(filePath, string.Concat("Original Company Name is ", name));

        //        //test
        //        cc.EnterText(driver, "Test Company Name", auth.companyName);
        //        cc.ClickButton(driver, submit);
        //        StringAssert.Contains("True", (name != cc.FetchTextValue(driver, auth.companyName)).ToString());

        //        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "New Company Name, ", cc.FetchTextValue(driver, auth.companyName), ", is not same as Orignal,", name, ".....Passed", Environment.NewLine));

        //        tglog.AppendLogText(filePath, cc.FetchTextValue(driver, auth.companyName));
        //        passFail = 0;

        //        // reset original
        //        submit = string.Concat(auth.saveBtn, "/div[3]/form/div[2]/button");
        //        Thread.Sleep(3000);
        //        cc.EnterText(driver, name, auth.companyName);
        //        cc.ClickButton(driver, submit);

        //    }
        //    catch (Exception e)
        //    {
        //        passFail = 1;
        //        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
        //    }
        //}

        //[TestMethod, TestCategory("Settings - Authentication Portal")]
        //public void Settings_AuthenticationPortal_ChangeCompanyPhone()
        //{
        //    string phone = "";
        //    string submit = "";

        //    if (qaDev.ToLower() == dev)
        //    {
        //        instance = 11250718;
        //        testName = "Settings_AuthenticationPortal_ChangeCompanyPhone_Dev";
        //    }
        //    else
        //    {
        //        if (qaDev.ToLower() == qa)
        //        {
        //            //instance = 4998766;
        //            testName = "Settings_AuthenticationPortal_ChangeCompanyPhone_QA";
        //        }
        //        else
        //        {
        //            if (qaDev.ToLower() == beta)
        //            {
        //                //instance = 4998768;
        //                testName = "Settings_AuthenticationPortal_ChangeCompanyPhone_Beta";
        //            }
        //            else
        //            {
        //                instance = 11263427;
        //                testName = "Settings_AuthenticationPortal_ChangeCompanyPhone_Prod";
        //            }
        //        }
        //    }
        //    passFail = 1;  //reset before test
        //    filePath = string.Concat(filePath, testName, ".txt");

        //    CreateLogFile();

        //    try
        //    {
        //        //Setup
        //        auth = mp.OpenAuthPort(driver);
        //        submit = string.Concat(auth.saveBtn, "/div[2]/form/div[2]/button");
        //        //tglog.AppendLogText(filePath, driver.Title);

        //        StringAssert.Contains(cc.FetchInnerHTML(driver, auth.header), "Auth Portal settings applies to all AuthMarks.");
        //        tglog.AppendLogText(filePath, "Opened Authentication Portal page ..... OK");

        //        phone = cc.FetchTextValue(driver, auth.companyName);
        //        tglog.AppendLogText(filePath, string.Concat("Original Company Phone is ", phone));

        //        //test
        //        cc.EnterText(driver, "(724) 864-1234", auth.companyPhone);
        //        cc.ClickButton(driver, submit);
        //        StringAssert.Contains("True", (phone != cc.FetchTextValue(driver, auth.companyPhone)).ToString());

        //        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "New Company Phone, ", cc.FetchTextValue(driver, auth.companyPhone), ", is not same as Orignal,", phone, ".....Passed", Environment.NewLine));

        //        tglog.AppendLogText(filePath, cc.FetchTextValue(driver, auth.companyPhone));
        //        passFail = 0;

        //        // reset original
        //        submit = string.Concat(auth.saveBtn, "/div[3]/form/div[2]/button");
        //        Thread.Sleep(3000);
        //        cc.EnterText(driver, phone, auth.companyPhone);
        //        cc.ClickButton(driver, submit);

        //    }
        //    catch (Exception e)
        //    {
        //        passFail = 1;
        //        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
        //    }
        //}

        [TestMethod, TestCategory("R2 - Authentication Portal")]
        public void Settings_AuthenticationPortal_ChangeShowData()
        {
            string showCapturedData = "";
            string submit = "";

            if (qaDev.ToLower() == dev)
            {
                instance = 11117521;
                testName = "Settings_AuthenticationPortal_ChangeShowData_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14692172;
                    testName = "Settings_AuthenticationPortal_ChangeShowData_Stage";
                }
                //else
                //{
                //    if (qaDev.ToLower() == beta)
                //    {
                //        //instance = 4998768;
                //        testName = "Settings_AuthenticationPortal_ChangeCaptureData_Beta";
                //    }
                    else
                    {
                        instance = 11263395;
                        testName = "Settings_AuthenticationPortal_ChangeShowData_Prod";
                    }
                //}
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                //Setup
                auth = mp.OpenAuthPort(driver);
                submit = string.Concat(auth.saveBtn, "/div[2]/form/div[2]/button");
                //tglog.AppendLogText(filePath, driver.Title);

                StringAssert.Contains(cc.FetchInnerHTML(driver, auth.header), "Auth Portal settings applies to all AuthMarks.");
                tglog.AppendLogText(filePath, "Opened Authentication Portal page ..... OK");

                Thread.Sleep(500);

                cc.ClickButton(driver, auth.displayPage);
                tglog.AppendLogText(filePath, "Clicked Display Tab ..... OK");
                Thread.Sleep(500);
                showCapturedData = cc.FetchChecked(driver, auth.showData).ToString();
                tglog.AppendLogText(filePath, string.Concat("Original Show Captured Data setting is ", showCapturedData));

                //test
                cc.ClickButton(driver, auth.showData);
                cc.ClickButton(driver, submit);
                Thread.Sleep(2000);

                passFail = 0;

                // reset original
                cc.ClickButton(driver, auth.displayPage);
                tglog.AppendLogText(filePath, "Clicked Display Tab ..... OK");
                Thread.Sleep(500);
                StringAssert.Contains("True", (showCapturedData != cc.FetchChecked(driver, auth.showData).ToString()).ToString());
                tglog.AppendLogText(filePath, string.Concat("Clicked Show Captured Data", Environment.NewLine));

                submit = string.Concat(auth.saveBtn, "/div[3]/form/div[2]/button");
                cc.ClickButton(driver, auth.showData);
                cc.ClickButton(driver, submit);

                Thread.Sleep(2000);
                StringAssert.Contains("True", (showCapturedData == cc.FetchChecked(driver, auth.showData).ToString()).ToString());
                tglog.AppendLogText(filePath, string.Concat("Reset Show Captured Data Setting", Environment.NewLine));

            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        [TestMethod, TestCategory("Under Review - Authentication Portal")]
        public void Settings_AuthenticationPortal_ChangeFailedMessage()
        {
            string message = "";
            string submit = "";

            if (qaDev.ToLower() == dev)
            {
                instance = 11250471;
                testName = "Settings_AuthenticationPortal_ChangeFailedMessage_Dev";
            }
            else
            {
                if (qaDev.ToLower() == qa)
                {
                    instance = 11507414;
                    testName = "Settings_AuthenticationPortal_ChangeFailedMessage_QA";
                }
                //else
                //{
                //    if (qaDev.ToLower() == beta)
                //    {
                //        //instance = 4998768;
                //        testName = "Settings_AuthenticationPortal_ChangeFailedMessage_Beta";
                //    }
                    else
                    {
                        instance = 11263423;
                        testName = "Settings_AuthenticationPortal_ChangeFailedMessage_Prod";
                    }
                //}
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                //Setup
                auth = mp.OpenAuthPort(driver);
                submit = string.Concat(auth.saveBtn, "/div[2]/form/div[2]/button");
                //tglog.AppendLogText(filePath, driver.Title);

                StringAssert.Contains(cc.FetchInnerHTML(driver, auth.header), "Auth Portal settings applies to all AuthMarks.");
                tglog.AppendLogText(filePath, "Opened Authentication Portal page ..... OK");

                //SetupAuthPortalSettings();
                Thread.Sleep(2000);
                //tglog.AppendLogText(filePath, "Entered Company Name, Phone and Email ..... OK");

                cc.ClickButton(driver, auth.customMsgTab);
                tglog.AppendLogText(filePath, "Clicked Custom Messages Tab ..... OK");
                Thread.Sleep(3000);

                message = cc.FetchTextValue(driver, auth.failedMessage);
                tglog.AppendLogText(filePath, string.Concat("Original Failed Message is ", message));

                //test
                cc.EnterText(driver, "This is a failed test", auth.failedMessage);
                cc.ClickButton(driver, submit);
                StringAssert.Contains("True", (message != cc.FetchTextValue(driver, auth.failedMessage)).ToString());

                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "New Message, ", cc.FetchTextValue(driver, auth.failedMessage), ", is not same as Orignal,", message, ".....Passed", Environment.NewLine));

                //StringAssert.Contains("Message Saved", cc.FetchPlaceholder(driver, auth.message));
                tglog.AppendLogText(filePath, cc.FetchTextValue(driver, auth.failedMessage));
                passFail = 0;

                // reset original
                cc.ClickButton(driver, auth.customMsgTab);
                tglog.AppendLogText(filePath, "Clicked Custom Messages Tab ..... OK");
                Thread.Sleep(3000);

                submit = string.Concat(auth.saveBtn, "/div[3]/form/div[2]/button");
                Thread.Sleep(3000);
                cc.EnterText(driver, "", auth.failedMessage);
                cc.ClickButton(driver, submit);

            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        [TestMethod, TestCategory("Under Review - Authentication Portal")]
        public void Settings_AuthenticationPortal_ChangeShowDocument()
        {
            string showDoc = "";
            string submit = "";

            if (qaDev.ToLower() == dev)
            {
                instance = 11117518;
                testName = "Settings_AuthenticationPortal_ChangeShowDocument_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14692171;
                    testName = "Settings_AuthenticationPortal_ChangeShowDocument_Stage";
                }
                //else
                //{
                //    if (qaDev.ToLower() == beta)
                //    {
                //        //instance = 4998768;
                //        testName = "Settings_AuthenticationPortal_ChangeShowDocument_Beta";
                //    }
                    else
                    {
                        instance = 11263392;
                        testName = "Settings_AuthenticationPortal_ChangeShowDocument_Prod";
                    }
                //}
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                auth = mp.OpenAuthPort(driver);
                submit = string.Concat(auth.saveBtn, "/div[2]/form/div[2]/button");

                StringAssert.Contains(cc.FetchInnerHTML(driver, auth.header), "Auth Portal settings applies to all AuthMarks.");
                tglog.AppendLogText(filePath, "Opened Authentication Portal page ..... OK");

                //SetupAuthPortalSettings();
                Thread.Sleep(2000);
                tglog.AppendLogText(filePath, "Entered Company Name, Phone and Email ..... OK");

                cc.ClickButton(driver, auth.displayPage);
                tglog.AppendLogText(filePath, "Clicked Display Tab ..... OK");
                Thread.Sleep(3000);
                showDoc = cc.FetchChecked(driver, auth.showDocument).ToString();
                tglog.AppendLogText(filePath, string.Concat("Original Show Document setting is ", showDoc));

                //test
                cc.ClickButton(driver, auth.showDocument);
                cc.ClickButton(driver, submit);
                Thread.Sleep(3000);
                Thread.Sleep(5000);
                passFail = 0;

                // reset original
                cc.ClickButton(driver, auth.displayPage);
                tglog.AppendLogText(filePath, "Clicked Display Tab ..... OK");
                Thread.Sleep(3000);
                //StringAssert.Contains("True", (showDoc != cc.FetchChecked(driver, auth.showDocument).ToString()).ToString());
                tglog.AppendLogText(filePath, string.Concat("Clicked Show Document", Environment.NewLine));

                submit = string.Concat(auth.saveBtn, "/div[3]/form/div[2]/button");
                cc.ClickButton(driver, auth.showDocument);
                cc.ClickButton(driver, submit);
                //cc.ClickButton(driver, auth.saveBtn);
                Thread.Sleep(3000);
                //StringAssert.Contains("True", (showDoc == cc.FetchChecked(driver, auth.showDocument).ToString()).ToString());
                tglog.AppendLogText(filePath, string.Concat("Reset Show Document Setting", Environment.NewLine));
                Thread.Sleep(5000);

            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        [TestMethod, TestCategory("Under Review - Authentication Portal")]
        public void Settings_AuthenticationPortal_ChangeShowUserInfo()
        {
            string showUserInfo = "";
            string submit = "";

            if (qaDev.ToLower() == dev)
            {
                instance = 11117519;
                testName = "Settings_AuthenticationPortal_ChangeShowUserInfo_Dev";
            }
            else
            {
                if (qaDev.ToLower() == qa)
                {
                    instance = 11507385;
                    testName = "Settings_AuthenticationPortal_ChangeShowUserInfo_QA";
                }
                //else
                //{
                //    if (qaDev.ToLower() == beta)
                //    {
                //        //instance = 4998768;
                //        testName = "Settings_AuthenticationPortal_ChangeShowUserInfo_Beta";
                //    }
                    else
                    {
                        instance = 11263393;
                        testName = "Settings_AuthenticationPortal_ChangeShowUserInfo_Prod";
                    }
                //}
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                //Setup
                auth = mp.OpenAuthPort(driver);
                submit = string.Concat(auth.saveBtn, "/div[2]/form/div[2]/button");
                //tglog.AppendLogText(filePath, driver.Title);

                StringAssert.Contains(cc.FetchInnerHTML(driver, auth.header), "Auth Portal settings applies to all AuthMarks.");
                tglog.AppendLogText(filePath, "Opened Authentication Portal page ..... OK");

                //SetupAuthPortalSettings();
                Thread.Sleep(2000);
                tglog.AppendLogText(filePath, "Entered Company Name, Phone and Email ..... OK");

                cc.ClickButton(driver, auth.displayPage);
                tglog.AppendLogText(filePath, "Clicked Display Tab ..... OK");
                Thread.Sleep(3000);

                showUserInfo = cc.FetchChecked(driver, auth.showIssuerInfo).ToString();
                tglog.AppendLogText(filePath, string.Concat("Original Show User Information setting is ", showUserInfo));

                //test
                cc.ClickButton(driver, auth.showIssuerInfo);
                cc.ClickButton(driver, submit);
                Thread.Sleep(3000);
                //StringAssert.Contains("True", (showUserInfo != cc.FetchChecked(driver, auth.showIssuerInfo).ToString()).ToString());
                tglog.AppendLogText(filePath, string.Concat("Clicked Show User Information", Environment.NewLine));

                Thread.Sleep(5000);
                passFail = 0;

                // reset original
                cc.ClickButton(driver, auth.displayPage);
                tglog.AppendLogText(filePath, "Clicked Display Tab ..... OK");
                Thread.Sleep(3000);
                submit = string.Concat(auth.saveBtn, "/div[3]/form/div[2]/button");
                cc.ClickButton(driver, auth.showIssuerInfo);
                cc.ClickButton(driver, submit);
                Thread.Sleep(3000);
                //StringAssert.Contains("True", (showUserInfo == cc.FetchChecked(driver, auth.showIssuerInfo).ToString()).ToString());
                tglog.AppendLogText(filePath, string.Concat("Reset Show User Information Setting", Environment.NewLine));
                Thread.Sleep(5000);

            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        [TestMethod, TestCategory("Under Review - Authentication Portal")]
        public void Settings_AuthenticationPortal_ChangeSuccessMessage()
        {
            string message = "";
            string submit = "";

            if (qaDev.ToLower() == dev)
            {
                instance = 11117523;
                testName = "Settings_AuthenticationPortal_ChangeMessage_Dev";
            }
            else
            {
                if (qaDev.ToLower() == qa)
                {
                    instance = 11507387;
                    testName = "Settings_AuthenticationPortal_ChangeMessage_QA";
                }
                //else
                //{
                //    if (qaDev.ToLower() == beta)
                //    {
                //        //instance = 4998768;
                //        testName = "Settings_AuthenticationPortal_ChangeMessage_Beta";
                //    }
                else
                {
                        instance = 11263396;
                        testName = "Settings_AuthenticationPortal_ChangeMessage_Prod";
                    }
                //}
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                //Setup
                auth = mp.OpenAuthPort(driver);
                submit = string.Concat(auth.saveBtn, "/div[2]/form/div[2]/button");
                //tglog.AppendLogText(filePath, driver.Title);

                StringAssert.Contains(cc.FetchInnerHTML(driver, auth.header), "Auth Portal settings applies to all AuthMarks.");
                tglog.AppendLogText(filePath, "Opened Authentication Portal page ..... OK");

                //SetupAuthPortalSettings();
                Thread.Sleep(2000);
                //tglog.AppendLogText(filePath, "Entered Company Name, Phone and Email ..... OK");

                cc.ClickButton(driver, auth.customMsgTab);
                tglog.AppendLogText(filePath, "Clicked Custom Messages Tab ..... OK");
                Thread.Sleep(3000);

                message = cc.FetchTextValue(driver, auth.successMess);
                tglog.AppendLogText(filePath, string.Concat("Original Success Message is ", message));

                //test
                cc.EnterText(driver, "This is a test", auth.successMess);
                cc.ClickButton(driver, submit);
                StringAssert.Contains("True", (message != cc.FetchTextValue(driver, auth.successMess)).ToString());

                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "New Message, ", cc.FetchTextValue(driver, auth.successMess), ", is not same as Orignal,", message, ".....Passed", Environment.NewLine));

                //StringAssert.Contains("Message Saved", cc.FetchPlaceholder(driver, auth.message));
                tglog.AppendLogText(filePath, cc.FetchTextValue(driver, auth.successMess));
                passFail = 0;

                // reset original
                //SetupAuthPortalSettings();
                Thread.Sleep(2000);
                //tglog.AppendLogText(filePath, "Entered Company Name, Phone and Email ..... OK");

                cc.ClickButton(driver, auth.customMsgTab);
                tglog.AppendLogText(filePath, "Clicked Custom Messages Tab ..... OK");
                Thread.Sleep(3000);

                submit = string.Concat(auth.saveBtn, "/div[3]/form/div[2]/button");
                Thread.Sleep(3000);
                cc.EnterText(driver, "", auth.successMess);
                cc.ClickButton(driver, submit);

            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        #endregion

        #region General
        [TestMethod, TestCategory("R1 - General")]
        public void R1_General_ChangeCaptureDocument()
        {
            string imagePath2 = "";
            string imagePath1 = "";
            string documentCaptureChecked = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 11117378;
                testName = "Settings_General_ChangeCaptureDocument_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14591673;
                    filename = "Hygieia_Amoxicillin_Prescription_input_Secured.pdf";
                    templateName = "HYGIEIA Prescription - Kitchen Sink";
                    testName = "Settings_General_ChangeCaptureDocument_Stage";
                }
                else
                {
                    instance = 14823107;
                    testName = "Settings_General_ChangeCaptureDocument_Prod";
                    templateName = "HYGIEIA Prescription - Kitchen Sink";
                    filename = "Hygieia_Amoxicillin_Prescription_input_Secured.pdf";
                }
            }
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                stt = mp.OpenSettings(driver);
                tglog.AppendLogText(filePath, "Opened Settings page");

                cc.ClickButton(driver, stt.printAgentSettingsLink);
                tglog.AppendLogText(filePath, "Opened Print Agent Settings page");

                documentCaptureChecked = cc.FetchChecked(driver, stt.document_DocumentCapture).ToString();
                tglog.AppendLogText(filePath, string.Concat("Original Capture Document setting is ", documentCaptureChecked));
                Thread.Sleep(500);

                //test
                cc.ClickButton(driver, stt.document_DocumentCapture2);
                //Thread.Sleep(2000);
                cc.ClickButton(driver, stt.document_submit);
                Thread.Sleep(500);
                //StringAssert.Contains("True", (documentCaptureChecked != cc.FetchChecked(driver, stt.document_DocumentCapture).ToString()).ToString());
                tglog.AppendLogText(filePath, string.Concat("Clicked Capture Document", Environment.NewLine));
                //Thread.Sleep(2000);

                imagePath1 = UploadFile(templateName, filename, imagePath, "Compare Document Image  ");

                Thread.Sleep(3000);
                mp.OpenSettings(driver);
                tglog.AppendLogText(filePath, "Opened Settings page");

                // reset original
                cc.ClickButton(driver, stt.document_DocumentCapture2);
                cc.ClickButton(driver, stt.document_submit);
                //Thread.Sleep(3000);
                StringAssert.Contains("True", (documentCaptureChecked == cc.FetchChecked(driver, stt.document_DocumentCapture).ToString()).ToString());
                tglog.AppendLogText(filePath, string.Concat("Reset Capture Document", Environment.NewLine));
                //Thread.Sleep(2000);

                imagePath2 = UploadFile(templateName, filename, imagePath, "Compare Document Image 2 ");
                Thread.Sleep(3000);

                passFail = 1;
                tglog.AppendLogText(filePath, "Test is complete. Please visually verify the differences betweeen the two documents in the screenshots before PASSING this test");

            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        [TestMethod, TestCategory("R2 - General")]
        public void Settings_General_EditLocale()
        {
            string locale = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 11117376;
                testName = "Settings_General_EditLocale_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14591769;
                    testName = "Settings_General_EditLocale_Stage";
                }
                    else
                    {
                        //instance = 11263385;
                        testName = "Settings_General_EditLocale_Prod";
                    }
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                stt = mp.OpenSettings(driver);
                cc.ClickButton(driver, stt.siteSettingsLink);
                tglog.AppendLogText(filePath, "Opened Site Settings page");

                //Thread.Sleep(3000);
                locale = cc.FetchSelectedOption(driver, stt.locale);
                tglog.AppendLogText(filePath, string.Concat("Original Locale Value is ", locale));

                //test
                cc.SelectDropDownByElement(driver, stt.locale, "Dutch");
                cc.ClickButton(driver, stt.siteSave);
                StringAssert.Contains("True", (locale != cc.FetchSelectedOption(driver, stt.locale)).ToString());
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "New Locale, ", cc.FetchSelectedOption(driver, stt.locale), ", is not same as Orignal,", locale, ".....Passed", Environment.NewLine));

                StringAssert.Contains("Settings Saved", cc.FetchInnerHTML(driver, stt.settingsSaved));
                tglog.AppendLogText(filePath, cc.FetchInnerHTML(driver, stt.settingsSaved));
                tglog.AppendLogText(filePath, "Returned to Settings page");
                Thread.Sleep(500);

                cc.ClickButton(driver, stt.siteSettingsLink);
                tglog.AppendLogText(filePath, "Opened Site Settings page");
                Thread.Sleep(500);

                // reset original
                cc.SelectDropDownByElement(driver, stt.locale, locale);
                cc.ClickButton(driver, stt.siteSave2);
                passFail = 0;
                Thread.Sleep(1000);
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        [TestMethod, TestCategory("R2 - General")]
        public void Settings_General_EditTimeZone()
        {
            string tz = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 11250946;
                testName = "Settings_General_EditTimeZone_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14591770;
                    testName = "Settings_General_EditTimeZone_Stage";
                }
                    else
                    {
                        //instance = 11263429;
                        testName = "Settings_General_EditTimeZone_Prod";
                    }
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                stt = mp.OpenSettings(driver);
                cc.ClickButton(driver, stt.siteSettingsLink);
                tglog.AppendLogText(filePath, "Opened Site Settings page");

                //Thread.Sleep(3000);
                tz = cc.FetchSelectedOption(driver, stt.timeZone);
                tglog.AppendLogText(filePath, string.Concat("Original Timezone Value is ", tz));

                //test
                cc.SelectDropDownByElement(driver, stt.timeZone, "(UTC-10:00) Hawaii");
                cc.ClickButton(driver, stt.siteSave);
                StringAssert.Contains("True", (tz != cc.FetchSelectedOption(driver, stt.timeZone)).ToString());
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "New Time Zone, ", cc.FetchSelectedOption(driver, stt.timeZone), ", is not same as Orignal,", tz, ".....Passed", Environment.NewLine));

                StringAssert.Contains("Settings Saved", cc.FetchInnerHTML(driver, stt.settingsSaved));
                tglog.AppendLogText(filePath, cc.FetchInnerHTML(driver, stt.settingsSaved));
                Thread.Sleep(500);
                tglog.AppendLogText(filePath, "Returned to Settings page");

                cc.ClickButton(driver, stt.siteSettingsLink);
                tglog.AppendLogText(filePath, "Opened Site Settings page");
                Thread.Sleep(500);

                // reset original
                cc.SelectDropDownByElement(driver, stt.timeZone, tz);
                cc.ClickButton(driver, stt.siteSave2);
                passFail = 0;
                Thread.Sleep(1000);

            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        //[TestMethod, TestCategory("Settings - General")]
        //public void Settings_General_EnableBlockchain()
        //{
        //    string blockchainChecked = "";
        //    //string submit = "";

        //    if (qaDev.ToLower() == dev)
        //    {
        //        //instance = 9887044;
        //        testName = "Settings_General_EnableBlockchain_Dev";
        //    }
        //    else
        //    {
        //        if (qaDev.ToLower() == staging)
        //        {
        //            //instance = 11415249;
        //            testName = "Settings_General_EnableBlockchain_Stage";
        //        }
        //        else
        //        {
        //            if (qaDev.ToLower() == beta)
        //            {
        //                //instance = 4998768;
        //                testName = "Settings_General_EnableBlockchain_Beta";
        //            }
        //            else
        //            {
        //                //instance = 10163196;
        //                testName = "Settings_General_EnableBlockchain_Prod";
        //                //}
        //            }
        //        }
        //    }
        //    passFail = 1;  //reset before test
        //    filePath = string.Concat(filePath, testName, ".txt");

        //    CreateLogFile();

        //    try
        //    {
        //        stt = mp.OpenSettings(driver);
        //        //submit = string.Concat(stt.submit, "/div[2]/form/div[3]/button");
        //        //tglog.AppendLogText(filePath, driver.Title);

        //        tglog.AppendLogText(filePath, "Opened Company Settings page ..... OK");

        //        blockchainChecked = cc.FetchChecked(driver, stt.enableBlockchain).ToString();
        //        tglog.AppendLogText(filePath, string.Concat("Original Blockchain setting is ", blockchainChecked));

        //        //test
        //        cc.ClickButton(driver, stt.enableBlockchain2);
        //        cc.ClickButton(driver, stt.document_submit);
        //        Thread.Sleep(3000);
        //        StringAssert.Contains("True", (blockchainChecked != cc.FetchChecked(driver, stt.enableBlockchain).ToString()).ToString());
        //        tglog.AppendLogText(filePath, string.Concat("Clicked Save Enable Blockchain", Environment.NewLine));

        //        Thread.Sleep(5000);
        //        passFail = 0;

        //        // reset original
        //        //submit = string.Concat(stt.submit, "/div[2]/form/div[3]/button");
        //        cc.ClickButton(driver, stt.enableBlockchain3);
        //        cc.ClickButton(driver, stt.document_submit);
        //        Thread.Sleep(3000);
        //        StringAssert.Contains("True", (blockchainChecked == cc.FetchChecked(driver, stt.enableBlockchain).ToString()).ToString());
        //        tglog.AppendLogText(filePath, string.Concat("Reset Save Enable Blockchain", Environment.NewLine));
        //        Thread.Sleep(5000);

        //    }
        //    catch (Exception e)
        //    {
        //        passFail = 1;
        //        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
        //    }
        //}

        [TestMethod, TestCategory("R1 - General")]
        public void R1_General_SaveTextCaptureFields()
        {
            string imagePath2 = "";
            string imagePath1 = "";
            string SaveTextCaptureChecked = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 11117378;
                testName = "Settings_General_SaveTextCaptureFields_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14591672;
                    filename = "Hygieia_Amoxicillin_Prescription_input_Secured.pdf";
                    testName = "Settings_General_SaveTextCaptureFields_Stage";
                    templateName = "HYGIEIA Prescription - Kitchen Sink";
                }
                else
                {
                    instance = 14823106;
                    testName = "Settings_General_SaveTextCaptureFields_Prod";
                    filename = "Hygieia_Amoxicillin_Prescription_input_Secured.pdf";
                    templateName = "HYGIEIA Prescription - Kitchen Sink";
                }
            }
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                stt = mp.OpenSettings(driver);
                tglog.AppendLogText(filePath, "Opened Settings page");

                SaveTextCaptureChecked = cc.FetchChecked(driver, stt.saveTextCaptureData).ToString();
                tglog.AppendLogText(filePath, string.Concat("Original Save Text Capture Fields setting is ", SaveTextCaptureChecked));

                //test
                cc.ClickButton(driver, stt.document_saveTextCaptureData2);
                //Thread.Sleep(1000);
                cc.ClickButton(driver,stt.document_submit);
                Thread.Sleep(500);
                StringAssert.Contains("True", (SaveTextCaptureChecked != cc.FetchChecked(driver, stt.saveTextCaptureData).ToString()).ToString());
                tglog.AppendLogText(filePath, string.Concat("Clicked Save Text Capture Fields", Environment.NewLine));

                //Thread.Sleep(2000);

                imagePath1 = UploadFile(templateName, filename, imagePath, "Compare Document Details  ");
                Thread.Sleep(3000);
                mp.OpenSettings(driver);
                tglog.AppendLogText(filePath, "Opened Settings page");

                // reset original
                cc.ClickButton(driver, stt.document_saveTextCaptureData2);
                cc.ClickButton(driver, stt.document_submit);
                Thread.Sleep(500);
                StringAssert.Contains("True", (SaveTextCaptureChecked == cc.FetchChecked(driver, stt.saveTextCaptureData).ToString()).ToString());
                tglog.AppendLogText(filePath, string.Concat("Reset Save Text Capture Fields", Environment.NewLine));
                Thread.Sleep(3000);

                imagePath2 = UploadFile(templateName, filename, imagePath, "Compare Document Details  ");
                Thread.Sleep(3000);
                mp.OpenSettings(driver);
                tglog.AppendLogText(filePath, "Opened Settings page");
                Thread.Sleep(1000);

                passFail = 1;
                tglog.AppendLogText(filePath, "Test is complete. Please visually verify the differences betweeen the two documents in the screenshots before PASSING this test");

            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        [TestMethod, TestCategory("R1 - General")]
        public void R1_General_SendAuditLogsToCloud()
        {
            string auditLogsChecked = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 11117377;
                testName = "Settings_General_SendAuditLogsToCloud_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14591674;
                    testName = "Settings_General_SendAuditLogsToCloud_Stage";
                }
                    else
                    {
                    instance = 14823108;
                    testName = "Settings_General_SendAuditLogsToCloud_Prod";
                    }
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                stt = mp.OpenSettings(driver);
                tglog.AppendLogText(filePath, "Opened Settings page");

                cc.ClickButton(driver, stt.printAgentSettingsLink);
                tglog.AppendLogText(filePath, "Opened Print Agent Settings page");

                Thread.Sleep(500);
                auditLogsChecked = cc.FetchChecked(driver, stt.auditLog).ToString();
                tglog.AppendLogText(filePath, string.Concat("Original Send All Audit Logs To Cloud setting is ", auditLogsChecked));

                //test
                cc.ClickButton(driver, stt.auditLog2);
                cc.ClickButton(driver, stt.printAgentSettingsSaved);
                Thread.Sleep(500);
                tglog.AppendLogText(filePath, "Returned to Settings page");

                cc.ClickButton(driver, stt.printAgentSettingsLink);
                tglog.AppendLogText(filePath, "Opened Print Agent Settings page");
                Thread.Sleep(500);

                StringAssert.Contains("True", (auditLogsChecked != cc.FetchChecked(driver, stt.auditLog).ToString()).ToString());
                tglog.AppendLogText(filePath, string.Concat("Clicked Send All Audit Logs To Cloud", Environment.NewLine));

                // reset original
                cc.ClickButton(driver, stt.printAgentSettingsLink);
                tglog.AppendLogText(filePath, "Opened Print Agent Settings page");
                Thread.Sleep(500);

                cc.ClickButton(driver, stt.auditLog2);
                cc.ClickButton(driver, stt.printAgentSettingsSaved2);
                Thread.Sleep(500);

                StringAssert.Contains("True", (auditLogsChecked == cc.FetchChecked(driver, stt.auditLog).ToString()).ToString());
                tglog.AppendLogText(filePath, string.Concat("Reset Send All Audit Logs To Cloud", Environment.NewLine));
                Thread.Sleep(1000);
                passFail = 0;

            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        [TestMethod, TestCategory("R2 - General")]
        public void Settings_General_SessionTimeout()
        {
            string timeoutSetting = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 11117379;
                testName = "Settings_General_SessionTimeout_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14591771;
                    testName = "Settings_General_SessionTimeout_Stage";
                }
                    else
                    {
                        instance = 11263388;
                        testName = "Settings_General_SessionTimeout_Prod";
                    }
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                stt = mp.OpenSettings(driver);
                cc.ClickButton(driver, stt.siteSettingsLink);
                tglog.AppendLogText(filePath, "Opened Site Settings page");

                Thread.Sleep(1000);
                timeoutSetting = cc.FetchIDefaultValue(driver, stt.timeoutSession);
                tglog.AppendLogText(filePath, string.Concat("Original Timeout Value is ", timeoutSetting));

                //test
                cc.EnterText(driver, "20", stt.timeoutSession);
                cc.ClickButton(driver, stt.siteSave);
                tglog.AppendLogText(filePath, "Returned to Settings page");

                Thread.Sleep(500);
                cc.ClickButton(driver, stt.siteSettingsLink);
                tglog.AppendLogText(filePath, "Opened Site Settings page");
                Thread.Sleep(500);

                StringAssert.Contains("True", (timeoutSetting != cc.FetchTextValue(driver, stt.timeoutSession)).ToString());
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "New Time Out Session, ", cc.FetchTextValue(driver, stt.timeoutSession), ", is not same as Orignal,", timeoutSetting, ".....Passed", Environment.NewLine));

                // reset original
                cc.EnterText(driver, timeoutSetting, stt.timeoutSession);
                cc.ClickButton(driver, stt.siteSave2);
                passFail = 0;
                Thread.Sleep(1000);

            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        [TestMethod, TestCategory("R2 - General")]
        public void Settings_General_SyncInterval()
        {
            string interval = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 11117380;
                testName = "Settings_General_SyncInterval_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14591775;
                    testName = "Settings_General_SyncInterval_Stage";
                }
                    else
                    {
                        //instance = 11263389;
                        testName = "Settings_General_SyncInterval_Prod";
                    }
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                stt = mp.OpenSettings(driver);
                tglog.AppendLogText(filePath, "Opened Settings page");

                cc.ClickButton(driver, stt.printAgentSettingsLink);
                tglog.AppendLogText(filePath, "Opened Print Agent Settings page");

                Thread.Sleep(500);
                interval = cc.FetchTextValue(driver, stt.syncInterval);
                tglog.AppendLogText(filePath, string.Concat("Original Sync Interval Value is ", interval));

                //test
                cc.EnterText(driver, "45", stt.syncInterval);
                cc.ClickButton(driver, stt.printAgentSettingsSaved);
                Thread.Sleep(500);
                tglog.AppendLogText(filePath, "Returned to Settings page");

                cc.ClickButton(driver, stt.printAgentSettingsLink);
                tglog.AppendLogText(filePath, "Opened Print Agent Settings page");
                Thread.Sleep(500);

                StringAssert.Contains("True", (interval != cc.FetchTextValue(driver, stt.syncInterval)).ToString());
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "New Sync Interval, ", cc.FetchTextValue(driver, stt.syncInterval), ", is not same as Orignal,", interval, ".....Passed", Environment.NewLine));

                // reset original
                cc.EnterText(driver, interval, stt.syncInterval);
                cc.ClickButton(driver, stt.printAgentSettingsSaved2);

                Thread.Sleep(500);
                StringAssert.Contains("True", (interval == cc.FetchTextValue(driver, stt.syncInterval)).ToString());
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Original Sync Intereval, ", interval, ", has been reset", Environment.NewLine));
                passFail = 0;
                Thread.Sleep(1000);

            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        #endregion

        #region Print Agent
        [TestMethod, TestCategory("R1 - Print Agent")]
        public void R1_PrintAgent_AddPrintAgent()
        {
            string found = "False";

            if (qaDev.ToLower() == dev)
            {
                //instance = 11128193;
                testName = "Settings_PrintAgent_AddPrintAgent_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14591778;
                    testName = "Settings_PrintAgent_AddPrintAgent_Stage";
                }
                    else
                    {
                    instance = 14823112;
                    testName = "Settings_PrintAgent_AddPrintAgent_Prod";
                    }
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                //setup
                OpenPrintAgents();
                StringAssert.Contains(cc.FetchInnerHTML(driver, spa.Header), "Print Agents");

                AddPrintAgent();
                rows = Convert.ToInt16(cc.FetchTableRows(driver, spa.agentTable2));

                //test
                // verify in list
                for (int x = 1; x <= rows; x++)
                {
                    if (cc.FetchCellValue(driver, "1", x.ToString(), spa.agentTable2).Contains(printerName))
                    {
                        tglog.AppendLogText(filePath, string.Concat("Print Client is ", printerName,"..... OK"));
                        found = "True";

                        if (cc.FetchCellTextValue(driver, "3", x.ToString(), spa.agentTable2).Contains(printClient)) 
                        {
                            tglog.AppendLogText(filePath, string.Concat("Print Client is ", printClient, "..... OK"));

                            if (cc.FetchCellValue(driver, "4", x.ToString(), spa.agentTable2).Contains(today))
                            {
                                tglog.AppendLogText(filePath, string.Concat("Update Date is ", today, "..... OK"));
                                break;
                            }
                        }
                    }
                }

                StringAssert.Contains(found.ToString(), "True");
                tglog.AppendLogText(filePath, string.Concat("Printer ", printerName, " was created"));

                //tear down
                RemovePrintAgent();
                if (cc.AcceptAssertOK(driver) == "")
                {
                    driver.SwitchTo().Window(baseWindowHandle);
                    tglog.AppendLogText(filePath, "Selected print agent was removed.....OK");
                    passFail = 0;
                }
                else
                {
                    passFail = 1;
                    tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Alert was not displayed!", Environment.NewLine));
                }
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        [TestMethod, TestCategory("R2 - Print Agent")]
        public void Settings_PrintAgent_CancelAdd()
        {

            if (qaDev.ToLower() == dev)
            {
                //instance = 11128194;
                testName = "Settings_PrintAgent_CancelAdd_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14591818;
                    testName = "Settings_PrintAgent_CancelAdd_Stage";
                }
                    else
                    {
                        //instance = 11263398;
                        testName = "Settings_PrintAgent_CancelAdd_Prod";
                    }
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();
            //tglog.AppendLogText(filePath, driver.Title);

            try
            {
                //setup
                OpenPrintAgents();
                rows = Convert.ToInt16(cc.FetchTableRows(driver, spa.agentTable));

                //test
                cspa = spa.CreatePrintAgent(driver);
                StringAssert.Contains(cc.FetchInnerHTML(driver, cspa.Header), "Create Print Agent");
                tglog.AppendLogText(filePath, "Opened Create Print Agent ..... OK");
                Thread.Sleep(500);

                printerName = "VPN Test";
                cc.EnterText(driver, printerName, cspa.VPN);
                cc.ScrollPage(driver);
                Thread.Sleep(500);
                printClient = cc.FetchTextValue(driver, cspa.printClient);
                cc.EnterText(driver, outputFilePath, cspa.xpsFile2);

                tglog.AppendLogText(filePath, string.Concat("Printer Name = ", printerName));
                tglog.AppendLogText(filePath, string.Concat("Print Client = ", printClient));
                tglog.AppendLogText(filePath, string.Concat("XPS File Path = ", outputFilePath));
                //Thread.Sleep(2000);

                cc.ClickButton(driver, cspa.cancelBtn);

                StringAssert.Contains(cc.FetchInnerHTML(driver, spa.Header), "Print Agents");
                passFail = 0;
                tglog.AppendLogText(filePath, "Opened Secure Print Agents ..... OK");

                // verify not in list
                for (int x = 1; x <= rows; x++)
                {
                    StringAssert.Contains((printerName != cc.FetchCellValue(driver, "1", x.ToString(), spa.agentTable)).ToString(), "True");
                }
                tglog.AppendLogText(filePath, string.Concat("Printer ", printerName, " was not created"));
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        [TestMethod, TestCategory("R2 - Print Agent")]
        public void Settings_PrintAgent_CancelDeletePrintAgent()
        {
            string found = "False";
            if (qaDev.ToLower() == dev)
            {
                //instance = 11128197;
                testName = "Settings_PrintAgent_CancelDeletePrintAgent_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14591819;
                    testName = "Settings_PrintAgent_CancelDeletePrintAgent_Stage";
                }
                    else
                    {
                        //instance = 11263401;
                        testName = "Settings_PrintAgent_CancelDeletePrintAgent_Prod";
                    }
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();
            baseWindowHandle = driver.CurrentWindowHandle;

            try
            {
                //setup & tear down
                OpenPrintAgents();
                StringAssert.Contains(cc.FetchInnerHTML(driver, spa.Header), "Print Agents");
                tglog.AppendLogText(filePath, "Opened Secure Print Agents ..... OK");

                AddPrintAgent();
                rows = Convert.ToInt16(cc.FetchTableRows(driver, spa.agentTable2));

                //test
                // verify in list
                for (int x = 1; x <= rows; x++)
                {
                    if (cc.FetchCellValue(driver, "1", x.ToString(), spa.agentTable2).Contains(printerName)) 
                    {
                        found = "True";
                        break;
                    }
                }

                StringAssert.Contains(found.ToString(), "True");
                tglog.AppendLogText(filePath, string.Concat("Printer ", printerName, " was created"));

                RemovePrintAgent();
                if (cc.CancelAssert(driver) == "")
                {
                    driver.SwitchTo().Window(baseWindowHandle);
                    tglog.AppendLogText(filePath, "Selected print agent was not removed.....OK");
                    passFail = 0;
                }
                else
                {
                    passFail = 1;
                    tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Alert was not displayed!", Environment.NewLine));
                }

                //tear down
                RemovePrintAgent();
                if (cc.AcceptAssertOK(driver) == "")
                {
                    driver.SwitchTo().Window(baseWindowHandle);
                    tglog.AppendLogText(filePath, "Selected print agent was removed.....OK");
                    passFail = 0;
                }
                else
                {
                    passFail = 1;
                    tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Alert was not displayed!", Environment.NewLine));
                }
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        [TestMethod, TestCategory("R2 - Print Agent")]
        public void Settings_PrintAgent_CancelEditPrintAgent()
        {

            if (qaDev.ToLower() == dev)
            {
                //instance = 11128195;
                testName = "Settings_PrintAgent_CancelEditPrintAgent_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14591820;
                    testName = "Settings_PrintAgent_CancelEditPrintAgent_Stage";
                }
                    else
                    {
                        //instance = 11263399;
                        testName = "Settings_PrintAgent_CancelEditPrintAgent_Prod";
                    }
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();
            //tglog.AppendLogText(filePath, driver.Title);

            try
            {
                //setup & tear down
                OpenPrintAgents();

                AddPrintAgent();
                rows = Convert.ToInt16(cc.FetchTableRows(driver, spa.agentTable2));

                //test
                for (int x = 1; x <= rows; x++)
                {
                    printerName = cc.FetchCellValue(driver, "1", x.ToString(), spa.agentTable2);
                    tglog.AppendLogText(filePath, string.Concat("Print Agent: ", printerName));
                    tglog.AppendLogText(filePath, string.Concat("Printer Output: ", cc.FetchCellValue(driver, "2", x.ToString(), spa.agentTable2)));
                    tglog.AppendLogText(filePath, string.Concat("Server Name: ", cc.FetchCellValue(driver, "3", x.ToString(), spa.agentTable2)));
                    tglog.AppendLogText(filePath, string.Concat("Updated: ", cc.FetchCellValue(driver, "4", x.ToString(), spa.agentTable2)));

                    if (cc.FetchCellValue(driver, "1", x.ToString(), spa.agentTable2).Contains("VPN Test"))
                    {
                        espa = spa.EditPrintAgent(driver, string.Concat(spa.agentTable2, "/tr[", x.ToString(), "]/td[5]/form/a[1]"));
                        StringAssert.Contains(cc.FetchInnerHTML(driver, espa.Header), "Edit Print Agent");
                        tglog.AppendLogText(filePath, "Opened Edit Print Agent ..... OK");
                        Thread.Sleep(500);
                        cc.EnterText(driver, "New Printer Name", espa.printerName);

                        cc.ScrollPage(driver);
                        cc.ClickButton(driver, espa.cancelBtn);

                        StringAssert.Contains(cc.FetchInnerHTML(driver,spa.Header), "Print Agents");
                        tglog.AppendLogText(filePath, "Opened Secure Print Agents ..... OK");
                        break;
                    }
                }

                // verify not in list
                for (int x = 1; x <= rows; x++)
                {
                    if (cc.FetchCellValue(driver, "1", x.ToString(), spa.agentTable).Contains("VPN Test"))
                    {
                        tglog.AppendLogText(filePath, string.Concat("Printer Name for ", printerName, " was not changed.....OK"));
                        passFail = 0;
                        break;
                    }
                }

                if (passFail == 1)
                {
                    tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Printer Name for ", printerName, " was changed.....FAILED!!!!!", Environment.NewLine));
                }

                //tear down
                RemovePrintAgent(rows);
                if (cc.AcceptAssertOK(driver) == "")
                {
                    driver.SwitchTo().Window(baseWindowHandle);
                    tglog.AppendLogText(filePath, "Selected print agent was removed.....OK");
                }
                else
                {
                    passFail = 1;
                    tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Alert was not displayed!", Environment.NewLine));
                }
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        [TestMethod, TestCategory("R2 - Print Agent")]
        public void Settings_PrintAgent_ChangeComputerName()
        {
            string computerName = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 11128198;
                testName = "Settings_PrintAgent_ChangeComputerName_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14591821;
                    testName = "Settings_PrintAgent_ChangeComputerName_Stage";
                }
                    else
                    {
                        //instance = 11263402;
                        testName = "Settings_PrintAgent_ChangeComputerName_Prod";
                    }
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();
            //tglog.AppendLogText(filePath, driver.Title);

            try
            {
                //setup & tear down
                OpenPrintAgents();

                AddPrintAgent();
                rows = Convert.ToInt16(cc.FetchTableRows(driver, spa.agentTable2));

                //test
                for (int x = 1; x <= rows; x++)
                {
                    printerName = cc.FetchCellValue(driver, "1", x.ToString(), spa.agentTable2);
                    computerName = cc.FetchCellValue(driver, "3", x.ToString(), spa.agentTable2); 
                    tglog.AppendLogText(filePath, string.Concat("Print Agent: ", printerName));
                    tglog.AppendLogText(filePath, string.Concat("Printer Output: ", cc.FetchCellValue(driver, "2", x.ToString(), spa.agentTable2)));
                    tglog.AppendLogText(filePath, string.Concat("Server Name: ", computerName));

                    if (cc.FetchCellValue(driver, "1", x.ToString(), spa.agentTable2).Contains("VPN Test"))
                    {
                        espa = spa.EditPrintAgent(driver, string.Concat(spa.agentTable2, "/tr[", x.ToString(), "]/td[5]/form/a[1]"));
                        StringAssert.Contains(cc.FetchInnerHTML(driver, espa.Header), "Edit Print Agent");
                        tglog.AppendLogText(filePath, "Opened Edit Print Agent ..... OK");
                        Thread.Sleep(500);
                        cc.SelectDropDownByOption(driver, espa.computerName,2);

                        cc.ScrollPage(driver);
                        cc.ClickButton(driver, espa.saveBtn);
                        Thread.Sleep(500);

                        tglog.AppendLogText(filePath, "Opened Secure Print Agents ..... OK");
                        break;
                    }
                }

                // verify not in list
                passFail = 0;
                for (int x = 1; x <= rows; x++)
                {
                    if (cc.FetchCellValue(driver, "1", x.ToString(), spa.agentTable2).Contains("VPN Test"))
                    {
                        if (cc.FetchCellValue(driver, "3", x.ToString(), spa.agentTable2).Contains(computerName))
                        {
                            tglog.AppendLogText(filePath, string.Concat("Computer Name was not changed.....Failed"));
                            passFail = 1;
                            break;
                        }
                    }
                }

                //tear down
                tglog.AppendLogText(filePath, "Computer Name was changed..... Passed");

                RemovePrintAgent();
                if (cc.AcceptAssertOK(driver) == "")
                {
                    driver.SwitchTo().Window(baseWindowHandle);
                    tglog.AppendLogText(filePath, "Selected print agent was removed.....OK");
                }
                else
                {
                    passFail = 1;
                    tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Alert was not displayed!", Environment.NewLine));
                }
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
                //tear down

                RemovePrintAgent();
                if (cc.AcceptAssertOK(driver) == "")
                {
                    driver.SwitchTo().Window(baseWindowHandle);
                    tglog.AppendLogText(filePath, "Selected print agent was removed.....OK");
                }
                else
                {
                    passFail = 1;
                    tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Alert was not displayed!", Environment.NewLine));
                }
            }
        }

        [TestMethod, TestCategory("R2 - Print Agent")]
        public void Settings_PrintAgent_ChangeFileNameFormat()
        {
            //string fileFormat = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 11128202;
                testName = "Settings_PrintAgent_ChangeFileNameFormat_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14591822;
                    testName = "Settings_PrintAgent_ChangeFileNameFormat_Stage";
                }
                    else
                    {
                        //instance = 11263406;
                        testName = "Settings_PrintAgent_ChangeFileNameFormat_Prod";
                    }
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                //setup & tear down
                OpenPrintAgents();

                AddPrintAgent();
                rows = Convert.ToInt16(cc.FetchTableRows(driver, spa.agentTable2));

                //test
                for (int x = 1; x <= rows; x++)
                {
                    printerName = cc.FetchCellValue(driver, "1", x.ToString(), spa.agentTable2);

                    if (cc.FetchCellValue(driver, "1", x.ToString(), spa.agentTable2).Contains("VPN Test"))
                    {
                        espa = spa.EditPrintAgent(driver, string.Concat(spa.agentTable2, "/tr[", x.ToString(), "]/td[5]/form/a[1]"));
                        StringAssert.Contains(cc.FetchInnerHTML(driver, espa.Header), "Edit Print Agent");
                        tglog.AppendLogText(filePath, "Opened Edit Print Agent ..... OK");
                        Thread.Sleep(500);

                        cc.EnterText(driver, "Rx - {PrintTime}", espa.fileNameFormat);

                        cc.ScrollPage(driver);
                        cc.ClickButton(driver, espa.saveBtn);
                        Thread.Sleep(500);
                        tglog.AppendLogText(filePath, "File Name Format has been changed.....PASSED!");

                        tglog.AppendLogText(filePath, "Opened Secure Print Agents ..... OK");
                        break;
                    }
                }

                //Reset Value
                for (int x = 1; x <= rows; x++)
                {
                    if (cc.FetchCellValue(driver, "1", x.ToString(), spa.agentTable2).Contains("VPN Test"))
                    {
                        espa = spa.EditPrintAgent(driver, string.Concat(spa.agentTable2, "/tr[", x.ToString(), "]/td[5]/form/a[1]"));
                        StringAssert.Contains(cc.FetchInnerHTML(driver, espa.Header), "Edit Print Agent");
                        tglog.AppendLogText(filePath, "Opened Edit Print Agent ..... OK");
                        Thread.Sleep(500);
                        cc.EnterText(driver, "{PrintTime}", espa.fileNameFormat);

                        cc.ScrollPage(driver);
                        cc.ClickButton(driver, espa.saveBtn);
                        tglog.AppendLogText(filePath, "File Name Format has been restored to original value.");

                        tglog.AppendLogText(filePath, "Opened Secure Print Agents ..... OK");
                        break;
                    }
                }

                //tear down
                RemovePrintAgent();
                if (cc.AcceptAssertOK(driver) == "")
                {
                    driver.SwitchTo().Window(baseWindowHandle);
                    tglog.AppendLogText(filePath, "Selected print agent was removed.....OK");
                }
                else
                {
                    passFail = 1;
                    tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Alert was not displayed!", Environment.NewLine));
                }
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        [TestMethod, TestCategory("R2 - Print Agent")]
        public void Settings_PrintAgent_ChangeFileNameFormatWithMetadata()
        {
            string fileFormat = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 11128203;
                testName = "Settings_PrintAgent_ChangeFileNameFormatWithMetadata_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14591823;
                    testName = "Settings_PrintAgent_ChangeFileNameFormatWithMetadata_Stage";
                }
                    else
                    {
                        //instance = 11263407;
                        testName = "Settings_PrintAgent_ChangeFileNameFormatWithMetadata_Prod";
                    }
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                //setup & tear down
                OpenPrintAgents();

                AddPrintAgent();
                rows = Convert.ToInt16(cc.FetchTableRows(driver, spa.agentTable2));

                //test
                for (int x = 1; x <= rows; x++)
                {
                    printerName = cc.FetchCellValue(driver, "1", x.ToString(), spa.agentTable2);

                    if (cc.FetchCellValue(driver, "1", x.ToString(), spa.agentTable2).Contains("VPN Test"))
                    {
                        espa = spa.EditPrintAgent(driver, string.Concat(spa.agentTable2, "/tr[", x.ToString(), "]/td[5]/form/a[1]"));
                        StringAssert.Contains(cc.FetchInnerHTML(driver, espa.Header), "Edit Print Agent");
                        tglog.AppendLogText(filePath, "Opened Edit Print Agent ..... OK");
                        Thread.Sleep(500);
                        fileFormat = cc.FetchPlaceholder(driver, espa.fileNameFormat);

                        cc.EnterText(driver, "Rx - {PrintTime}", espa.fileNameFormat);
                        cc.SelectDropDownByOption(driver, espa.addMetadata, 3);

                        cc.ScrollPage(driver);
                        cc.ClickButton(driver, espa.saveBtn);
                        Thread.Sleep(500);
                        tglog.AppendLogText(filePath, string.Concat("File Name Format, ",fileFormat," has been changed.....PASSED!"));

                        break;
                    }
                }

                //Reset Value
                for (int x = 1; x <= rows; x++)
                {
                    if (cc.FetchCellValue(driver, "1", x.ToString(), spa.agentTable2).Contains("VPN Test"))
                    {
                        espa = spa.EditPrintAgent(driver, string.Concat(spa.agentTable2, "/tr[", x.ToString(), "]/td[5]/form/a[1]"));
                        StringAssert.Contains(cc.FetchInnerHTML(driver, espa.Header), "Edit Print Agent");
                        tglog.AppendLogText(filePath, "Opened Edit Print Agent ..... OK");
                        Thread.Sleep(500);

                        cc.EnterText(driver, fileFormat, espa.fileNameFormat);

                        cc.ScrollPage(driver);
                        cc.ClickButton(driver, espa.saveBtn);
                        tglog.AppendLogText(filePath, string.Concat("File Name Format has been restored to ",fileFormat));

                        break;
                    }
                }

                //tear down
                RemovePrintAgent();
                if (cc.AcceptAssertOK(driver) == "")
                {
                    driver.SwitchTo().Window(baseWindowHandle);
                    tglog.AppendLogText(filePath, "Selected print agent was removed.....OK");
                }
                else
                {
                    passFail = 1;
                    tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Alert was not displayed!", Environment.NewLine));
                }
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        [TestMethod, TestCategory("R1 - Print Agent")]
        public void R1_PrintAgent_ChangeOutputPath()
        {

            if (qaDev.ToLower() == dev)
            {
                //instance = 11128200;
                testName = "Settings_PrintAgent_ChangeOutputPath_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14591780;
                    testName = "Settings_PrintAgent_ChangeOutputPath_Stage";
                }
                    else
                    {
                    instance = 14823114;
                    testName = "Settings_PrintAgent_ChangeOutputPath_Prod";
                    }
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                //setup & tear down
                OpenPrintAgents();

                AddPrintAgent();
                rows = Convert.ToInt16(cc.FetchTableRows(driver, spa.agentTable2));

                //test
                for (int x = 1; x <= rows; x++)
                {
                    printerName = cc.FetchCellValue(driver, "1", x.ToString(), spa.agentTable2);

                    if (cc.FetchCellValue(driver, "1", x.ToString(), spa.agentTable2).Contains("VPN Test"))
                    {
                        espa = spa.EditPrintAgent(driver, string.Concat(spa.agentTable2, "/tr[", x.ToString(), "]/td[5]/form/a[1]"));
                        StringAssert.Contains(cc.FetchInnerHTML(driver, espa.Header), "Edit Print Agent");
                        tglog.AppendLogText(filePath, "Opened Edit Print Agent ..... OK");
                        Thread.Sleep(500);

                        cc.ClickButton(driver, espa.fileOutputTab);
                        Thread.Sleep(1000);
                        cc.EnterText(driver, "C:\\Test Files\\", espa.xpsPath);

                        cc.ScrollPage(driver);
                        cc.ClickButton(driver, espa.saveBtn2);
                        tglog.AppendLogText(filePath, "Output File Path has been changed.....PASSED!");
                        tglog.AppendLogText(filePath, "Opened Secure Print Agents ..... OK");
                        break;
                    }
                }

                //Reset Value
                //for (int x = 1; x <= rows; x++)
                //{
                //    if (cc.FetchCellValue(driver, "1", x.ToString(), spa.agentTable2).Contains("VPN Test"))
                //    {
                //        espa = spa.EditPrintAgent(driver, string.Concat(spa.agentTable2, "/tr[", x.ToString(), "]/td[5]/form/a[1]"));
                //        StringAssert.Contains(cc.FetchInnerHTML(driver, espa.Header), "Edit Print Agent");
                //        tglog.AppendLogText(filePath, "Opened Edit Print Agent ..... OK");
                //        Thread.Sleep(3000);

                //        cc.EnterText(driver, outputFilePath, espa.xpsPath);

                //        cc.ScrollPage(driver);
                //        cc.ClickButton(driver, espa.saveBtn);
                //        tglog.AppendLogText(filePath, "Output File Path has been restored to original value.");

                //        tglog.AppendLogText(filePath, "Opened Secure Print Agents ..... OK");
                //        break;
                //    }
                //}

                //tear down
                RemovePrintAgent();
                if (cc.AcceptAssertOK(driver) == "")
                {
                    driver.SwitchTo().Window(baseWindowHandle);
                    tglog.AppendLogText(filePath, "Selected print agent was removed.....OK");
                }
                else
                {
                    passFail = 1;
                    tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Alert was not displayed!", Environment.NewLine));
                }
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        [TestMethod, TestCategory("R2 - Print Agent")]
        public void Settings_PrintAgent_ChangePrintAgentName()
        {

            if (qaDev.ToLower() == dev)
            {
                //instance = 11128199;
                testName = "Settings_PrintAgent_ChangePrintAgentName_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14591824;
                    testName = "Settings_PrintAgent_ChangePrintAgentName_Stage";
                }
                    else
                    {
                        //instance = 11263403;
                        testName = "Settings_PrintAgent_ChangePrintAgentName_Prod";
                    }
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                //setup & tear down
                OpenPrintAgents();

                AddPrintAgent();
                rows = Convert.ToInt16(cc.FetchTableRows(driver, spa.agentTable2));

                //test
                for (int x = 1; x <= rows; x++)
                {
                    printerName = cc.FetchCellValue(driver, "1", x.ToString(), spa.agentTable2);
                    tglog.AppendLogText(filePath, string.Concat("Print Agent: ", printerName));
                    tglog.AppendLogText(filePath, string.Concat("Printer Output: ", cc.FetchCellValue(driver, "2", x.ToString(), spa.agentTable2)));
                    tglog.AppendLogText(filePath, string.Concat("Server Name: ", cc.FetchCellValue(driver, "3", x.ToString(), spa.agentTable2)));

                    if (cc.FetchCellValue(driver, "1", x.ToString(), spa.agentTable2).Contains("VPN Test"))
                    {
                        espa = spa.EditPrintAgent(driver, string.Concat(spa.agentTable2, "/tr[", x.ToString(), "]/td[5]/form/a[1]"));
                        StringAssert.Contains(cc.FetchInnerHTML(driver, espa.Header), "Edit Print Agent");
                        tglog.AppendLogText(filePath, "Opened Edit Print Agent ..... OK");
                        Thread.Sleep(500);
                        cc.EnterText(driver, "New Printer Name", espa.printerName);

                        cc.ScrollPage(driver);
                        cc.ClickButton(driver, espa.saveBtn);
                        Thread.Sleep(500);

                        tglog.AppendLogText(filePath, "Opened Secure Print Agents ..... OK");
                        break;
                    }
                }

                // verify not in list
                passFail = 0;
                for (int x = 1; x <= rows; x++)
                {
                    if (cc.FetchCellValue(driver, "1", x.ToString(), spa.agentTable2).Contains("VPN Test"))
                    {
                        tglog.AppendLogText(filePath, string.Concat("Printer Name for ", printerName, " was not changed.....Failed"));
                        passFail = 1;
                        break;
                    }
                }

                //Reset Value
                for (int x = 1; x <= rows; x++)
                {
                    if (cc.FetchCellValue(driver, "1", x.ToString(), spa.agentTable2).Contains("New Printer Name"))
                    {
                        espa = spa.EditPrintAgent(driver, string.Concat(spa.agentTable2, "/tr[", x.ToString(), "]/td[5]/form/a[1]"));
                        StringAssert.Contains(cc.FetchInnerHTML(driver, espa.Header), "Edit Print Agent");
                        tglog.AppendLogText(filePath, "Opened Edit Print Agent ..... OK");
                        Thread.Sleep(500);
                        cc.EnterText(driver, "VPN Test", espa.printerName);

                        cc.ScrollPage(driver);
                        cc.ClickButton(driver, espa.saveBtn);

                        tglog.AppendLogText(filePath, "Opened Secure Print Agents ..... OK");
                        break;
                    }
                }

                //tear down
                RemovePrintAgent();
                if (cc.AcceptAssertOK(driver) == "")
                {
                    driver.SwitchTo().Window(baseWindowHandle);
                    tglog.AppendLogText(filePath, "Selected print agent was removed.....OK");
                }
                else
                {
                    passFail = 1;
                    tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Alert was not displayed!", Environment.NewLine));
                }
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //tear down
                RemovePrintAgent();
                if (cc.AcceptAssertOK(driver) == "")
                {
                    driver.SwitchTo().Window(baseWindowHandle);
                    tglog.AppendLogText(filePath, "Selected print agent was removed.....OK");
                }
                else
                {
                    passFail = 1;
                    tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Alert was not displayed!", Environment.NewLine));
                }
            }
        }

        [TestMethod, TestCategory("R2 - Print Agent")]
        public void Settings_PrintAgent_ChangePrinter()
        {
            string computerName = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 11128201;
                testName = "Settings_PrintAgent_ChangePrinter_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14591825;
                    testName = "Settings_PrintAgent_ChangePrinter_QA";
                }
                    else
                    {
                        //instance = 11263405;
                        testName = "Settings_PrintAgent_ChangePrinter_Prod";
                    }
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();
            //tglog.AppendLogText(filePath, driver.Title);

            try
            {
                //setup & tear down
                OpenPrintAgents();

                AddPrintAgent();
                rows = Convert.ToInt16(cc.FetchTableRows(driver, spa.agentTable2));

                //test
                for (int x = 1; x <= rows; x++)
                {
                    printerName = cc.FetchCellValue(driver, "1", x.ToString(), spa.agentTable2);
                    computerName = cc.FetchCellValue(driver, "3", x.ToString(), spa.agentTable2);
                    tglog.AppendLogText(filePath, string.Concat("Print Agent: ", printerName));
                    tglog.AppendLogText(filePath, string.Concat("Printer Output: ", cc.FetchCellValue(driver, "2", x.ToString(), spa.agentTable2)));
                    tglog.AppendLogText(filePath, string.Concat("Server Name: ", computerName));
                    tglog.AppendLogText(filePath, string.Concat("Updated: ", cc.FetchCellValue(driver, "4", x.ToString(), spa.agentTable2)));

                    if (cc.FetchCellValue(driver, "1", x.ToString(), spa.agentTable2).Contains("VPN Test"))
                    {
                        espa = spa.EditPrintAgent(driver, string.Concat(spa.agentTable2, "/tr[", x.ToString(), "]/td[5]/form/a[1]"));
                        tglog.AppendLogText(filePath, "Opened Edit Print Agent ..... OK");
                        Thread.Sleep(500);
                        cc.SelectDropDownByElement(driver, espa.computerName, "TCUMMINS");
                        cc.SelectDropDownByOption(driver, espa.printer, 1);

                        cc.ScrollPage(driver);
                        cc.ClickButton(driver, espa.saveBtn);
                        Thread.Sleep(500);

                        tglog.AppendLogText(filePath, "Opened Secure Print Agents ..... OK");
                        break;
                    }
                }

                // verify not in list
                passFail = 0;
                for (int x = 1; x <= rows; x++)
                {
                    if (cc.FetchCellValue(driver, "1", x.ToString(), spa.agentTable2).Contains("VPN Test"))
                    {
                        if (cc.FetchCellValue(driver, "3", x.ToString(), spa.agentTable2).Contains(""))
                        {
                            tglog.AppendLogText(filePath, string.Concat("New Printer was selected.....Passed"));
                            passFail = 0;
                            break;
                        }
                        else
                        {
                            tglog.AppendLogText(filePath, string.Concat("New Printer was not selected.....Failed"));
                            passFail = 1;
                            break;
                        }
                    }
                }

                 //tear down

                RemovePrintAgent();
                if (cc.AcceptAssertOK(driver) == "")
                {
                    driver.SwitchTo().Window(baseWindowHandle);
                    tglog.AppendLogText(filePath, "Selected print agent was removed.....OK");
                }
                else
                {
                    passFail = 1;
                    tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Alert was not displayed!", Environment.NewLine));
                }
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        //[TestMethod, TestCategory("NOT READY Settings - Print Agent")]
        //public void Settings_PrintAgent_ChangeStatus()
        //{
        //    string status = "";
        //    string inUse = "";

        //    if (qaDev.ToLower() == dev)
        //    {
        //        //instance = 9899331;
        //        testName = "Settings_PrintAgent_ChangeStatus_Dev";
        //    }
        //    else
        //    {
        //        if (qaDev.ToLower() == qa)
        //        {
        //            //instance = 4602034;
        //            testName = "Settings_PrintAgent_ChangeStatus_QA";
        //        }
        //        else
        //        {
        //            if (qaDev.ToLower() == beta)
        //            {
        //                //instance = 4749098;
        //                testName = "Settings_PrintAgent_DeletePrintAgent_Beta";
        //            }
        //            else
        //            {
        //                //if (qaDev.ToLower() == prod)
        //                //{
        //                //instance = 5355731;
        //                testName = "Settings_PrintAgent_ChangeStatus_Prod";
        //                //}
        //            }
        //        }

        //    }
        //    passFail = 1;  //reset before test
        //    filePath = string.Concat(filePath, testName, ".txt");

        //    CreateLogFile();
        //    baseWindowHandle = driver.CurrentWindowHandle;

        //    try
        //    {
        //        //setup & tear down
        //        OpenPrintAgents();

        //        AddPrintAgent();
        //        rows = Convert.ToInt16(cc.FetchTableRows(driver, spa.agentTable));
        //        inUse = cc.FetchInnerHTML(driver, spa.agentCount);

        //        for (int x = 1; x <= rows; x++)
        //        {
        //            printerName = cc.FetchCellValue(driver, "1", x.ToString(), spa.agentTable3);

        //            if (cc.FetchCellValue(driver, "1", x.ToString(), spa.agentTable3).Contains("VPN Test"))
        //            {
        //                status = cc.FetchInnerHTML(driver, string.Concat(spa.agentTable3, "/tr[", x.ToString(), "]/td[6]/span"));

        //                cc.ClickButton(driver, string.Concat(spa.agentTable3, "/tr[", x.ToString(), "]/td[6]/form/a[2]"));
        //                StringAssert.Contains("True", (status += cc.FetchInnerHTML(driver, string.Concat(spa.agentTable3, "/tr[", x.ToString(), "]/td[6]/span")).ToString()));
        //                //Thread.Sleep(3000);

        //                //cc.SelectDropDownByOption(driver, espa.computerName, 10);
        //                ////TestContext.WriteLine(cc.FetchSelectedOption(driver, espa.computerName));
        //                ////TestContext.WriteLine(cc.FetchChildCount(driver, espa.printer).ToString());
        //                //cc.SelectDropDownByOption(driver, espa.printer, 1);

        //                //cc.ScrollPage(driver);
        //                //cc.ClickButton(driver, espa.saveBtn);
        //                //Thread.Sleep(3000);

        //                //StringAssert.Contains(cc.FetchInnerHTML(driver, spa.Header2), "Print Agents");
        //                //tglog.AppendLogText(filePath, "Opened Secure Print Agents ..... OK");
        //                break;
        //            }
        //        }
        //        //tear down
        //        OpenPrintAgents();

        //        RemovePrintAgent();
        //        if (cc.AcceptAssertOK(driver) == "")
        //        {
        //            driver.SwitchTo().Window(baseWindowHandle);
        //            tglog.AppendLogText(filePath, "Selected print agent was removed.....OK");
        //        }
        //        else
        //        {
        //            passFail = 1;
        //            tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Alert was not displayed!", Environment.NewLine));
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        passFail = 1;
        //        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
        //    }
        //}

        [TestMethod, TestCategory("R1 - Print Agent")]
        public void R1_PrintAgent_DeletePrintAgent()
        {
            string found = "False";

            if (qaDev.ToLower() == dev)
            {
                //instance = 11128196;
                testName = "Settings_PrintAgent_DeletePrintAgent_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14591779;
                    testName = "Settings_PrintAgent_DeletePrintAgent_Stage";
                }
                    else
                    {
                    instance = 14823113;
                    testName = "Settings_PrintAgent_DeletePrintAgent_Prod";
                    }
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();
            baseWindowHandle = driver.CurrentWindowHandle;

            try
            {
                //setup & tear down
                OpenPrintAgents();
                StringAssert.Contains(cc.FetchInnerHTML(driver, spa.Header), "Print Agents");
                tglog.AppendLogText(filePath, "Opened Secure Print Agents ..... OK");

                AddPrintAgent();
                rows = Convert.ToInt16(cc.FetchTableRows(driver, spa.agentTable2));

                //test
                // verify in list
                for (int x = 1; x <= rows; x++)
                {
                    if (cc.FetchCellValue(driver, "1", x.ToString(), spa.agentTable2).Contains(printerName)) 
                    {
                        found = "True";
                        break;
                    }
                }

                StringAssert.Contains(found.ToString(), "True");
                tglog.AppendLogText(filePath, string.Concat("Printer ", printerName, " was created"));

                RemovePrintAgent();
                if (cc.AcceptAssertOK(driver) == "")
                {
                    driver.SwitchTo().Window(baseWindowHandle);
                    tglog.AppendLogText(filePath, "Selected print agent was removed.....OK");
                    passFail = 0;
                }
                else
                {
                    passFail = 1;
                    tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Alert was not displayed!", Environment.NewLine));
                }
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        [TestMethod, TestCategory("R1 - Print Agent")]
        public void R1_PrintAgent_DisablePrintAgent()
        {
            string cnt = "";
            string computer = "";
            string physicalPrinter = "";
            string printAgent = "";
            string status = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 11128207;
                testName = "Settings_PrintAgent_DisablePrintAgent_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14591781;
                    testName = "Settings_PrintAgent_DisablePrintAgent_Stage";
                }
                else
                {
                    instance = 14823115;
                    testName = "Settings_PrintAgent_DisablePrintAgent_Prod";
                }
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                //setup
                OpenPrintAgents();
                AddPrintAgent();
                cnt = cc.FetchInnerText(driver, spa.agentCount);
                tglog.AppendLogText(filePath, string.Concat("Print Agents In Use = ", cc.FetchInnerText(driver, spa.agentCount)));

                Thread.Sleep(500);
                cc.RefreshPage(driver);
                Thread.Sleep(1000);
                rows = Convert.ToInt16(cc.FetchTableRows(driver, spa.agentTable));

                //test
                for (int x = 1; x <= rows; x++)
                {
                    printAgent = cc.FetchInnerHTML(driver, string.Concat(spa.agentTable, "/tr[", x.ToString(), "]/td[1]/span[1]"));
                    physicalPrinter = cc.FetchInnerHTML(driver, string.Concat(spa.agentTable, "/tr[", x.ToString(), "]/td[2]"));
                    computer = cc.FetchInnerHTML(driver, string.Concat(spa.agentTable, "/tr[", x.ToString(), "]/td[3]"));
                    tglog.AppendLogText(filePath, string.Concat(printerName, "/", physicalPrinter,"/",computer));

                    if (cc.FetchCellValue(driver, "1", x.ToString(), spa.agentTable).Contains("VPN Test"))
                    {
                        status = cc.FetchInnerHTML(driver, string.Concat(spa.agentTable, "/tr[", x.ToString(), "]/td[1]/span[2]"));
                        tglog.AppendLogText(filePath, string.Concat("Print Agent: ",printerName, "/Printer: ", physicalPrinter, "/Computer: ", computer, "/Status: ",status));

                        cc.ClickButton(driver, string.Concat(spa.agentTable, "/tr[", x.ToString(), "]/td[5]/form/a[2]"));
                        Thread.Sleep(1000);
                        cc.RefreshPage(driver);
                        Thread.Sleep(1000);

                        //test
                        StringAssert.Contains("True", (status != cc.FetchInnerHTML(driver, string.Concat(spa.agentTable, "/tr[", x.ToString(), "]/td[1]/span[2]"))).ToString());
                        tglog.AppendLogText(filePath, string.Concat("Agent Status was changed to ", cc.FetchInnerHTML(driver, string.Concat(spa.agentTable, "/tr[", x.ToString(), "]/td[1]/span[2]"))));

                        tglog.AppendLogText(filePath, string.Concat("Print Agents In Use was changed to ", cc.FetchInnerText(driver, spa.agentCount)));
                        StringAssert.Contains("True", (cc.FetchInnerText(driver, spa.agentCount) != cnt).ToString());

                        passFail = 0;
                        Thread.Sleep(1000);
                        break;
                    }
                }

                //tear down
                RemovePrintAgent(rows);
                if (cc.AcceptAssertOK(driver) == "")
                {
                    driver.SwitchTo().Window(baseWindowHandle);
                    tglog.AppendLogText(filePath, "Selected print agent was removed.....OK");
                }
                else
                {
                    passFail = 1;
                    tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Alert was not displayed!", Environment.NewLine));
                }
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //tear down
                RemovePrintAgent(rows);
                if (cc.AcceptAssertOK(driver) == "")
                {
                    driver.SwitchTo().Window(baseWindowHandle);
                    tglog.AppendLogText(filePath, "Selected print agent was removed.....OK");
                }
                else
                {
                    passFail = 1;
                    tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Alert was not displayed!", Environment.NewLine));
                }

            }
        }

        //[TestMethod, TestCategory("NOT READY Settings - Print Agent")]
        //public void Settings_PrintAgent_AssignTemplate()
        //{
        //    int assigned = 0;
        //    string cnt = "";
        //    string computer = "";
        //    string physicalPrinter = "";
        //    string printAgent = "";
        //    string status = "";
        //    int templateCount = 0;

        //    if (qaDev.ToLower() == dev)
        //    {
        //        //instance = 11128207;
        //        testName = "Settings_PrintAgent_AssignTemplate_Dev";
        //    }
        //    else
        //    {
        //        if (qaDev.ToLower() == staging)
        //        {
        //            //instance = 13722611;
        //            testName = "Settings_PrintAgent_AssignTemplate_Stage";
        //        }
        //        else
        //        {
        //            //instance = 11263408;
        //            testName = "Settings_PrintAgent_AssignTemplate_Prod";
        //        }
        //    }
        //    passFail = 1;  //reset before test
        //    filePath = string.Concat(filePath, testName, ".txt");

        //    CreateLogFile();

        //    try
        //    {
        //        //setup
        //        OpenPrintAgents();
        //        cnt = cc.FetchInnerText(driver, spa.agentCount);
        //        tglog.AppendLogText(filePath, string.Concat("Print Agents In Use = ", cc.FetchInnerText(driver, spa.agentCount)));

        //        AddPrintAgent();
        //        Thread.Sleep(5000);
        //        cc.RefreshPage(driver);
        //        Thread.Sleep(5000);
        //        rows = Convert.ToInt16(cc.FetchTableRows(driver, spa.agentTable));

        //        for (int x = 1; x <= rows; x++)
        //        {
        //            printAgent = cc.FetchInnerHTML(driver, string.Concat(spa.agentTable, "/tr[", x.ToString(), "]/td[1]/a"));
        //            physicalPrinter = cc.FetchInnerHTML(driver, string.Concat(spa.agentTable, "/tr[", x.ToString(), "]/td[2]"));
        //            computer = cc.FetchInnerHTML(driver, string.Concat(spa.agentTable, "/tr[", x.ToString(), "]/td[3]"));
        //            tglog.AppendLogText(filePath, string.Concat(printerName, "/", physicalPrinter, "/", computer));

        //            if (cc.FetchCellValue(driver, "1", x.ToString(), spa.agentTable).Contains("VPN Test"))
        //            {
        //                status = cc.FetchInnerHTML(driver, string.Concat(spa.agentTable, "/tr[", x.ToString(), "]/td[1]/span"));
        //                tglog.AppendLogText(filePath, string.Concat("Print Agent: ", printerName, "/Printer: ", physicalPrinter, "/Computer: ", computer, "/Status: ", status));

        //                //test
        //                dspa = spa.PrintAgentDetail(driver, string.Concat(spa.agentTable, "/tr[", x.ToString(), "]/td[1]/a"));
        //                Thread.Sleep(3000);

        //                espa = dspa.EditPrintAgent(driver);
        //                tglog.AppendLogText(filePath, "Open Edit Print Agent.....");
        //                Thread.Sleep(3000);

        //                //test
        //               assigned = Convert.ToInt16(cc.FetchChildCount(driver, espa.assignedTemplates));
        //               templateCount = Convert.ToInt16(cc.FetchChildCount(driver, espa.availableTemplates));

        //                for (int a=1; a<=templateCount; a++)
        //                {
        //                    tglog.AppendLogText(filePath, string.Concat(a.ToString(), cc.FetchInnerText(driver, string.Concat(espa.availableTemplates, "/li[", a.ToString(), "]"))));


        //                    cc.DragandDrop(driver, string.Concat(espa.availableTemplates, "/li[", a.ToString(), "]/span"), espa.assignedTemplates);

        //                    assigned = Convert.ToInt16(cc.FetchChildCount(driver, espa.assignedTemplates));
        //                    for (int b=1; b<=assigned; b++)
        //                    {
        //                        tglog.AppendLogText(filePath, string.Concat("     ",b.ToString(), cc.FetchInnerText(driver, string.Concat(espa.assignedTemplates, "/li[", b.ToString(), "]"))));
        //                    }
        //                }

        //                cc.ClickButton(driver, espa.saveBtn);

        //                passFail = 0;
        //                Thread.Sleep(3000);
        //                break;
        //            }
        //        }

        //        //tear down
        //        RemovePrintAgent();
        //        if (cc.AcceptAssertOK(driver) == "")
        //        {
        //            driver.SwitchTo().Window(baseWindowHandle);
        //            tglog.AppendLogText(filePath, "Selected print agent was removed.....OK");
        //        }
        //        else
        //        {
        //            passFail = 1;
        //            tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Alert was not displayed!", Environment.NewLine));
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        passFail = 1;
        //        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

        //        //tear down
        //        RemovePrintAgent(rows);
        //        if (cc.AcceptAssertOK(driver) == "")
        //        {
        //            driver.SwitchTo().Window(baseWindowHandle);
        //            tglog.AppendLogText(filePath, "Selected print agent was removed.....OK");
        //        }
        //        else
        //        {
        //            passFail = 1;
        //            tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Alert was not displayed!", Environment.NewLine));
        //        }

        //    }
        //}

        [TestMethod, TestCategory("R2 - Print Agent")]
        public void Settings_PrintAgent_EnablePreprocessBackup()
        {
            string preprocess = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 11128200;
                testName = "Settings_PrintAgent_EnableProprocessBackup_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14591829;
                    testName = "Settings_PrintAgent_EnableProprocessBackup_Stage";
                }
                else
                {
                    //instance = 11263404;
                    testName = "Settings_PrintAgent_EnableProprocessBackup_Prod";
                }
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                //setup & tear down
                OpenPrintAgents();

                AddPrintAgent();
                rows = Convert.ToInt16(cc.FetchTableRows(driver, spa.agentTable2));
                Thread.Sleep(3000);
                //test
                for (int x = 1; x <= rows; x++)
                {
                    printerName = cc.FetchCellValue(driver, "1", x.ToString(), spa.agentTable2);

                    if (cc.FetchCellValue(driver, "1", x.ToString(), spa.agentTable2).Contains("VPN Test"))
                    {
                        espa = spa.EditPrintAgent(driver, string.Concat(spa.agentTable2, "/tr[", x.ToString(), "]/td[5]/form/a[1]"));
                        StringAssert.Contains(cc.FetchInnerHTML(driver, espa.Header), "Edit Print Agent");
                        tglog.AppendLogText(filePath, "Opened Edit Print Agent ..... OK");
                        Thread.Sleep(500);

                        preprocess = cc.FetchChecked(driver, espa.preprocessBackup);
                        tglog.AppendLogText(filePath, string.Concat("Pre-process Backup = ", preprocess));

                        cc.ClickButton(driver, espa.preprocessBackup);

                        cc.ScrollPage(driver);
                        cc.ClickButton(driver, espa.saveBtn);
                        Thread.Sleep(500);
                        tglog.AppendLogText(filePath, "Opened Secure Print Agents ..... OK");
                        break;
                    }
                }

                //Reset Value
                for (int x = 1; x <= rows; x++)
                {
                    if (cc.FetchCellValue(driver, "1", x.ToString(), spa.agentTable2).Contains("VPN Test"))
                    {
                        espa = spa.EditPrintAgent(driver, string.Concat(spa.agentTable2, "/tr[", x.ToString(), "]/td[5]/form/a[1]"));
                        StringAssert.Contains(cc.FetchInnerHTML(driver, espa.Header), "Edit Print Agent");
                        tglog.AppendLogText(filePath, "Opened Edit Print Agent ..... OK");
                        Thread.Sleep(500);

                        if (preprocess != cc.FetchChecked(driver, espa.preprocessBackup))
                        {
                            tglog.AppendLogText(filePath, "Pre-process Backup has been changed.....PASSED!");

                            cc.ClickButton(driver, espa.preprocessBackup);
                            StringAssert.Contains("True", (preprocess == cc.FetchChecked(driver, espa.preprocessBackup)).ToString());

                            cc.ScrollPage(driver);
                            cc.ClickButton(driver, espa.saveBtn);
                            tglog.AppendLogText(filePath, "Pre-process Backup has been restored to original value.");
                            passFail = 0;
                        }
                        else
                        {
                            cc.ScrollPage(driver);
                            cc.ClickButton(driver, espa.cancelBtn);
                            passFail = 1;
                        }

                        tglog.AppendLogText(filePath, "Opened Secure Print Agents ..... OK");
                        break;
                    }
                }

                //tear down
                RemovePrintAgent();
                if (cc.AcceptAssertOK(driver) == "")
                {
                    driver.SwitchTo().Window(baseWindowHandle);
                    tglog.AppendLogText(filePath, "Selected print agent was removed.....OK");
                }
                else
                {
                    passFail = 1;
                    tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Alert was not displayed!", Environment.NewLine));
                }
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        [TestMethod, TestCategory("R2 - Print Agent")]
        public void Settings_PrintAgent_OverridePantograph()
        {
            string pantograph = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 11128200;
                testName = "Settings_PrintAgent_OverridePantograph_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14591830;
                    testName = "Settings_PrintAgent_OverridePantograph_Stage";
                }
                else
                {
                    //instance = 11263404;
                    testName = "Settings_PrintAgent_OverridePantograph_Prod";
                }
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                //setup & tear down
                OpenPrintAgents();

                AddPrintAgent();
                rows = Convert.ToInt16(cc.FetchTableRows(driver, spa.agentTable2));
                Thread.Sleep(3000);
                //test
                for (int x = 1; x <= rows; x++)
                {
                    printerName = cc.FetchCellValue(driver, "1", x.ToString(), spa.agentTable2);

                    if (cc.FetchCellValue(driver, "1", x.ToString(), spa.agentTable2).Contains("VPN Test"))
                    {
                        espa = spa.EditPrintAgent(driver, string.Concat(spa.agentTable2, "/tr[", x.ToString(), "]/td[5]/form/a[1]"));
                        StringAssert.Contains(cc.FetchInnerHTML(driver, espa.Header), "Edit Print Agent");
                        tglog.AppendLogText(filePath, "Opened Edit Print Agent ..... OK");
                        Thread.Sleep(500);

                        pantograph = cc.FetchChecked(driver, espa.overridePantograph);
                        tglog.AppendLogText(filePath, string.Concat("Override Pantograph = ", pantograph));

                        cc.ClickButton(driver, espa.overridePantograph);

                        cc.ScrollPage(driver);
                        cc.ClickButton(driver, espa.saveBtn);
                        Thread.Sleep(500);
                        tglog.AppendLogText(filePath, "Opened Secure Print Agents ..... OK");
                        break;
                    }
                }

                //Reset Value
                for (int x = 1; x <= rows; x++)
                {
                    if (cc.FetchCellValue(driver, "1", x.ToString(), spa.agentTable2).Contains("VPN Test"))
                    {
                        espa = spa.EditPrintAgent(driver, string.Concat(spa.agentTable2, "/tr[", x.ToString(), "]/td[5]/form/a[1]"));
                        StringAssert.Contains(cc.FetchInnerHTML(driver, espa.Header), "Edit Print Agent");
                        tglog.AppendLogText(filePath, "Opened Edit Print Agent ..... OK");
                        Thread.Sleep(500);

                        if (pantograph != cc.FetchChecked(driver, espa.overridePantograph))
                        {
                            tglog.AppendLogText(filePath, "Override Pantograph has been changed - New Fields are Exposed.....PASSED!");
                            tglog.AppendLogText(filePath, string.Concat("     Background: ", cc.FetchPlaceholder(driver, espa.backgroundValue)));
                            tglog.AppendLogText(filePath, string.Concat("     Foreground: ", cc.FetchPlaceholder(driver, espa.foregroundValue)));

                            cc.ClickButton(driver, espa.overridePantograph);
                            StringAssert.Contains("True", (pantograph == cc.FetchChecked(driver, espa.overridePantograph)).ToString());

                            cc.ScrollPage(driver);
                            cc.ClickButton(driver, espa.saveBtn);
                            tglog.AppendLogText(filePath, "Override Pantograph has been restored to original value.");
                            passFail = 0;
                        }
                        else
                        {
                            cc.ScrollPage(driver);
                            cc.ClickButton(driver, espa.cancelBtn);
                            passFail = 1;
                        }

                        tglog.AppendLogText(filePath, "Opened Secure Print Agents ..... OK");
                        break;
                    }
                }

                //tear down
                RemovePrintAgent();
                if (cc.AcceptAssertOK(driver) == "")
                {
                    driver.SwitchTo().Window(baseWindowHandle);
                    tglog.AppendLogText(filePath, "Selected print agent was removed.....OK");
                }
                else
                {
                    passFail = 1;
                    tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Alert was not displayed!", Environment.NewLine));
                }
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        [TestMethod, TestCategory("R2 - Print Agent")]
        public void Settings_PrintAgent_VerifyCountAfterAddPrintAgent()
        {
            string cnt = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 11128207;
                testName = "Settings_PrintAgent_VerifyCountAfterAddPrintAgent_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14591827;
                    testName = "Settings_PrintAgent_VerifyCountAfterAddPrintAgent_Stage";
                }
                    else
                    {
                        //instance = 11263408;
                        testName = "Settings_PrintAgent_VerifyCountAfterAddPrintAgent_Prod";
                    }
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                //setup
                OpenPrintAgents();
                cnt = cc.FetchInnerText(driver, spa.agentCount);
                tglog.AppendLogText(filePath, cnt);

                AddPrintAgent();

                //test
                StringAssert.Contains("True", (cnt != cc.FetchInnerText(driver, spa.agentCount2)).ToString());
                passFail = 0;
                tglog.AppendLogText(filePath, string.Concat(cc.FetchInnerText(driver, spa.agentCount2), ".....PASSED!"));

                //tear down
                RemovePrintAgent();
                if (cc.AcceptAssertOK(driver) == "")
                {
                    driver.SwitchTo().Window(baseWindowHandle);
                    tglog.AppendLogText(filePath, "Selected print agent was removed.....OK");
                }
                else
                {
                    passFail = 1;
                    tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Alert was not displayed!", Environment.NewLine));
                }
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //tear down
                RemovePrintAgent();
                if (cc.AcceptAssertOK(driver) == "")
                {
                    driver.SwitchTo().Window(baseWindowHandle);
                    tglog.AppendLogText(filePath, "Selected print agent was removed.....OK");
                }
                else
                {
                    passFail = 1;
                    tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Alert was not displayed!", Environment.NewLine));
                }

            }
        }

        [TestMethod, TestCategory("R2 - Print Agent")]
        public void Settings_PrintAgent_VerifyCountAfterDeletePrintAgent()
        {
            string cnt = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 11128208;
                testName = "Settings_PrintAgent_VerifyCountAfterDeletePrintAgent_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14591828;
                    testName = "Settings_PrintAgent_VerifyCountAfterDeletePrintAgent_Stage";
                }
                    else
                    {
                        //instance = 11263409;
                        testName = "Settings_PrintAgent_VerifyCountAfterDeletePrintAgent_Prod";
                    }
                //}
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();
            baseWindowHandle = driver.CurrentWindowHandle;

            try
            {
                //setup & tear down
                OpenPrintAgents();
                AddPrintAgent();
                cnt = cc.FetchInnerText(driver, spa.agentCount2);
                tglog.AppendLogText(filePath, cnt);
                RemovePrintAgent();

                if (cc.AcceptAssertOK(driver) == "")
                {
                    //test
                    driver.SwitchTo().Window(baseWindowHandle);
                    StringAssert.Contains((itemCnt - 1).ToString(), cc.FetchChildCount(driver, spa.agentTable2));
                    tglog.AppendLogText(filePath, "Selected print agent was removed.....OK");

                    StringAssert.Contains("True", (cnt != cc.FetchInnerText(driver, spa.agentCount2)).ToString());
                    passFail = 0;
                    tglog.AppendLogText(filePath, string.Concat(cc.FetchInnerText(driver, spa.agentCount2), ".....PASSED!"));
                }
                else
                {
                    passFail = 1;
                    tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Alert was not displayed!", Environment.NewLine));
                }

            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        //[TestMethod, TestCategory("Settings - Print Agent")]
        //public void Settings_PrintAgent_VerifyCreateDate()
        //{

        //    string today = DateTime.Now.ToString("d");

        //    if (qaDev.ToLower() == dev)
        //    {
        //        instance = 10080585;
        //        testName = "Settings_PrintAgent_VerifyCreateDate_Dev";
        //    }
        //    else
        //    {
        //        if (qaDev.ToLower() == qa)
        //        {
        //            //instance = 4602034;
        //            testName = "Settings_PrintAgent_VerifyCreateDate_QA";
        //        }
        //        else
        //        {
        //            if (qaDev.ToLower() == beta)
        //            {
        //                //instance = 4749098;
        //                testName = "Settings_PrintAgent_VerifyCreateDate_Beta";
        //            }
        //            else
        //            {
        //                //if (qaDev.ToLower() == prod)
        //                //{
        //                instance = 10163231;
        //                testName = "Settings_PrintAgent_VerifyCreateDate_Prod";
        //                //}
        //            }
        //        }

        //    }
        //    passFail = 1;  //reset before test
        //    filePath = string.Concat(filePath, testName, ".txt");

        //    CreateLogFile();

        //    try
        //    {
        //        //setup
        //        OpenPrintAgents();

        //        AddPrintAgent();
        //        rows = Convert.ToInt16(cc.FetchTableRows(driver, spa.agentTable));

        //        StringAssert.Contains(cc.FetchInnerHTML(driver, spa.Header), "Print Agents");
        //        tglog.AppendLogText(filePath, string.Concat("Added New Print Agent ", printerName));
        //        tglog.AppendLogText(filePath, "Opened Secure Print Agents ..... OK");
        //        tglog.AppendLogText(filePath, string.Concat("Printer ", printerName, " was created ", today));

        //        //test
        //        for (int x = 1; x <= rows; x++)
        //        {
        //            if (cc.FetchCellValue(driver, "1", x.ToString(), spa.agentTable).Contains(printerName))
        //            {
        //                if (cc.FetchCellValue(driver, "4", x.ToString(), spa.agentTable).Contains(today))
        //                {
        //                    tglog.AppendLogText(filePath, "New Print Agent Create Date.....PASSED!");

        //                    if (cc.FetchCellValue(driver, "5", x.ToString(), spa.agentTable).Contains(today))
        //                    {
        //                        passFail = 0;
        //                        tglog.AppendLogText(filePath, "New Print Agent Update Date.....PASSED!");
        //                        break;
        //                    }
        //                    else
        //                    {
        //                        passFail = 1;
        //                        tglog.AppendLogText(filePath, "New Print Agent Update Date.....FAILED!!!");
        //                        break;
        //                    }
        //                }
        //                else
        //                {
        //                    passFail = 1;
        //                    tglog.AppendLogText(filePath, "New Print Agent Create Date.....FAILED!!!");
        //                    break;
        //                }
        //            }
        //        }

        //        //tear down
        //        RemovePrintAgent();
        //        if (cc.AcceptAssertOK(driver) == "")
        //        {
        //            driver.SwitchTo().Window(baseWindowHandle);
        //            tglog.AppendLogText(filePath, "Selected print agent was removed.....OK");
        //        }
        //        else
        //        {
        //            passFail = 1;
        //            tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Alert was not displayed!", Environment.NewLine));
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        passFail = 1;
        //        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
        //    }
        //}

        [TestMethod, TestCategory("R2 - Print Agent")]
        public void Settings_PrintAgent_VerifyUpdatedDate()
        {

            //string today = DateTime.Now.ToString("d");

            if (qaDev.ToLower() == dev)
            {
                //instance = 11128210;
                testName = "Settings_PrintAgent_VerifyUpdatedDate_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14591826;
                    testName = "Settings_PrintAgent_VerifyUpdatedDate_Stage";
                }
                    else
                    {
                        //instance = 11263410;
                        testName = "Settings_PrintAgent_VerifyUpdatedDate_Prod";
                    }
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                //setup & tear down
                OpenPrintAgents();

                AddPrintAgent();
                rows = Convert.ToInt16(cc.FetchTableRows(driver, spa.agentTable2));

                //test
                for (int x = 1; x <= rows; x++)
                {
                    printerName = cc.FetchCellValue(driver, "1", x.ToString(), spa.agentTable2);

                    if (cc.FetchCellValue(driver, "1", x.ToString(), spa.agentTable2).Contains("Test Agent"))
                    {
                        espa = spa.EditPrintAgent(driver, string.Concat(spa.agentTable2, "/tr[", x.ToString(), "]/td[5]/form/a[1]"));
                        StringAssert.Contains(cc.FetchInnerHTML(driver, espa.Header), "Edit Print Agent");
                        tglog.AppendLogText(filePath, "Opened Edit Print Agent ..... OK");
                        Thread.Sleep(3000);

                        cc.EnterText(driver, "c:\\xpsFiles", espa.xpsPath);

                        cc.ScrollPage(driver);
                        cc.ClickButton(driver, espa.saveBtn);
                        Thread.Sleep(1000);
                        tglog.AppendLogText(filePath, "Output File Path has been changed.....PASSED!");

                        if (cc.FetchCellValue(driver, "4", x.ToString(), spa.agentTable2).Contains(today))
                        {
                            passFail = 0;
                            tglog.AppendLogText(filePath, "Print Agent Update Date.....PASSED!");
                            break;
                        }
                        else
                        {
                            passFail = 1;
                            tglog.AppendLogText(filePath, "Print Agent Update Date.....FAILED!!!");
                            break;
                        }
                    }
                }

                //Reset Value
                for (int x = 1; x <= rows; x++)
                {
                    if (cc.FetchCellValue(driver, "1", x.ToString(), spa.agentTable2).Contains("Test Agent"))
                    {
                        espa = spa.EditPrintAgent(driver, string.Concat(spa.agentTable2, "/tr[", x.ToString(), "]/td[5]/form/a[1]"));
                        StringAssert.Contains(cc.FetchInnerHTML(driver, espa.Header), "Edit Print Agent");
                        tglog.AppendLogText(filePath, "Opened Edit Print Agent ..... OK");
                        Thread.Sleep(3000);
                        cc.EnterText(driver, outputFilePath, espa.xpsPath);
                        StringAssert.Contains("False", (outputFilePath == cc.FetchInnerHTML(driver, espa.xpsPath)).ToString());

                        cc.ScrollPage(driver);
                        cc.ClickButton(driver, espa.saveBtn);
                        tglog.AppendLogText(filePath, "Output File Path has been restored to original value.");

                        tglog.AppendLogText(filePath, "Opened Secure Print Agents ..... OK");
                        break;
                    }
                }

                //tear down
                RemovePrintAgent();
                if (cc.AcceptAssertOK(driver) == "")
                {
                    driver.SwitchTo().Window(baseWindowHandle);
                    tglog.AppendLogText(filePath, "Selected print agent was removed.....OK");
                }
                else
                {
                    passFail = 1;
                    tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Alert was not displayed!", Environment.NewLine));
                }
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        #endregion

        #region Serialization
        [TestMethod, TestCategory("R2 - Serialziation")]
        public void Settings_Serialziation_AddJobMetadata()
        {
            string newTxt = "";
            string txt = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 11128276;
                testName = "Settings_Serialziation_AddJobMetadata_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14602762;
                    testName = "Settings_Serialziation_AddJobMetadata_Stage";
                }
                //else
                //{
                //    if (qaDev.ToLower() == beta)
                //    {
                //        //instance = 4998768;
                //        testName = "Settings_Serialziation_AddJobMetadata_Beta";
                //    }
                    else
                    {
                        //instance = 11263415;
                        testName = "Settings_Serialziation_AddJobMetadata_Prod";
                    }
                //}
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                //setup
                SetupSerializationRegion();
                //SetupBarcodeSetting();

                //test
                txt = cc.FetchTextValue(driver, ser.textArea);
                tglog.AppendLogText(filePath, string.Concat("Original Text is ", txt));

                cc.SelectDropDownByOption(driver, ser.jobMetadata, 3);
                cc.ClickButton(driver, ser.saveBtn);
                Thread.Sleep(500);

                newTxt = cc.FetchTextValue(driver, ser.textArea);
                tglog.AppendLogText(filePath, string.Concat("New Text is ", newTxt));
                StringAssert.Contains("True", (txt != newTxt).ToString());
                tglog.AppendLogText(filePath, string.Concat("Original Text Value Does Not Equal Current Text Value..... PASSED!", Environment.NewLine));
                passFail = 0;

                // reset original
                ResetAddRegion();
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        [TestMethod, TestCategory("R2 - Serialziation")]
        public void Settings_Serialziation_ChangeFont()
        {
            string font = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 11128277;
                testName = "Settings_Serialziation_ChangeFont_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14602764;
                    testName = "Settings_Serialziation_ChangeFont_Stage";
                }
                //else
                //{
                //    if (qaDev.ToLower() == beta)
                //    {
                //        //instance = 4998768;
                //        testName = "Settings_Serialziation_ChangeFont_Beta";
                //    }
                    else
                    {
                        //instance = 11263416;
                        testName = "Settings_Serialziation_ChangeFont_Prod";
                    }
                //}
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                //setup
                SetupSerializationRegion();
                //SetupBarcodeSetting();

                //test
                font = cc.FetchSelectedOption(driver, ser.fontList);
                tglog.AppendLogText(filePath, string.Concat("Original Font is ", font));

                cc.SelectDropDownByOption(driver, ser.fontList, 5);
                Thread.Sleep(500);

                StringAssert.Contains("True", (font != cc.FetchSelectedOption(driver, ser.fontList)).ToString());
                tglog.AppendLogText(filePath, string.Concat("Selected Font is ", cc.FetchSelectedOption(driver, ser.fontList)).ToString());
                tglog.AppendLogText(filePath, string.Concat("Original Font Does Not Equal Current Font..... PASSED!", Environment.NewLine));
                passFail = 0;

                // reset original
                ResetAddRegion();

            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        [TestMethod, TestCategory("R2 - Serialziation")]
        public void Settings_Serialziation_ChangeFontSize()
        {
            string fontsize = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 11128278;
                testName = "Settings_Serialziation_ChangeFontSize_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14602765;
                    testName = "Settings_Serialziation_ChangeFontSize_Stage";
                }
                //else
                //{
                //    if (qaDev.ToLower() == beta)
                //    {
                //        //instance = 4998768;
                //        testName = "Settings_Serialziation_ChangeFontSize_Beta";
                //    }
                    else
                    {
                        //instance = 11263417;
                        testName = "Settings_Serialziation_ChangeFontSize_Prod";
                    }
                //}
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                //setup
                SetupSerializationRegion();
                //SetupBarcodeSetting();

                //test
                fontsize = cc.FetchTextValue(driver, ser.fontSize);
                tglog.AppendLogText(filePath, string.Concat("Original Font Size is ", fontsize));

                cc.SelectDropDownByOption(driver, ser.fontSize, 5);
                cc.ClickButton(driver, ser.saveBtn);
                Thread.Sleep(500);

                StringAssert.Contains("True", (fontsize != cc.FetchTextValue(driver, ser.fontSize)).ToString());
                tglog.AppendLogText(filePath, string.Concat("Current Font Size is ", cc.FetchTextValue(driver, ser.fontSize)).ToString());
                tglog.AppendLogText(filePath, string.Concat("Original Font Size Value Does Not Equal Current Font Size Value..... PASSED!", Environment.NewLine));
                passFail = 0;

                // reset original
                ResetAddRegion();
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        [TestMethod, TestCategory("R2 - Serialziation")]
        public void Settings_Serialziation_ChangePadding()
        {
            string pad = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 11128274;
                testName = "Settings_Serialziation_ChangePadding_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14602760;
                    testName = "Settings_Serialziation_ChangePadding_Stage";
                }
                //else
                //{
                //    if (qaDev.ToLower() == beta)
                //    {
                //        //instance = 4998768;
                //        testName = "Settings_Serialziation_ChangePadding_Beta";
                //    }
                    else
                    {
                        //instance = 11263413;
                        testName = "Settings_Serialziation_ChangePadding_Prod";
                    }
                //}
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                //setup
                SetupSerializationRegion();

                //test
                pad = cc.FetchTextValue(driver, ser.padding);
                tglog.AppendLogText(filePath, string.Concat("Original Padding is ", pad));

                cc.EnterText(driver,"32", ser.padding);
                cc.ClickButton(driver, ser.saveBtn);
                Thread.Sleep(500);

                StringAssert.Contains("True", (pad != cc.FetchTextValue(driver, ser.padding)).ToString());
                tglog.AppendLogText(filePath, string.Concat("New Padding is ", cc.FetchTextValue(driver, ser.padding)).ToString());
                tglog.AppendLogText(filePath, string.Concat("Original Padding Value Does Not Equal Current Padding Value..... PASSED!", Environment.NewLine));
                passFail = 0;

                // reset original
                ResetAddRegion();
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        [TestMethod, TestCategory("R1 - Serialziation")]
        public void R1_Serialziation_ChangePosition()
        {
            string addRegion = "";
            string post = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 11128273;
                testName = "Settings_Serialziation_ChangePosition_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14602678;
                    testName = "Settings_Serialziation_ChangePosition_Stage";
                }
                //else
                //{
                //    if (qaDev.ToLower() == beta)
                //    {
                //        //instance = 4998768;
                //        testName = "Settings_Serialziation_ChangePosition_Beta";
                //    }
                    else
                    {
                    instance = 14823116;
                    testName = "Settings_Serialziation_ChangePosition_Prod";
                    }
                //}
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                //setup
                ser = mp.OpenSerialization(driver);

                StringAssert.Contains(cc.FetchInnerHTML(driver, ser.Header), "Serialization");
                tglog.AppendLogText(filePath, "Opened Serialization ..... OK");

                addRegion = cc.FetchChecked(driver, ser.addVisualRegion).ToString();
                tglog.AppendLogText(filePath, string.Concat("Original Add Visual Region setting is ", addRegion));

                //Thread.Sleep(3000);
                cc.ClickButton(driver, ser.addVisualRegion2);
                Thread.Sleep(500);
                tglog.AppendLogText(filePath, "Clicked Add Visual Region");

                //test
                cc.ClickButton(driver, ser.addRegionBtn);
                cc.ClickButton(driver, ser.saveBtn);
                tglog.AppendLogText(filePath, string.Concat("Added Visual Region", Environment.NewLine));
                Thread.Sleep(1000);

                //SetupBarcodeSetting();

                post = cc.FetchSelectedOption(driver, ser.position);
                tglog.AppendLogText(filePath, string.Concat("Original Position is ", post));

                cc.SelectDropDownByOption(driver, ser.position, 1);
                //Thread.Sleep(1000);

                StringAssert.Contains("True", (post != cc.FetchSelectedOption(driver, ser.position)).ToString());
                tglog.AppendLogText(filePath, string.Concat("Selected Position is ", cc.FetchSelectedOption(driver, ser.position)).ToString());
                tglog.AppendLogText(filePath, string.Concat("Original Position Value Does Not Equal Current Position..... PASSED!", Environment.NewLine));
                passFail = 0;

                // reset original
                cc.ClickButton(driver, ser.removeButton);
                tglog.AppendLogText(filePath, "Removed Added Region");

                cc.ClickButton(driver, ser.addVisualRegion2);
                cc.ClickButton(driver, ser.saveBtn);
                //Thread.Sleep(3000);

                StringAssert.Contains("True", (addRegion == cc.FetchChecked(driver, ser.addVisualRegion).ToString()).ToString());
                tglog.AppendLogText(filePath, string.Concat("Reset Add Visual Region", Environment.NewLine));
                Thread.Sleep(2000);

            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        [TestMethod, TestCategory("R2 - Serialziation")]
        public void Settings_Serialziation_ChangeText()
        {
            string txt = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 11128275;
                testName = "Settings_Serialziation_ChangeText_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14602761;
                    testName = "Settings_Serialziation_ChangeText_Stage";
                }
                //else
                //{
                //    if (qaDev.ToLower() == beta)
                //    {
                //        //instance = 4998768;
                //        testName = "Settings_Serialziation_ChangeText_Beta";
                //    }
                    else
                    {
                    //instance = 11263414;
                    testName = "Settings_Serialziation_ChangeText_Prod";
                    }
                //}
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                //setup
                SetupSerializationRegion();
                //SetupBarcodeSetting();

                //test
                txt = cc.FetchTextValue(driver, ser.textArea);
                tglog.AppendLogText(filePath, string.Concat("Original Text is ", txt));

                cc.EnterText(driver, "This is a test", ser.textArea);
                cc.ClickButton(driver, ser.saveBtn);
                Thread.Sleep(500);

                StringAssert.Contains("True", (txt != cc.FetchTextValue(driver, ser.textArea)).ToString());
                tglog.AppendLogText(filePath, string.Concat("New Text is ", cc.FetchTextValue(driver, ser.textArea)).ToString());
                tglog.AppendLogText(filePath, string.Concat("Original Text Value Does Not Equal Current Text Value..... PASSED!", Environment.NewLine));
                passFail = 0;

                // reset original
                ResetAddRegion();
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        [TestMethod, TestCategory("R2 - Serialziation")]
        public void Settings_Serialziation_ChangeTransform()
        {
            string transform = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 11128279;
                testName = "Settings_Serialziation_ChangeTransform_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14602766;
                    testName = "Settings_Serialziation_ChangeTransform_Stage";
                }
                //else
                //{
                //    if (qaDev.ToLower() == beta)
                //    {
                //        //instance = 4998768;
                //        testName = "Settings_Serialziation_ChangeTransform_Beta";
                //    }
                    else
                    {
                        //instance = 11263418;
                        testName = "Settings_Serialziation_ChangeTransform_Prod";
                    }
                //}
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                //setup
                SetupSerializationRegion();
                //SetupBarcodeSetting();

                //test
                transform = cc.FetchTextValue(driver, ser.transform);
                tglog.AppendLogText(filePath, string.Concat("Original Transform is ", transform));

                cc.SelectDropDownByElement(driver, ser.transform, "ToUpper");
                cc.ClickButton(driver, ser.saveBtn);
                Thread.Sleep(500);

                StringAssert.Contains("True", (transform != cc.FetchTextValue(driver, ser.transform)).ToString());
                tglog.AppendLogText(filePath, string.Concat("Current Transform is ", cc.FetchTextValue(driver, ser.transform)).ToString());
                tglog.AppendLogText(filePath, string.Concat("Original Transform Value Does Not Equal Current Transform Value..... PASSED!", Environment.NewLine));
                passFail = 0;

                // reset original
                ResetAddRegion();
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        [TestMethod, TestCategory("R1 - Serialziation")]
        public void R1_Serialziation_DisableBarCode()
        {
            string disableEnable = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 11128276;
                testName = "Settings_Serialziation_DisableBarCode_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14602680;
                    testName = "Settings_Serialziation_DisableBarCode_Stage";
                }
                //else
                //{
                //    if (qaDev.ToLower() == beta)
                //    {
                //        //instance = 4998768;
                //        testName = "Settings_Serialziation_AddJobMetadata_Beta";
                //    }
                else
                {
                    instance = 14823118;
                    testName = "Settings_Serialziation_DisableBarCode_Prod";
                }
                //}
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                //setup
                SetupSerializationRegion();

                //test
                disableEnable = cc.FetchChecked(driver, ser.disableEnableBarCode);
                tglog.AppendLogText(filePath, string.Concat("Original Setting is ", disableEnable));

                cc.ClickButton(driver, ser.disableEnableBarCode2);
                Thread.Sleep(2000);

                tglog.AppendLogText(filePath, string.Concat("Current Setting is ", cc.FetchChecked(driver, ser.disableEnableBarCode).ToString()));
                StringAssert.Contains("True", (disableEnable != cc.FetchChecked(driver, ser.disableEnableBarCode)).ToString());
                tglog.AppendLogText(filePath, string.Concat("Original Value Does Not Equal Current Value..... PASSED!", Environment.NewLine));
                passFail = 0;

                // reset original
                ResetAddRegion();
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        [TestMethod, TestCategory("R1 - Serialziation")]
        public void R1_Serialziation_DisableSerialization()
        {
            string disableEnable = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 11128276;
                testName = "Settings_Serialziation_DisableSerialization_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14602679;
                    testName = "Settings_Serialziation_DisableSerialization_Stage";
                }
                //else
                //{
                //    if (qaDev.ToLower() == beta)
                //    {
                //        //instance = 4998768;
                //        testName = "Settings_Serialziation_AddJobMetadata_Beta";
                //    }
                else
                {
                    instance = 14823117;
                    testName = "Settings_Serialziation_DisableSerialization_Prod";
                }
                //}
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                //setup
                SetupSerializationRegion();
                //SetupBarcodeSetting();

                //test
                disableEnable = cc.FetchChecked(driver, ser.disableEnableSerialization);
                tglog.AppendLogText(filePath, string.Concat("Original Setting is ", disableEnable));

                //cc.SelectDropDownByOption(driver, ser.jobMetadata, 3);
                cc.ClickButton(driver, ser.disableEnableSerialization2);
                Thread.Sleep(2000);

                //newTxt = cc.FetchTextValue(driver, ser.textArea);
                tglog.AppendLogText(filePath, string.Concat("Current Setting is ", cc.FetchChecked(driver, ser.disableEnableSerialization).ToString()));
                StringAssert.Contains("True", (disableEnable != cc.FetchChecked(driver, ser.disableEnableSerialization)).ToString());
                tglog.AppendLogText(filePath, string.Concat("Original Text Value Does Not Equal Current Text Value..... PASSED!", Environment.NewLine));
                passFail = 0;

                // reset original
                ResetAddRegion();
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        [TestMethod, TestCategory("R2 - Serialziation")]
        public void Settings_Serialziation_TextandJobMetadata()
        {
            string newTxt = "";
            string txt = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 11128276;
                testName = "Settings_Serialziation_TextandJobMetadata_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14602763;
                    testName = "Settings_Serialziation_TextandJobMetadata_Stage";
                }
                //else
                //{
                //    if (qaDev.ToLower() == beta)
                //    {
                //        //instance = 4998768;
                //        testName = "Settings_Serialziation_AddJobMetadata_Beta";
                //    }
                else
                {
                    //instance = 11263415;
                    testName = "Settings_Serialziation_TextandJobMetadata_Prod";
                }
                //}
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                //setup
                SetupSerializationRegion();
                //SetupBarcodeSetting();

                //test
                cc.EnterText(driver, "This is a test - ", ser.textArea);
                cc.ClickButton(driver, ser.saveBtn);
                Thread.Sleep(500);

                txt = cc.FetchTextValue(driver, ser.textArea);
                tglog.AppendLogText(filePath, string.Concat("Original Text is ", txt));

                cc.SelectDropDownByOption(driver, ser.jobMetadata, 3);
                cc.ClickButton(driver, ser.saveBtn);
                Thread.Sleep(500);

                newTxt = cc.FetchTextValue(driver, ser.textArea);
                tglog.AppendLogText(filePath, string.Concat("New Text is ", newTxt));
                StringAssert.Contains("True", (txt != newTxt).ToString());
                tglog.AppendLogText(filePath, string.Concat("Original Text Value Does Not Equal Current Text Value..... PASSED!", Environment.NewLine));
                passFail = 0;

                // reset original
                ResetAddRegion();
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        #endregion

        #region Serialization - Barcode
        [TestMethod, TestCategory("R2 - Serialziation")]
        public void Settings_Serialziation_ChangeBarcodeBeforeText()
        {
            string barcodeBeforText = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 11128283;
                testName = "Settings_Serialziation_ChangeBarcodeBeforeText_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14602769;
                    testName = "Settings_Serialziation_ChangeBarcodeBeforeText_Stage";
                }
                //else
                //{
                //    if (qaDev.ToLower() == beta)
                //    {
                //        //instance = 4998768;
                //        testName = "Settings_Serialziation_ChangeBarcodeBeforeText_Beta";
                //    }
                    else
                    {
                        //instance = 11263422;
                        testName = "Settings_Serialziation_ChangeBarcodeBeforeText_Prod";
                    }
                //}
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                //setup
                SetupSerializationRegion();

                cc.EnterText(driver, "This is a test", ser.textArea);
                cc.ClickButton(driver, ser.saveBtn);
                Thread.Sleep(500);

                //test
                barcodeBeforText = cc.FetchChecked(driver, ser.beforeText);
                tglog.AppendLogText(filePath, string.Concat("Original Barcode Before Text is ", barcodeBeforText));

                //Thread.Sleep(3000);
                cc.ClickButton(driver, ser.beforeText2);
                cc.ClickButton(driver, ser.saveBtn);
                Thread.Sleep(500);

                StringAssert.Contains("True", (barcodeBeforText != cc.FetchChecked(driver, ser.beforeText).ToString()).ToString());
                tglog.AppendLogText(filePath, string.Concat("Current Barcode Before Text is ", cc.FetchChecked(driver, ser.beforeText).ToString()).ToString());
                tglog.AppendLogText(filePath, string.Concat("Clicked Barcode Before Text ", Environment.NewLine));
                passFail = 0;

                // reset original
                ResetAddRegion();
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        [TestMethod, TestCategory("R2 - Serialziation")]
        public void Settings_Serialziation_ChangeBarcodeSize()
        {
            string size = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 11128280;
                testName = "Settings_Serialziation_ChangeBarcodeSize_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14602767;
                    testName = "Settings_Serialziation_ChangeBarcodeSize_Stage";
                }
                //else
                //{
                //    if (qaDev.ToLower() == beta)
                //    {
                //        //instance = 4998768;
                //        testName = "Settings_Serialziation_ChangeBarcodeSize_Beta";
                //    }
                    else
                    {
                        //instance = 11263419;
                        testName = "Settings_Serialziation_ChangeBarcodeSize_Prod";
                    }
                //}
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                //setup
                SetupSerializationRegion();
                //SetupSerializationSettings();

                //test
                size = cc.FetchTextValue(driver, ser.barcodeSize);
                tglog.AppendLogText(filePath, string.Concat("Original Barcode Size is ", size));

                cc.SelectDropDownByElement(driver, ser.barcodeSize, "Large");
                cc.ClickButton(driver, ser.saveBtn);
                //Thread.Sleep(3000);

                StringAssert.Contains("True", (size != cc.FetchTextValue(driver, ser.barcodeSize)).ToString());
                tglog.AppendLogText(filePath, string.Concat("Current Barcode Size is ", cc.FetchTextValue(driver, ser.barcodeSize)).ToString());
                tglog.AppendLogText(filePath, string.Concat("Original Barcode Size Value Does Not Equal Current Barcode Size Value..... PASSED!", Environment.NewLine));
                passFail = 0;

                // reset original
                ResetAddRegion();
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        [TestMethod, TestCategory("R2 - Serialziation")]
        public void Settings_Serialziation_ChangeBarcodeType()
        {
            string bType = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 11128281;
                testName = "Settings_Serialziation_ChangeBarcodeType_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14602770;
                    testName = "Settings_Serialziation_ChangeBarcodeType_Stage";
                }
                //else
                //{
                //    if (qaDev.ToLower() == beta)
                //    {
                //        //instance = 4998768;
                //        testName = "Settings_Serialziation_ChangeBarcodeType_Beta";
                //    }
                    else
                    {
                        //instance = 11263420;
                        testName = "Settings_Serialziation_ChangeBarcodeType_Prod";
                    }
                //}
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                //setup
                SetupSerializationRegion();
                //SetupSerializationSettings();

                //test
                bType = cc.FetchTextValue(driver, ser.barcodeType);
                tglog.AppendLogText(filePath, string.Concat("Barcode Type is ", bType));
                tglog.AppendLogText(filePath, string.Concat("Barcode Type count is ", cc.FetchChildCount(driver, ser.barcodeType)));

                passFail = 0;

                // reset original
                ResetAddRegion();
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        [TestMethod, TestCategory("R2 - Serialziation")]
        public void Settings_Serialziation_IncludePageNumber()
        {
            string includePage = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 11128282;
                testName = "Settings_Serialziation_IncludePageNumber_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14602768;
                    testName = "Settings_Serialziation_IncludePageNumber_Stage";
                }
                //else
                //{
                //    if (qaDev.ToLower() == beta)
                //    {
                //        //instance = 4998768;
                //        testName = "Settings_Serialziation_IncludePageNumber_Beta";
                //    }
                    else
                    {
                        //instance = 11263421;
                        testName = "Settings_Serialziation_IncludePageNumber_Prod";
                    }
                //}
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                SetupSerializationRegion();
                //SetupSerializationSettings();

                //test
                includePage = cc.FetchChecked(driver, ser.includePageNumber);
                tglog.AppendLogText(filePath, string.Concat("Original Include Page Number is ", includePage));

                //Thread.Sleep(3000);
                cc.ClickButton(driver, ser.includePageNumber2);
                cc.ClickButton(driver, ser.saveBtn);
                Thread.Sleep(500);

                StringAssert.Contains("True", (includePage != cc.FetchChecked(driver, ser.includePageNumber).ToString()).ToString());
                tglog.AppendLogText(filePath, string.Concat("Original Include Page Number is ", cc.FetchChecked(driver, ser.includePageNumber).ToString()).ToString());
                tglog.AppendLogText(filePath, string.Concat("Clicked Include Page Number", Environment.NewLine));
                passFail = 0;

                // reset original
                ResetAddRegion();

            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }
        #endregion

        //-------------------------------------------------------------------------------------------
        #region Methods
        private void AddPrintAgent()
        {
            cspa = spa.CreatePrintAgent(driver);
            StringAssert.Contains(cc.FetchInnerHTML(driver, cspa.Header), "Create Print Agent");
            tglog.AppendLogText(filePath, "Opened Create Print Agent ..... OK");
            Thread.Sleep(500);

            printerName = "VPN Test";
            cc.EnterText(driver, printerName, cspa.VPN);
            cc.ScrollPage(driver);
            Thread.Sleep(500);
            printClient = cc.FetchTextValue(driver, cspa.printClient);

            cc.ClickButton(driver, cspa.fileOutputTab);
            Thread.Sleep(500);
            cc.ClickButton(driver, cspa.enableFileOutput);
            Thread.Sleep(500);
            cc.EnterText(driver, outputFilePath, cspa.xpsFile);

            tglog.AppendLogText(filePath, string.Concat("Printer Name = ", printerName));
            tglog.AppendLogText(filePath, string.Concat("Print Client = ", printClient));
            tglog.AppendLogText(filePath, string.Concat("XPS File Path = ", outputFilePath));

            cc.ClickButton(driver, cspa.saveBtn);
        }

        private void CreateLogFile()
        {
            tglog.CreateLogFile(filePath, string.Concat(Environment.NewLine, "----------- Start testing using ", brow, " -----------", Environment.NewLine));
            tglog.AppendLogText(filePath, string.Concat("Set up ", brow, " ..... OK"));
            tglog.AppendLogText(filePath, string.Concat("URL = ", baseURL));
            tglog.AppendLogText(filePath, string.Concat("Envionment = ", qaDev));
            tglog.AppendLogText(filePath, string.Concat("User = ", user));
            tglog.AppendLogText(filePath, "Opened SecureDocs in browser ..... OK");
            tglog.AppendLogText(filePath, string.Concat("Build Version: ", build));
            tglog.AppendLogText(filePath, "Logged into SecureDocs ..... OK");
            tglog.AppendLogText(filePath, string.Concat("Instance = ", instance));
            tglog.AppendLogText(filePath, string.Concat("FilePath = ", filePath));
            tglog.AppendLogText(filePath, "Opened SecureDocs Dashboard ..... OK");
        }

        private void OpenPrintAgents()
        {
            try
            {
                spa = mp.OpenPrintAgent(driver);

                StringAssert.Contains(cc.FetchInnerHTML(driver, spa.Header), "Print Agents");
                passFail = 0;
                tglog.AppendLogText(filePath, "Opened Secure Print Agents ..... OK");

            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        private void OpenSecurityTemplates()
        {
            try
            {
                st = mp.OpenSecurityTemplates(driver);
                passFail = 0;
                tglog.AppendLogText(filePath, "Opened Security Templates ..... OK");

            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        private string UploadFile(string text, string filename, string picPath, string title)
        {
            string ele = "";
            string imgpath = "";
            int itemCount = 0;
            string secureBtn = "";

            OpenSecurityTemplates();
            baseWindowHandle = driver.CurrentWindowHandle;

            itemCount = Convert.ToInt16(cc.FetchChildCount(driver, st.templateGrid));
            for (int x = 1; x <= itemCount; x++)
            {
                secureBtn = string.Concat(st.templateGrid, "/div[", x.ToString(), "]/div[1]/div/a[2]");
                ele = string.Concat(st.templateGrid, "/div[", x.ToString(), "]/div[2]/a");
                templateName = cc.FetchInnerHTML(driver, ele);

                if (templateName == text)
                {
                    //***** Show Upload
                    sst = st.OpenShowUpload(driver,secureBtn);

                    StringAssert.Contains(cc.FetchInnerText(driver, sst.Header), "Secure Document with ");
                    tglog.AppendLogText(filePath, "Opened Secure Document popup ..... OK");
                    Thread.Sleep(1000);

                    cc.ClickButton(driver, sst.browseBtn);
                    Thread.Sleep(500);

                    cc.UploadFile("C:\\Test Files\\", filename);
                    tglog.AppendLogText(filePath, "Uploaded File ..... OK");
                    Thread.Sleep(500);

                    actd = sst.OpenSecuredDocumentViewer(driver);
                    Thread.Sleep(15000);
                    tglog.AppendLogText(filePath, "Secured Document Shown in Viewer..... OK");
                    imgpath = cc.TakeScreenShotWithDate(driver, picPath, title, ScreenshotImageFormat.Png, 0);
                    cc.ClickButton(driver, actd.backToDashboard);
                    Thread.Sleep(500);
                    tglog.AppendLogText(filePath, "Closed Secured Document Viewer and Returned to Dashboard ..... OK");
                    driver.SwitchTo().Window(baseWindowHandle);
                    break;
                }
            }
            return imgpath;
        }

        private void RemovePrintAgent()
        {
            rows = Convert.ToInt16(cc.FetchTableRows(driver, spa.agentTable2));

            for (int x = 1; x <= rows; x++)
            {
                itemCnt = Convert.ToInt16(cc.FetchChildCount(driver, spa.agentTable2));
                if (cc.FetchCellValue(driver, "1", x.ToString(), spa.agentTable2).Contains("VPN Test"))
                {
                    cc.ClickButton(driver, string.Concat(spa.agentTable2, "/tr[", x.ToString(), "]/td[5]/form/button"));
                    Thread.Sleep(500);
                }
            }
        }

        private void RemovePrintAgent(int rows)
        {
            for (int x = 1; x <= rows; x++)
            {
                itemCnt = Convert.ToInt16(cc.FetchChildCount(driver, spa.agentTable));
                if (cc.FetchCellValue(driver, "1", x.ToString(), spa.agentTable).Contains("VPN Test"))
                {
                    cc.ClickButton(driver, string.Concat(spa.agentTable, "/tr[", x.ToString(), "]/td[5]/form/button"));
                    Thread.Sleep(500);
                }
            }
        }

        private void ResetAddRegion()
        {
            cc.ClickButton(driver, ser.removeButton);
            tglog.AppendLogText(filePath, "Removed Added Region");

            cc.ClickButton(driver, ser.addVisualRegion2);
            cc.ClickButton(driver, ser.saveBtn);
            //Thread.Sleep(3000);

            tglog.AppendLogText(filePath, string.Concat("Reset Add Visual Region", Environment.NewLine));
            Thread.Sleep(1000);
        }

        private void SetupAuthPortalSettings()
        {
            cc.EnterText(driver, "TROY Security Solutions", auth.companyName);
            cc.EnterText(driver, "TROYGroup.com", auth.companyEmail);
        }

        private void SetupBarcodeSetting()
        {
            tglog.AppendLogText(filePath, string.Concat("Barcode Status is ", cc.FetchChecked(driver, ser.barcode).ToString()));
            cc.ClickButton(driver, ser.barcode2);
            tglog.AppendLogText(filePath, string.Concat("Barcode Area Is Not Available", Environment.NewLine));
        }

        private void SetupSerializationSettings()
        {
            tglog.AppendLogText(filePath, string.Concat("Serialization is ", cc.FetchChecked(driver, ser.serialization).ToString()));
            cc.ClickButton(driver, ser.serialization2);
            tglog.AppendLogText(filePath, string.Concat("Serialization Area Is Not Available", Environment.NewLine));
        }

        private void SetupDriver()
        {
            if ((brow.ToLower() == "ie"))
            {
                this.driver = new InternetExplorerDriver("C:\\Selenium");
            }

            if ((brow.ToLower() == "chrome"))
            {
                this.driver = new ChromeDriver(@"C:\Selenium");
            }

            if ((brow.ToLower() == "firefox"))
            {
                this.driver = new FirefoxDriver();
            }

        }

        private void SetupSerializationRegion()
        {
            string addRegion = "";

            ser = mp.OpenSerialization(driver);
            //tglog.AppendLogText(filePath, driver.Title);

            StringAssert.Contains(cc.FetchInnerHTML(driver, ser.Header), "Serialization");
            tglog.AppendLogText(filePath, "Opened Serialization Settings page ..... OK");

            addRegion = cc.FetchChecked(driver, ser.addVisualRegion).ToString();
            tglog.AppendLogText(filePath, string.Concat("Original Add Visual Region setting is ", addRegion));

            Thread.Sleep(500);
            cc.ClickButton(driver, ser.addVisualRegion2);
            Thread.Sleep(500);
            tglog.AppendLogText(filePath, "Clicked Add Visual Region");

            cc.ClickButton(driver, ser.addRegionBtn);
            tglog.AppendLogText(filePath, string.Concat("Added Visual Region", Environment.NewLine));
            Thread.Sleep(500);
        }

        private void SetUpEnvironVars()
        {
            if (qaDev.ToLower() == dev)
            {
                filePath = string.Concat(filePath, "Dev\\");
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    filePath = string.Concat(filePath, "Stage\\");
                }
                    else
                    {
                        filePath = string.Concat(filePath, "Prod\\");
                    }
            }
        }

        private void SendToPractitest()
        {
            if (qaDev.ToUpper() == "DEV")
            {
                ptc.PT_Post_Specific_Run(filePath, passFail, resultMsg, instance, project, brow, "Development");
            }
            else
            {
                if (qaDev.ToUpper() == "STAGE")
                {
                    ptc.PT_Post_Specific_Run(filePath, passFail, resultMsg, instance, project, brow, "Stage");
                }
                    else
                    {
                        ptc.PT_Post_Specific_Run(filePath, passFail, resultMsg, instance, project, brow, "Production");
                    }
            }
        }
        #endregion

        //--------------------------------------------------------------------------------------------
        #region Additional test attributes
        [TestInitialize()]
        public void MyTestInitialize()
        {
            cc = new Common.Common_Code(driver);

            brow = ConfigurationManager.AppSettings["browser"];
            baseURL = ConfigurationManager.AppSettings["DocrityURL"];
            project = ConfigurationManager.AppSettings["project"];
            qaDev = ConfigurationManager.AppSettings["testEnv"];
            user = ConfigurationManager.AppSettings["testUser"].ToUpper();
            string pswd = ConfigurationManager.AppSettings["TestPswd"];

            SetupDriver();
            SetUpEnvironVars();

            lp = new SecureRx.Page_Elements.LoginPage(driver, baseURL);
            build = lp.FetchBuildVersion(driver);
            mp = lp.SDLogin(user, pswd, driver);

            //Did Home Menu open
            if (mp == null)
            {
                throw new NoSuchWindowException("Docrity home page was not displayed!");
            }
            else
            {
                passFail = 0;
            }
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {

            driver.SwitchTo().Window(baseWindowHandle);
            Thread.Sleep(1000);

            try
            {
                mp.LogoutandCloseBrowser(driver);
                tglog.AppendLogText(filePath, "Logged out and Closed browser..... OK");
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }

            if (passFail != 0)
            {
                resultMsg = string.Concat(testName, " ..... FAILED!!!!! Please refer to log file");
                //driver.Close();
            }
            else
            {
                resultMsg = string.Concat(testName, " ..... PASSED!!!!!");
            }

            SendToPractitest();

            tglog.AppendLogText(filePath, "Close Docrity");
            tglog.AppendLogText(filePath, "\n Test Finished \n");
        }

        #endregion

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;
    }
}