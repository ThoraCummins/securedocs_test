﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Data;
using System.IO;
using System.Net;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;

namespace SecureRx.Tests
{
    [TestClass]
    public class Exclusion_Tests
    {
        #region libraries
        SecureRx.Page_Elements.MainPage mp = null;
        SecureRx.Page_Elements.LoginPage lp = null;

        SecureRx.Page_Elements.Templates.Name_Template nt = null;
        SecureRx.Page_Elements.Templates.SecurityTemplates st = null;
        SecureRx.Page_Elements.Templates.Create_Security_Templates cst = null;
        SecureRx.Page_Elements.Templates.Remove_Security_Template rst = null;
        SecureRx.Page_Elements.Templates.Template tst = null;

        SecureRx.Common.Common_Code cc = null;
        SecureRx.Common.PT_Client ptc = new SecureRx.Common.PT_Client();
        SecureRx.Common.TGLogging tglog = new SecureRx.Common.TGLogging();
        #endregion

        //Vars
        private string baseURL = "";
        private string baseWindowHandle = "";
        //private string beta = "beta";
        private string brow = "";
        private string build = "";
        private string dev = "dev";
        private IWebDriver driver = null;
        private string filePath = "C:\\Test Logs\\SecureRx\\";
        private string imagePath = "";
        public string imagePath2 = "";
        public string imagePath1 = "";
        private int instance = 0;
        private int passFail = 1;
        private string project = "4896";
        private string qaDev = "";
        private string resultMsg = "";
        private string staging = "stage";
        private string testName = "";
        private string user = "";

        [TestMethod, TestCategory("R1 - Designer")]
        public void Striped_WatermarkExclusion_Exclude_TROYMark()
        {
            string templateName = "";
            string imagePath2 = "";
            string imagePath1 = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 10614864;
                testName = "StripedWatermarkExclusion_Exclude_TROYMark_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14921953;
                    testName = "StripedWatermarkExclusion_Exclude_TROYMark_Stage";
                }
                else
                {
                    //instance = 13109755;
                    testName = "StripedWatermarkExclusion_Exclude_TROYMark_Prod";
                }
            }
            passFail = 1;  //reset before test
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Striped Watermark Exclusion-DO NOT USE";
            CreateLogFile();

            //setup
            CreateTemplateUnderTest(templateName);

            Thread.Sleep(8000);
            tglog.AppendLogText(filePath, "Opened a Security Template ..... OK");

            try
            {
                tst.AddStripedWatermarkExclusion(driver);
                Thread.Sleep(2000);
                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Striped Watermark Exclude TROYMark ", ScreenshotImageFormat.Png, 0);
                Thread.Sleep(3000);
                tglog.AppendLogText(filePath, "Added StripedWatermark with Exclusion.....OK");

                //Test
                tglog.AppendLogText(filePath, cc.FetchChecked(driver, tst.exclusion_TROYMark));
                Thread.Sleep(3000);

                cc.ClickButton(driver, tst.exclusion_TROYMark);
                Thread.Sleep(2000);

                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Striped Watermark Exclude TROYMark ", ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);

                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                passFail = 0;

                //Finish and Teardown
                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template Changes.....OK");
                Thread.Sleep(5000);

                tst.ExitDesigner(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }
            catch (Exception e)
            {
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Finish and Teardown
                tst.ExitDesigner2(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        [TestMethod, TestCategory("R2 - Designer")]
        public void Striped_WatermarkExclusion_Exclude_UnexcludeTROYMark()
        {
            string templateName = "";
            string imagePath2 = "";
            string imagePath1 = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 10614864;
                testName = "StripedWatermarkExclusion_Exclude_UnexcludeTROYMark_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14922032;
                    testName = "StripedWatermarkExclusion_Exclude_UnexcludeTROYMark_Stage";
                }
                else
                {
                    instance = 13109754;
                    testName = "StripedWatermarkExclusion_Exclude_UnexcludeTROYMark_Prod";
                }
            }
            passFail = 1;  //reset before test
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Striped Watermark Exclusion-DO NOT USE";
            CreateLogFile();

            //setup
            CreateTemplateUnderTest(templateName);

            Thread.Sleep(8000);
            tglog.AppendLogText(filePath, "Opened a Security Template ..... OK");

            try
            {
                tst.AddStripedWatermarkExclusion(driver);
                Thread.Sleep(2000);
                tglog.AppendLogText(filePath, "Added StripedWatermark with Exclusion.....OK");

                //Test
                cc.ClickButton(driver, tst.exclusion_TROYMark);
                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Striped Watermark unExclude Pantograph ", ScreenshotImageFormat.Png, 0);
                Thread.Sleep(3000);
                tglog.AppendLogText(filePath, cc.FetchChecked(driver, tst.exclusion_TROYMark));

                cc.ClickButton(driver, tst.exclusion_TROYMark);
                tglog.AppendLogText(filePath, cc.FetchChecked(driver, tst.exclusion_TROYMark));
                Thread.Sleep(3000);
                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Striped Watermark unExclude TROYMark ", ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);

                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                passFail = 0;

                //Finish and Teardown
                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template Changes.....OK");
                Thread.Sleep(5000);

                tst.ExitDesigner(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }
            catch (Exception e)
            {
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Finish and Teardown
                tst.ExitDesigner2(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        [TestMethod, TestCategory("R1 - Designer")]
        public void Striped_WatermarkExclusion_Position_Move()
        {
            string templateName = "";
            string testValu = "";
            string testValu2 = "";
            string imagePath2 = "";
            string imagePath1 = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 10614864;
                testName = "StripedWatermarkExclusion_Position_Move_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14921950;
                    testName = "StripedWatermarkExclusion_Position_Move_Stage";
                }
                else
                {
                    //instance = 13109752;
                    testName = "StripedWatermarkExclusion_Position_Move_Prod";
                }
            }
            passFail = 1;  //reset before test
            testValu = "300";
            testValu2 = "200";
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Striped Watermark Exclusion-DO NOT USE";
            CreateLogFile();

            //setup
            CreateTemplateUnderTest(templateName);

            Thread.Sleep(8000);
            tglog.AppendLogText(filePath, "Opened a Security Template ..... OK");

            try
            {
                tst.AddStripedWatermarkExclusion(driver);
                Thread.Sleep(2000);
                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Striped Watermark Exclusion Position ", ScreenshotImageFormat.Png, 0);
                Thread.Sleep(3000);
                tglog.AppendLogText(filePath, "Added StripedWatermark with Exclusion.....OK");

                //Test
                cc.EnterText(driver, testValu2, tst.troyMark_X);
                tglog.AppendLogText(filePath, string.Concat("Striped Watermark Exclusion X Coordinate = ", testValu2));

                cc.EnterText(driver, testValu, tst.troyMark_Y);
                tglog.AppendLogText(filePath, string.Concat("Striped Watermark Exclusion Y Coordinate = ", testValu));

                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Striped Watermark Exclusion Position ", ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);

                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                passFail = 0;

                //Finish and Teardown
                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template Changes.....OK");
                Thread.Sleep(5000);

                tst.ExitDesigner(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }
            catch (Exception e)
            {
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Finish and Teardown
                tst.ExitDesigner2(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        [TestMethod, TestCategory("R1 - Designer")]
        public void Striped_WatermarkExclusion_Position_Resize()
        {
            string templateName = "";
            string testValu = "";
            string testValu2 = "";
            string imagePath2 = "";
            string imagePath1 = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 10614864;
                testName = "StripedWatermarkExclusion_Position_Resize_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14921951;
                    testName = "StripedWatermarkExclusion_Position_Resize_Stage";
                }
                else
                {
                    //instance = 13109753;
                    testName = "StripedWatermarkExclusion_Position_Resize_Prod";
                }
            }
            passFail = 1;  //reset before test
            testValu = "300";
            testValu2 = "200";
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Striped Watermark Exclusion-DO NOT USE";
            CreateLogFile();

            //setup
            CreateTemplateUnderTest(templateName);

            Thread.Sleep(8000);
            tglog.AppendLogText(filePath, "Opened a Security Template ..... OK");

            try
            {
                tst.AddStripedWatermarkExclusion(driver);
                Thread.Sleep(2000);
                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Striped Watermark Exclusion Width & Height ", ScreenshotImageFormat.Png, 0);
                Thread.Sleep(3000);
                tglog.AppendLogText(filePath, "Added StripedWatermark with Exclusion.....OK");

                //Test
                cc.EnterText(driver, testValu2, tst.troyMark_Width);
                tglog.AppendLogText(filePath, string.Concat("Striped Watermark Exclusion Width = ", testValu2));

                cc.EnterText(driver, testValu, tst.troyMark_Height);
                tglog.AppendLogText(filePath, string.Concat("Striped Watermark Exclusion Height = ", testValu));

                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Striped Watermark Exclusion Width & Height ", ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);

                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                passFail = 0;

                //Finish and Teardown
                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template Changes.....OK");
                Thread.Sleep(5000);

                tst.ExitDesigner(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }
            catch (Exception e)
            {
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Finish and Teardown
                tst.ExitDesigner2(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        //[TestMethod, TestCategory("Templates - Striped Watermark Exclusion Tests")]
        //public void Exclusion_Position_Resize()
        //{
        //    string templateName = "";
        //    string testValu = "";
        //    string testValu2 = "";

        //    if (qaDev.ToLower() == dev)
        //    {
        //        instance = 10614865;
        //        testName = "Exclusion_Position_Resize_Dev";
        //    }
        //    else
        //    {
        //        if (qaDev.ToLower() == qa)
        //        {
        //            instance = 11415336;
        //            testName = "Exclusion_Position_Resize_QA";
        //        }
        //        //    else
        //        //    {
        //        //        if (qaDev.ToLower() == beta)
        //        //        {
        //        //            instance = 7225653;
        //        //            testName = "Exclusion_Position_Resize_Beta";
        //        //        }
        //        else
        //        {
        //            instance = 11583209;
        //            testName = "Exclusion_Position_Resize_Prod";
        //        }
        //        //        }
        //        //    }
        //    }
        //    passFail = 1;  //reset before test
        //    testValu = "300";
        //    testValu2 = "200";
        //    imagePath = string.Concat(filePath, "Test Files\\");
        //    imagePath1 = imagePath;
        //    imagePath2 = imagePath;
        //    filePath = string.Concat(filePath, testName, ".txt");
        //    templateName = "Exclusion-DO NOT USE";
        //    CreateLogFile();

        //    try
        //    {
        //        //setup
        //        CreateTemplateUnderTest(templateName);
        //        baseWindowHandle = driver.CurrentWindowHandle;

        //        //Test
        //        SetupExclusion();

        //        imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, string.Concat("Exclusion Height and Width Change ", testValu, " "), ScreenshotImageFormat.Png, 0);
        //        Thread.Sleep(3000);

        //        cc.EnterText(driver, testValu2, tst.elementWidth);
        //        tglog.AppendLogText(filePath, string.Concat("Exclusion Width = ", testValu2));

        //        cc.EnterText(driver, testValu, tst.elementHeight);
        //        tglog.AppendLogText(filePath, string.Concat("Exclusion Height = ", testValu));

        //        imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, string.Concat("Exclusion Height and Width Change ", testValu, " "), ScreenshotImageFormat.Png, 1);
        //        Thread.Sleep(3000);

        //        cc.ClickButton(driver, tst.saveButton);
        //        tglog.AppendLogText(filePath, "Saved Template ..... OK");
        //        Thread.Sleep(3000);

        //        cc.ClickButton(driver, tst.templateHomeBtn);
        //        tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
        //        Thread.Sleep(3000);
        //        cc.ScrollPage(driver);

        //        //Tear Down
        //        RemoveTemplateUnderTest(templateName);

        //        StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
        //        tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
        //        passFail = 0;
        //    }
        //    catch (Exception e)
        //    {
        //        passFail = 1;
        //        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

        //        //Tear Down
        //        cc.ClickButton(driver, tst.templateHomeBtn);
        //        tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
        //        Thread.Sleep(5000);
        //        cc.ScrollPage(driver);
        //        RemoveTemplateUnderTest(templateName);
        //        Thread.Sleep(5000);
        //    }

        //    Thread.Sleep(5000);
        //}

        //[TestMethod, TestCategory("Templates - Striped Watermark Exclusion Tests")]
        //public void Exclusion_Pantograph_Exclude()
        //{
        //    string templateName = "";

        //    if (qaDev.ToLower() == dev)
        //    {
        //        instance = 10614866;
        //        testName = "Exclusion_Pantograph_Exclude_Dev";
        //    }
        //    else
        //    {
        //        if (qaDev.ToLower() == qa)
        //        {
        //            instance = 11415337;
        //            testName = "Exclusion_Pantograph_Exclude_QA";
        //        }
        //        //    else
        //        //    {
        //        //        if (qaDev.ToLower() == beta)
        //        //        {
        //        //            instance = 7225653;
        //        //            testName = "Exclusion_Pantograph_Exclude_Beta";
        //        //        }
        //        else
        //        {
        //            instance = 11583210;
        //            testName = "Exclusion_Pantograph_Exclude_Prod";
        //        }
        //        //        }
        //        //    }
        //    }
        //    passFail = 1;  //reset before test
        //    imagePath = string.Concat(filePath, "Test Files\\");
        //    imagePath1 = imagePath;
        //    imagePath2 = imagePath;
        //    filePath = string.Concat(filePath, testName, ".txt");
        //    templateName = "Exclusion-DO NOT USE";
        //    CreateLogFile();

        //    try
        //    {
        //        //setup
        //        CreateTemplateUnderTest(templateName);
        //        baseWindowHandle = driver.CurrentWindowHandle;

        //        SetupPantograph();

        //        //Test
        //        SetupExclusion();

        //        imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, string.Concat("Exclude Pantograph ", cc.FetchChecked(driver, tst.exclusion_Pantograph), " "), ScreenshotImageFormat.Png, 0);
        //        tglog.AppendLogText(filePath, string.Concat("Pantograpsh = ", cc.FetchChecked(driver, tst.exclusion_Pantograph)));
        //        Thread.Sleep(3000);

        //        cc.ClickButton(driver, tst.exclusion_Pantograph);
        //        tglog.AppendLogText(filePath, string.Concat("Exclusion Pantograph = ", cc.FetchChecked(driver, tst.exclusion_Pantograph)));

        //        imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, string.Concat("Exclude Pantograph ", cc.FetchChecked(driver, tst.exclusion_Pantograph), " "), ScreenshotImageFormat.Png, 1);
        //        Thread.Sleep(3000);

        //        cc.ClickButton(driver, tst.saveButton);
        //        tglog.AppendLogText(filePath, "Saved Template ..... OK");
        //        Thread.Sleep(5000);

        //        cc.ClickButton(driver, tst.templateHomeBtn);
        //        tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
        //        Thread.Sleep(3000);
        //        cc.ScrollPage(driver);

        //        //Tear Down
        //        RemoveTemplateUnderTest(templateName);

        //        StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
        //        tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
        //        passFail = 0;
        //    }
        //    catch (Exception e)
        //    {
        //        passFail = 1;
        //        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

        //        //Tear Down
        //        cc.ClickButton(driver, tst.templateHomeBtn);
        //        tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
        //        Thread.Sleep(5000);
        //        cc.ScrollPage(driver);
        //        RemoveTemplateUnderTest(templateName);
        //        Thread.Sleep(5000);
        //    }

        //    Thread.Sleep(5000);
        //}

        //[TestMethod, TestCategory("Templates - Striped Watermark Exclusion Tests")]
        //public void Exclusion_TROYMark_Exclude()
        //{
        //    string templateName = "";

        //    if (qaDev.ToLower() == dev)
        //    {
        //        instance = 10614867;
        //        testName = "Exclusion_TROYMark_Exclude_Dev";
        //    }
        //    else
        //    {
        //        if (qaDev.ToLower() == qa)
        //        {
        //            instance = 11415338;
        //            testName = "Exclusion_TROYMark_Exclude_QA";
        //        }
        //        //    else
        //        //    {
        //        //        if (qaDev.ToLower() == beta)
        //        //        {
        //        //            instance = 7225653;
        //        //            testName = "Exclusion_TROYMark_Exclude_Beta";
        //        //        }
        //        else
        //        {
        //            instance = 11583211;
        //            testName = "Exclusion_TROYMark_Exclude_Prod";
        //        }
        //        //        }
        //        //    }
        //    }
        //    passFail = 1;  //reset before test
        //    imagePath = string.Concat(filePath, "Test Files\\");
        //    imagePath1 = imagePath;
        //    imagePath2 = imagePath;
        //    filePath = string.Concat(filePath, testName, ".txt");
        //    templateName = "Exclusion-DO NOT USE";
        //    CreateLogFile();

        //    try
        //    {
        //        //setup
        //        CreateTemplateUnderTest(templateName);
        //        baseWindowHandle = driver.CurrentWindowHandle;

        //        SetupTROYMark();

        //        //Test
        //        SetupExclusion();

        //        imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, string.Concat("Exclude TROYMark ", cc.FetchChecked(driver, tst.exclusion_Pantograph), " "), ScreenshotImageFormat.Png, 0);
        //        tglog.AppendLogText(filePath, string.Concat("TROYMark = ", cc.FetchChecked(driver, tst.exclusion_Pantograph)));
        //        Thread.Sleep(3000);

        //        cc.ClickButton(driver, tst.exclusion_TROYMark);
        //        tglog.AppendLogText(filePath, string.Concat("Exclusion TROYMark = ", cc.FetchChecked(driver, tst.exclusion_Pantograph)));

        //        imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, string.Concat("Exclude TROYMark ", cc.FetchChecked(driver, tst.exclusion_Pantograph), " "), ScreenshotImageFormat.Png, 1);
        //        Thread.Sleep(3000);

        //        cc.ClickButton(driver, tst.saveButton);
        //        tglog.AppendLogText(filePath, "Saved Template ..... OK");
        //        Thread.Sleep(5000);

        //        cc.ClickButton(driver, tst.templateHomeBtn);
        //        tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
        //        Thread.Sleep(3000);
        //        cc.ScrollPage(driver);

        //        //Tear Down
        //        RemoveTemplateUnderTest(templateName);

        //        StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
        //        tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
        //        passFail = 0;
        //    }
        //    catch (Exception e)
        //    {
        //        passFail = 1;
        //        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

        //        //Tear Down
        //        cc.ClickButton(driver, tst.templateHomeBtn);
        //        tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
        //        Thread.Sleep(5000);
        //        cc.ScrollPage(driver);
        //        RemoveTemplateUnderTest(templateName);
        //        Thread.Sleep(5000);
        //    }

        //    Thread.Sleep(5000);
        //}

        // --------------------------------------------------------------------- Methods -------------------------------------------------------------------

        //#region Methods
        //private void CreateLog()
        //{
        //    tglog.CreateLogFile(filePath, string.Concat(Environment.NewLine, "----------- Start testing using ", brow, " -----------", Environment.NewLine));
        //    tglog.AppendLogText(filePath, string.Concat("Set up ", brow, " ..... OK"));
        //    tglog.AppendLogText(filePath, string.Concat("URL = ", baseURL));
        //    tglog.AppendLogText(filePath, string.Concat("Envionment = ", qaDev));
        //    tglog.AppendLogText(filePath, string.Concat("User = ", user));
        //    tglog.AppendLogText(filePath, "Opened SecureDocs in browser ..... OK");
        //    tglog.AppendLogText(filePath, string.Concat("Build Version: ", build));
        //    tglog.AppendLogText(filePath, "Logged into SecureDocs ..... OK");
        //    tglog.AppendLogText(filePath, string.Concat("Instance = ", instance));
        //    tglog.AppendLogText(filePath, string.Concat("FilePath = ", filePath));
        //    tglog.AppendLogText(filePath, "Opened SecureDocs Dashboard ..... OK");
        //}

        //private void CreateTemplate(string text)
        //{
        //    OpenTemplates();
        //    cst = st.OpenCreateTemplate(driver);
        //    //tglog.AppendLogText(filePath, driver.Title);

        //    tglog.AppendLogText(filePath, "Opened New Security Template popup ..... OK");

        //    tst = cst.OpenTemplate(driver);
        //    driver.SwitchTo().Window(baseWindowHandle);
        //    tglog.AppendLogText(filePath, "Opened New Security Template ..... OK");

        //    baseWindowHandle = driver.CurrentWindowHandle;

        //    Thread.Sleep(5000);
        //    nt = tst.OpenNameTemplate(driver);
        //    Thread.Sleep(3000);

        //    nt.NameTemplate(driver, text);
        //    Thread.Sleep(8000);
        //}

        //private void OpenTemplates()
        //{
        //    try
        //    {
        //        st = mp.OpenSecurityTemplates(driver);
        //        //tglog.AppendLogText(filePath, driver.Title);

        //        //StringAssert.Contains(cc.FetchInnerHTML(driver, st.breadcrumb), "Security Templates");
        //        passFail = 0;
        //        tglog.AppendLogText(filePath, "Opened Security Templates ..... OK");

        //    }
        //    catch (Exception e)
        //    {
        //        passFail = 1;
        //        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
        //    }
        //}

        //private void RemoveTemplate(string text)
        //{
        //    string ele = "";
        //    string delBtn = "";
        //    int itemCount = 0;
        //    string templateName = "";

        //    itemCount = Convert.ToInt16(cc.FetchChildCount(driver, st.templateGrid));
        //    for (int x = 1; x <= itemCount; x++)
        //    {
        //        delBtn = string.Concat(st.templateGrid, "/div[", x.ToString(), "]/div[1]/div/a[3]");
        //        ele = string.Concat(st.templateGrid, "/div[", x.ToString(), "]/div[2]/a");
        //        templateName = cc.FetchInnerHTML(driver, ele);

        //        if (templateName == text)
        //        {
        //            rst = st.RemoveTemplate(driver, delBtn);

        //            StringAssert.Contains(string.Concat("Delete Template '", templateName, "'"), cc.FetchInnerHTML(driver, rst.header));
        //            tglog.AppendLogText(filePath, "Opened Remove Security Template..... OK");

        //            st = rst.SecurityTemplateYES(driver, "Yes");
        //            tglog.AppendLogText(filePath, string.Concat("Removed template ", templateName));
        //            break;
        //        }
        //    }
        //}

        //private void SetupPractitest()
        //{
        //    if (qaDev.ToUpper() == "DEV")
        //    {
        //        ptc.PT_Post_Specific_Run(filePath, passFail, resultMsg, instance, project, brow, "Development");
        //    }
        //    else
        //    {
        //        if (qaDev.ToUpper() == "STAGE")
        //        {
        //            ptc.PT_Post_Specific_Run(filePath, passFail, resultMsg, instance, project, brow, "Stage");
        //        }
        //        else
        //        {
        //            ptc.PT_Post_Specific_Run(filePath, passFail, resultMsg, instance, project, brow, "Production");
        //        }
        //    }
        //}

        //private void SetupWebDriver()
        //{
        //    if ((brow.ToLower() == "ie"))
        //    {
        //        this.driver = new InternetExplorerDriver("C:\\Selenium");
        //    }

        //    if ((brow.ToLower() == "chrome"))
        //    {
        //        this.driver = new ChromeDriver(@"C:\Selenium");
        //    }

        //    //if ((brow.ToLower() == "firefox"))
        //    //{
        //    //    this.driver = new FirefoxDriver();
        //    //}

        //}

        //private void CaptureText()
        //{
        //    cc.ClickButton(driver, tst.captureElements);
        //    tglog.AppendLogText(filePath, "Exposed Security Elements ..... OK");

        //    cc.ClickButton(driver, tst.captureText);
        //    tglog.AppendLogText(filePath, "Added Data Capture To Template ..... OK");
        //}

        //private void SetUpEnvironVariables()
        //{
        //    if (qaDev.ToLower() == dev)
        //    {
        //        filePath = string.Concat(filePath, "Dev\\");
        //    }
        //    else
        //    {
        //        if (qaDev.ToLower() == staging)
        //        {
        //            filePath = string.Concat(filePath, "Stage\\");
        //        }
        //        else
        //        {
        //            filePath = string.Concat(filePath, "Prod\\");
        //        }
        //    }
        //}
        //#endregion

        // --------------------------------------------------------------------- Methods -------------------------------------------------------------------

        #region Methods
        private void CreateLogFile()
        {
            tglog.CreateLogFile(filePath, string.Concat(Environment.NewLine, "----------- Start testing using ", brow, " -----------", Environment.NewLine));
            tglog.AppendLogText(filePath, string.Concat("Set up ", brow, " ..... OK"));
            tglog.AppendLogText(filePath, string.Concat("URL = ", baseURL));
            tglog.AppendLogText(filePath, string.Concat("Envionment = ", qaDev));
            tglog.AppendLogText(filePath, string.Concat("User = ", user));
            tglog.AppendLogText(filePath, "Opened SecureDocs in browser ..... OK");
            tglog.AppendLogText(filePath, string.Concat("Build Version: ", build));
            tglog.AppendLogText(filePath, "Logged into SecureDocs ..... OK");
            tglog.AppendLogText(filePath, string.Concat("Instance = ", instance));
            tglog.AppendLogText(filePath, string.Concat("FilePath = ", filePath));
            tglog.AppendLogText(filePath, "Opened SecureRx Dashboard ..... OK");
        }

        private void CreateTemplateUnderTest(string text)
        {
            OpenSecurityTemplates();
            cst = st.OpenCreateTemplate(driver);
            //tglog.AppendLogText(filePath, driver.Title);

            tglog.AppendLogText(filePath, "Opened New Security Template popup ..... OK");

            tst = cst.OpenTemplate(driver);
            driver.SwitchTo().Window(baseWindowHandle);
            tglog.AppendLogText(filePath, "Opened New Security Template ..... OK");

            baseWindowHandle = driver.CurrentWindowHandle;

            Thread.Sleep(5000);
            nt = tst.OpenNameTemplate(driver);
            Thread.Sleep(3000);

            nt.NameTemplate(driver, text);
            Thread.Sleep(8000);
        }

        private void OpenSecurityTemplates()
        {
            try
            {
                st = mp.OpenSecurityTemplates(driver);
                //tglog.AppendLogText(filePath, driver.Title);

                //StringAssert.Contains(cc.FetchInnerHTML(driver, st.breadcrumb), "Security Templates");
                passFail = 0;
                tglog.AppendLogText(filePath, "Opened Security Templates ..... OK");

            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        private void RemoveTemplateUnderTest(string text)
        {
            string ele = "";
            string delBtn = "";
            int itemCount = 0;
            string templateName = "";

            itemCount = Convert.ToInt16(cc.FetchChildCount(driver, st.templateGrid));
            for (int x = 1; x <= itemCount; x++)
            {
                delBtn = string.Concat(st.templateGrid, "/div[", x.ToString(), "]/div[1]/div/a[3]");
                ele = string.Concat(st.templateGrid, "/div[", x.ToString(), "]/div[2]/a");
                templateName = cc.FetchInnerHTML(driver, ele);

                if (templateName == text)
                {
                    rst = st.RemoveTemplate(driver, delBtn);

                    StringAssert.Contains(string.Concat("Delete Template '", templateName, "'"), cc.FetchInnerHTML(driver, rst.header));
                    tglog.AppendLogText(filePath, "Opened Remove Security Template..... OK");

                    st = rst.SecurityTemplateYES(driver, "Yes");
                    tglog.AppendLogText(filePath, string.Concat("Removed template ", templateName));
                    break;
                }
            }
        }

        private void SendToPractitest()
        {
            if (qaDev.ToUpper() == "DEV")
            {
                ptc.PT_Post_Specific_Run(filePath, passFail, resultMsg, instance, project, brow, "Development");
            }
            else
            {
                if (qaDev.ToUpper() == "QA")
                {
                    ptc.PT_Post_Specific_Run(filePath, passFail, resultMsg, instance, project, brow, "QA");
                }
                else
                {
                    if (qaDev.ToUpper() == "BETA")
                    {
                        ptc.PT_Post_Specific_Run(filePath, passFail, resultMsg, instance, project, brow, "Beta");
                    }
                    else
                    {
                        ptc.PT_Post_Specific_Run(filePath, passFail, resultMsg, instance, project, brow, "Production");
                    }
                }
            }
        }

        private void SetupDriver()
        {
            if ((brow.ToLower() == "ie"))
            {
                this.driver = new InternetExplorerDriver("C:\\Selenium");
            }

            if ((brow.ToLower() == "chrome"))
            {
                this.driver = new ChromeDriver(@"C:\Selenium");
            }

            //if ((brow.ToLower() == "firefox"))
            //{
            //    this.driver = new FirefoxDriver();
            //}

        }

        private void SetupExclusion()
        {
            cc.ClickButton(driver, tst.securityElements);
            tglog.AppendLogText(filePath, "Exposed Security Elements ..... OK");
            Thread.Sleep(2000);

            cc.ClickButton(driver, tst.exclusionImg);
            tglog.AppendLogText(filePath, "Added Exclusion To Template ..... OK");
        }

        private void SetupCaptureText()
        {
            cc.ClickButton(driver, tst.captureElements);
            tglog.AppendLogText(filePath, "Exposed Security Elements ..... OK");

            cc.ClickButton(driver, tst.captureText);
            tglog.AppendLogText(filePath, "Added Data Capture To Template ..... OK");
        }

        private void SetUpEnvironVars()
        {
            if (qaDev.ToLower() == dev)
            {
                filePath = string.Concat(filePath, "Dev\\");
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    filePath = string.Concat(filePath, "Stage\\");
                }
                else
                {
                    filePath = string.Concat(filePath, "Prod\\");
                }
            }
        }
        private void SetupPantograph()
        {
            cc.ClickButton(driver, tst.securityElements);
            tglog.AppendLogText(filePath, "Exposed Security Elements ..... OK");
            Thread.Sleep(2000);

            cc.ClickButton(driver, tst.pantographImg);
            tglog.AppendLogText(filePath, "Added Pantograph To Template ..... OK");

            cc.ClickButton(driver, tst.pantographFullPage);
            tglog.AppendLogText(filePath, "Full Page Pantograph ..... OK");
        }

        private void SetupTROYMark()
        {
            cc.ClickButton(driver, tst.securityElements);
            tglog.AppendLogText(filePath, "Exposed Security Elements ..... OK");
            Thread.Sleep(2000);

            cc.ClickButton(driver, tst.troymarkImg);
            tglog.AppendLogText(filePath, "Added TROY To Template ..... OK");

            cc.ClickButton(driver, tst.troymarkFullPage);
            tglog.AppendLogText(filePath, "Full Page TROYMark ..... OK");
        }
        #endregion

        //--------------------------------------------------------------------------------------------
        #region Additional test attributes
        [TestInitialize()]
        public void MyTestInitialize()
        {
            cc = new SecureRx.Common.Common_Code(driver);

            brow = ConfigurationManager.AppSettings["browser"];
            baseURL = ConfigurationManager.AppSettings["DocrityURL"];
            project = ConfigurationManager.AppSettings["project"];
            qaDev = ConfigurationManager.AppSettings["testEnv"];
            user = ConfigurationManager.AppSettings["testUser"].ToUpper();
            string pswd = ConfigurationManager.AppSettings["TestPswd"];

            this.SetupDriver();
            SetUpEnvironVars();

            lp = new SecureRx.Page_Elements.LoginPage(driver, baseURL);
            build = lp.FetchBuildVersion(driver);
            mp = lp.SDLogin(user, pswd, driver);

            //Did Home Menu open
            if (mp == null)
            {
                throw new NoSuchWindowException("Docrity home page was not displayed!");
            }
            else
            {
                passFail = 0;
            }
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {

            driver.SwitchTo().Window(baseWindowHandle);
            Thread.Sleep(1000);

            try
            {
                mp.LogoutandCloseBrowser(driver);
                tglog.AppendLogText(filePath, "Logged out and Closed browser..... OK");
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }

            if (passFail != 0)
            {
                resultMsg = string.Concat(testName, " ..... FAILED!!!!! Please refer to log file");
                //driver.Close();
            }
            else
            {
                resultMsg = string.Concat(testName, " ..... PASSED!!!!!");
            }

            SendToPractitest();

            tglog.AppendLogText(filePath, "Close Docrity");
            tglog.AppendLogText(filePath, "\n Test Finished \n");
        }

        #endregion

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;
    }
}
