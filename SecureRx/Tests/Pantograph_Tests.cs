﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Data;
using System.IO;
using System.Net;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;

namespace SecureRx.Tests
{
    [TestClass]
    public class Pantograph_Tests
    {
        #region libraries
        SecureRx.Page_Elements.MainPage mp = null;
        SecureRx.Page_Elements.LoginPage lp = null;

        SecureRx.Page_Elements.Templates.Download_Pantograph_Test_Patterns dptp = null;
        //SecureRx.Page_Elements.Templates.SecurityTemplates dpst = null;
        SecureRx.Page_Elements.Templates.SecurityTemplates st = null;
        SecureRx.Page_Elements.Templates.Create_Security_Templates cst = null;
        SecureRx.Page_Elements.Templates.Remove_Security_Template rst = null;
        SecureRx.Page_Elements.Templates.Template tst = null;

        SecureRx.Common.Common_Code cc = null;
        SecureRx.Common.PT_Client ptc = new SecureRx.Common.PT_Client();
        SecureRx.Common.TGLogging tglog = new SecureRx.Common.TGLogging();
        #endregion

        //Vars
        private string baseURL = "";
        private string baseWindowHandle = "";
        private string beta = "beta";
        private string brow = "";
        private string build = "";
        private string dev = "dev";
        private IWebDriver driver = null;
        private string filePath = "C:\\Test Logs\\SecureRx\\";
        private string imagePath = "";
        private int instance = 0;
        private int passFail = 1;
        //private string prod = "prod";
        private string project = "4896";
        private string qa = "qa";
        private string qaDev = "";
        private string resultMsg = "";
        private string testName = "";
        private string user = "";

        //-------------------------------------------------------------------------------------------
        [TestMethod, TestCategory("Under Review - Pantograph Tests")]
        public void Pantograph_IncludeTag_ChangeFont()
        {
            string templateName = "";
            string EnableMPBorder = "";
            string imagePath2 = "";
            string imagePath1 = "";
            string newText = "";
            string originalFont = "";
            string text = "";

            if (qaDev.ToLower() == dev)
            {
                instance = 11617923;
                testName = "Pantograph_MicroprintBorder_ChangeIncludeTagFont_Dev";
            }
            else
            {
                if (qaDev.ToLower() == qa)
                {
                    instance = 11659687;
                    testName = "Pantograph_MicroprintBorder_ChangeIncludeTagFont_QA";
                }
                //else
                //{
                //    if (qaDev.ToLower() == beta)
                //    {
                //        instance = 7225653;
                //        testName = "Pantograph_MicroprintBorder_ChangeIncludeTagFont_Beta";
                //    }
                else
                {
                    instance = 11660201;
                    testName = "Pantograph_MicroprintBorder_ChangeIncludeTagFont_Prod";
                }
                //}
            }
            passFail = 1;  //reset before test
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Pantograph-DO NOT USE";
            text = "Test MP Static Text.....QA";
            CreateLogFile();

            try
            {
                //setup
                CreateTemplateUnderTest(templateName);
                baseWindowHandle = driver.CurrentWindowHandle;

                SetupPantograph();

                EnableMPBorder = cc.FetchChecked(driver, tst.pantographEnableMicroprintBorder);
                tglog.AppendLogText(filePath, string.Concat("Enable MP Border = ", EnableMPBorder));
                cc.ClickButton(driver, tst.pantographEnableMicroprintBorder);
                tglog.AppendLogText(filePath, string.Concat("Enable MP Border = ", cc.FetchChecked(driver, tst.pantographEnableMicroprintBorder)));

                Thread.Sleep(3000);

                cc.ClickButton(driver, tst.pantographMPEditText);
                tglog.AppendLogText(filePath, "Opened Edit Text Popup.....");

                cc.EnterText(driver, text, tst.templateTextInput);
                newText = cc.FetchIDefaultValue(driver, tst.templateTextInput);
                cc.ClickButton(driver, tst.templateTextSave);
                tglog.AppendLogText(filePath, string.Concat("Enter Static Text in Pantograph, ", newText, ",.....OK"));
                Thread.Sleep(3000);

                //Test
                cc.ScrollPage(driver);
                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Pantograph Include Tag Font ", ScreenshotImageFormat.Png, 0);
                originalFont = cc.FetchSelectedOption(driver, tst.pantographIncludeTagFont);
                cc.SelectDropDownByElement(driver, tst.pantographIncludeTagFont,"Courier");
                Thread.Sleep(3000);
                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Pantograph Include Tag Font ", ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);

                //save 
                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template ..... OK");
                Thread.Sleep(3000);

                try
                {
                    tglog.AppendLogText(filePath, string.Concat("Original font is ", originalFont, " and new font is ", cc.FetchSelectedOption(driver, tst.pantographIncludeTagFont)));
                    StringAssert.Contains((originalFont != cc.FetchSelectedOption(driver, tst.pantographIncludeTagFont)).ToString(), true.ToString());

                    cc.ClickButton(driver, tst.templateHomeBtn);
                    tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                    Thread.Sleep(3000);
                    cc.ScrollPage(driver);

                    //Tear Down
                    RemoveTemplateUnderTest(templateName);

                    StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                    tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                    passFail = 0;
                }
                catch (Exception e)
                {
                    tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                    cc.ClickButton(driver, tst.templateHomeBtn);
                    tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                    Thread.Sleep(3000);
                    cc.ScrollPage(driver);

                    //Tear Down
                    RemoveTemplateUnderTest(templateName);

                    StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                    tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                    passFail = 1;

                }
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Tear Down
                cc.ClickButton(driver, tst.templateHomeBtn);
                tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                Thread.Sleep(5000);
                cc.ScrollPage(driver);
                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        [TestMethod, TestCategory("Under Review - Pantograph Tests")]
        public void Pantograph_IncludeTag_ChangeFontSize()
        {
            string templateName = "";
            string EnableMPBorder = "";
            string imagePath2 = "";
            string imagePath1 = "";
            string newText = "";
            string originalFontSize = "";
            string text = "";

            if (qaDev.ToLower() == dev)
            {
                instance = 11617924;
                testName = "Pantograph_MicroprintBorder_ChangeIncludeTagFontSize_Dev";
            }
            else
            {
                if (qaDev.ToLower() == qa)
                {
                    instance = 11659688;
                    testName = "Pantograph_MicroprintBorder_ChangeIncludeTagFontSize_QA";
                }
                //else
                //{
                //    if (qaDev.ToLower() == beta)
                //    {
                //        instance = 7225653;
                //        testName = "Pantograph_MicroprintBorder_ChangeIncludeTagFontSize_Beta";
                //    }
                else
                {
                    instance = 11660202;
                    testName = "Pantograph_MicroprintBorder_ChangeIncludeTagFontSize_Prod";
                }
                //}
            }
            passFail = 1;  //reset before test
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Pantograph-DO NOT USE";
            text = "Test MP Static Text.....QA";
            CreateLogFile();

            try
            {
                //setup
                CreateTemplateUnderTest(templateName);
                baseWindowHandle = driver.CurrentWindowHandle;

                SetupPantograph();

                EnableMPBorder = cc.FetchChecked(driver, tst.pantographEnableMicroprintBorder);
                tglog.AppendLogText(filePath, string.Concat("Enable MP Border = ", EnableMPBorder));
                cc.ClickButton(driver, tst.pantographEnableMicroprintBorder);
                tglog.AppendLogText(filePath, string.Concat("Enable MP Border = ", cc.FetchChecked(driver, tst.pantographEnableMicroprintBorder)));

                Thread.Sleep(3000);

                cc.ClickButton(driver, tst.pantographMPEditText);
                tglog.AppendLogText(filePath, "Opened Edit Text Popup.....");

                cc.EnterText(driver, text, tst.templateTextInput);
                newText = cc.FetchIDefaultValue(driver, tst.templateTextInput);
                cc.ClickButton(driver, tst.templateTextSave);
                tglog.AppendLogText(filePath, string.Concat("Enter Static Text in Pantograph, ", newText, ",.....OK"));
                Thread.Sleep(3000);

                //Test
                cc.ScrollPage(driver);
                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Pantograph Include Tag Font Size ", ScreenshotImageFormat.Png, 0);
                originalFontSize = cc.FetchSelectedOption(driver, tst.pantographIncludeTagFontSize);
                cc.SelectDropDownByElement(driver, tst.pantographIncludeTagFontSize, "3 pt");
                Thread.Sleep(3000);
                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Pantograph Include Tag Font Size ", ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);

                //save 
                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template ..... OK");
                Thread.Sleep(3000);

                try
                {
                    tglog.AppendLogText(filePath, string.Concat("Original font size is ", originalFontSize, " and new font size is ", cc.FetchSelectedOption(driver, tst.pantographIncludeTagFontSize)));
                    StringAssert.Contains((originalFontSize != cc.FetchSelectedOption(driver, tst.pantographIncludeTagFontSize)).ToString(), true.ToString());

                    cc.ClickButton(driver, tst.templateHomeBtn);
                    tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                    Thread.Sleep(3000);
                    cc.ScrollPage(driver);

                    //Tear Down
                    RemoveTemplateUnderTest(templateName);

                    StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                    tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                    passFail = 0;
                }
                catch (Exception e)
                {
                    tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                    cc.ClickButton(driver, tst.templateHomeBtn);
                    tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                    Thread.Sleep(3000);
                    cc.ScrollPage(driver);

                    //Tear Down
                    RemoveTemplateUnderTest(templateName);

                    StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                    tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                    passFail = 1;

                }
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Tear Down
                cc.ClickButton(driver, tst.templateHomeBtn);
                tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                Thread.Sleep(5000);
                cc.ScrollPage(driver);
                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        [TestMethod, TestCategory("Under Review - Pantograph Tests")]
        public void Pantograph_IncludeTag_ChangeText()
        {
            string templateName = "";
            string EnableMPBorder = "";
            string imagePath2 = "";
            string imagePath1 = "";
            string newText = "";
            string originalText = "";
            string text = "";

            if (qaDev.ToLower() == dev)
            {
                instance = 11617922;
                testName = "Pantograph_MicroprintBorder_ChangeIncludeTagText_Dev";
            }
            else
            {
                if (qaDev.ToLower() == qa)
                {
                    instance = 11659686;
                    testName = "Pantograph_MicroprintBorder_ChangeIncludeTagText_QA";
                }
                //else
                //{
                //    if (qaDev.ToLower() == beta)
                //    {
                //        instance = 7225653;
                //        testName = "Pantograph_MicroprintBorder_ChangeIncludeTagText_Beta";
                //    }
                else
                {
                    instance = 11660200;
                    testName = "Pantograph_MicroprintBorder_ChangeIncludeTagText_Prod";
                }
                //}
            }
            passFail = 1;  //reset before test
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Pantograph-DO NOT USE";
            text = "Test MP Static Text.....QA";
            CreateLogFile();

            try
            {
                //setup
                CreateTemplateUnderTest(templateName);
                baseWindowHandle = driver.CurrentWindowHandle;

                SetupPantograph();

                EnableMPBorder = cc.FetchChecked(driver, tst.pantographEnableMicroprintBorder);
                tglog.AppendLogText(filePath, string.Concat("Enable MP Border = ", EnableMPBorder));
                cc.ClickButton(driver, tst.pantographEnableMicroprintBorder);
                tglog.AppendLogText(filePath, string.Concat("Enable MP Border = ", cc.FetchChecked(driver, tst.pantographEnableMicroprintBorder)));

                Thread.Sleep(3000);

                cc.ClickButton(driver, tst.pantographMPEditText);
                tglog.AppendLogText(filePath, "Opened Edit Text Popup.....");

                cc.EnterText(driver, text, tst.templateTextInput);
                newText = cc.FetchIDefaultValue(driver, tst.templateTextInput);
                cc.ClickButton(driver, tst.templateTextSave);
                tglog.AppendLogText(filePath, string.Concat("Enter Static Text in Pantograph, ", newText, ",.....OK"));
                Thread.Sleep(3000);

                //Test
                cc.ScrollPage(driver);
                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Pantograph Include Tag Text ", ScreenshotImageFormat.Png, 0);
                originalText = cc.FetchIDefaultValue(driver, tst.pantographIncludeTagText);
                cc.EnterText(driver, "QA", tst.pantographIncludeTagText);
                Thread.Sleep(3000);
                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Pantograph Include Tag Text ", ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);

                //save 
                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template ..... OK");
                Thread.Sleep(3000);

                try
                {
                    tglog.AppendLogText(filePath, string.Concat("Original Text is ", originalText, " and new Text is ", cc.FetchIDefaultValue(driver, tst.pantographIncludeTagText)));
                    StringAssert.Contains((originalText != cc.FetchIDefaultValue(driver, tst.pantographIncludeTagText)).ToString(), true.ToString());

                    cc.ClickButton(driver, tst.templateHomeBtn);
                    tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                    Thread.Sleep(3000);
                    cc.ScrollPage(driver);

                    //Tear Down
                    RemoveTemplateUnderTest(templateName);

                    StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                    tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                    passFail = 0;
                }
                catch (Exception e)
                {
                    tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                    cc.ClickButton(driver, tst.templateHomeBtn);
                    tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                    Thread.Sleep(3000);
                    cc.ScrollPage(driver);

                    //Tear Down
                    RemoveTemplateUnderTest(templateName);

                    StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                    tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                    passFail = 1;

                }
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Tear Down
                cc.ClickButton(driver, tst.templateHomeBtn);
                tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                Thread.Sleep(5000);
                cc.ScrollPage(driver);
                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        [TestMethod, TestCategory("Under Review - Pantograph Tests")]
        public void Pantograph_IncludeTag_Uncheck()
        {
            string templateName = "";
            string EnableMPBorder = "";
            string imagePath2 = "";
            string imagePath1 = "";
            string newText = "";
            string includeTag = "";
            string text = "";

            if (qaDev.ToLower() == dev)
            {
                instance = 11630161;
                testName = "Pantograph_IncludeTag_Uncheck_Dev";
            }
            else
            {
                if (qaDev.ToLower() == qa)
                {
                    instance = 11659690;
                    testName = "Pantograph_IncludeTag_Uncheck_QA";
                }
                //else
                //{
                //    if (qaDev.ToLower() == beta)
                //    {
                //        instance = 7225653;
                //        testName = "Pantograph_IncludeTag_Uncheck_Beta";
                //    }
                else
                {
                    instance = 11660204;
                    testName = "Pantograph_IncludeTag_Uncheck_Prod";
                }
                //}
            }
            passFail = 1;  //reset before test
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Pantograph-DO NOT USE";
            text = "Test MP Static Text.....QA";
            CreateLogFile();

            try
            {
                //setup
                CreateTemplateUnderTest(templateName);
                baseWindowHandle = driver.CurrentWindowHandle;

                SetupPantograph();

                EnableMPBorder = cc.FetchChecked(driver, tst.pantographEnableMicroprintBorder);
                tglog.AppendLogText(filePath, string.Concat("Enable MP Border = ", EnableMPBorder));
                cc.ClickButton(driver, tst.pantographEnableMicroprintBorder);
                tglog.AppendLogText(filePath, string.Concat("Enable MP Border = ", cc.FetchChecked(driver, tst.pantographEnableMicroprintBorder)));

                Thread.Sleep(3000);

                cc.ClickButton(driver, tst.pantographMPEditText);
                tglog.AppendLogText(filePath, "Opened Edit Text Popup.....");

                cc.EnterText(driver, text, tst.templateTextInput);
                newText = cc.FetchIDefaultValue(driver, tst.templateTextInput);
                cc.ClickButton(driver, tst.templateTextSave);
                tglog.AppendLogText(filePath, string.Concat("Enter Static Text in Pantograph, ", newText, ",.....OK"));
                Thread.Sleep(3000);

                //Test
                cc.ScrollPage(driver);
                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Pantograph Uncheck Include Tag ", ScreenshotImageFormat.Png, 0);
                includeTag = cc.FetchChecked(driver, tst.pantographIncludeTag);
                cc.ClickButton(driver, tst.pantographIncludeTag);
                Thread.Sleep(3000);
                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Pantograph Uncheck Include Tag ", ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);

                //save 
                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template ..... OK");
                Thread.Sleep(3000);

                try
                {
                    tglog.AppendLogText(filePath, string.Concat("Original Include Tag is ", includeTag, " and new Include Tag is ", cc.FetchChecked(driver, tst.pantographIncludeTag)));
                    StringAssert.Contains((includeTag != cc.FetchChecked(driver, tst.pantographIncludeTag)).ToString(), true.ToString());

                    cc.ClickButton(driver, tst.templateHomeBtn);
                    tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                    Thread.Sleep(3000);
                    cc.ScrollPage(driver);

                    //Tear Down
                    RemoveTemplateUnderTest(templateName);

                    StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                    tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                    passFail = 0;
                }
                catch (Exception e)
                {
                    tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                    cc.ClickButton(driver, tst.templateHomeBtn);
                    tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                    Thread.Sleep(3000);
                    cc.ScrollPage(driver);

                    //Tear Down
                    RemoveTemplateUnderTest(templateName);

                    StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                    tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                    passFail = 1;

                }
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Tear Down
                cc.ClickButton(driver, tst.templateHomeBtn);
                tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                Thread.Sleep(5000);
                cc.ScrollPage(driver);
                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        //-------------------------------------------------------------------------------------------
        [TestMethod, TestCategory("Under Review - Pantograph Tests")]
        public void Pantograph_MicroprintBorder_AddStaticText()
        {
            string templateName = "";
            string EnableMPBorder = "";
            string imagePath2 = "";
            string imagePath1 = "";
            string newText = "";
            string originalText = "";
            string text = "";

            if (qaDev.ToLower() == dev)
            {
                instance = 11584111;
                testName = "Pantograph_MicroprintBorder_AddStaticText_Dev";
            }
            else
            {
                if (qaDev.ToLower() == qa)
                {
                    instance = 11659852;
                    testName = "Pantograph_MicroprintBorder_AddStaticText_QA";
                }
                //else
                //{
                //    if (qaDev.ToLower() == beta)
                //    {
                //        instance = 7225653;
                //        testName = "Pantograph_MicroprintBorder_AddStaticText_Beta";
                //    }
                else
                {
                    instance = 11660181;
                    testName = "Pantograph_MicroprintBorder_AddStaticText_Prod";
                }
                //}
            }
            passFail = 1;  //reset before test
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Pantograph-DO NOT USE";
            text = "Test MP Static Text.....QA";
            CreateLogFile();

            try
            {
                //setup
                CreateTemplateUnderTest(templateName);
                baseWindowHandle = driver.CurrentWindowHandle;

                //Test
                SetupPantograph();

                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Pantograph Microprint Static Text ", ScreenshotImageFormat.Png, 0);
                Thread.Sleep(3000);

                EnableMPBorder = cc.FetchChecked(driver, tst.pantographEnableMicroprintBorder);
                tglog.AppendLogText(filePath, string.Concat("Enable MP Border = ",EnableMPBorder));
                cc.ClickButton(driver, tst.pantographEnableMicroprintBorder);
                tglog.AppendLogText(filePath, string.Concat("Enable MP Border = ", cc.FetchChecked(driver, tst.pantographEnableMicroprintBorder))) ;

                cc.ClickButton(driver, tst.pantographMPEditText);
                tglog.AppendLogText(filePath, "Opened Edit Text Popup.....");

                originalText = cc.FetchIDefaultValue(driver, tst.templateTextInput);
                cc.EnterText(driver, text, tst.templateTextInput);
                newText = cc.FetchIDefaultValue(driver, tst.templateTextInput);
                cc.ClickButton(driver, tst.templateTextSave);
                tglog.AppendLogText(filePath, string.Concat("Enter Static Text in Pantograph, ", newText, ",.....OK"));
                Thread.Sleep(3000);

                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath,"Pantograph Microprint Static Text ", ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);

                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template ..... OK");
                Thread.Sleep(3000);

                cc.ClickButton(driver, tst.templateHomeBtn);
                tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                Thread.Sleep(3000);
                cc.ScrollPage(driver);

                //Tear Down
                RemoveTemplateUnderTest(templateName);

                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                passFail = 0;
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Tear Down
                cc.ClickButton(driver, tst.templateHomeBtn);
                tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                Thread.Sleep(5000);
                cc.ScrollPage(driver);
                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        [TestMethod, TestCategory("Under Review - Pantograph Tests")]
        public void Pantograph_MicroprintBorder_ChangeFont()
        {
            string templateName = "";
            string EnableMPBorder = "";
            string imagePath2 = "";
            string imagePath1 = "";
            string newText = "";
            string selectedFont = "";
            string text = "";

            if (qaDev.ToLower() == dev)
            {
                instance = 11617918;
                testName = "Pantograph_MicroprintBorder_ChangeFont_Dev";
            }
            else
            {
                if (qaDev.ToLower() == qa)
                {
                    instance = 11659682;
                    testName = "Pantograph_MicroprintBorder_ChangeFont_QA";
                }
                //else
                //{
                //    if (qaDev.ToLower() == beta)
                //    {
                //        instance = 7225653;
                //        testName = "Pantograph_MicroprintBorder_ChangeFont_Beta";
                //    }
                else
                {
                    instance = 11660196;
                    testName = "Pantograph_MicroprintBorder_ChangeFont_Prod";
                }
                //}
            }
            passFail = 1;  //reset before test
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Pantograph-DO NOT USE";
            text = "Test MP Static Text.....QA";
            CreateLogFile();

            try
            {
                //setup
                CreateTemplateUnderTest(templateName);
                baseWindowHandle = driver.CurrentWindowHandle;

                SetupPantograph();

                EnableMPBorder = cc.FetchChecked(driver, tst.pantographEnableMicroprintBorder);
                tglog.AppendLogText(filePath, string.Concat("Enable MP Border = ", EnableMPBorder));
                cc.ClickButton(driver, tst.pantographEnableMicroprintBorder);
                tglog.AppendLogText(filePath, string.Concat("Enable MP Border = ", cc.FetchChecked(driver, tst.pantographEnableMicroprintBorder)));

                Thread.Sleep(3000);

                cc.ClickButton(driver, tst.pantographMPEditText);
                tglog.AppendLogText(filePath, "Opened Edit Text Popup.....");

                cc.EnterText(driver, text, tst.templateTextInput);
                newText = cc.FetchIDefaultValue(driver, tst.templateTextInput);
                cc.ClickButton(driver, tst.templateTextSave);
                tglog.AppendLogText(filePath, string.Concat("Enter Static Text in Pantograph, ", newText, ",.....OK"));
                Thread.Sleep(3000);

                //Test
                cc.ScrollPage(driver);
                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Pantograph MP Border Font ", ScreenshotImageFormat.Png, 0);
                selectedFont = cc.FetchSelectedOption(driver, tst.pantographMPBorderFont);
                cc.SelectDropDownByElement(driver, tst.pantographMPBorderFont, "Courier");
                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Pantograph MP Border Font ", ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);

                //save 
                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template ..... OK");
                Thread.Sleep(3000);

                try
                {
                    tglog.AppendLogText(filePath, string.Concat("Original Font is ", selectedFont, " and new font is ", cc.FetchSelectedOption(driver, tst.pantographMPBorderFont)));
                    StringAssert.Contains((selectedFont != cc.FetchSelectedOption(driver, tst.pantographMPBorderFont)).ToString(), true.ToString());

                    cc.ClickButton(driver, tst.templateHomeBtn);
                    tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                    Thread.Sleep(3000);
                    cc.ScrollPage(driver);

                    //Tear Down
                    RemoveTemplateUnderTest(templateName);

                    StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                    tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                    passFail = 0;
                }
                catch (Exception e)
                {
                    tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                    cc.ClickButton(driver, tst.templateHomeBtn);
                    tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                    Thread.Sleep(3000);
                    cc.ScrollPage(driver);

                    //Tear Down
                    RemoveTemplateUnderTest(templateName);

                    StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                    tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                    passFail = 1;

                }
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Tear Down
                cc.ClickButton(driver, tst.templateHomeBtn);
                tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                Thread.Sleep(5000);
                cc.ScrollPage(driver);
                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        [TestMethod, TestCategory("Under Review - Pantograph Tests")]
        public void Pantograph_MicroprintBorder_ChangeFontSize()
        {
            string templateName = "";
            string EnableMPBorder = "";
            string imagePath2 = "";
            string imagePath1 = "";
            string newText = "";
            string selectedFontSIze = "";
            string text = "";

            if (qaDev.ToLower() == dev)
            {
                instance = 11617919;
                testName = "Pantograph_MicroprintBorder_ChangeFontSize_Dev";
            }
            else
            {
                if (qaDev.ToLower() == qa)
                {
                    instance = 11659683;
                    testName = "Pantograph_MicroprintBorder_ChangeFontSize_QA";
                }
                //else
                //{
                //    if (qaDev.ToLower() == beta)
                //    {
                //        instance = 7225653;
                //        testName = "Pantograph_MicroprintBorder_ChangeFontSize_Beta";
                //    }
                else
                {
                    instance = 11660197;
                    testName = "Pantograph_MicroprintBorder_ChangeFontSize_Prod";
                }
                //}
            }
            passFail = 1;  //reset before test
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Pantograph-DO NOT USE";
            text = "Test MP Static Text.....QA";
            CreateLogFile();

            try
            {
                //setup
                CreateTemplateUnderTest(templateName);
                baseWindowHandle = driver.CurrentWindowHandle;

                SetupPantograph();

                EnableMPBorder = cc.FetchChecked(driver, tst.pantographEnableMicroprintBorder);
                tglog.AppendLogText(filePath, string.Concat("Enable MP Border = ", EnableMPBorder));
                cc.ClickButton(driver, tst.pantographEnableMicroprintBorder);
                tglog.AppendLogText(filePath, string.Concat("Enable MP Border = ", cc.FetchChecked(driver, tst.pantographEnableMicroprintBorder)));

                Thread.Sleep(3000);

                cc.ClickButton(driver, tst.pantographMPEditText);
                tglog.AppendLogText(filePath, "Opened Edit Text Popup.....");

                cc.EnterText(driver, text, tst.templateTextInput);
                newText = cc.FetchIDefaultValue(driver, tst.templateTextInput);
                cc.ClickButton(driver, tst.templateTextSave);
                tglog.AppendLogText(filePath, string.Concat("Enter Static Text in Pantograph, ", newText, ",.....OK"));
                Thread.Sleep(3000);

                //Test
                cc.ScrollPage(driver);
                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Pantograph MP Border Font Size ", ScreenshotImageFormat.Png, 0);
                selectedFontSIze = cc.FetchSelectedOption(driver, tst.pantographMPBorderFontSize);
                cc.SelectDropDownByElement(driver, tst.pantographMPBorderFontSize, "4 pt");
                Thread.Sleep(3000);
                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Pantograph MP Border Font Size ", ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);

                //save 
                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template ..... OK");
                Thread.Sleep(3000);

                try
                {
                    tglog.AppendLogText(filePath, string.Concat("Original Font Size is ", selectedFontSIze, " and new font size is ", cc.FetchSelectedOption(driver, tst.pantographMPBorderFontSize)));
                    StringAssert.Contains((selectedFontSIze != cc.FetchSelectedOption(driver, tst.pantographMPBorderFontSize)).ToString(), true.ToString());

                    cc.ClickButton(driver, tst.templateHomeBtn);
                    tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                    Thread.Sleep(3000);
                    cc.ScrollPage(driver);

                    //Tear Down
                    RemoveTemplateUnderTest(templateName);

                    StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                    tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                    passFail = 0;
                }
                catch (Exception e)
                {
                    tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                    cc.ClickButton(driver, tst.templateHomeBtn);
                    tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                    Thread.Sleep(3000);
                    cc.ScrollPage(driver);

                    //Tear Down
                    RemoveTemplateUnderTest(templateName);

                    StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                    tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                    passFail = 1;

                }
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Tear Down
                cc.ClickButton(driver, tst.templateHomeBtn);
                tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                Thread.Sleep(5000);
                cc.ScrollPage(driver);
                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        [TestMethod, TestCategory("Under Review - Pantograph Tests")]
        public void Pantograph_MicroprintBorder_ChangeMargin()
        {
            string templateName = "";
            string EnableMPBorder = "";
            string imagePath2 = "";
            string imagePath1 = "";
            string newText = "";
            string originalMargin = "";
            string text = "";

            if (qaDev.ToLower() == dev)
            {
                instance = 11584112;
                testName = "Pantograph_MicroprintBorder_ChangeMargin_Dev";
            }
            else
            {
                if (qaDev.ToLower() == qa)
                {
                    instance = 11659853;
                    testName = "Pantograph_MicroprintBorder_ChangeMargin_QA";
                }
                //else
                //{
                //    if (qaDev.ToLower() == beta)
                //    {
                //        instance = 7225653;
                //        testName = "Pantograph_MicroprintBorder_ChangeMargin_Beta";
                //    }
                else
                {
                    instance = 11660182;
                    testName = "Pantograph_MicroprintBorder_ChangeMargin_Prod";
                }
                //}
            }
            passFail = 1;  //reset before test
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Pantograph-DO NOT USE";
            text = "Test MP Static Text.....QA";
            CreateLogFile();

            try
            {
                //setup
                CreateTemplateUnderTest(templateName);
                baseWindowHandle = driver.CurrentWindowHandle;

                SetupPantograph();

                EnableMPBorder = cc.FetchChecked(driver, tst.pantographEnableMicroprintBorder);
                tglog.AppendLogText(filePath, string.Concat("Enable MP Border = ", EnableMPBorder));
                cc.ClickButton(driver, tst.pantographEnableMicroprintBorder);
                tglog.AppendLogText(filePath, string.Concat("Enable MP Border = ", cc.FetchChecked(driver, tst.pantographEnableMicroprintBorder)));

                Thread.Sleep(3000);

                cc.ClickButton(driver, tst.pantographMPEditText);
                tglog.AppendLogText(filePath, "Opened Edit Text Popup.....");

                cc.EnterText(driver, text, tst.templateTextInput);
                newText = cc.FetchIDefaultValue(driver, tst.templateTextInput);
                cc.ClickButton(driver, tst.templateTextSave);
                tglog.AppendLogText(filePath, string.Concat("Enter Static Text in Pantograph, ", newText, ",.....OK"));
                Thread.Sleep(3000);

                //Test
                cc.ScrollPage(driver);
                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Pantograph MP Border Margin ", ScreenshotImageFormat.Png, 0);
                originalMargin = cc.FetchIDefaultValue(driver, tst.pantographMPBorderMargin);
                cc.EnterText(driver, "32", tst.pantographMPBorderMargin);
                Thread.Sleep(3000);
                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Pantograph MP Border Margin ", ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);

                //save 
                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template ..... OK");
                Thread.Sleep(3000);

                try
                {
                    tglog.AppendLogText(filePath, string.Concat("Original Margin is ", originalMargin, " and new margin is ", cc.FetchIDefaultValue(driver, tst.pantographMPBorderMargin)));
                    StringAssert.Contains((originalMargin != cc.FetchIDefaultValue(driver, tst.pantographMPBorderMargin)).ToString(), true.ToString());

                    cc.ClickButton(driver, tst.templateHomeBtn);
                    tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                    Thread.Sleep(3000);
                    cc.ScrollPage(driver);

                    //Tear Down
                    RemoveTemplateUnderTest(templateName);

                    StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                    tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                    passFail = 0;
                }
                catch (Exception e)
                {
                    tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                    cc.ClickButton(driver, tst.templateHomeBtn);
                    tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                    Thread.Sleep(3000);
                    cc.ScrollPage(driver);

                    //Tear Down
                    RemoveTemplateUnderTest(templateName);

                    StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                    tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                    passFail = 1;

                }
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Tear Down
                cc.ClickButton(driver, tst.templateHomeBtn);
                tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                Thread.Sleep(5000);
                cc.ScrollPage(driver);
                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        [TestMethod, TestCategory("Under Review - Pantograph Tests")]
        public void Pantograph_MicroprintBorder_ChangePadding()
        {
            string templateName = "";
            string EnableMPBorder = "";
            string imagePath2 = "";
            string imagePath1 = "";
            string newText = "";
            string originalPadding = "";
            string text = "";

            if (qaDev.ToLower() == dev)
            {
                instance = 11617920;
                testName = "Pantograph_MicroprintBorder_ChangePadding_Dev";
            }
            else
            {
                if (qaDev.ToLower() == qa)
                {
                    instance = 11659684;
                    testName = "Pantograph_MicroprintBorder_ChangePadding_QA";
                }
                //else
                //{
                //    if (qaDev.ToLower() == beta)
                //    {
                //        instance = 7225653;
                //        testName = "Pantograph_MicroprintBorder_ChangePadding_Beta";
                //    }
                else
                {
                    instance = 11660198;
                    testName = "Pantograph_MicroprintBorder_ChangePadding_Prod";
                }
                //}
            }
            passFail = 1;  //reset before test
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Pantograph-DO NOT USE";
            text = "Test MP Static Text.....QA";
            CreateLogFile();

            try
            {
                //setup
                CreateTemplateUnderTest(templateName);
                baseWindowHandle = driver.CurrentWindowHandle;

                SetupPantograph();

                EnableMPBorder = cc.FetchChecked(driver, tst.pantographEnableMicroprintBorder);
                tglog.AppendLogText(filePath, string.Concat("Enable MP Border = ", EnableMPBorder));
                cc.ClickButton(driver, tst.pantographEnableMicroprintBorder);
                tglog.AppendLogText(filePath, string.Concat("Enable MP Border = ", cc.FetchChecked(driver, tst.pantographEnableMicroprintBorder)));

                Thread.Sleep(3000);

                cc.ClickButton(driver, tst.pantographMPEditText);
                tglog.AppendLogText(filePath, "Opened Edit Text Popup.....");

                cc.EnterText(driver, text, tst.templateTextInput);
                newText = cc.FetchIDefaultValue(driver, tst.templateTextInput);
                cc.ClickButton(driver, tst.templateTextSave);
                tglog.AppendLogText(filePath, string.Concat("Enter Static Text in Pantograph, ", newText, ",.....OK"));
                Thread.Sleep(3000);

                //Test
                cc.ScrollPage(driver);
                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Pantograph MP Border Padding ", ScreenshotImageFormat.Png, 0);
                originalPadding = cc.FetchIDefaultValue(driver, tst.pantographMPBorderPadding);
                cc.EnterText(driver, "32", tst.pantographMPBorderPadding);
                Thread.Sleep(3000);
                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Pantograph MP Border Padding ", ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);

                //save 
                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template ..... OK");
                Thread.Sleep(3000);

                try
                {
                    tglog.AppendLogText(filePath, string.Concat("Original Padding is ", originalPadding, " and new padding is ", cc.FetchIDefaultValue(driver, tst.pantographMPBorderPadding)));
                    StringAssert.Contains((originalPadding != cc.FetchIDefaultValue(driver, tst.pantographMPBorderPadding)).ToString(), true.ToString());

                    cc.ClickButton(driver, tst.templateHomeBtn);
                    tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                    Thread.Sleep(3000);
                    cc.ScrollPage(driver);

                    //Tear Down
                    RemoveTemplateUnderTest(templateName);

                    StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                    tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                    passFail = 0;
                }
                catch (Exception e)
                {
                    tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                    cc.ClickButton(driver, tst.templateHomeBtn);
                    tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                    Thread.Sleep(3000);
                    cc.ScrollPage(driver);

                    //Tear Down
                    RemoveTemplateUnderTest(templateName);

                    StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                    tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                    passFail = 1;

                }
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Tear Down
                cc.ClickButton(driver, tst.templateHomeBtn);
                tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                Thread.Sleep(5000);
                cc.ScrollPage(driver);
                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        [TestMethod, TestCategory("Under Review - Pantograph Tests")]
        public void Pantograph_MicroprintBorder_ChangeTextColor()
        {
            string imagePath2 = "";
            string imagePath1 = "";
            string originalHex = "";
            string templateName = "";

            if (qaDev.ToLower() == dev)
            {
                instance = 11617921;
                testName = "Pantograph_MicroprintBorder_ChangeTextColor_Dev";
            }
            else
            {
                if (qaDev.ToLower() == qa)
                {
                    instance = 11659685;
                    testName = "Pantograph_MicroprintBorder_ChangeTextColor_QA";
                }
                //    else
                //    {
                //        if (qaDev.ToLower() == beta)
                //        {
                //            instance = 7225653;
                //            testName = "Pantograph_MicroprintBorder_ChangeTextColor_Beta";
                //        }
                else
                {
                    instance = 11660199;
                    testName = "Pantograph_MicroprintBorder_ChangeTextColor_Prod";
                }
                //        }
                //    }
            }
            passFail = 1;  //reset before test
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Pantograph-DO NOT USE";
            CreateLogFile();

            try
            {
                //setup
                CreateTemplateUnderTest(templateName);
                baseWindowHandle = driver.CurrentWindowHandle;

                //Test
                SetupPantograph();

                cc.ClickButton(driver, tst.pantographEnableMicroprintBorder);
                tglog.AppendLogText(filePath, string.Concat("Enable MP Border = ", cc.FetchChecked(driver, tst.pantographEnableMicroprintBorder)));
                Thread.Sleep(3000);

                cc.ClickButton(driver, tst.pantographMPEditText);
                tglog.AppendLogText(filePath, "Opened Edit Text Popup.....");

                cc.EnterText(driver, "Testing MP Border Color.....", tst.templateTextInput);
                cc.ClickButton(driver, tst.templateTextSave);
                Thread.Sleep(3000);

                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Pantograph Text Color ", ScreenshotImageFormat.Png, 0);
                Thread.Sleep(3000);

                //test
                cc.ClickButton(driver, tst.pantographMPBorderColor);
                Thread.Sleep(3000);
                originalHex = cc.FetchIDefaultValue(driver, tst.pantographMPText_Hex);
                tglog.AppendLogText(filePath, string.Concat("Text R Value = ", cc.FetchIDefaultValue(driver, tst.pantographMPText_R)));
                tglog.AppendLogText(filePath, string.Concat("Text G Value = ", cc.FetchIDefaultValue(driver, tst.pantographMPText_G)));
                tglog.AppendLogText(filePath, string.Concat("Text B Value = ", cc.FetchIDefaultValue(driver, tst.pantographMPText_B)));
                tglog.AppendLogText(filePath, string.Concat("Text A Value = ", cc.FetchIDefaultValue(driver, tst.pantographMPText_A)));
                tglog.AppendLogText(filePath, string.Concat("Text Hex Value = ", originalHex));

                cc.EnterText(driver, "31", tst.pantographMPText_R);
                Thread.Sleep(3000);
                cc.EnterText(driver, "219", tst.pantographMPText_G);
                Thread.Sleep(3000);
                cc.EnterText(driver, "219", tst.pantographMPText_B);
                Thread.Sleep(3000);
                cc.EnterText(driver, "90", tst.pantographMPText_A);
                Thread.Sleep(3000);

                tglog.AppendLogText(filePath, string.Concat("Text R Value = ", cc.FetchIDefaultValue(driver, tst.pantographMPText_R)));
                tglog.AppendLogText(filePath, string.Concat("Text G Value = ", cc.FetchIDefaultValue(driver, tst.pantographMPText_G)));
                tglog.AppendLogText(filePath, string.Concat("Text B Value = ", cc.FetchIDefaultValue(driver, tst.pantographMPText_B)));
                tglog.AppendLogText(filePath, string.Concat("Text A Value = ", cc.FetchIDefaultValue(driver, tst.pantographMPText_A)));
                tglog.AppendLogText(filePath, string.Concat("Text Hex Value = ", cc.FetchIDefaultValue(driver, tst.pantographMPText_Hex)));

                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Pantograph Text Color ", ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);

                StringAssert.Contains(false.ToString(), (originalHex == cc.FetchIDefaultValue(driver, tst.pantographMPText_Hex)).ToString());
                tglog.AppendLogText(filePath, string.Concat("Original Hex, ", originalHex, " does not equal ", cc.FetchIDefaultValue(driver, tst.pantographMPText_Hex)));

                cc.ClickButton(driver, tst.pantographMPBorderColor2);
                Thread.Sleep(2000);
                cc.ScrollUP(driver);

                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template ..... OK");
                Thread.Sleep(5000);

                cc.ClickButton(driver, tst.templateHomeBtn);
                tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                Thread.Sleep(3000);
                cc.ScrollPage(driver);

                //Tear Down
                RemoveTemplateUnderTest(templateName);

                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));

                passFail = 0;
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Tear Down
                cc.ClickButton(driver, tst.templateHomeBtn);
                tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                Thread.Sleep(5000);
                cc.ScrollPage(driver);
                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        [TestMethod, TestCategory("Under Review - Pantograph Tests")]
        public void Pantograph_MicroprintBorder_RemoveAllSpaces()
        {
            string templateName = "";
            string EnableMPBorder = "";
            string imagePath2 = "";
            string imagePath1 = "";
            string newText = "";
            string removeChecked = "";
            string text = "";

            if (qaDev.ToLower() == dev)
            {
                instance = 11617917;
                testName = "Pantograph_MicroprintBorder_RemoveAllSpaces_Dev";
            }
            else
            {
                if (qaDev.ToLower() == qa)
                {
                    instance = 11659681;
                    testName = "Pantograph_MicroprintBorder_RemoveAllSpaces_QA";
                }
                //else
                //{
                //    if (qaDev.ToLower() == beta)
                //    {
                //        instance = 7225653;
                //        testName = "Pantograph_MicroprintBorder_RemoveAllSpaces_Beta";
                //    }
                else
                {
                    instance = 11660195;
                    testName = "Pantograph_MicroprintBorder_RemoveAllSpaces_Prod";
                }
                //}
            }
            passFail = 1;  //reset before test
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Pantograph-DO NOT USE";
            text = "Test MP Static Text.....QA";
            CreateLogFile();

            try
            {
                //setup
                CreateTemplateUnderTest(templateName);
                baseWindowHandle = driver.CurrentWindowHandle;

                SetupPantograph();

                EnableMPBorder = cc.FetchChecked(driver, tst.pantographEnableMicroprintBorder);
                tglog.AppendLogText(filePath, string.Concat("Enable MP Border = ", EnableMPBorder));
                cc.ClickButton(driver, tst.pantographEnableMicroprintBorder);
                tglog.AppendLogText(filePath, string.Concat("Enable MP Border = ", cc.FetchChecked(driver, tst.pantographEnableMicroprintBorder)));

                Thread.Sleep(3000);

                cc.ClickButton(driver, tst.pantographMPEditText);
                tglog.AppendLogText(filePath, "Opened Edit Text Popup.....");

                cc.EnterText(driver, text, tst.templateTextInput);
                newText = cc.FetchIDefaultValue(driver, tst.templateTextInput);
                cc.ClickButton(driver, tst.templateTextSave);
                tglog.AppendLogText(filePath, string.Concat("Enter Static Text in Pantograph, ", newText, ",.....OK"));
                Thread.Sleep(3000);

                //Test
                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Pantograph MP Remove All Spaces", ScreenshotImageFormat.Png, 0);
                removeChecked = cc.FetchChecked(driver, tst.pantographRemoveAllSpaces);
                cc.ClickButton(driver, tst.pantographRemoveAllSpaces);
                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Pantograph MP Remove All Spaces", ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);

                //save and return to templates
                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template ..... OK");
                Thread.Sleep(3000);

                cc.ClickButton(driver, tst.templateHomeBtn);
                tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                Thread.Sleep(3000);
                cc.ScrollPage(driver);

                //Tear Down
                RemoveTemplateUnderTest(templateName);

                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                passFail = 0;
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Tear Down
                cc.ClickButton(driver, tst.templateHomeBtn);
                tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                Thread.Sleep(5000);
                cc.ScrollPage(driver);
                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        [TestMethod, TestCategory("Under Review - Pantograph Tests")]
        public void Pantograph_MicroprintBorder_CancelEditText()
        {
            string templateName = "";
            string EnableMPBorder = "";
            string imagePath2 = "";
            string imagePath1 = "";
            string newText = "";
            string originalText = "";
            string text = "";

            if (qaDev.ToLower() == dev)
            {
                instance = 11617916;
                testName = "Pantograph_MicroprintBorder_CancelEditText_Dev";
            }
            else
            {
                if (qaDev.ToLower() == qa)
                {
                    instance = 11659680;
                    testName = "Pantograph_MicroprintBorder_CancelEditText_QA";
                }
                //else
                //{
                //    if (qaDev.ToLower() == beta)
                //    {
                //        instance = 7225653;
                //        testName = "Pantograph_MicroprintBorder_CancelEditText_Beta";
                //    }
                else
                {
                    instance = 11660194;
                    testName = "Pantograph_MicroprintBorder_CancelEditText_Prod";
                }
                //}
            }
            passFail = 1;  //reset before test
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Pantograph-DO NOT USE";
            text = "Test MP Static Text.....QA";
            CreateLogFile();

            try
            {
                //setup
                CreateTemplateUnderTest(templateName);
                baseWindowHandle = driver.CurrentWindowHandle;

                //Test
                SetupPantograph();

                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Pantograph Cancel MP Border Edit Text ", ScreenshotImageFormat.Png, 0);
                Thread.Sleep(3000);

                EnableMPBorder = cc.FetchChecked(driver, tst.pantographEnableMicroprintBorder);
                tglog.AppendLogText(filePath, string.Concat("Enable MP Border = ", EnableMPBorder));
                cc.ClickButton(driver, tst.pantographEnableMicroprintBorder);
                tglog.AppendLogText(filePath, string.Concat("Enable MP Border = ", cc.FetchChecked(driver, tst.pantographEnableMicroprintBorder)));

                cc.ClickButton(driver, tst.pantographMPEditText);
                tglog.AppendLogText(filePath, "Opened Edit Text Popup.....");

                originalText = cc.FetchIDefaultValue(driver, tst.templateTextInput);
                cc.EnterText(driver, text, tst.templateTextInput);
                newText = cc.FetchIDefaultValue(driver, tst.templateTextInput);
                cc.ClickButton(driver, tst.templateTextCancel);
                tglog.AppendLogText(filePath, string.Concat("Cancel Edit Text in Pantograph, ", newText, ",.....OK"));
                Thread.Sleep(3000);

                Thread.Sleep(3000);

                // verify 
                try
                {
                    cc.ClickButton(driver, tst.pantographMPEditText);
                    tglog.AppendLogText(filePath, "Opened Edit Text Popup.....");
                    Thread.Sleep(5000);

                    tglog.AppendLogText(filePath, string.Concat(cc.FetchIDefaultValue(driver, tst.templateTextInput), " should be ithe same as ", originalText));
                    StringAssert.Contains((originalText == cc.FetchIDefaultValue(driver, tst.templateTextInput)).ToString(), true.ToString());

                    cc.ClickButton(driver, tst.templateTextCancel);
                    Thread.Sleep(3000);

                    imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Pantograph Cancel MP Border Edit Text", ScreenshotImageFormat.Png, 1);
                    cc.ClickButton(driver, tst.saveButton);
                    tglog.AppendLogText(filePath, "Saved Template ..... OK");
                    Thread.Sleep(3000);

                    cc.ClickButton(driver, tst.templateHomeBtn);
                    tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                    Thread.Sleep(3000);
                    cc.ScrollPage(driver);

                    //Tear Down
                    RemoveTemplateUnderTest(templateName);

                    try
                    {
                        StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), true.ToString());
                        tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                        passFail = 0;
                    }
                    catch (Exception e)
                    {
                        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
                        passFail = 1;
                    }
                }
                catch (Exception e)
                {
                    cc.ClickButton(driver, tst.templateTextCancel);
                    Thread.Sleep(3000);

                    tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
                    passFail = 1;

                    imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Pantograph Enter Static Text", ScreenshotImageFormat.Png, 1);
                    cc.ClickButton(driver, tst.saveButton);
                    tglog.AppendLogText(filePath, "Saved Template ..... OK");
                    Thread.Sleep(3000);

                    cc.ClickButton(driver, tst.templateHomeBtn);
                    tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                    Thread.Sleep(3000);
                    cc.ScrollPage(driver);

                    //Tear Down
                    RemoveTemplateUnderTest(templateName);
                }
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Tear Down
                cc.ClickButton(driver, tst.templateHomeBtn);
                tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                Thread.Sleep(5000);
                cc.ScrollPage(driver);
                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        [TestMethod, TestCategory("Under Review - Pantograph Tests")]
        public void Pantograph_MicroprintBorder_SelectJobMetadata()
        {
            string imagePath2 = "";
            string imagePath1 = "";
            string templateName = "";

            if (qaDev.ToLower() == dev)
            {
                instance = 11617915;
                testName = "Pantograph_MicroprintBorder_SelectJobMetadata_Dev";
            }
            else
            {
                if (qaDev.ToLower() == qa)
                {
                    instance = 11659679;
                    testName = "Pantograph_MicroprintBorder_SelectJobMetadata_QA";
                }
                //    else
                //    {
                //        if (qaDev.ToLower() == beta)
                //        {
                //            instance = 7225653;
                //            testName = "Pantograph_MicroprintBorder_SelectJobMetadata_Beta";
                //        }
                else
                {
                    instance = 11660193;
                    testName = "Pantograph_MicroprintBorder_SelectJobMetadata_Prod";
                }
                //        }
                //    }
            }
            passFail = 1;  //reset before test
            imagePath = string.Concat(filePath, "Test Files\\");
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Pantograph-DO NOT USE";
            CreateLogFile();

            try
            {
                //setup
                CreateTemplateUnderTest(templateName);
                baseWindowHandle = driver.CurrentWindowHandle;

                SetupPantograph();
                Thread.Sleep(3000);
                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Pantograph Select MP Border Job Metadata", ScreenshotImageFormat.Png, 0);

                //Test
                cc.ClickButton(driver, tst.pantographEnableMicroprintBorder);
                cc.ClickButton(driver, tst.pantographMPEditText);
                Thread.Sleep(5000);

                cc.ClickButton(driver, tst.templateMetaData);
                tst.ClickMetadataButton(driver, tst.templateMetadataList, "UserName");
                Thread.Sleep(2000);
                cc.ClickButton(driver, tst.templateTextSave);
                tglog.AppendLogText(filePath, "Selected Job Metadata.....OK");
                Thread.Sleep(3000);

                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template ..... OK");
                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Pantograph Select MP Border Job Metadata", ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);

                cc.ClickButton(driver, tst.templateHomeBtn);
                tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                Thread.Sleep(3000);
                cc.ScrollPage(driver);

                //Tear Down
                RemoveTemplateUnderTest(templateName);

                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                passFail = 0;
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Tear Down
                cc.ClickButton(driver, tst.templateHomeBtn);
                tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                Thread.Sleep(5000);
                cc.ScrollPage(driver);
                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        //-----------------------------------------------------------------------------------------------
        [TestMethod, TestCategory("Under Review - Pantograph Tests")]
        public void Pantograph_PantographProperties_ChangeBackgroundColor()
        {
            string imagePath2 = "";
            string imagePath1 = "";
            string originalColor = "";
            string templateName = "";

            if (qaDev.ToLower() == dev)
            {
                instance = 11602706;
                testName = "Pantograph_PantographProperties_ChangeBackgroundColor_Dev";
            }
            else
            {
                if (qaDev.ToLower() == qa)
                {
                    instance = 11659676;
                    testName = "Pantograph_PantographProperties_ChangeBackgroundColor_QA";
                }
                //    else
                //    {
                //        if (qaDev.ToLower() == beta)
                //        {
                //            instance = 7225653;
                //            testName = "Pantograph_PantographProperties_ChangeBackgroundColor_Beta";
                //        }
                else
                {
                    instance = 11660190;
                    testName = "Pantograph_PantographProperties_ChangeBackgroundColor_Prod";
                }
                //        }
                //    }
            }
            passFail = 1;  //reset before test
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Pantograph-DO NOT USE";
            CreateLogFile();

            try
            {
                //setup
                CreateTemplateUnderTest(templateName);
                baseWindowHandle = driver.CurrentWindowHandle;

                //Test
                SetupPantograph();

                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Pantograph Background Color ", ScreenshotImageFormat.Png, 0);
                Thread.Sleep(3000);

                cc.ClickButton(driver, tst.pantographBackground);
                originalColor = cc.FetchIDefaultValue(driver, tst.pantographBackground);

                cc.EnterText(driver, "290", tst.pantographBackground);
                Thread.Sleep(1000);

                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Pantograph Background Color ", ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);

                StringAssert.Contains(false.ToString(), (originalColor == cc.FetchIDefaultValue(driver, tst.pantographBackground)).ToString());
                tglog.AppendLogText(filePath, string.Concat("Original Hex, ", originalColor, " does not equal ", cc.FetchIDefaultValue(driver, tst.pantographBackground)));

                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template ..... OK");
                Thread.Sleep(5000);

                cc.ClickButton(driver, tst.templateHomeBtn);
                tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                Thread.Sleep(3000);
                cc.ScrollPage(driver);

                //Tear Down
                RemoveTemplateUnderTest(templateName);

                try
                {
                    StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                    tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                    passFail = 0;
                }
                catch (Exception e)
                {
                    passFail = 1;
                    tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
                }
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Tear Down
                cc.ClickButton(driver, tst.templateHomeBtn);
                tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                Thread.Sleep(5000);
                cc.ScrollPage(driver);
                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        [TestMethod, TestCategory("Under Review - Pantograph Tests")]
        public void Pantograph_PantographProperties_ChangeForegroundColor()
        {
            string imagePath2 = "";
            string imagePath1 = "";
            string originalColor = "";
            string templateName = "";

            if (qaDev.ToLower() == dev)
            {
                instance = 11602707;
                testName = "Pantograph_PantographProperties_ChangeForegroundColor_Dev";
            }
            else
            {
                if (qaDev.ToLower() == qa)
                {
                    instance = 11659677;
                    testName = "Pantograph_PantographProperties_ChangeForegroundColor_QA";
                }
                //    else
                //    {
                //        if (qaDev.ToLower() == beta)
                //        {
                //            instance = 7225653;
                //            testName = "Pantograph_PantographProperties_ChangeForegroundColor_Beta";
                //        }
                else
                {
                    instance = 11660191;
                    testName = "Pantograph_PantographProperties_ChangeForegroundColor_Prod";
                }
                //        }
                //    }
            }
            passFail = 1;  //reset before test
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Pantograph-DO NOT USE";
            CreateLogFile();

            try
            {
                //setup
                CreateTemplateUnderTest(templateName);
                baseWindowHandle = driver.CurrentWindowHandle;

                //Test
                SetupPantograph();

                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Pantograph Foreground Color ", ScreenshotImageFormat.Png, 0);
                Thread.Sleep(3000);

                cc.ClickButton(driver, tst.pantographForeground);
                originalColor = cc.FetchIDefaultValue(driver, tst.pantographForeground);

                cc.EnterText(driver, "90", tst.pantographForeground);
                Thread.Sleep(1000);

                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Pantograph Foreground Color ", ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);

                StringAssert.Contains(false.ToString(), (originalColor == cc.FetchIDefaultValue(driver, tst.pantographForeground)).ToString());
                tglog.AppendLogText(filePath, string.Concat("Original Hex, ", originalColor, " does not equal ", cc.FetchIDefaultValue(driver, tst.pantographForeground)));

                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template ..... OK");
                Thread.Sleep(5000);

                cc.ClickButton(driver, tst.templateHomeBtn);
                tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                Thread.Sleep(3000);
                cc.ScrollPage(driver);

                //Tear Down
                RemoveTemplateUnderTest(templateName);

                try
                {
                    StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                    tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                    passFail = 0;
                }
                catch (Exception e)
                {
                    passFail = 1;
                    tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
                }
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Tear Down
                cc.ClickButton(driver, tst.templateHomeBtn);
                tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                Thread.Sleep(5000);
                cc.ScrollPage(driver);
                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        [TestMethod, TestCategory("Under Review - Pantograph Tests")]
        public void Pantograph_PantographProperties_ChangeInterferencePattern()
        {
            string imagePath2 = "";
            string imagePath1 = "";
            string originalPattern = "";
            string templateName = "";

            if (qaDev.ToLower() == dev)
            {
                instance = 11602708;
                testName = "Pantograph_PantographProperties_ChangeInterferencePattern_Dev";
            }
            else
            {
                if (qaDev.ToLower() == qa)
                {
                    instance = 11659678;
                    testName = "Pantograph_PantographProperties_ChangeInterferencePattern_QA";
                }
                //    else
                //    {
                //        if (qaDev.ToLower() == beta)
                //        {
                //            instance = 7225653;
                //            testName = "Pantograph_PantographProperties_ChangeInterferencePattern_Beta";
                //        }
                else
                {
                    instance = 11660192;
                    testName = "Pantograph_PantographProperties_ChangeInterferencePattern_Prod";
                }
                //        }
                //    }
            }
            passFail = 1;  //reset before test
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Pantograph-DO NOT USE";
            CreateLogFile();

            try
            {
                //setup
                CreateTemplateUnderTest(templateName);
                baseWindowHandle = driver.CurrentWindowHandle;

                //Test
                SetupPantograph();

                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Pantograph Interference Pattern ", ScreenshotImageFormat.Png, 0);
                Thread.Sleep(3000);

                cc.ClickButton(driver, tst.pantographInterferencePattern);
                originalPattern = cc.FetchIDefaultValue(driver, tst.pantographInterferencePattern);

                cc.EnterText(driver, "10", tst.pantographInterferencePattern);
                Thread.Sleep(1000);

                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Pantograph Interference Pattern ", ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);

                StringAssert.Contains(false.ToString(), (originalPattern == cc.FetchIDefaultValue(driver, tst.pantographInterferencePattern)).ToString());
                tglog.AppendLogText(filePath, string.Concat("Original Pattern, ", originalPattern, " does not equal ", cc.FetchIDefaultValue(driver, tst.pantographInterferencePattern)));

                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template ..... OK");
                Thread.Sleep(5000);

                cc.ClickButton(driver, tst.templateHomeBtn);
                tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                Thread.Sleep(3000);
                cc.ScrollPage(driver);

                //Tear Down
                RemoveTemplateUnderTest(templateName);

                try
                {
                    StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                    tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                    passFail = 0;
                }
                catch (Exception e)
                {
                    passFail = 1;
                    tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
                }
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Tear Down
                cc.ClickButton(driver, tst.templateHomeBtn);
                tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                Thread.Sleep(5000);
                cc.ScrollPage(driver);
                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        [TestMethod, TestCategory("Under Review - Pantograph Tests")]
        public void Pantograph_DownloadTestPattern_DownloadTestPatterns()
        {
            string templateName = "";
            string EnableMPBorder = "";

            if (qaDev.ToLower() == dev)
            {
                instance = 11657846;
                testName = "Pantograph_PantographProperties_DownloadTestPatterns_Dev";
            }
            else
            {
                if (qaDev.ToLower() == qa)
                {
                    instance = 11659689;
                    testName = "Pantograph_PantographProperties_DownloadTestPatterns_QA";
                }
                //else
                //{
                //    if (qaDev.ToLower() == beta)
                //    {
                //        instance = 7225653;
                //        testName = "Pantograph_PantographProperties_DownloadTestPatterns_Beta";
                //    }
                else
                {
                    instance = 11660203;
                    testName = "Pantograph_PantographProperties_DownloadTestPatterns_Prod";
                }
                //}
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Pantograph-DO NOT USE";
            CreateLogFile();

            try
            {
                //setup
                CreateTemplateUnderTest(templateName);
                baseWindowHandle = driver.CurrentWindowHandle;

                SetupPantograph();

                EnableMPBorder = cc.FetchChecked(driver, tst.pantographEnableMicroprintBorder);
                tglog.AppendLogText(filePath, string.Concat("Enable MP Border = ", EnableMPBorder));
                Thread.Sleep(3000);

                //Test
                dptp = tst.OpenPatterns(driver);
                tglog.AppendLogText(filePath, "Opened Download Test Patterns page.....OK");
                Thread.Sleep(3000);

                cc.ClickButton(driver, dptp.testPattern_1);
                Thread.Sleep(5000);
                tglog.AppendLogText(filePath, "Downloaded Pantograph Test Pattern 01.pdf");
                Thread.Sleep(3000);

            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }

            Thread.Sleep(5000);
        }

        //--------------------------------------------------------------------------------------------------
        [TestMethod, TestCategory("Under Review - Pantograph Tests")]
        public void Pantograph_Position_ChangePosition()
        {
            string imagePath2 = "";
            string imagePath1 = "";
            string templateName = "";

            if (qaDev.ToLower() == dev)
            {
                instance = 11584272;
                testName = "Pantograph_Position_ChangePosition_Dev";
            }
            else
            {
                if (qaDev.ToLower() == qa)
                {
                    instance = 11659670;
                    testName = "Pantograph_Position_ChangePosition_QA";
                }
                //    else
                //    {
                //        if (qaDev.ToLower() == beta)
                //        {
                //            instance = 7225653;
                //            testName = "Pantograph_Position_ChangePosition_Beta";
                //        }
                else
                {
                    instance = 11660184;
                    testName = "Pantograph_Position_ChangePosition_Prod";
                }
                //        }
                //    }
            }
            passFail = 1;  //reset before test
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Pantograph-DO NOT USE";
            CreateLogFile();

            try
            {
                //setup
                CreateTemplateUnderTest(templateName);
                baseWindowHandle = driver.CurrentWindowHandle;

                SetupAuthMark();

                SetupPantograph();
                Thread.Sleep(2000);

                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Pantograph Position ", ScreenshotImageFormat.Png, 0);
                tglog.AppendLogText(filePath, cc.FetchSelectedOption(driver, tst.pantographPosition));
                Thread.Sleep(3000);

                //Test
                try
                {
                    cc.SelectDropDownByElement(driver, tst.pantographPosition, "Foreground");
                    Thread.Sleep(2000);
                    tglog.AppendLogText(filePath, cc.FetchSelectedOption(driver, tst.pantographPosition));

                    imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Pantograph Position ", ScreenshotImageFormat.Png, 1);
                    cc.ClickButton(driver, tst.saveButton);
                    tglog.AppendLogText(filePath, "Saved Template ..... OK");
                    Thread.Sleep(3000);

                    cc.ClickButton(driver, tst.templateHomeBtn);
                    tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                    Thread.Sleep(3000);
                    cc.ScrollPage(driver);
                }
                catch (Exception e)
                {
                    passFail = 1;
                    tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
                    cc.ClickButton(driver, tst.saveButton);
                    tglog.AppendLogText(filePath, "Saved Template ..... OK");
                    Thread.Sleep(3000);

                    cc.ClickButton(driver, tst.templateHomeBtn);
                    tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                    Thread.Sleep(3000);
                    cc.ScrollPage(driver);
                }
                //Tear Down
                RemoveTemplateUnderTest(templateName);

                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                passFail = 0;
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Tear Down
                cc.ClickButton(driver, tst.templateHomeBtn);
                tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                Thread.Sleep(5000);
                cc.ScrollPage(driver);
                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        [TestMethod, TestCategory("Under Review - Pantograph Tests")]
        public void Pantograph_Position_FullPage()
        {
            string templateName = "";
            string imagePath2 = "";
            string imagePath1 = "";

            if (qaDev.ToLower() == dev)
            {
                instance = 11584215;
                testName = "Pantograph_Position_FullPage_Dev";
            }
            else
            {
                if (qaDev.ToLower() == qa)
                {
                    instance = 11659669;
                    testName = "Pantograph_Position_FullPage_QA";
                }
                //    else
                //    {
                //        if (qaDev.ToLower() == beta)
                //        {
                //            instance = 7225653;
                //            testName = "Pantograph_Position_FullPage_Beta";
                //        }
                else
                {
                    instance = 11660183;
                    testName = "Pantograph_Position_FullPage_Prod";
                }
                //        }
                //    }
            }
            passFail = 1;  //reset before test
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Pantograph-DO NOT USE";
            CreateLogFile();

            try
            {
                //setup
                CreateTemplateUnderTest(templateName);
                baseWindowHandle = driver.CurrentWindowHandle;

                //Test
                SetupPantograph();

                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, string.Concat("Full Page Pantograph ", cc.FetchChecked(driver, tst.pantographFullPage), " "), ScreenshotImageFormat.Png, 0);
                tglog.AppendLogText(filePath, string.Concat("Full Page Pantograph = ", cc.FetchChecked(driver, tst.pantographFullPage)));
                Thread.Sleep(3000);

                cc.ClickButton(driver, tst.pantographFullPage);
                tglog.AppendLogText(filePath, string.Concat("Full Page = ", cc.FetchChecked(driver, tst.pantographFullPage)));

                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, string.Concat("Full Page Pantograph ", cc.FetchChecked(driver, tst.pantographFullPage), " "), ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);

                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template ..... OK");
                Thread.Sleep(5000);

                cc.ClickButton(driver, tst.templateHomeBtn);
                tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                Thread.Sleep(3000);
                cc.ScrollPage(driver);

                //Tear Down
                RemoveTemplateUnderTest(templateName);

                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                passFail = 0;
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Tear Down
                cc.ClickButton(driver, tst.templateHomeBtn);
                tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                Thread.Sleep(5000);
                cc.ScrollPage(driver);
                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        [TestMethod, TestCategory("Under Review - Pantograph Tests")]
        public void Pantograph_Position_Move()
        {
            string templateName = "";
            string testValu = "";
            string testValu2 = "";
            string imagePath2 = "";
            string imagePath1 = "";

            if (qaDev.ToLower() == dev)
            {
                instance = 11584105;
                testName = "Pantograph_Position_Move_Dev";
            }
            else
            {
                if (qaDev.ToLower() == qa)
                {
                    instance = 11659847;
                    testName = "Pantograph_Position_Move_QA";
                }
                //else
                //{
                //    if (qaDev.ToLower() == beta)
                //    {
                //        instance = 7225653;
                //        testName = "Pantograph_Position_Move_Beta";
                //    }
                else
                {
                    instance = 11660176;
                    testName = "Pantograph_Position_Move_Prod";
                }
                //}
            }
            passFail = 1;  //reset before test
            testValu = "561";
            testValu2 = "486";
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Pantograph-DO NOT USE";
            CreateLogFile();

            try
            {
                //setup
                CreateTemplateUnderTest(templateName);
                baseWindowHandle = driver.CurrentWindowHandle;

                //Test
                SetupPantograph();

                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, string.Concat("Pantograph X and Y Coordinate Change ", testValu, " "), ScreenshotImageFormat.Png, 0);
                Thread.Sleep(3000);

                cc.EnterText(driver, testValu2, tst.elementX);
                tglog.AppendLogText(filePath, string.Concat("Pantograph X Coordinate = ", testValu2));

                cc.EnterText(driver, testValu, tst.elementY);
                tglog.AppendLogText(filePath, string.Concat("Pantograph Y Coordinate = ", testValu));

                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, string.Concat("Pantograph X and Y Coordinate Change ", testValu, " "), ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);

                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template ..... OK");
                Thread.Sleep(3000);

                cc.ClickButton(driver, tst.templateHomeBtn);
                tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                Thread.Sleep(3000);
                cc.ScrollPage(driver);

                //Tear Down
                RemoveTemplateUnderTest(templateName);

                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                passFail = 0;
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Tear Down
                cc.ClickButton(driver, tst.templateHomeBtn);
                tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                Thread.Sleep(5000);
                cc.ScrollPage(driver);
                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        [TestMethod, TestCategory("Under Review - Pantograph Tests")]
        public void Pantograph_Position_Resize()
        {
            string templateName = "";
            string testValu = "";
            string testValu2 = "";
            string imagePath2 = "";
            string imagePath1 = "";

            if (qaDev.ToLower() == dev)
            {
                instance = 11584106;
                testName = "Pantograph_Position_Resize_Dev";
            }
            else
            {
                if (qaDev.ToLower() == qa)
                {
                    instance = 11659848;
                    testName = "Pantograph_Position_Resize_QA";
                }
                //    else
                //    {
                //        if (qaDev.ToLower() == beta)
                //        {
                //            instance = 7225653;
                //            testName = "Pantograph_Position_Resize_Beta";
                //        }
                else
                {
                    instance = 11660177;
                    testName = "Pantograph_Position_Resize_Prod";
                }
            }
            //    }
            //}
            passFail = 1;  //reset before test
            testValu = "300";
            testValu2 = "200";
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Pantograph-DO NOT USE";
            CreateLogFile();

            try
            {
                //setup
                CreateTemplateUnderTest(templateName);
                baseWindowHandle = driver.CurrentWindowHandle;

                //Test
                SetupPantograph();

                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, string.Concat("Pantograph Height and Width Change ", testValu, " "), ScreenshotImageFormat.Png, 0);
                Thread.Sleep(3000);

                cc.EnterText(driver, testValu2, tst.elementWidth);
                tglog.AppendLogText(filePath, string.Concat("Pantograph Width = ", testValu2));

                cc.EnterText(driver, testValu, tst.elementHeight);
                tglog.AppendLogText(filePath, string.Concat("Pantograph Height = ", testValu));

                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, string.Concat("Pantograph Height and Width Change ", testValu, " "), ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);

                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template ..... OK");
                Thread.Sleep(3000);

                cc.ClickButton(driver, tst.templateHomeBtn);
                tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                Thread.Sleep(3000);
                cc.ScrollPage(driver);

                //Tear Down
                RemoveTemplateUnderTest(templateName);

                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                passFail = 0;
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Tear Down
                cc.ClickButton(driver, tst.templateHomeBtn);
                tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                Thread.Sleep(5000);
                cc.ScrollPage(driver);
                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        //--------------------------------------------------------------------------------------------------
        [TestMethod, TestCategory("Under Review - Pantograph Tests")]
        public void Pantograph_TextProperties_AlignText()
        {
            string templateName = "";

            if (qaDev.ToLower() == dev)
            {
                instance = 11600787;
                testName = "Pantograph_TextProperties_AlignText_Dev";
            }
            else
            {
                if (qaDev.ToLower() == qa)
                {
                    instance = 11659673;
                    testName = "Pantograph_TextProperties_AlignText_QA";
                }
                //    else
                //    {
                //        if (qaDev.ToLower() == beta)
                //        {
                //            instance = 7225653;
                //            testName = "Pantograph_TextProperties_AlignText_Beta";
                //        }
                else
                {
                    instance = 11660187;
                    testName = "Pantograph_TextProperties_AlignText_Prod";
                }
                //        }
                //    }
            }
            passFail = 1;  //reset before test
            imagePath = string.Concat(filePath, "Test Files\\");
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Pantograph-DO NOT USE";
            CreateLogFile();

            try
            {
                //setup
                CreateTemplateUnderTest(templateName);
                baseWindowHandle = driver.CurrentWindowHandle;

                SetupPantograph();

                Thread.Sleep(3000);
                tglog.AppendLogText(filePath, cc.FetchClass(driver, tst.element_AlignLeft));
                tglog.AppendLogText(filePath, cc.FetchClass(driver, tst.element_AlignCenter));
                tglog.AppendLogText(filePath, string.Concat(cc.FetchClass(driver, tst.element_AlignRight), Environment.NewLine));

                //Test
                cc.ClickButton(driver, tst.element_AlignRight);
                tglog.AppendLogText(filePath, "Click Align Right");
                tglog.AppendLogText(filePath, cc.FetchClass(driver, tst.element_AlignLeft));
                tglog.AppendLogText(filePath, cc.FetchClass(driver, tst.element_AlignCenter));
                tglog.AppendLogText(filePath, string.Concat(cc.FetchClass(driver, tst.element_AlignRight), Environment.NewLine));
                StringAssert.Contains(cc.FetchClass(driver, tst.element_AlignRight), "btn btn-toggle-on");
                StringAssert.Contains(cc.FetchClass(driver, tst.element_AlignCenter), "btn btn-toggle-off");
                StringAssert.Contains(cc.FetchClass(driver, tst.element_AlignLeft), "btn btn-toggle-off");

                cc.ClickButton(driver, tst.element_AlignLeft);
                tglog.AppendLogText(filePath, "Click Align Left");
                tglog.AppendLogText(filePath, cc.FetchClass(driver, tst.element_AlignLeft));
                tglog.AppendLogText(filePath, cc.FetchClass(driver, tst.element_AlignCenter));
                tglog.AppendLogText(filePath, string.Concat(cc.FetchClass(driver, tst.element_AlignRight), Environment.NewLine));
                StringAssert.Contains(cc.FetchClass(driver, tst.element_AlignRight), "btn btn-toggle-off");
                StringAssert.Contains(cc.FetchClass(driver, tst.element_AlignCenter), "btn btn-toggle-off");
                StringAssert.Contains(cc.FetchClass(driver, tst.element_AlignLeft), "btn btn-toggle-on");

                cc.ClickButton(driver, tst.element_AlignCenter);
                tglog.AppendLogText(filePath, "Click Align Center");
                tglog.AppendLogText(filePath, cc.FetchClass(driver, tst.element_AlignLeft));
                tglog.AppendLogText(filePath, cc.FetchClass(driver, tst.element_AlignCenter));
                tglog.AppendLogText(filePath, string.Concat(cc.FetchClass(driver, tst.element_AlignRight), Environment.NewLine));
                StringAssert.Contains(cc.FetchClass(driver, tst.element_AlignRight), "btn btn-toggle-off");
                StringAssert.Contains(cc.FetchClass(driver, tst.element_AlignCenter), "btn btn-toggle-on");
                StringAssert.Contains(cc.FetchClass(driver, tst.element_AlignLeft), "btn btn-toggle-off");

                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template ..... OK");
                Thread.Sleep(3000);

                cc.ClickButton(driver, tst.templateHomeBtn);
                tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                Thread.Sleep(3000);
                cc.ScrollPage(driver);

                //Tear Down
                RemoveTemplateUnderTest(templateName);

                passFail = 0;
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Tear Down
                cc.ClickButton(driver, tst.templateHomeBtn);
                tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                Thread.Sleep(5000);
                cc.ScrollPage(driver);
                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        [TestMethod, TestCategory("Under Review - Pantograph Tests")]
        public void Pantograph_TextProperties_CancelEditText()
        {
            string imagePath2 = "";
            string imagePath1 = "";
            string originalText = "";
            string templateName = "";
            string text = "";
            string newText = "";

            if (qaDev.ToLower() == dev)
            {
                instance = 11584311;
                testName = "Pantograph_TextProperties_CancelEditText_Dev";
            }
            else
            {
                if (qaDev.ToLower() == qa)
                {
                    instance = 11659671;
                    testName = "Pantograph_TextProperties_CancelEditText_QA";
                }
                //    else
                //    {
                //        if (qaDev.ToLower() == beta)
                //        {
                //            instance = 7225653;
                //            testName = "Pantograph_TextProperties_CancelEditText_Beta";
                //        }
                else
                {
                    instance = 11660185;
                    testName = "Pantograph_TextProperties_CancelEditText_Prod";
                }
                //        }
                //    }
            }
            passFail = 1;  //reset before test
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Pantograph-DO NOT USE";
            text = "Test Static Text.....QA";
            CreateLogFile();

            try
            {
                //setup
                CreateTemplateUnderTest(templateName);
                baseWindowHandle = driver.CurrentWindowHandle;

                SetupPantograph();
                Thread.Sleep(3000);
                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Pantograph Cancel Edit Text", ScreenshotImageFormat.Png, 0);

                //Test
                cc.ClickButton(driver, tst.element_EditText);
                Thread.Sleep(1000);

                originalText = cc.FetchIDefaultValue(driver, tst.templateTextInput);
                cc.EnterText(driver, text, tst.templateTextInput);
                newText = cc.FetchIDefaultValue(driver, tst.templateTextInput);
                cc.ClickButton(driver, tst.templateTextCancel);
                tglog.AppendLogText(filePath, "Cancel Edit  Text in Pantograph.....OK");
                Thread.Sleep(3000);

                // verify 
                try
                {
                    cc.ClickButton(driver, tst.element_EditText);
                    Thread.Sleep(5000);

                    tglog.AppendLogText(filePath, string.Concat(cc.FetchIDefaultValue(driver, tst.templateTextInput), " should be ithe same as ", originalText));
                    StringAssert.Contains((originalText == cc.FetchIDefaultValue(driver, tst.templateTextInput)).ToString(), true.ToString());

                    cc.ClickButton(driver, tst.templateTextCancel);
                    Thread.Sleep(3000);

                    imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Pantograph Cancel Edit Text", ScreenshotImageFormat.Png, 1);
                    cc.ClickButton(driver, tst.saveButton);
                    tglog.AppendLogText(filePath, "Saved Template ..... OK");
                    Thread.Sleep(3000);

                    cc.ClickButton(driver, tst.templateHomeBtn);
                    tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                    Thread.Sleep(3000);
                    cc.ScrollPage(driver);

                    //Tear Down
                    RemoveTemplateUnderTest(templateName);

                    try
                    {
                        StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), true.ToString());
                        tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                        passFail = 0;
                    }
                    catch (Exception e)
                    {
                        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
                        passFail = 1;
                    }
                }
                catch (Exception e)
                {
                    cc.ClickButton(driver, tst.templateTextCancel);
                    Thread.Sleep(3000);

                    tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
                    passFail = 1;

                    imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Pantograph Enter Static Text", ScreenshotImageFormat.Png, 1);
                    cc.ClickButton(driver, tst.saveButton);
                    tglog.AppendLogText(filePath, "Saved Template ..... OK");
                    Thread.Sleep(3000);

                    cc.ClickButton(driver, tst.templateHomeBtn);
                    tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                    Thread.Sleep(3000);
                    cc.ScrollPage(driver);

                    //Tear Down
                    RemoveTemplateUnderTest(templateName);
                }
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Tear Down
                cc.ClickButton(driver, tst.templateHomeBtn);
                tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                Thread.Sleep(5000);
                cc.ScrollPage(driver);
                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        [TestMethod, TestCategory("Under Review - Pantograph Tests")]
        public void Pantograph_TextProperties_ChangeFont()
        {
            string templateName = "";
            string imagePath2 = "";
            string imagePath1 = "";

            if (qaDev.ToLower() == dev)
            {
                instance = 11584109;
                testName = "Pantograph_TextProperties_ChangeFont_Dev";
            }
            else
            {
                if (qaDev.ToLower() == qa)
                {
                    instance = 11659850;
                    testName = "Pantograph_TextProperties_ChangeFont_QA";
                }
                //    else
                //    {
                //        if (qaDev.ToLower() == beta)
                //        {
                //            instance = 7225653;
                //            testName = "TROYMark_TextProperties_ChangeFont_Beta";
                //        }
                else
                {
                    instance = 11660179;
                    testName = "Pantograph_TextProperties_ChangeFont_Prod";
                }
                //        }
                //    }
            }
            passFail = 1;  //reset before test
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Pantograph-DO NOT USE";
            CreateLogFile();

            try
            {
                //setup
                CreateTemplateUnderTest(templateName);
                baseWindowHandle = driver.CurrentWindowHandle;

                //Test
               SetupPantograph();

                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, string.Concat("Pantograph Font Change ", cc.FetchSelectedOption(driver, tst.troyMarkFont), " "), ScreenshotImageFormat.Png, 0);
                Thread.Sleep(3000);

                cc.SelectDropDownByElement(driver, tst.pantographFont, "Courier");
                tglog.AppendLogText(filePath, string.Concat("Pantograph Font = ", cc.FetchSelectedOption(driver, tst.pantographFont)));

                cc.ClickButton(driver, tst.saveButton);
                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, string.Concat("PantographMark Font Change ", cc.FetchSelectedOption(driver, tst.pantographFont), " "), ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);
                tglog.AppendLogText(filePath, "Saved Template ..... OK");
                Thread.Sleep(3000);

                cc.ClickButton(driver, tst.templateHomeBtn);
                tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                Thread.Sleep(3000);
                cc.ScrollPage(driver);

                //Tear Down
                RemoveTemplateUnderTest(templateName);

                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                passFail = 0;
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Tear Down
                cc.ClickButton(driver, tst.templateHomeBtn);
                tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                Thread.Sleep(5000);
                cc.ScrollPage(driver);
                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        [TestMethod, TestCategory("Under Review - Pantograph Tests")]
        public void Pantograph_TextProperties_ChangeFontSize()
        {
            string templateName = "";
            string imagePath2 = "";
            string imagePath1 = "";

            if (qaDev.ToLower() == dev)
            {
                instance = 11601072;
                testName = " Pantograph_TextProperties_ChangeFontSize_Dev";
            }
            else
            {
                if (qaDev.ToLower() == qa)
                {
                    instance = 11659674;
                    testName = " Pantograph_TextProperties_ChangeFontSize_QA";
                }
                //    else
                //    {
                //        if (qaDev.ToLower() == beta)
                //        {
                //            instance = 7225653;
                //            testName = " Pantograph_TextProperties_ChangeFontSize_Beta";
                //        }
                else
                {
                    instance = 11660188;
                    testName = " Pantograph_TextProperties_ChangeFontSize_Prod";
                }
                //        }
                //    }
            }
            passFail = 1;  //reset before test
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Pantograph-DO NOT USE";
            CreateLogFile();

            try
            {
                //setup
                CreateTemplateUnderTest(templateName);
                baseWindowHandle = driver.CurrentWindowHandle;

                //Test
                SetupPantograph();

                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, string.Concat("Pantograph Font Size Change ", cc.FetchIDefaultValue(driver, tst.pantographFontSize), " "), ScreenshotImageFormat.Png, 0);
                Thread.Sleep(3000);

                cc.EnterText(driver, "18", tst.pantographFontSize);
                tglog.AppendLogText(filePath, string.Concat("Pantograph Font Size = ", cc.FetchIDefaultValue(driver, tst.pantographFontSize)));

                cc.ClickButton(driver, tst.saveButton);
                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, string.Concat("Pantograph Font Size Change ", cc.FetchIDefaultValue(driver, tst.pantographFontSize), " "), ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);
                tglog.AppendLogText(filePath, "Saved Template ..... OK");
                Thread.Sleep(3000);

                cc.ClickButton(driver, tst.templateHomeBtn);
                tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                Thread.Sleep(3000);
                cc.ScrollPage(driver);

                //Tear Down
                RemoveTemplateUnderTest(templateName);

                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                passFail = 0;
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Tear Down
                cc.ClickButton(driver, tst.templateHomeBtn);
                tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                Thread.Sleep(5000);
                cc.ScrollPage(driver);
                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        [TestMethod, TestCategory("Under Review - Pantograph Tests")]
        public void Pantograph_TextProperties_ChangeLineSpacing()
        {
            string templateName = "";
            string imagePath2 = "";
            string imagePath1 = "";

            if (qaDev.ToLower() == dev)
            {
                instance = 11584110;
                testName = " Pantograph_TextProperties_ChangeLineSpacing_Dev";
            }
            else
            {
                if (qaDev.ToLower() == qa)
                {
                    instance = 11659851;
                    testName = " Pantograph_TextProperties_ChangeLineSpacing_QA";
                }
                //    else
                //    {
                //        if (qaDev.ToLower() == beta)
                //        {
                //            instance = 7225653;
                //            testName = " Pantograph_TextProperties_ChangeLineSpacing_Beta";
                //        }
                else
                {
                    instance = 11660180;
                    testName = " Pantograph_TextProperties_ChangeLineSpacing_Prod";
                }
                //        }
                //    }
            }
            passFail = 1;  //reset before test
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Pantograph-DO NOT USE";
            CreateLogFile();

            try
            {
                //setup
                CreateTemplateUnderTest(templateName);
                baseWindowHandle = driver.CurrentWindowHandle;

                //Test
                SetupPantograph();

                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, string.Concat("Pantograph Line Spacing Change ", cc.FetchIDefaultValue(driver, tst.pantographLineSpacing), " "), ScreenshotImageFormat.Png, 0);
                Thread.Sleep(3000);

                cc.EnterText(driver, "24", tst.pantographLineSpacing);
                tglog.AppendLogText(filePath, string.Concat("Pantograph Line Spacing = ", cc.FetchIDefaultValue(driver, tst.pantographLineSpacing)));

                cc.ClickButton(driver, tst.saveButton);
                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, string.Concat("Pantograph Line Spacing Change ", cc.FetchIDefaultValue(driver, tst.pantographLineSpacing), " "), ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);
                tglog.AppendLogText(filePath, "Saved Template ..... OK");
                Thread.Sleep(3000);

                cc.ClickButton(driver, tst.templateHomeBtn);
                tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                Thread.Sleep(3000);
                cc.ScrollPage(driver);

                //Tear Down
                RemoveTemplateUnderTest(templateName);

                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                passFail = 0;
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Tear Down
                cc.ClickButton(driver, tst.templateHomeBtn);
                tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                Thread.Sleep(5000);
                cc.ScrollPage(driver);
                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        [TestMethod, TestCategory("Under Review - Pantograph Tests")]
        public void Pantograph_TextProperties_ChangeTransformText()
        {
            string templateName = "";
            string imagePath2 = "";
            string imagePath1 = "";

            if (qaDev.ToLower() == dev)
            {
                instance = 11601073;
                testName = "Pantograph_TextProperties_ChangeTransformText_Dev";
            }
            else
            {
                if (qaDev.ToLower() == qa)
                {
                    instance = 11659675;
                    testName = "TROYMark_TextProperties_ChangeTransformText_QA";
                }
                //    else
                //    {
                //        if (qaDev.ToLower() == beta)
                //        {
                //            instance = 7225653;
                //            testName = "TROYMark_TextProperties_ChangeTransformText_Beta";
                //        }
                else
                {
                    instance = 11660189;
                    testName = "TROYMark_TextProperties_ChangeTransformText_Prod";
                }
                //        }
                //    }
            }
            passFail = 1;  //reset before test
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Pantograph-DO NOT USE";
            CreateLogFile();

            try
            {
                //setup
                CreateTemplateUnderTest(templateName);
                baseWindowHandle = driver.CurrentWindowHandle;

                SetupPantograph();

                cc.ClickButton(driver, tst.element_EditText);
                Thread.Sleep(1000);

                cc.EnterText(driver, "Test Text Transform", tst.templateTextInput);
                cc.ClickButton(driver, tst.templateTextSave);
                Thread.Sleep(3000);

                tglog.AppendLogText(filePath, "Enter Static Text in Pantograph.....OK");
                cc.ClickButton(driver, tst.saveButton);
                Thread.Sleep(3000);
                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, string.Concat("Pantograph Transform Text ", cc.FetchSelectedOption(driver, tst.pantographTransform), " "), ScreenshotImageFormat.Png, 0);

                //Test
                cc.SelectDropDownByElement(driver, tst.pantographTransform, "ToUpper");
                tglog.AppendLogText(filePath, string.Concat("Pantograph Text Transform = ", cc.FetchSelectedOption(driver, tst.pantographTransform)));

                cc.ClickButton(driver, tst.saveButton);
                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, string.Concat("Pantograph Transform Text ", cc.FetchSelectedOption(driver, tst.pantographTransform), " "), ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);
                tglog.AppendLogText(filePath, "Saved Template ..... OK");
                Thread.Sleep(3000);

                cc.ClickButton(driver, tst.templateHomeBtn);
                tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                Thread.Sleep(3000);
                cc.ScrollPage(driver);

                //Tear Down
                RemoveTemplateUnderTest(templateName);

                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                passFail = 0;
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Tear Down
                cc.ClickButton(driver, tst.templateHomeBtn);
                tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                Thread.Sleep(5000);
                cc.ScrollPage(driver);
                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        [TestMethod, TestCategory("Under Review - Pantograph Tests")]
        public void Pantograph_TextProperties_EnterStaticText()
        {
            string imagePath2 = "";
            string imagePath1 = "";
            string originalText = "";
            string templateName = "";
            string text = "";
            string newText = "";

            if (qaDev.ToLower() == dev)
            {
                instance = 11584108;
                testName = "Pantograph_TextProperties_EnterStaticText_Dev";
            }
            else
            {
                if (qaDev.ToLower() == qa)
                {
                    instance = 11659849;
                    testName = "Pantograph_TextProperties_EnterStaticText_QA";
                }
                //    else
                //    {
                //        if (qaDev.ToLower() == beta)
                //        {
                //            instance = 7225653;
                //            testName = "Pantograph_TextProperties_EnterStaticText_Beta";
                //        }
                else
                {
                    instance = 11660178;
                    testName = "Pantograph_TextProperties_EnterStaticText_Prod";
                }
                //        }
                //    }
            }
            passFail = 1;  //reset before test
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Pantograph-DO NOT USE";
            text = "Test Static Text.....QA";
            CreateLogFile();

            try
            {
                //setup
                CreateTemplateUnderTest(templateName);
                baseWindowHandle = driver.CurrentWindowHandle;

                SetupPantograph();
                Thread.Sleep(3000);
                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Pantograph Enter Static Text", ScreenshotImageFormat.Png, 0);

                //Test
                cc.ClickButton(driver, tst.element_EditText);
                Thread.Sleep(1000);

                originalText = cc.FetchIDefaultValue(driver, tst.templateTextInput);
                cc.EnterText(driver, text, tst.templateTextInput);
                newText = cc.FetchIDefaultValue(driver, tst.templateTextInput);
                cc.ClickButton(driver, tst.templateTextSave);
                tglog.AppendLogText(filePath, "Enter Static Text in Pantograph.....OK");
                Thread.Sleep(3000);

                // verify 
                try
                {
                    cc.ClickButton(driver, tst.element_EditText);
                    Thread.Sleep(1000);

                    StringAssert.Contains((newText != originalText).ToString(), true.ToString());
                    tglog.AppendLogText(filePath, string.Concat(newText, " is not the same as ", originalText));

                    cc.ClickButton(driver, tst.templateTextCancel);
                    Thread.Sleep(3000);

                    imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Pantograph Enter Static Text", ScreenshotImageFormat.Png, 1);
                    cc.ClickButton(driver, tst.saveButton);
                    tglog.AppendLogText(filePath, "Saved Template ..... OK");
                    Thread.Sleep(3000);

                    cc.ClickButton(driver, tst.templateHomeBtn);
                    tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                    Thread.Sleep(3000);
                    cc.ScrollPage(driver);

                    //Tear Down
                    RemoveTemplateUnderTest(templateName);

                    try
                    {
                        StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                        tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                        passFail = 0;
                    }
                    catch (Exception e)
                    {
                        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
                        passFail = 1;
                    }
                }
                catch (Exception e)
                {
                    cc.ClickButton(driver, tst.templateTextCancel);
                    Thread.Sleep(3000);

                    tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
                    passFail = 1;

                    imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Pantograph Enter Static Text", ScreenshotImageFormat.Png, 1);
                    cc.ClickButton(driver, tst.saveButton);
                    tglog.AppendLogText(filePath, "Saved Template ..... OK");
                    Thread.Sleep(3000);

                    cc.ClickButton(driver, tst.templateHomeBtn);
                    tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                    Thread.Sleep(3000);
                    cc.ScrollPage(driver);

                    //Tear Down
                    RemoveTemplateUnderTest(templateName);
                }
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Tear Down
                cc.ClickButton(driver, tst.templateHomeBtn);
                tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                Thread.Sleep(5000);
                cc.ScrollPage(driver);
                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        [TestMethod, TestCategory("Under Review - Pantograph Tests")]
        public void Pantograph_TextProperties_SelectJobMetadata()
        {
            string imagePath2 = "";
            string imagePath1 = "";
            string templateName = "";

            if (qaDev.ToLower() == dev)
            {
                instance = 11600412;
                testName = "Pantograph_TextProperties_SelectJobMetadata_Dev";
            }
            else
            {
                if (qaDev.ToLower() == qa)
                {
                    instance = 11659672;
                    testName = "Pantograph_TextProperties_SelectJobMetadata_QA";
                }
                //    else
                //    {
                //        if (qaDev.ToLower() == beta)
                //        {
                //            instance = 7225653;
                //            testName = "Pantograph_TextProperties_SelectJobMetadata_Beta";
                //        }
                else
                {
                    instance = 11660186;
                    testName = "Pantograph_TextProperties_SelectJobMetadata_Prod";
                }
                //        }
                //    }
            }
            passFail = 1;  //reset before test
            imagePath = string.Concat(filePath, "Test Files\\");
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Pantograph-DO NOT USE";
            CreateLogFile();

            try
            {
                //setup
                CreateTemplateUnderTest(templateName);
                baseWindowHandle = driver.CurrentWindowHandle;

                SetupPantograph();
                Thread.Sleep(3000);
                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Pantograph Select Job Metadata", ScreenshotImageFormat.Png, 0);

                //Test
                cc.ClickButton(driver, tst.element_EditText);
                Thread.Sleep(1000);

                cc.ClickButton(driver, tst.templateMetaData);
                tst.ClickMetadataButton(driver, tst.templateMetadataList, "Serial Number");
                Thread.Sleep(2000);
                cc.ClickButton(driver, tst.templateTextSave);
                tglog.AppendLogText(filePath, "Selected Job Metadata.....OK");
                Thread.Sleep(3000);

                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template ..... OK");
                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Pantograph Select Job Metadata", ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);

                cc.ClickButton(driver, tst.templateHomeBtn);
                tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                Thread.Sleep(3000);
                cc.ScrollPage(driver);

                //Tear Down
                RemoveTemplateUnderTest(templateName);

                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                passFail = 0;
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Tear Down
                cc.ClickButton(driver, tst.templateHomeBtn);
                tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
                Thread.Sleep(5000);
                cc.ScrollPage(driver);
                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        //-------------------------------------------------------------------------------------------
        #region Methods
        private void CreateLogFile()
        {
            tglog.CreateLogFile(filePath, string.Concat(Environment.NewLine, "----------- Start testing using ", brow, " -----------", Environment.NewLine));
            tglog.AppendLogText(filePath, string.Concat("Set up ", brow, " ..... OK"));
            tglog.AppendLogText(filePath, string.Concat("URL = ", baseURL));
            tglog.AppendLogText(filePath, string.Concat("Envionment = ", qaDev));
            tglog.AppendLogText(filePath, string.Concat("User = ", user));
            tglog.AppendLogText(filePath, "Opened SecureDocs in browser ..... OK");
            tglog.AppendLogText(filePath, string.Concat("Build Version: ", build));
            tglog.AppendLogText(filePath, "Logged into SecureDocs ..... OK");
            tglog.AppendLogText(filePath, string.Concat("Instance = ", instance));
            tglog.AppendLogText(filePath, string.Concat("FilePath = ", filePath));
            tglog.AppendLogText(filePath, "Opened SecureDocs Dashboard ..... OK");
        }

        private void CreateTemplateUnderTest(string text)
        {
            OpenSecurityTemplates();
            cst = st.OpenCreateTemplate(driver);

            tglog.AppendLogText(filePath, "Opened New Security Template popup ..... OK");

            tst = cst.OpenTemplate(driver);
            Thread.Sleep(5000);

            tglog.AppendLogText(filePath, "Created Blank Template ..... OK");

            cc.SwitchToiFrame(driver); // switch to iFrame containing templates
            cc.EnterText(driver, text, tst.templateName);

            cc.ClickButton(driver, tst.saveButton);
            Thread.Sleep(5000);

        }

        private void OpenSecurityTemplates()
        {
            try
            {
                st = mp.OpenSecurityTemplates(driver);
                //tglog.AppendLogText(filePath, driver.Title);

                passFail = 0;
                tglog.AppendLogText(filePath, "Opened Security Templates ..... OK");

            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        private void RemoveTemplateUnderTest(string text)
        {
            string ele = "";
            string element = "";
            int itemCount = 0;
            string templateName = "";

            itemCount = Convert.ToInt16(cc.FetchChildCount(driver, st.templateGrid));

            for (int x = 1; x <= itemCount; x++)
            {
                element = string.Concat(st.templateGrid, "/div[", x.ToString(), "]/div[3]/a");
                ele = string.Concat(st.templateGrid, "/div[", x.ToString(), "]/div[1]/div/a[2]");
                //tglog.AppendLogText(filePath, string.Concat("Name: ", element, " / remove: ", ele));
                templateName = cc.FetchInnerHTML(driver, element);

                //tglog.AppendLogText(filePath, string.Concat("text = ", text, " / template name = ", templateName));
                if (templateName == text)
                {
                    rst = st.RemoveTemplate(driver, ele);

                    StringAssert.Contains(string.Concat("Delete Template '", templateName, "'"), cc.FetchInnerHTML(driver, rst.header));
                    tglog.AppendLogText(filePath, "Opened Remove Security Template..... OK");

                    st = rst.SecurityTemplateYES(driver, "Yes");
                    tglog.AppendLogText(filePath, string.Concat("Removed template", templateName));
                    break;
                }
            }
        }

        private void SetupAuthMark()
        {
            cc.ClickButton(driver, tst.securityElements);
            tglog.AppendLogText(filePath, "Exposed Security Elements ..... OK");

            cc.ClickButton(driver, tst.authMarkImage);
            tglog.AppendLogText(filePath, "Added Authmark To Template ..... OK");
        }

        private void SendToPractitest()
        {
            if (qaDev.ToUpper() == "DEV")
            {
                ptc.PT_Post_Specific_Run(filePath, passFail, resultMsg, instance, project, brow, "Development");
            }
            else
            {
                if (qaDev.ToUpper() == "QA")
                {
                    ptc.PT_Post_Specific_Run(filePath, passFail, resultMsg, instance, project, brow, "QA");
                }
                else
                {
                    if (qaDev.ToUpper() == "BETA")
                    {
                        ptc.PT_Post_Specific_Run(filePath, passFail, resultMsg, instance, project, brow, "Beta");
                    }
                    else
                    {
                        ptc.PT_Post_Specific_Run(filePath, passFail, resultMsg, instance, project, brow, "Production");
                    }
                }
            }
        }

        private void SetupDriver()
        {
            if ((brow.ToLower() == "ie"))
            {
                this.driver = new InternetExplorerDriver("C:\\Selenium");
            }

            if ((brow.ToLower() == "chrome"))
            {
                this.driver = new ChromeDriver(@"C:\Selenium");
            }

            //if ((brow.ToLower() == "firefox"))
            //{
            //    this.driver = new FirefoxDriver();
            //}

        }

        private void SetupPantograph()
        {
            cc.ClickButton(driver, tst.securityElements);
            tglog.AppendLogText(filePath, "Exposed Security Elements ..... OK");

            cc.ClickButton(driver, tst.pantographImg);
            tglog.AppendLogText(filePath, "Added Pantograph To Template ..... OK");

            cc.EnterText(driver, "42", tst.elementX);
            cc.EnterText(driver, "177", tst.elementY);
            cc.EnterText(driver, "501", tst.elementWidth);
            cc.EnterText(driver, "501", tst.elementHeight);
        }

        private void SetupCaptureText()
        {
            cc.ClickButton(driver, tst.captureElements);
            tglog.AppendLogText(filePath, "Exposed Security Elements ..... OK");

            cc.ClickButton(driver, tst.captureText);
            tglog.AppendLogText(filePath, "Added Data Capture To Template ..... OK");
        }

        private void SetUpEnvironVars()
        {
            if (qaDev.ToLower() == dev)
            {
                filePath = string.Concat(filePath, "Dev\\");
            }
            else
            {
                if (qaDev.ToLower() == qa)
                {
                    filePath = string.Concat(filePath, "QA\\");
                }
                else
                {
                    if (qaDev.ToLower() == beta)
                    {
                        filePath = string.Concat(filePath, "Beta\\");
                    }
                    else
                    {
                        filePath = string.Concat(filePath, "Prod\\");
                    }
                }
            }
        }
        #endregion

        //--------------------------------------------------------------------------------------------
        #region Additional test attributes
        [TestInitialize()]
        public void MyTestInitialize()
        {
            cc = new SecureRx.Common.Common_Code(driver);

            brow = ConfigurationManager.AppSettings["browser"];
            baseURL = ConfigurationManager.AppSettings["DocrityURL"];
            project = ConfigurationManager.AppSettings["project"];
            qaDev = ConfigurationManager.AppSettings["testEnv"];
            user = ConfigurationManager.AppSettings["testUser"].ToUpper();
            string pswd = ConfigurationManager.AppSettings["TestPswd"];

            SetupDriver();
            SetUpEnvironVars();

            lp = new SecureRx.Page_Elements.LoginPage(driver, baseURL);
            build = lp.FetchBuildVersion(driver);
            mp = lp.SDLogin(user, pswd, driver);

            //Did Home Menu open
            if (mp == null)
            {
                throw new NoSuchWindowException("Docrity home page was not displayed!");
            }
            else
            {
                passFail = 0;
            }
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {

            driver.SwitchTo().Window(baseWindowHandle);
            Thread.Sleep(1000);

            try
            {
                mp.LogoutandCloseBrowser(driver);
                tglog.AppendLogText(filePath, "Logged out and Closed browser..... OK");
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }

            if (passFail != 0)
            {
                resultMsg = string.Concat(testName, " ..... FAILED!!!!! Please refer to log file");
                //driver.Close();
            }
            else
            {
                resultMsg = string.Concat(testName, " ..... PASSED!!!!!");
            }

            SendToPractitest();

            tglog.AppendLogText(filePath, "Close Docrity");
            tglog.AppendLogText(filePath, "\n Test Finished \n");
        }

        #endregion

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;
    }
}
