﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Data;
using System.IO;
using System.Net;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;

namespace SecureRx.Tests
{
    [TestClass]
    public class Image_Tests
    {
        #region libraries
        SecureRx.Page_Elements.MainPage mp = null;
        SecureRx.Page_Elements.LoginPage lp = null;

        SecureRx.Page_Elements.Templates.SecurityTemplates st = null;
        SecureRx.Page_Elements.Templates.Create_Security_Templates cst = null;
        SecureRx.Page_Elements.Templates.Remove_Security_Template rst = null;
        SecureRx.Page_Elements.Templates.Template tst = null;

        SecureRx.Common.Common_Code cc = null;
        SecureRx.Common.PT_Client ptc = new SecureRx.Common.PT_Client();
        SecureRx.Common.TGLogging tglog = new SecureRx.Common.TGLogging();
        #endregion

        //Vars
        private string baseURL = "";
        private string baseWindowHandle = "";
        private string beta = "beta";
        private string brow = "";
        private string build = "";
        private string dev = "dev";
        private IWebDriver driver = null;
        private string filePath = "C:\\Test Logs\\SecureRx\\";
        private string imageFilesPath = @"C:\SecureRx\Images\";
        private string imagePath = "";
        public string imagePath2 = "";
        public string imagePath1 = "";
        private int instance = 0;
        private int passFail = 1;
        //private string prod = "prod";
        private string project = "4896";
        private string qa = "qa";
        private string qaDev = "";
        private string resultMsg = "";
        private string testName = "";
        private string user = "";

        //[TestMethod, TestCategory("Templates - Image Tests")]
        //public void Image_ImageProperties_ChangeImageOpacity()
        //{
        //    string imagePath2 = "";
        //    string imagePath1 = "";
        //    string maintainAR = "";
        //    string templateName = "";
        //    string testImage = "";

        //    if (qaDev.ToLower() == dev)
        //    {
        //        instance = 11402115;
        //        testName = "Image_ImageProperties_ChangeImageOpacity_Dev";
        //    }
        //    else
        //    {
        //        if (qaDev.ToLower() == qa)
        //        {
        //            instance = 11507474;
        //            testName = "Image_ImageProperties_ChangeImageOpacity_QA";
        //        }
        //        //    else
        //        //    {
        //        //        if (qaDev.ToLower() == beta)
        //        //        {
        //        //            instance = 7225653;
        //        //            testName = "Image_ImageProperties_ChangeImageOpacity_Beta";
        //        //        }
        //        else
        //        {
        //            instance = 11583423;
        //            testName = "Image_ImageProperties_ChangeImageOpacity_Prod";
        //        }
        //        //        }
        //        //    }
        //    }
        //    passFail = 1;  //reset before test
        //    imagePath = string.Concat(filePath, "Test Files\\");
        //    imagePath1 = imagePath;
        //    imagePath2 = imagePath;
        //    filePath = string.Concat(filePath, testName, ".txt");
        //    templateName = "Image-DO NOT USE";
        //    CreateLogFile();

        //    try
        //    {
        //        //setup
        //        CreateTemplateUnderTest(templateName);
        //        baseWindowHandle = driver.CurrentWindowHandle;

        //        SetupImage();

        //        maintainAR = cc.FetchChecked(driver, tst.imageMaintainAspectRatio);
        //        tglog.AppendLogText(filePath, maintainAR);
        //        Thread.Sleep(3000);

        //        cc.ClickButton(driver, tst.uploadImage);
        //        Thread.Sleep(1000);
        //        tglog.AppendLogText(filePath, "Clicked Upload Image");

        //        cc.ClickButton(driver, tst.selectImageArea);
        //        Thread.Sleep(1000);
        //        tglog.AppendLogText(filePath, "Click Select File Area");

        //        testImage = string.Concat(imageFilesPath, "Beach People.jpg");
        //        //cc.UploadFile(testImage);
        //        System.Windows.Forms.SendKeys.SendWait(string.Concat(imageFilesPath, "green sea turtle.jpg"));
        //        System.Windows.Forms.SendKeys.SendWait("{ENTER}");

        //        tglog.AppendLogText(filePath, "Uploaded File ..... OK");
        //        Thread.Sleep(3000);
        //        imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Image Change Opacity ", ScreenshotImageFormat.Png, 0);

        //        //Test
        //        try
        //        {
        //            cc.MoveSlider(driver, tst.imageOpacity, 50);
        //            imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Image Change Opacity ", ScreenshotImageFormat.Png, 1);
        //            Thread.Sleep(3000);
        //            tglog.AppendLogText(filePath, "Changed Opacity ..... OK");

        //            cc.ClickButton(driver, tst.saveButton);
        //            tglog.AppendLogText(filePath, "Saved Template ..... OK");
        //            Thread.Sleep(10000);

        //            cc.ClickButton(driver, tst.templateHomeBtn);
        //            tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
        //            Thread.Sleep(3000);
        //            cc.ScrollPage(driver);

        //            //Tear Down
        //            RemoveTemplateUnderTest(templateName);

        //            try
        //            {
        //                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
        //                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
        //                passFail = 0;
        //            }
        //            catch (Exception er)
        //            {
        //                passFail = 1;
        //                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, er.Message.ToString(), Environment.NewLine));
        //            }
        //        }
        //        catch (Exception e)
        //        {
        //            passFail = 1;
        //            tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
        //            cc.ClickButton(driver, tst.saveButton);
        //            tglog.AppendLogText(filePath, "Saved Template ..... OK");
        //            Thread.Sleep(3000);

        //            cc.ClickButton(driver, tst.templateHomeBtn);
        //            tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
        //            Thread.Sleep(3000);
        //            cc.ScrollPage(driver);

        //            //Tear Down
        //            RemoveTemplateUnderTest(templateName);
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        passFail = 1;
        //        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

        //        //Tear Down
        //        cc.ClickButton(driver, tst.templateHomeBtn);
        //        tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
        //        Thread.Sleep(5000);
        //        cc.ScrollPage(driver);
        //        RemoveTemplateUnderTest(templateName);
        //        Thread.Sleep(5000);
        //    }

        //    Thread.Sleep(5000);
        //}

        //[TestMethod, TestCategory("Templates - Image Tests")]
        //public void Image_ImageProperties_ChangeImageRotation()
        //{
        //    string imagePath2 = "";
        //    string imagePath1 = "";
        //    string maintainAR = "";
        //    string templateName = "";

        //    if (qaDev.ToLower() == dev)
        //    {
        //        instance = 11402116;
        //        testName = "Image_ImageProperties_ChangeImageRotation_Dev";
        //    }
        //    else
        //    {
        //        if (qaDev.ToLower() == qa)
        //        {
        //            instance = 11507475;
        //            testName = "Image_ImageProperties_ChangeImageRotation_QA";
        //        }
        //        //    else
        //        //    {
        //        //        if (qaDev.ToLower() == beta)
        //        //        {
        //        //            instance = 7225653;
        //        //            testName = "Image_ImageProperties_ChangeImageRotation_Beta";
        //        //        }
        //        else
        //        {
        //            instance = 11583424;
        //            testName = "Image_ImageProperties_ChangeImageRotation_Prod";
        //        }
        //        //        }
        //        //    }
        //    }
        //    passFail = 1;  //reset before test
        //    imagePath = string.Concat(filePath, "Test Files\\");
        //    imagePath1 = imagePath;
        //    imagePath2 = imagePath;
        //    filePath = string.Concat(filePath, testName, ".txt");
        //    templateName = "Image-DO NOT USE";
        //    CreateLogFile();

        //    try
        //    {
        //        //setup
        //        CreateTemplateUnderTest(templateName);
        //        baseWindowHandle = driver.CurrentWindowHandle;

        //        SetupImage();

        //        maintainAR = cc.FetchChecked(driver, tst.imageMaintainAspectRatio);
        //        tglog.AppendLogText(filePath, maintainAR);
        //        Thread.Sleep(3000);

        //        cc.ClickButton(driver, tst.uploadImage);
        //        Thread.Sleep(1000);
        //        tglog.AppendLogText(filePath, "Clicked Upload Image");

        //        cc.ClickButton(driver, tst.selectImageArea);
        //        Thread.Sleep(1000);
        //        tglog.AppendLogText(filePath, "Click Select File Area");

        //        System.Windows.Forms.SendKeys.SendWait(string.Concat(imageFilesPath, "green sea turtle.jpg"));
        //        System.Windows.Forms.SendKeys.SendWait("{ENTER}");
        //        tglog.AppendLogText(filePath, "Uploaded File ..... OK");
        //        Thread.Sleep(3000);
        //        imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Image Change Image Rotation ", ScreenshotImageFormat.Png, 0);

        //        //Test
        //        try
        //        {
        //            cc.MoveSlider(driver, tst.imageRotation, 30);
        //            imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Image Change Image Rotation ", ScreenshotImageFormat.Png, 1);
        //            Thread.Sleep(3000);
        //            tglog.AppendLogText(filePath, "Rotated Image ..... OK");

        //            cc.ClickButton(driver, tst.saveButton);
        //            tglog.AppendLogText(filePath, "Saved Template ..... OK");
        //            Thread.Sleep(5000);

        //            cc.ClickButton(driver, tst.templateHomeBtn);
        //            tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
        //            Thread.Sleep(3000);
        //            cc.ScrollPage(driver);

        //            //Tear Down
        //            RemoveTemplateUnderTest(templateName);

        //            try
        //            {
        //                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
        //                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
        //                passFail = 0;
        //            }
        //            catch (Exception er)
        //            {
        //                passFail = 1;
        //                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, er.Message.ToString(), Environment.NewLine));
        //            }
        //        }
        //        catch (Exception e)
        //        {
        //            passFail = 1;
        //            tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
        //            cc.ClickButton(driver, tst.saveButton);
        //            tglog.AppendLogText(filePath, "Saved Template ..... OK");
        //            Thread.Sleep(3000);

        //            cc.ClickButton(driver, tst.templateHomeBtn);
        //            tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
        //            Thread.Sleep(3000);
        //            cc.ScrollPage(driver);

        //            //Tear Down
        //            RemoveTemplateUnderTest(templateName);
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        passFail = 1;
        //        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

        //        //Tear Down
        //        cc.ClickButton(driver, tst.templateHomeBtn);
        //        tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
        //        Thread.Sleep(5000);
        //        cc.ScrollPage(driver);
        //        RemoveTemplateUnderTest(templateName);
        //        Thread.Sleep(5000);
        //    }

        //    Thread.Sleep(5000);
        //}

        //[TestMethod, TestCategory("Templates - Image Tests")]
        //public void Image_ImageProperties_ChangeMaintainAspectRatio()
        //{
        //    string imagePath2 = "";
        //    string imagePath1 = "";
        //    string maintainAR = "";
        //    string templateName = "";

        //    if (qaDev.ToLower() == dev)
        //    {
        //        instance = 11402114;
        //        testName = "Image_ImageProperties_ChangeMaintainAspectRatio_Dev";
        //    }
        //    else
        //    {
        //        if (qaDev.ToLower() == qa)
        //        {
        //            instance = 11507473;
        //            testName = "Image_ImageProperties_ChangeMaintainAspectRatio_QA";
        //        }
        //        //    else
        //        //    {
        //        //        if (qaDev.ToLower() == beta)
        //        //        {
        //        //            instance = 7225653;
        //        //            testName = "Image_ImageProperties_ChangeMaintainAspectRatio_Beta";
        //        //        }
        //        else
        //        {
        //            instance = 11583422;
        //            testName = "Image_ImageProperties_ChangeMaintainAspectRatio_Prod";
        //        }
        //        //        }
        //        //    }
        //    }
        //    passFail = 1;  //reset before test
        //    imagePath = string.Concat(filePath, "Test Files\\");
        //    imagePath1 = imagePath;
        //    imagePath2 = imagePath;
        //    filePath = string.Concat(filePath, testName, ".txt");
        //    templateName = "Image-DO NOT USE";
        //    CreateLogFile();

        //    try
        //    {
        //        //setup
        //        CreateTemplateUnderTest(templateName);
        //        baseWindowHandle = driver.CurrentWindowHandle;

        //        SetupImage();

        //        maintainAR = cc.FetchChecked(driver, tst.imageMaintainAspectRatio);
        //        tglog.AppendLogText(filePath, maintainAR);
        //        Thread.Sleep(3000);

        //        cc.ClickButton(driver, tst.uploadImage);
        //        Thread.Sleep(1000);
        //        tglog.AppendLogText(filePath, "Clicked Upload Image");

        //        cc.ClickButton(driver, tst.selectImageArea);
        //        Thread.Sleep(1000);
        //        tglog.AppendLogText(filePath, "Click Select File Area");

        //        System.Windows.Forms.SendKeys.SendWait(string.Concat(imageFilesPath, "green sea turtle.jpg"));
        //        System.Windows.Forms.SendKeys.SendWait("{ENTER}");
        //        tglog.AppendLogText(filePath, "Uploaded File ..... OK");
        //        Thread.Sleep(3000);
        //        imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Image Aspect Ratio", ScreenshotImageFormat.Png, 0);

        //        //Test
        //        try
        //        {
        //            cc.ClickButton(driver, tst.imageMaintainAspectRatio);
        //            StringAssert.Contains(cc.FetchChecked(driver, tst.imageMaintainAspectRatio), "False");

        //            imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Image Aspect Ratio ", ScreenshotImageFormat.Png, 1);
        //            Thread.Sleep(3000);

        //            cc.ClickButton(driver, tst.saveButton);
        //            tglog.AppendLogText(filePath, "Saved Template ..... OK");
        //            Thread.Sleep(5000);

        //            cc.ClickButton(driver, tst.templateHomeBtn);
        //            tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
        //            Thread.Sleep(3000);
        //            cc.ScrollPage(driver);

        //            //Tear Down
        //            RemoveTemplateUnderTest(templateName);

        //            try
        //            {
        //                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
        //                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
        //                passFail = 0;
        //            }
        //            catch (Exception er)
        //            {
        //                passFail = 1;
        //                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, er.Message.ToString(), Environment.NewLine));
        //            }
        //        }
        //        catch (Exception e)
        //        {
        //            passFail = 1;
        //            tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
        //            cc.ClickButton(driver, tst.saveButton);
        //            tglog.AppendLogText(filePath, "Saved Template ..... OK");
        //            Thread.Sleep(3000);

        //            cc.ClickButton(driver, tst.templateHomeBtn);
        //            tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
        //            Thread.Sleep(3000);
        //            cc.ScrollPage(driver);

        //            //Tear Down
        //            RemoveTemplateUnderTest(templateName);
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        passFail = 1;
        //        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

        //        //Tear Down
        //        cc.ClickButton(driver, tst.templateHomeBtn);
        //        tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
        //        Thread.Sleep(5000);
        //        cc.ScrollPage(driver);
        //        RemoveTemplateUnderTest(templateName);
        //        Thread.Sleep(5000);
        //    }

        //    Thread.Sleep(5000);
        //}

        //[TestMethod, TestCategory("Templates - Image Tests")]
        //public void Image_Position_ChangePosition()
        //{
        //    string imagePath2 = "";
        //    string imagePath1 = "";
        //    string templateName = "";

        //    if (qaDev.ToLower() == dev)
        //    {
        //        instance = 11221171;
        //        testName = "Image_Position_ChangePosition_Dev";
        //    }
        //    else
        //    {
        //        if (qaDev.ToLower() == qa)
        //        {
        //            instance = 11507471;
        //            testName = "Image_Position_ChangePosition_QA";
        //        }
        //        //    else
        //        //    {
        //        //        if (qaDev.ToLower() == beta)
        //        //        {
        //        //            instance = 7225653;
        //        //            testName = "Image_Position_ChangePosition_Beta";
        //        //        }
        //        else
        //        {
        //            instance = 11311944;
        //            testName = "Image_Position_ChangePosition_Prod";
        //        }
        //        //        }
        //        //    }
        //    }
        //    passFail = 1;  //reset before test
        //    imagePath = string.Concat(filePath, "Test Files\\");
        //    imagePath1 = imagePath;
        //    imagePath2 = imagePath;
        //    filePath = string.Concat(filePath, testName, ".txt");
        //    templateName = "Image-DO NOT USE";
        //    CreateLogFile();

        //    try
        //    {
        //        //setup
        //        CreateTemplateUnderTest(templateName);
        //        baseWindowHandle = driver.CurrentWindowHandle;

        //        SetupAuthMark();
        //        Thread.Sleep(2000);
        //        SetupImage();

        //        imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Image Position ", ScreenshotImageFormat.Png, 0);
        //        tglog.AppendLogText(filePath, cc.FetchSelectedOption(driver, tst.troyMark_Location));
        //        Thread.Sleep(3000);

        //        //Test
        //        try
        //        {
        //            cc.SelectDropDownByElement(driver, tst.troyMark_Location, "Foreground");
        //            Thread.Sleep(2000);

        //            imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Image Position ", ScreenshotImageFormat.Png, 1);
        //            Thread.Sleep(3000);
        //            cc.ClickButton(driver, tst.saveButton);
        //            tglog.AppendLogText(filePath, "Saved Template ..... OK");
        //            Thread.Sleep(3000);

        //            cc.ClickButton(driver, tst.templateHomeBtn);
        //            tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
        //            Thread.Sleep(3000);
        //            cc.ScrollPage(driver);
        //        }
        //        catch (Exception e)
        //        {
        //            passFail = 1;
        //            tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
        //            cc.ClickButton(driver, tst.saveButton);
        //            tglog.AppendLogText(filePath, "Saved Template ..... OK");
        //            Thread.Sleep(3000);

        //            cc.ClickButton(driver, tst.templateHomeBtn);
        //            tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
        //            Thread.Sleep(3000);
        //            cc.ScrollPage(driver);
        //        }
        //        //Tear Down
        //        RemoveTemplateUnderTest(templateName);

        //        try
        //        {
        //            StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
        //            tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
        //            passFail = 0;
        //        }
        //        catch(Exception e)
        //        {
        //            passFail = 1;
        //            tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        passFail = 1;
        //        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

        //        //Tear Down
        //        cc.ClickButton(driver, tst.templateHomeBtn);
        //        tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
        //        Thread.Sleep(5000);
        //        cc.ScrollPage(driver);
        //        RemoveTemplateUnderTest(templateName);
        //        Thread.Sleep(5000);
        //    }

        //    Thread.Sleep(5000);
        //}

        //[TestMethod, TestCategory("Templates - Image Tests")]
        //public void Image_Position_MoveImage()
        //{
        //    string templateName = "";
        //    string testValu = "";
        //    string testValu2 = "";
        //    string imagePath2 = "";
        //    string imagePath1 = "";

        //    if (qaDev.ToLower() == dev)
        //    {
        //        instance = 11221169;
        //        testName = "Image_Position_MoveImage_Dev";
        //    }
        //    else
        //    {
        //        if (qaDev.ToLower() == qa)
        //        {
        //            instance = 11507469;
        //            testName = "Image_Position_MoveImage_QA";
        //        }
        //        //    else
        //        //    {
        //        //        if (qaDev.ToLower() == beta)
        //        //        {
        //        //            instance = 7225653;
        //        //            testName = "Image_Position_MoveImage_Beta";
        //        //        }
        //        else
        //        {
        //            instance = 11311942;
        //            testName = "Image_Position_MoveImage_Prod";
        //        }
        //        //        }
        //        //    }
        //    }
        //    passFail = 1;  //reset before test
        //    testValu = "561";
        //    testValu2 = "486";
        //    imagePath = string.Concat(filePath, "Test Files\\");
        //    imagePath1 = imagePath;
        //    imagePath2 = imagePath;
        //    filePath = string.Concat(filePath, testName, ".txt");
        //    templateName = "Image-DO NOT USE";
        //    CreateLogFile();

        //    try
        //    {
        //        //setup
        //        CreateTemplateUnderTest(templateName);
        //        baseWindowHandle = driver.CurrentWindowHandle;

        //        //Test
        //        SetupImage();

        //        imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, string.Concat("Image X and Y Coordinate Change ", testValu, " "), ScreenshotImageFormat.Png, 0);
        //        Thread.Sleep(3000);

        //        cc.EnterText(driver, testValu2, tst.elementX);
        //        tglog.AppendLogText(filePath, string.Concat("Image X Coordinate = ", testValu2));

        //        cc.EnterText(driver, testValu, tst.elementY);
        //        tglog.AppendLogText(filePath, string.Concat("Image Y Coordinate = ", testValu));

        //        imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, string.Concat("Image X and Y Coordinate Change ", testValu, " "), ScreenshotImageFormat.Png, 1);
        //        Thread.Sleep(3000);
        //        cc.ClickButton(driver, tst.saveButton);
        //        tglog.AppendLogText(filePath, "Saved Template ..... OK");
        //        Thread.Sleep(3000);

        //        cc.ClickButton(driver, tst.templateHomeBtn);
        //        tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
        //        Thread.Sleep(3000);
        //        cc.ScrollPage(driver);

        //        //Tear Down
        //        RemoveTemplateUnderTest(templateName);

        //        try
        //        {
        //            StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
        //            tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
        //            passFail = 0;
        //        }
        //        catch(Exception e)
        //        {
        //            passFail = 1;
        //            tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        passFail = 1;
        //        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

        //        //Tear Down
        //        cc.ClickButton(driver, tst.templateHomeBtn);
        //        tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
        //        Thread.Sleep(5000);
        //        cc.ScrollPage(driver);
        //        RemoveTemplateUnderTest(templateName);
        //        Thread.Sleep(5000);
        //    }

        //    Thread.Sleep(5000);
        //}

        //[TestMethod, TestCategory("Templates - Image Tests")]
        //public void Image_Position_ResizeImage()
        //{
        //    string templateName = "";
        //    string testValu = "";
        //    string testValu2 = "";

        //    if (qaDev.ToLower() == dev)
        //    {
        //        instance = 11221170;
        //        testName = "Image_Position_ResizeImage_Dev";
        //    }
        //    else
        //    {
        //        if (qaDev.ToLower() == qa)
        //        {
        //            instance = 11507470;
        //            testName = "Image_Position_ResizeImage_QA";
        //        }
        //        //    else
        //        //    {
        //        //        if (qaDev.ToLower() == beta)
        //        //        {
        //        //            instance = 7225653;
        //        //            testName = "Image_Position_ResizeImage_Beta";
        //        //        }
        //        else
        //        {
        //            instance = 11311943;
        //            testName = "Image_Position_ResizeImage_Prod";
        //        }
        //        //        }
        //        //    }
        //    }
        //    passFail = 1;  //reset before test
        //    testValu = "300";
        //    testValu2 = "200";
        //    imagePath = string.Concat(filePath, "Test Files\\");
        //    imagePath1 = imagePath;
        //    imagePath2 = imagePath;
        //    filePath = string.Concat(filePath, testName, ".txt");
        //    templateName = "Image-DO NOT USE";
        //    CreateLogFile();

        //    try
        //    {
        //        //setup
        //        CreateTemplateUnderTest(templateName);
        //        baseWindowHandle = driver.CurrentWindowHandle;

        //        //Test
        //        SetupImage();

        //        imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, string.Concat("Image Height and Width Change ", testValu, " "), ScreenshotImageFormat.Png, 0);
        //        Thread.Sleep(3000);

        //        cc.EnterText(driver, testValu2, tst.elementWidth);
        //        tglog.AppendLogText(filePath, string.Concat("Image Width = ", testValu2));

        //        cc.EnterText(driver, testValu, tst.elementHeight);
        //        tglog.AppendLogText(filePath, string.Concat("Image Height = ", testValu));

        //        imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, string.Concat("Image Height and Width Change ", testValu, " "), ScreenshotImageFormat.Png, 1);
        //        Thread.Sleep(3000);

        //        cc.ClickButton(driver, tst.saveButton);
        //        tglog.AppendLogText(filePath, "Saved Template ..... OK");
        //        Thread.Sleep(3000);

        //        cc.ClickButton(driver, tst.templateHomeBtn);
        //        tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
        //        Thread.Sleep(3000);
        //        cc.ScrollPage(driver);

        //        //Tear Down
        //        RemoveTemplateUnderTest(templateName);

        //        try
        //        {
        //            StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
        //            tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
        //            passFail = 0;
        //        }
        //        catch(Exception e)
        //        {
        //            passFail = 1;
        //            tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        passFail = 1;
        //        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

        //        //Tear Down
        //        cc.ClickButton(driver, tst.templateHomeBtn);
        //        tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
        //        Thread.Sleep(5000);
        //        cc.ScrollPage(driver);
        //        RemoveTemplateUnderTest(templateName);
        //        Thread.Sleep(5000);
        //    }

        //    Thread.Sleep(5000);
        //}

        //[TestMethod, TestCategory("Templates - Image Tests")]
        //public void Image_ImageProperties_ChangeGrayscale()
        //{
        //    string imagePath2 = "";
        //    string imagePath1 = "";
        //    string maintainAR = "";
        //    string templateName = "";

        //    if (qaDev.ToLower() == dev)
        //    {
        //        instance = 11583461;
        //        testName = "Image_ImageProperties_ChangeGrayscale_Dev";
        //    }
        //    else
        //    {
        //        if (qaDev.ToLower() == qa)
        //        {
        //            instance = 11583514;
        //            testName = "Image_ImageProperties_ChangeGrayscale_QA";
        //        }
        //        //    //    else
        //        //    //    {
        //        //    //        if (qaDev.ToLower() == beta)
        //        //    //        {
        //        //    //            instance = 7225653;
        //        //    //            testName = "Image_ImageProperties_ChangeGrayscale_Beta";
        //        //    //        }
        //        else
        //        {
        //            instance = 11583521;
        //            testName = "Image_ImageProperties_ChangeGrayscale_Prod";
        //        }
        //        //}
        //        //}
        //    }
        //    passFail = 1;  //reset before test
        //    imagePath = string.Concat(filePath, "Test Files\\");
        //    imagePath1 = imagePath;
        //    imagePath2 = imagePath;
        //    filePath = string.Concat(filePath, testName, ".txt");
        //    templateName = "Image-DO NOT USE";
        //    CreateLogFile();

        //    try
        //    {
        //        //setup
        //        CreateTemplateUnderTest(templateName);
        //        baseWindowHandle = driver.CurrentWindowHandle;

        //        SetupImage();

        //        maintainAR = cc.FetchChecked(driver, tst.imageMaintainAspectRatio);
        //        tglog.AppendLogText(filePath, maintainAR);
        //        Thread.Sleep(3000);

        //        cc.ClickButton(driver, tst.uploadImage);
        //        Thread.Sleep(1000);
        //        tglog.AppendLogText(filePath, "Clicked Upload Image");

        //        cc.ClickButton(driver, tst.selectImageArea);
        //        Thread.Sleep(1000);
        //        tglog.AppendLogText(filePath, "Click Select File Area");

        //        System.Windows.Forms.SendKeys.SendWait(string.Concat(imageFilesPath, "green sea turtle.jpg"));
        //        System.Windows.Forms.SendKeys.SendWait("{ENTER}");
        //        tglog.AppendLogText(filePath, "Uploaded File ..... OK");
        //        Thread.Sleep(3000);
        //        imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Gray Scale ", ScreenshotImageFormat.Png, 0);

        //        //Test
        //        try
        //        {
        //            cc.ClickButton(driver, tst.imageGrayscale);
        //            StringAssert.Contains(cc.FetchChecked(driver, tst.imageGrayscale), "True");

        //            imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Gray Scale ", ScreenshotImageFormat.Png, 1);
        //            Thread.Sleep(3000);

        //            cc.ClickButton(driver, tst.saveButton);
        //            tglog.AppendLogText(filePath, "Saved Template ..... OK");
        //            Thread.Sleep(5000);

        //            cc.ClickButton(driver, tst.templateHomeBtn);
        //            tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
        //            Thread.Sleep(3000);
        //            cc.ScrollPage(driver);

        //            //Tear Down
        //            RemoveTemplateUnderTest(templateName);

        //            try
        //            {
        //                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
        //                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
        //                passFail = 0;
        //            }
        //            catch (Exception er)
        //            {
        //                passFail = 1;
        //                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, er.Message.ToString(), Environment.NewLine));
        //            }
        //        }
        //        catch (Exception e)
        //        {
        //            passFail = 1;
        //            tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
        //            cc.ClickButton(driver, tst.saveButton);
        //            tglog.AppendLogText(filePath, "Saved Template ..... OK");
        //            Thread.Sleep(3000);

        //            cc.ClickButton(driver, tst.templateHomeBtn);
        //            tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
        //            Thread.Sleep(3000);
        //            cc.ScrollPage(driver);

        //            //Tear Down
        //            RemoveTemplateUnderTest(templateName);
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        passFail = 1;
        //        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

        //        //Tear Down
        //        cc.ClickButton(driver, tst.templateHomeBtn);
        //        tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
        //        Thread.Sleep(5000);
        //        cc.ScrollPage(driver);
        //        RemoveTemplateUnderTest(templateName);
        //        Thread.Sleep(5000);
        //    }

        //    Thread.Sleep(5000);
        //}

//        [TestMethod, TestCategory("Templates - Image Tests")]
//        public void Image_ImageProperties_UploadImage()
//        {
//            string imagePath2 = "";
//            string imagePath1 = "";
//            string templateName = "";

//            if (qaDev.ToLower() == dev)
//            {
//                instance = 11402113;
//                testName = "Image_ImageProperties_UploadImage_Dev";
//            }
//            else
//            {
//                if (qaDev.ToLower() == qa)
//                {
//                    instance = 11507472;
//                    testName = "Image_ImageProperties_UploadImage_QA";
//                }
//                //    else
//                //    {
//                //        if (qaDev.ToLower() == beta)
//                //        {
//                //            instance = 7225653;
//                //            testName = "Image_ImageProperties_UploadImage_Beta";
//                //        }
//                else
//                {
//                    instance = 11583421;
//                    testName = "Image_ImageProperties_UploadImage_Prod";
//                }
//                //        }
//                //    }
//            }
//            passFail = 1;  //reset before test
//            imagePath = string.Concat(filePath, "Test Files\\");
//            imagePath1 = imagePath;
//            imagePath2 = imagePath;
//            filePath = string.Concat(filePath, testName, ".txt");
//            templateName = "Image-DO NOT USE";
//            CreateLogFile();

//            try
//            {
//                //setup
//                CreateTemplateUnderTest(templateName);
//                baseWindowHandle = driver.CurrentWindowHandle;

//                SetupImage();

//                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Upload Image ", ScreenshotImageFormat.Png, 0);
//                Thread.Sleep(3000);

//                cc.ClickButton(driver, tst.uploadImage);
//                Thread.Sleep(1000);
//                tglog.AppendLogText(filePath, "Clicked Upload Image");

//                cc.ClickButton(driver, tst.selectImageArea);
//                Thread.Sleep(1000);
//                tglog.AppendLogText(filePath, "Click Select File Area");

//                //Test
//                try
//                {
//                    System.Windows.Forms.SendKeys.SendWait(string.Concat(imageFilesPath, "green sea turtle.jpg"));
//                    System.Windows.Forms.SendKeys.SendWait("{ENTER}");
//                    tglog.AppendLogText(filePath, "Uploaded File ..... OK");
//                    Thread.Sleep(3000)
//;
//                    imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Upload Image ", ScreenshotImageFormat.Png, 1);
//                    Thread.Sleep(3000);

//                    cc.ClickButton(driver, tst.saveButton);
//                    tglog.AppendLogText(filePath, "Saved Template ..... OK");
//                    Thread.Sleep(5000);

//                    cc.ClickButton(driver, tst.templateHomeBtn);
//                    tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
//                    Thread.Sleep(3000);
//                    cc.ScrollPage(driver);

//                    //Tear Down
//                    RemoveTemplateUnderTest(templateName);

//                    try
//                    {
//                        StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
//                        tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
//                        passFail = 0;
//                    }
//                    catch (Exception er)
//                    {
//                        passFail = 1;
//                        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, er.Message.ToString(), Environment.NewLine));
//                    }
//                }
//                catch (Exception e)
//                {
//                    passFail = 1;
//                    tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
//                    cc.ClickButton(driver, tst.saveButton);
//                    tglog.AppendLogText(filePath, "Saved Template ..... OK");
//                    Thread.Sleep(3000);

//                    cc.ClickButton(driver, tst.templateHomeBtn);
//                    tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
//                    Thread.Sleep(3000);
//                    cc.ScrollPage(driver);

//                    //Tear Down
//                    RemoveTemplateUnderTest(templateName);
//                }
//            }
//            catch (Exception e)
//            {
//                passFail = 1;
//                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

//                //Tear Down
//                cc.ClickButton(driver, tst.templateHomeBtn);
//                tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
//                Thread.Sleep(5000);
//                cc.ScrollPage(driver);
//                RemoveTemplateUnderTest(templateName);
//                Thread.Sleep(5000);
//            }

//            Thread.Sleep(5000);
//        }

        //[TestMethod, TestCategory("Templates - Image Tests")]
        //public void Image_ImageProperties_UseNativeSize()
        //{
        //    string imagePath2 = "";
        //    string imagePath1 = "";
        //    string maintainAR = "";
        //    string templateName = "";

        //    if (qaDev.ToLower() == dev)
        //    {
        //        instance = 11583460;
        //        testName = " Image_ImageProperties_UseNativeSize_Dev";
        //    }
        //    else
        //    {
        //        if (qaDev.ToLower() == qa)
        //        {
        //            instance = 11583513;
        //            testName = " Image_ImageProperties_UseNativeSize_QA";
        //        }
        //        //    else
        //        //    {
        //        //        if (qaDev.ToLower() == beta)
        //        //        {
        //        //            instance = 7225653;
        //        //            testName = " Image_ImageProperties_UseNativeSize_Beta";
        //        //        }
        //        else
        //        {
        //            instance = 11583520;
        //            testName = " Image_ImageProperties_UseNativeSize_Prod";
        //        }
        //        //        }
        //        //    }
        //    }
        //    passFail = 1;  //reset before test
        //    imagePath = string.Concat(filePath, "Test Files\\");
        //    imagePath1 = imagePath;
        //    imagePath2 = imagePath;
        //    filePath = string.Concat(filePath, testName, ".txt");
        //    templateName = "Image-DO NOT USE";
        //    CreateLogFile();

        //    try
        //    {
        //        //setup
        //        CreateTemplateUnderTest(templateName);
        //        baseWindowHandle = driver.CurrentWindowHandle;

        //        SetupImage();

        //        maintainAR = cc.FetchChecked(driver, tst.imageMaintainAspectRatio);
        //        tglog.AppendLogText(filePath, maintainAR);
        //        Thread.Sleep(3000);

        //        cc.ClickButton(driver, tst.uploadImage);
        //        Thread.Sleep(1000);
        //        tglog.AppendLogText(filePath, "Clicked Upload Image");

        //        cc.ClickButton(driver, tst.selectImageArea);
        //        Thread.Sleep(1000);
        //        tglog.AppendLogText(filePath, "Click Select File Area");

        //        System.Windows.Forms.SendKeys.SendWait(string.Concat(imageFilesPath, "green sea turtle.jpg"));
        //        System.Windows.Forms.SendKeys.SendWait("{ENTER}");
        //        tglog.AppendLogText(filePath, "Uploaded File ..... OK");
        //        Thread.Sleep(3000);
        //        imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Native Size ", ScreenshotImageFormat.Png, 0);

        //        //Test
        //        try
        //        {
        //            cc.ClickButton(driver, tst.useNativeSize);
        //            StringAssert.Contains(cc.FetchChecked(driver, tst.useNativeSize), "False");

        //            imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Native Size ", ScreenshotImageFormat.Png, 1);
        //            Thread.Sleep(3000);

        //            cc.ClickButton(driver, tst.saveButton);
        //            tglog.AppendLogText(filePath, "Saved Template ..... OK");
        //            Thread.Sleep(5000);

        //            cc.ClickButton(driver, tst.templateHomeBtn);
        //            tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
        //            Thread.Sleep(3000);
        //            cc.ScrollPage(driver);

        //            //Tear Down
        //            RemoveTemplateUnderTest(templateName);

        //            try
        //            {
        //                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
        //                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
        //                passFail = 0;
        //            }
        //            catch (Exception er)
        //            {
        //                passFail = 1;
        //                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, er.Message.ToString(), Environment.NewLine));
        //            }
        //        }
        //        catch (Exception e)
        //        {
        //            passFail = 1;
        //            tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
        //            cc.ClickButton(driver, tst.saveButton);
        //            tglog.AppendLogText(filePath, "Saved Template ..... OK");
        //            Thread.Sleep(3000);

        //            cc.ClickButton(driver, tst.templateHomeBtn);
        //            tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
        //            Thread.Sleep(3000);
        //            cc.ScrollPage(driver);

        //            //Tear Down
        //            RemoveTemplateUnderTest(templateName);
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        passFail = 1;
        //        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

        //        //Tear Down
        //        cc.ClickButton(driver, tst.templateHomeBtn);
        //        tglog.AppendLogText(filePath, "Returned to Security Templates page .....OK!");
        //        Thread.Sleep(5000);
        //        cc.ScrollPage(driver);
        //        RemoveTemplateUnderTest(templateName);
        //        Thread.Sleep(5000);
        //    }

        //    Thread.Sleep(5000);
        //}

        #region Methods
        private void CreateLogFile()
        {
            tglog.CreateLogFile(filePath, string.Concat(Environment.NewLine, "----------- Start testing using ", brow, " -----------", Environment.NewLine));
            tglog.AppendLogText(filePath, string.Concat("Set up ", brow, " ..... OK"));
            tglog.AppendLogText(filePath, string.Concat("URL = ", baseURL));
            tglog.AppendLogText(filePath, string.Concat("Envionment = ", qaDev));
            tglog.AppendLogText(filePath, string.Concat("User = ", user));
            tglog.AppendLogText(filePath, "Opened SecureDocs in browser ..... OK");
            tglog.AppendLogText(filePath, string.Concat("Build Version: ", build));
            tglog.AppendLogText(filePath, "Logged into SecureDocs ..... OK");
            tglog.AppendLogText(filePath, string.Concat("Instance = ", instance));
            tglog.AppendLogText(filePath, string.Concat("FilePath = ", filePath));
            tglog.AppendLogText(filePath, "Opened SecureDocs Dashboard ..... OK");
        }

        private void CreateTemplateUnderTest(string text)
        {
            OpenSecurityTemplates();
            cst = st.OpenCreateTemplate(driver);
            //tglog.AppendLogText(filePath, driver.Title);

            //StringAssert.Contains(cc.FetchInnerHTML(driver, cst.Header), "New Security Template");
            tglog.AppendLogText(filePath, "Opened New Security Template popup ..... OK");

            tst = cst.OpenTemplate(driver);
            Thread.Sleep(5000);

            tglog.AppendLogText(filePath, "Created Blank Template ..... OK");

            cc.SwitchToiFrame(driver); // switch to iFrame containing templates
            cc.EnterText(driver, text, tst.templateName);

            cc.ClickButton(driver, tst.saveButton);
            Thread.Sleep(5000);

        }

        private void OpenSecurityTemplates()
        {
            try
            {
                st = mp.OpenSecurityTemplates(driver);
                //tglog.AppendLogText(filePath, driver.Title);

                //StringAssert.Contains(cc.FetchInnerHTML(driver, st.breadcrumb), "Security Templates");
                passFail = 0;
                tglog.AppendLogText(filePath, "Opened Security Templates ..... OK");

            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        private void RemoveTemplateUnderTest(string text)
        {
            string ele = "";
            string element = "";
            int itemCount = 0;
            string templateName = "";

            itemCount = Convert.ToInt16(cc.FetchChildCount(driver, st.templateGrid));
            for (int x = 1; x <= itemCount; x++)
            {
                element = string.Concat(st.templateGrid, "/div[", x.ToString(), "]/div[3]/a");
                ele = string.Concat(st.templateGrid, "/div[", x.ToString(), "]/div[1]/div/a[2]");
                templateName = cc.FetchInnerHTML(driver, element);

                if (templateName == text)
                {
                    rst = st.RemoveTemplate(driver, ele);

                    StringAssert.Contains(string.Concat("Delete Template '", templateName, "'"), cc.FetchInnerHTML(driver, rst.header));
                    tglog.AppendLogText(filePath, "Opened Remove Security Template..... OK");

                    st = rst.SecurityTemplateYES(driver, "Yes");
                    tglog.AppendLogText(filePath, string.Concat("Removed template ", templateName));
                    break;
                }
            }
        }

        private void SendToPractitest()
        {
            if (qaDev.ToUpper() == "DEV")
            {
                ptc.PT_Post_Specific_Run(filePath, passFail, resultMsg, instance, project, brow, "Development");
            }
            else
            {
                if (qaDev.ToUpper() == "QA")
                {
                    ptc.PT_Post_Specific_Run(filePath, passFail, resultMsg, instance, project, brow, "QA");
                }
                else
                {
                    if (qaDev.ToUpper() == "BETA")
                    {
                        ptc.PT_Post_Specific_Run(filePath, passFail, resultMsg, instance, project, brow, "Beta");
                    }
                    else
                    {
                        ptc.PT_Post_Specific_Run(filePath, passFail, resultMsg, instance, project, brow, "Production");
                    }
                }
            }
        }

        private void SetupAuthMark()
        {
            cc.ClickButton(driver, tst.securityElements);
            tglog.AppendLogText(filePath, "Exposed Security Elements ..... OK");

            cc.ClickButton(driver, tst.authMarkImage);
            tglog.AppendLogText(filePath, "Added Authmark To Template ..... OK");
        }

        private void SetupDriver()
        {
            if ((brow.ToLower() == "ie"))
            {
                this.driver = new InternetExplorerDriver("C:\\Selenium");
            }

            if ((brow.ToLower() == "chrome"))
            {
                this.driver = new ChromeDriver(@"C:\Selenium");
            }

            //if ((brow.ToLower() == "firefox"))
            //{
            //    this.driver = new FirefoxDriver();
            //}

        }

        private void SetupImage()
        {
            cc.ClickButton(driver, tst.securityElements);
            tglog.AppendLogText(filePath, "Exposed Security Elements ..... OK");
            Thread.Sleep(2000);

            cc.ClickButton(driver, tst.imageImg);
            tglog.AppendLogText(filePath, "Added Image To Template ..... OK");
        }

        private void SetupCaptureText()
        {
            cc.ClickButton(driver, tst.captureElements);
            tglog.AppendLogText(filePath, "Exposed Security Elements ..... OK");

            cc.ClickButton(driver, tst.captureText);
            tglog.AppendLogText(filePath, "Added Data Capture To Template ..... OK");
        }

        private void SetUpEnvironVars()
        {
            if (qaDev.ToLower() == dev)
            {
                filePath = string.Concat(filePath, "Dev\\");
            }
            else
            {
                if (qaDev.ToLower() == qa)
                {
                    filePath = string.Concat(filePath, "QA\\");
                }
                else
                {
                    if (qaDev.ToLower() == beta)
                    {
                        filePath = string.Concat(filePath, "Beta\\");
                    }
                    else
                    {
                        filePath = string.Concat(filePath, "Prod\\");
                    }
                }
            }
        }
        #endregion

        //--------------------------------------------------------------------------------------------
        #region Additional test attributes
        [TestInitialize()]
        public void MyTestInitialize()
        {
            cc = new SecureRx.Common.Common_Code(driver);

            brow = ConfigurationManager.AppSettings["browser"];
            baseURL = ConfigurationManager.AppSettings["DocrityURL"];
            project = ConfigurationManager.AppSettings["project"];
            qaDev = ConfigurationManager.AppSettings["testEnv"];
            user = ConfigurationManager.AppSettings["testUser"].ToUpper();
            string pswd = ConfigurationManager.AppSettings["TestPswd"];

            SetupDriver();
            SetUpEnvironVars();

            lp = new SecureRx.Page_Elements.LoginPage(driver, baseURL);
            build = lp.FetchBuildVersion(driver);
            mp = lp.SDLogin(user, pswd, driver);

            //Did Home Menu open
            if (mp == null)
            {
                throw new NoSuchWindowException("Docrity home page was not displayed!");
            }
            else
            {
                passFail = 0;
            }
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {

            driver.SwitchTo().Window(baseWindowHandle);
            Thread.Sleep(1000);

            try
            {
                mp.LogoutandCloseBrowser(driver);
                tglog.AppendLogText(filePath, "Logged out and Closed browser..... OK");
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }

            if (passFail != 0)
            {
                resultMsg = string.Concat(testName, " ..... FAILED!!!!! Please refer to log file");
                //driver.Close();
            }
            else
            {
                resultMsg = string.Concat(testName, " ..... PASSED!!!!!");
            }

            SendToPractitest();

            tglog.AppendLogText(filePath, "Close Docrity");
            tglog.AppendLogText(filePath, "\n Test Finished \n");
        }

        #endregion

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;
    }
}
