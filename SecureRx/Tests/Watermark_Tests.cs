﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Data;
using System.IO;
using System.Net;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;

namespace SecureRx.Tests
{
    [TestClass]
    public class Watermark_Tests
    {
        #region libraries
        SecureRx.Page_Elements.MainPage mp = null;
        SecureRx.Page_Elements.LoginPage lp = null;

        SecureRx.Page_Elements.Templates.Name_Template nt = null;
        SecureRx.Page_Elements.Templates.SecurityTemplates st = null;
        SecureRx.Page_Elements.Templates.Create_Security_Templates cst = null;
        SecureRx.Page_Elements.Templates.Remove_Security_Template rst = null;
        SecureRx.Page_Elements.Templates.Template tst = null;

        SecureRx.Common.Common_Code cc = null;
        SecureRx.Common.PT_Client ptc = new SecureRx.Common.PT_Client();
        SecureRx.Common.TGLogging tglog = new SecureRx.Common.TGLogging();
        #endregion

        //Vars
        private string baseURL = "";
        private string baseWindowHandle = "";
        private string brow = "";
        private string build = "";
        private string dev = "dev";
        private IWebDriver driver = null;
        private string filePath = "C:\\Test Logs\\SecureRx\\";
        private string imagePath = "";
        public string imagePath2 = "";
        public string imagePath1 = "";
        private string imageFilesPath = @"C:\SecureRx\Images\";
        private int instance = 0;
        private int passFail = 1;
        private string project = "4896";
        private string qaDev = "";
        private string resultMsg = "";
        private string staging = "stage";
        private string testName = "";
        private string user = "";

        #region Image Watermark Tests
        [TestMethod, TestCategory("R1 - Designer")]
        public void ImageWatermark_Position_ChangePosition()
        {
            string imagePath2 = "";
            string imagePath1 = "";
            string templateName = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 10614858;
                testName = "ImageWatermark_Position_ChangePosition_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14915395;
                    testName = "ImageWatermark_Position_ChangePosition_Stage";
                }
                else
                {
                    //instance = 13109731;
                    testName = "ImageWatermark_Position_ChangePosition_Prod";
                }
            }
            passFail = 1;  //reset before test
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Image Watermark-DO NOT USE";
            CreateLogFile();

            //setup
            CreateTemplateUnderTest(templateName);

            Thread.Sleep(8000);
            tglog.AppendLogText(filePath, "Opened a Security Template ..... OK");

            //Test
            try
            {
                tst.AddAuthmark(driver);
                tglog.AppendLogText(filePath, "Added Authmark.....OK");
                Thread.Sleep(2000);

                tst.AddImageWatermark(driver);
                tglog.AppendLogText(filePath, "Added Image Watermark.....OK");

                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Image Watermark Position ", ScreenshotImageFormat.Png, 0);
                tglog.AppendLogText(filePath, cc.FetchSelectedOption(driver, tst.imageWatermarkPosition));
                Thread.Sleep(3000);

                cc.SelectDropDownByElement(driver, tst.imageWatermarkPosition, "Foreground");
                Thread.Sleep(2000);

                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Image Watermark Position ", ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);

                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                passFail = 0;

                //Finish and Teardown
                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template Changes.....OK");
                Thread.Sleep(5000);

                tst.ExitDesigner(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }
            catch (Exception e)
            {
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Finish and Teardown
                tst.ExitDesigner2(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        [TestMethod, TestCategory("R2 - Designer")]
        public void ImageWatermark_ImageProperties_MaintainAspectRation()
        {
            string imagePath2 = "";
            string imagePath1 = "";
            string maintainAR = "";
            string templateName = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 11074325;
                testName = "ImageWatermark_ImageProperties_MaintainAspectRation_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14916579;
                    testName = "imageWatermark_ImageProperties_MaintainAspectRation_Stage";
                }
                else
                {
                    //instance = 13109735;
                    testName = "ImageWatermark_ImageProperties_MaintainAspectRation_Prod";
                }
            }
            passFail = 1;  //reset before test
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Image Watermark-DO NOT USE";
            CreateLogFile();

            //setup
            CreateTemplateUnderTest(templateName);

            Thread.Sleep(8000);
            tglog.AppendLogText(filePath, "Opened a Security Template ..... OK");

            try
            {
                //Test
                tst.AddImageWatermark(driver);
                tglog.AppendLogText(filePath, "Added Image Watermark.....OK");

                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Image Watermark Aspect Ratio ", ScreenshotImageFormat.Png, 0);
                Thread.Sleep(3000);

                maintainAR = cc.FetchChecked(driver, tst.imageWatermarkMaintainAspectRatio);
                tglog.AppendLogText(filePath, maintainAR);
                Thread.Sleep(3000);

                UploadImage("green sea turtle.jpg");
                cc.ClickButton(driver, tst.imageWatermarkMaintainAspectRatio);
                StringAssert.Contains(cc.FetchChecked(driver, tst.imageWatermarkMaintainAspectRatio), "False");

                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Image Watermark Aspect Ratio ", ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);

                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                passFail = 0;

                //Finish and Teardown
                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template Changes.....OK");
                Thread.Sleep(5000);

                tst.ExitDesigner(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);

            }
            catch (Exception e)
            {
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Finish and Teardown
                tst.ExitDesigner2(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        [TestMethod, TestCategory("R1 - Designer")]
        public void ImageWatermark_Position_MoveWaterMark()
        {
            string templateName = "";
            string testValu = "";
            string testValu2 = "";
            string imagePath2 = "";
            string imagePath1 = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 10614850;
                testName = " ImageWatermark_Position_MoveWaterMark_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14916487;
                    testName = " ImageWatermark_Position_MoveWaterMark_Stage";
                }
                else
                {
                    //instance = 13109729;
                    testName = " ImageWatermark_Position_MoveWaterMark_Prod";
                }
            }
            passFail = 1;  //reset before test
            testValu = "561";
            testValu2 = "486";
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Image Watermark-DO NOT USE";
            CreateLogFile();

            //setup
            CreateTemplateUnderTest(templateName);

            Thread.Sleep(8000);
            tglog.AppendLogText(filePath, "Opened a Security Template ..... OK");

            //Test
            try
            {
                tst.AddImageWatermark(driver);
                tglog.AppendLogText(filePath, "Added Image Watermark.....OK");

                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Image Watermark X and Y ", ScreenshotImageFormat.Png, 0);
                tglog.AppendLogText(filePath, string.Concat("X = ", cc.FetchIDefaultValue(driver, tst.troyMark_X), " / Y = ", cc.FetchIDefaultValue(driver, tst.troyMark_Y)));
                Thread.Sleep(3000);

                cc.EnterText(driver, testValu2, tst.troyMark_X);
                tglog.AppendLogText(filePath, string.Concat("Image Watermark X Coordinate = ", testValu2));

                cc.EnterText(driver, testValu, tst.troyMark_Y);
                tglog.AppendLogText(filePath, string.Concat("Image Watermark Y Coordinate = ", testValu));

                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Image Watermark X and Y ", ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);

                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                passFail = 0;

                //Finish and Teardown
                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template Changes.....OK");
                Thread.Sleep(5000);

                tst.ExitDesigner(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }
            catch (Exception e)
            {
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Finish and Teardown
                tst.ExitDesigner2(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        [TestMethod, TestCategory("R1 - Designer")]
        public void ImageWatermark_Position_ResizeWatermark()
        {
            string templateName = "";
            string testValu = "";
            string testValu2 = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 10614851;
                testName = "ImageWatermark_Position_ResizeWatermark_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14915435;
                    testName = "ImageWatermark_Position_ResizeWatermark_Stage";
                }
                else
                {
                    //instance = 13109730;
                    testName = "ImageWatermark_Position_ResizeWatermark_Prod";
                }
            }
            passFail = 1;  //reset before test
            testValu = "300";
            testValu2 = "200";
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Image Watermark-DO NOT USE";
            CreateLogFile();

            //setup
            CreateTemplateUnderTest(templateName);

            Thread.Sleep(8000);
            tglog.AppendLogText(filePath, "Opened a Security Template ..... OK");

            try
            {
                //Test
                tst.AddImageWatermark(driver);
                tglog.AppendLogText(filePath, "Added Image Watermark.....OK");

                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Image Watermark Width and Height ", ScreenshotImageFormat.Png, 0);
                tglog.AppendLogText(filePath, string.Concat("Width = ", cc.FetchIDefaultValue(driver, tst.troyMark_Width), " / Height = ", cc.FetchIDefaultValue(driver, tst.troyMark_Height)));
                Thread.Sleep(3000);

                cc.EnterText(driver, testValu2, tst.troyMark_Width);
                tglog.AppendLogText(filePath, string.Concat("Image Watermark Width = ", testValu2));

                cc.EnterText(driver, testValu, tst.troyMark_Height);
                tglog.AppendLogText(filePath, string.Concat("Image Watermark Height = ", testValu));

                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Image Watermark Width and Height ", ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);

                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                passFail = 0;

                //Finish and Teardown
                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template Changes.....OK");
                Thread.Sleep(5000);

                tst.ExitDesigner(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }
            catch (Exception e)
            {
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Finish and Teardown
                tst.ExitDesigner2(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        [TestMethod, TestCategory("R2 - Designer")]
        public void ImageWatermark_ImageProperties_ChangeGrayscale()
        {
            string imagePath2 = "";
            string imagePath1 = "";
            string maintainAR = "";
            string templateName = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 11074325;
                testName = "ImageWatermark_ImageProperties_ChangeGrayscale_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14916580;
                    testName = "ImageWatermark_ImageProperties_ChangeGrayscale_Stage";
                }
                else
                {
                    //instance = 13109735;
                    testName = "IImageWatermark_ImageProperties_ChangeGrayscale_Prod";
                }
            }
            passFail = 1;  //reset before test
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Image Watermark-DO NOT USE";
            CreateLogFile();

            //setup
            CreateTemplateUnderTest(templateName);

            Thread.Sleep(8000);
            tglog.AppendLogText(filePath, "Opened a Security Template ..... OK");

            try
            {
                //Test
                tst.AddImageWatermark(driver);
                tglog.AppendLogText(filePath, "Added Image Watermark.....OK");

                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Image Watermark Width and Height ", ScreenshotImageFormat.Png, 0);
                tglog.AppendLogText(filePath, string.Concat("Width = ", cc.FetchIDefaultValue(driver, tst.troyMark_Width), " / Height = ", cc.FetchIDefaultValue(driver, tst.troyMark_Height)));
                Thread.Sleep(3000);

                maintainAR = cc.FetchChecked(driver, tst.imageWatermarkMaintainAspectRatio);
                tglog.AppendLogText(filePath, maintainAR);
                Thread.Sleep(3000);

                UploadImage("green sea turtle.jpg");
                cc.ClickButton(driver, tst.imageWatermarkGrayscale);
                StringAssert.Contains(cc.FetchChecked(driver, tst.imageWatermarkGrayscale), "True");

                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Image Watermark Width and Height ", ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);

                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                passFail = 0;

                //Finish and Teardown
                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template Changes.....OK");
                Thread.Sleep(5000);

                tst.ExitDesigner(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);

            }
            catch (Exception e)
            {
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Finish and Teardown
                tst.ExitDesigner2(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        [TestMethod, TestCategory("R2 - Designer")]
        public void ImageWatermark_ImageProperties_ChangeOpacity()
        {
            string templateName = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 10614851;
                testName = "ImageWatermark_ImageProperties_ChangeOpacity_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14916583;
                    testName = "ImageWatermark_ImageProperties_ChangeOpacity_Stage";
                }
                else
                {
                    //instance = 13109730;
                    testName = "ImageWatermark_ImageProperties_ChangeOpacity_Prod";
                }
            }
            passFail = 1;  //reset before test
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Image Watermark-DO NOT USE";
            CreateLogFile();

            //setup
            CreateTemplateUnderTest(templateName);

            Thread.Sleep(8000);
            tglog.AppendLogText(filePath, "Opened a Security Template ..... OK");

            try
            {
                //Test
                tst.AddImageWatermark(driver);
                tglog.AppendLogText(filePath, "Added Image Watermark.....OK");

                UploadImage("green sea turtle.jpg");
                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Image Watermark Change Opacity ", ScreenshotImageFormat.Png, 0);
                Thread.Sleep(3000);

                cc.MoveSlider(driver, tst.imageWatermarkOpacity, 50);
                tglog.AppendLogText(filePath, "Changed Image Watermark Opacity.....OK");

                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Image Watermark Change Opacity ", ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);

                //Finish and Teardown
                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template Changes.....OK");
                Thread.Sleep(5000);

                tst.ExitDesigner(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);

                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                passFail = 0;

            }
            catch (Exception e)
            {
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Finish and Teardown
                tst.ExitDesigner2(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        [TestMethod, TestCategory("R2 - Designer")]
        public void ImageWatermark_ImageProperties_ChangeRotation()
        {
            string templateName = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 10614851;
                testName = "ImageWatermark_ImageProperties_ChangeRotation_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14916582;
                    testName = "ImageWatermark_ImageProperties_ChangeRotation_Stage";
                }
                else
                {
                    //instance = 13109730;
                    testName = "ImageWatermark_ImageProperties_ChangeRotation_Prod";
                }
            }
            passFail = 1;  //reset before test
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Image Watermark-DO NOT USE";
            CreateLogFile();

            //setup
            CreateTemplateUnderTest(templateName);

            Thread.Sleep(8000);
            tglog.AppendLogText(filePath, "Opened a Security Template ..... OK");

            try
            {
                //Test
                tst.AddImageWatermark(driver);
                tglog.AppendLogText(filePath, "Added Image Watermark.....OK");

                UploadImage("green sea turtle.jpg");
                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Image Watermark Change Rotation ", ScreenshotImageFormat.Png, 0);
                Thread.Sleep(3000);

                cc.MoveSlider(driver, tst.imageWatermarkRotation, 90);
                tglog.AppendLogText(filePath, "Changed Image Watermark Rotation.....OK");

                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Image Watermark Change Rotation ", ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);

                //Finish and Teardown
                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template Changes.....OK");
                Thread.Sleep(5000);

                tst.ExitDesigner(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);

                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                passFail = 0;

            }
            catch (Exception e)
            {
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Finish and Teardown
                tst.ExitDesigner2(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        [TestMethod, TestCategory("R2 - Designer")]
        public void ImageWatermark_ImageProperties_UseNativeSize()
        {
            string templateName = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 10614851;
                testName = "ImageWatermark_ImageProperties_UseNativeSize_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14916581;
                    testName = "ImageWatermark_ImageProperties_UseNativeSize_Stage";
                }
                else
                {
                    //instance = 13109730;
                    testName = "ImageWatermark_ImageProperties_UseNativeSize_Prod";
                }
            }
            passFail = 1;  //reset before test
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Image Watermark-DO NOT USE";
            CreateLogFile();

            //setup
            CreateTemplateUnderTest(templateName);

            Thread.Sleep(8000);
            tglog.AppendLogText(filePath, "Opened a Security Template ..... OK");

            try
            {
                //Test
                tst.AddImageWatermark(driver);
                tglog.AppendLogText(filePath, "Added Image Watermark.....OK");

                UploadImage("green sea turtle.jpg");
                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Image Watermark Native Size ", ScreenshotImageFormat.Png, 0);
                Thread.Sleep(3000);

                cc.ClickButton(driver, tst.imageWatermarkNativeSize);

                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Image Watermark Native Size ", ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);

                //Finish and Teardown
                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template Changes.....OK");
                Thread.Sleep(5000);

                tst.ExitDesigner(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);

                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                passFail = 0;

            }
            catch (Exception e)
            {
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Finish and Teardown
                tst.ExitDesigner2(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        #endregion

        #region Watermark Tests
        [TestMethod, TestCategory("R1 - Designer")]
        public void Watermark_Position_MoveWaterMark()
        {
            string templateName = "";
            string testValu = "";
            string testValu2 = "";
            string imagePath2 = "";
            string imagePath1 = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 10614850;
                testName = "Watermark_Position_MoveWaterMark_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14927039;
                    testName = "Watermark_Position_MoveWaterMark_Stage";
                }
                else
                {
                    //instance = 13109711;
                    testName = "Watermark_Position_MoveWaterMark_Prod";
                }
            }
            passFail = 1;  //reset before test
            testValu = "561";
            testValu2 = "486";
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Watermark-DO NOT USE";
            CreateLogFile();

            //setup
            CreateTemplateUnderTest(templateName);

            Thread.Sleep(8000);
            tglog.AppendLogText(filePath, "Opened a Security Template ..... OK");

            //Test
            try
            {
                tst.AddWatermark(driver);
                tglog.AppendLogText(filePath, "Added Watermark.....OK");

                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Watermark X and Y ", ScreenshotImageFormat.Png, 0);
                tglog.AppendLogText(filePath, string.Concat("X = ", cc.FetchIDefaultValue(driver, tst.troyMark_X), " / Y = ", cc.FetchIDefaultValue(driver, tst.troyMark_Y)));
                Thread.Sleep(3000);

                cc.EnterText(driver, testValu2, tst.troyMark_X);
                tglog.AppendLogText(filePath, string.Concat("Watermark X Coordinate = ", testValu2));

                cc.EnterText(driver, testValu, tst.troyMark_Y);
                tglog.AppendLogText(filePath, string.Concat("Watermark Y Coordinate = ", testValu));

                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Watermark X and Y ", ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);

                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                passFail = 0;

                //Finish and Teardown
                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template Changes.....OK");
                Thread.Sleep(5000);

                tst.ExitDesigner(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }
            catch (Exception e)
            {
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Finish and Teardown
                tst.ExitDesigner2(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        [TestMethod, TestCategory("R1 - Designer")]
        public void Watermark_Position_ResizeWatermark()
        {
            string templateName = "";
            string testValu = "";
            string testValu2 = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 10614851;
                testName = "Watermark_Position_ResizeWatermark_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14927040;
                    testName = "Watermark_Position_ResizeWatermark_Stage";
                }
                else
                {
                    //instance = 13109712;
                    testName = "Watermark_Position_ResizeWatermark_Prod";
                }
            }
            passFail = 1;  //reset before test
            testValu = "200";
            testValu2 = "200";
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Watermark-DO NOT USE";
            CreateLogFile();

            //setup
            CreateTemplateUnderTest(templateName);

            Thread.Sleep(8000);
            tglog.AppendLogText(filePath, "Opened a Security Template ..... OK");

            try
            {
                //Test
                tst.AddWatermark(driver);
                tglog.AppendLogText(filePath, "Added Watermark.....OK");

                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Watermark Width and Height ", ScreenshotImageFormat.Png, 0);
                tglog.AppendLogText(filePath, string.Concat("Width = ", cc.FetchIDefaultValue(driver, tst.troyMark_Width), " / Height = ", cc.FetchIDefaultValue(driver, tst.troyMark_Height)));
                Thread.Sleep(3000);

                cc.EnterText(driver, testValu2, tst.troyMark_Width);
                tglog.AppendLogText(filePath, string.Concat("Watermark Width = ", testValu2));

                cc.EnterText(driver, testValu, tst.troyMark_Height);
                tglog.AppendLogText(filePath, string.Concat("Watermark Height = ", testValu));

                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Watermark Width and Height ", ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);

                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                passFail = 0;

                //Finish and Teardown
                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template Changes.....OK");
                Thread.Sleep(5000);

                tst.ExitDesigner(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }
            catch (Exception e)
            {
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Finish and Teardown
                tst.ExitDesigner2(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        [TestMethod, TestCategory("R1 - Designer")]
        public void Watermark_Position_ClickFullPage()
        {
            string imagePath2 = "";
            string imagePath1 = "";
            string templateName = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 10614853;
                testName = "Watermark_Position_ClickFullPage_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14927041;
                    testName = "Watermark_Position_ClickFullPage_Stage";
                }
                else
                {
                    //instance = 13109713;
                    testName = "Watermark_Position_ClickFullPage_Prod";
                }
            }
            passFail = 1;  //reset before test
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Watermark-DO NOT USE";
            CreateLogFile();

            //setup
            CreateTemplateUnderTest(templateName);

            Thread.Sleep(8000);
            tglog.AppendLogText(filePath, "Opened a Security Template ..... OK");

            //Test
            try
            {
                tst.AddAuthmark(driver);
                tglog.AppendLogText(filePath, "Added Authmark.....OK");
                Thread.Sleep(2000);

                tst.AddWatermark(driver);
                tglog.AppendLogText(filePath, "Added Watermark.....OK");

                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Watermark Full Page ", ScreenshotImageFormat.Png, 0);
                tglog.AppendLogText(filePath, cc.FetchChecked(driver, tst.troymarkFullPage));
                Thread.Sleep(3000);

                cc.ClickButton(driver, tst.troymarkFullPage);
                Thread.Sleep(2000);

                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Watermark Full Page ", ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);

                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                passFail = 0;

                //Finish and Teardown
                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template Changes.....OK");
                Thread.Sleep(5000);

                tst.ExitDesigner(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }
            catch (Exception e)
            {
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Finish and Teardown
                tst.ExitDesigner2(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        [TestMethod, TestCategory("R1 - Designer")]
        public void Watermark_Position_ChangePosition()
        {
            string imagePath2 = "";
            string imagePath1 = "";
            string templateName = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 10614858;
                testName = "Watermark_Position_ChangePosition_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14927042;
                    testName = "Watermark_Position_ChangePosition_Stage";
                }
                else
                {
                    //instance = 13109714;
                    testName = "Watermark_Position_ChangePosition_Prod";
                }
            }
            passFail = 1;  //reset before test
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Watermark-DO NOT USE";
            CreateLogFile();

            //setup
            CreateTemplateUnderTest(templateName);

            Thread.Sleep(8000);
            tglog.AppendLogText(filePath, "Opened a Security Template ..... OK");

            //Test
            try
            {
                tst.AddAuthmark(driver);
                tglog.AppendLogText(filePath, "Added Authmark.....OK");
                Thread.Sleep(2000);

                tst.AddWatermark(driver);
                tglog.AppendLogText(filePath, "Added Watermark.....OK");

                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Watermark Position ", ScreenshotImageFormat.Png, 0);
                tglog.AppendLogText(filePath, cc.FetchSelectedOption(driver, tst.troyMark_Location));
                Thread.Sleep(3000);

                cc.SelectDropDownByElement(driver, tst.troyMark_Location, "Foreground");
                Thread.Sleep(2000);

                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Watermark Position ", ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);

                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                passFail = 0;

                //Finish and Teardown
                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template Changes.....OK");
                Thread.Sleep(5000);

                tst.ExitDesigner(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }
            catch (Exception e)
            {
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Finish and Teardown
                tst.ExitDesigner2(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        [TestMethod, TestCategory("R2 - Designer")]
        public void Watermark_TextProperties_ChangeAlignment()
        {
            string imagePath3 = "";
            string templateName = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 10614851;
                testName = "Watermark_TextProperties_ChangeAlignment_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14927189;
                    testName = "Watermark_TextProperties_ChangeAlignment_Stage";
                }
                else
                {
                    //instance = 13109730;
                    testName = "Watermark_TextProperties_ChangeAlignment_Prod";
                }
            }
            passFail = 1;  //reset before test
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            imagePath3 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Watermark-DO NOT USE";
            CreateLogFile();

            //setup
            CreateTemplateUnderTest(templateName);

            Thread.Sleep(8000);
            tglog.AppendLogText(filePath, "Opened a Security Template ..... OK");

            try
            {
                //Test
                tst.AddWatermark(driver);
                tglog.AppendLogText(filePath, "Added Watermark.....OK");

                cc.ClickButton(driver, tst.watermarkAlignLeft);
                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Watermark Left Alignment ", ScreenshotImageFormat.Png, 0);
                tglog.AppendLogText(filePath, "Text Left Aligned.....OK");
                Thread.Sleep(3000);

                cc.ClickButton(driver, tst.watermarkAlignRight);
                tglog.AppendLogText(filePath, "Text Right Aligned.....OK");
                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Watermark Right Alignment ", ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);

                cc.ClickButton(driver, tst.watermarkAlignCenter);
                tglog.AppendLogText(filePath, "Text Center Aligned.....OK");
                imagePath3 = cc.TakeScreenShotWithDate(driver, imagePath, "Watermark Center Alignment ", ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);

                //Finish and Teardown
                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template Changes.....OK");
                Thread.Sleep(5000);

                tst.ExitDesigner(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);

                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                tglog.AppendLogText(filePath, string.Concat("Images Comparison 1 & 2 = ", cc.CompareImages(imagePath1, imagePath2).ToString()));

                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath3).ToString(), false.ToString());
                tglog.AppendLogText(filePath, string.Concat("Images Comparison 1 & 3 = ", cc.CompareImages(imagePath1, imagePath3).ToString()));

                StringAssert.Contains(cc.CompareImages(imagePath2, imagePath3).ToString(), false.ToString());
                tglog.AppendLogText(filePath, string.Concat("Images Comparison 2 & 3 = ", cc.CompareImages(imagePath2, imagePath3).ToString()));
                passFail = 0;

            }
            catch (Exception e)
            {
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Finish and Teardown
                tst.ExitDesigner2(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        [TestMethod, TestCategory("R2 - Designer")]
        public void Watermark_TextProperties_CancelEditText()
        {
            string imagePath2 = "";
            string imagePath1 = "";
            string newText = "";
            string originalText = "";
            string templateName = "";
            string text = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 10614857;
                testName = "Watermark_TextProperties_CancelEditText_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14927190;
                    testName = "Watermark_TextProperties_CancelEditText_Stage";
                }
                else
                {
                    //instance = 13109758;
                    testName = "Watermark_TextProperties_CancelEditText_Prod";
                }
            }
            passFail = 1;  //reset before test
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Watermark-DO NOT USE";
            text = "Test Static Text.....QA";
            CreateLogFile();

            //setup
            CreateTemplateUnderTest(templateName);

            Thread.Sleep(8000);
            tglog.AppendLogText(filePath, "Opened a Security Template ..... OK");

            //Test
            try
            {
                tst.AddWatermark(driver);
                tglog.AppendLogText(filePath, "Added Watermark.....OK");

                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Watermark Cancel Edit Text ", ScreenshotImageFormat.Png, 0);
                Thread.Sleep(3000);

                cc.ClickButton(driver, tst.watermarkEditButton);
                Thread.Sleep(1000);

                originalText = cc.FetchIDefaultValue(driver, tst.watermark_InputText);
                cc.EnterText(driver, text, tst.watermark_InputText);
                cc.ClickButton(driver, tst.templateTextCancel);
                tglog.AppendLogText(filePath, "Canceled Edit Text in Watermark.....OK");
                Thread.Sleep(3000);

                // verify 
                cc.ClickButton(driver, tst.watermarkEditButton);
                Thread.Sleep(1000);
                newText = cc.FetchIDefaultValue(driver, tst.watermark_InputText);

                cc.ClickButton(driver, tst.templateTextCancel);
                Thread.Sleep(3000);

                StringAssert.Contains((newText == originalText).ToString(), true.ToString());
                tglog.AppendLogText(filePath, string.Concat("'", newText, "'", " is the same as ", "'", originalText, "'"));

                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Watermark Cancel Edit Text ", ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);

                //StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), true.ToString());
                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                passFail = 0;

                //Finish and Teardown
                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template Changes.....OK");
                Thread.Sleep(5000);

                tst.ExitDesigner(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }
            catch (Exception e)
            {
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Finish and Teardown
                tst.ExitDesigner(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        [TestMethod, TestCategory("R2 - Designer")]
        public void Watermark_TextProperties_ChangeFont()
        {
            string templateName = "";
            string imagePath2 = "";
            string imagePath1 = "";
            //string text = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 10614859;
                testName = "Watermark_TextProperties_ChangeFont_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14927127;
                    testName = "Watermark_TextProperties_ChangeFont_Stage";
                }
                else
                {
                    //instance = 13109720;
                    testName = "Watermark_TextProperties_ChangeFont_Prod";
                }
            }
            passFail = 1;  //reset before test
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Watermark-DO NOT USE";
            //text = "Test Static Text.....Staging";
            CreateLogFile();

            //setup
            CreateTemplateUnderTest(templateName);

            Thread.Sleep(8000);
            tglog.AppendLogText(filePath, "Opened a Security Template ..... OK");

            //Test
            try
            {
                tst.AddWatermark(driver);
                tglog.AppendLogText(filePath, "Added Watermark.....OK");

                //cc.ClickButton(driver, tst.watermarkEditButton);
                //Thread.Sleep(1000);

                //cc.EnterText(driver, text, tst.watermark_InputText);
                //cc.ClickButton(driver, tst.templateTextSave);
                //tglog.AppendLogText(filePath, "Enter Static Text in Watermark.....OK");
                //Thread.Sleep(3000);

                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Watermark Change Font ", ScreenshotImageFormat.Png, 0);
                Thread.Sleep(3000);

                cc.SelectDropDownByElement(driver, tst.watermarkFont, "Courier");
                tglog.AppendLogText(filePath, string.Concat("Watermark Font = ", cc.FetchSelectedOption(driver, tst.watermarkFont)));

                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Watermark Change Font ", ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);

                //Finish and Teardown
                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template Changes.....OK");
                Thread.Sleep(5000);

                tst.ExitDesigner(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);

                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                passFail = 0;

                Thread.Sleep(5000);
            }
            catch (Exception e)
            {
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Finish and Teardown
                tst.ExitDesigner(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        [TestMethod, TestCategory("R2 - Designer")]
        public void Watermark_TextProperties_ChangeFontSize()
        {
            string templateName = "";
            string imagePath2 = "";
            string imagePath1 = "";
            //string text = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 10614860;
                testName = "Watermark_TextProperties_ChangeFontSize_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14927129;
                    testName = "Watermark_TextProperties_ChangeFontSize_Stage";
                }
                else
                {
                    //instance = 13109722;
                    testName = "Watermark_TextProperties_ChangeFontSize_Prod";
                }
            }
            passFail = 1;  //reset before test
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Watermark-DO NOT USE";
            //text = "Test Static Text.....Staging";
            CreateLogFile();

            //setup
            CreateTemplateUnderTest(templateName);

            Thread.Sleep(8000);
            tglog.AppendLogText(filePath, "Opened a Security Template ..... OK");

            //Test
            try
            {
                tst.AddWatermark(driver);
                tglog.AppendLogText(filePath, "Added Watermark.....OK");

                //cc.ClickButton(driver, tst.watermarkEditButton);
                //Thread.Sleep(1000);

                //cc.EnterText(driver, text, tst.watermark_InputText);
                //cc.ClickButton(driver, tst.templateTextSave);
                //tglog.AppendLogText(filePath, "Enter Static Text in Watermark.....OK");
                //Thread.Sleep(3000);

                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Watermark Change Font Size ", ScreenshotImageFormat.Png, 0);
                Thread.Sleep(3000);

                cc.EnterText(driver, "20", tst.watermarkFontSize);
                tglog.AppendLogText(filePath, string.Concat("Watermark Font Size = ", cc.FetchIDefaultValue(driver, tst.watermarkFontSize)));

                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Watermark Change Font Size ", ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);

                //Finish and Teardown
                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template Changes.....OK");
                Thread.Sleep(5000);

                tst.ExitDesigner(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);

                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                passFail = 0;

                Thread.Sleep(5000);
            }
            catch (Exception e)
            {
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Finish and Teardown
                tst.ExitDesigner(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);

        }

        [TestMethod, TestCategory("R2 - Designer")]
        public void Watermark_TextProperties_ChangeLineSpacing()
        {
            string templateName = "";
            string imagePath2 = "";
            string imagePath1 = "";
            //string text = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 10614861;
                testName = "Watermark_TextProperties_ChangeLineSpacing_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14927130;
                    testName = "Watermark_TextProperties_ChangeLineSpacing_Stage";
                }
                else
                {
                    //instance = 13109723;
                    testName = "Watermark_TextProperties_ChangeLineSpacing_Prod";
                }
            }
            passFail = 1;  //reset before test
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Watermark-DO NOT USE";
            //text = "Test Static Text.....Staging";
            CreateLogFile();

            //setup
            CreateTemplateUnderTest(templateName);

            Thread.Sleep(8000);
            tglog.AppendLogText(filePath, "Opened a Security Template ..... OK");

            //Test
            try
            {
                tst.AddWatermark(driver);
                tglog.AppendLogText(filePath, "Added Watermark.....OK");

                //cc.ClickButton(driver, tst.watermarkEditButton);
                //Thread.Sleep(1000);

                //cc.EnterText(driver, text, tst.watermark_InputText);
                //cc.ClickButton(driver, tst.templateTextSave);
                //tglog.AppendLogText(filePath, "Enter Static Text in Watermark.....OK");
                //Thread.Sleep(3000);

                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Watermark Change Text Line Spacing ", ScreenshotImageFormat.Png, 0);
                Thread.Sleep(3000);

                cc.EnterText(driver, "24", tst.watermark_Spacing);
                tglog.AppendLogText(filePath, string.Concat("Watermark Text Line Spacing = ", cc.FetchIDefaultValue(driver, tst.watermark_Spacing)));

                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Watermark Change Text Line Spacing ", ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);

                //Finish and Teardown
                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template Changes.....OK");
                Thread.Sleep(5000);

                tst.ExitDesigner(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);

                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                passFail = 0;

                Thread.Sleep(5000);
            }
            catch (Exception e)
            {
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Finish and Teardown
                tst.ExitDesigner(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);

        }

        [TestMethod, TestCategory("R2 - Designer")]
        public void Watermark_TextProperties_ChangeTextColor()
        {
            string imagePath2 = "";
            string imagePath1 = "";
            string originalHex = "";
            string templateName = "";
            //string text = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 10614852;
                testName = "Watermark_TextProperties_ChangeTextColor_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14927128;
                    testName = "StripedWatermark_TextProperties_ChangeTextColor_Stage";
                }
                else
                {
                    //instance = 13109721;
                    testName = "Watermark_TextProperties_ChangeTextColor_Prod";
                }
            }
            passFail = 1;  //reset before test
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Watermark-DO NOT USE";
            //text = "Test Static Text.....Staging";
            CreateLogFile();

            //setup
            CreateTemplateUnderTest(templateName);

            Thread.Sleep(8000);
            tglog.AppendLogText(filePath, "Opened a Security Template ..... OK");

            //Test
            try
            {
                tst.AddWatermark(driver);
                tglog.AppendLogText(filePath, "Added Watermark.....OK");

                //cc.ClickButton(driver, tst.element_EditText);
                //Thread.Sleep(1000);

                //cc.EnterText(driver, text, tst.templateTextInput);
                //cc.ClickButton(driver, tst.templateTextSave);
                //tglog.AppendLogText(filePath, "Enter Static Text in TROYMark.....OK");
                //Thread.Sleep(3000);

                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Watermark Change Font ", ScreenshotImageFormat.Png, 0);
                Thread.Sleep(3000);

                cc.ClickButton(driver, tst.watermarkTextColor);
                Thread.Sleep(3000);
                originalHex = cc.FetchIDefaultValue(driver, tst.watermark_Text_Hex);
                tglog.AppendLogText(filePath, string.Concat("Text R Value = ", cc.FetchIDefaultValue(driver, tst.watermark_Text_R)));
                tglog.AppendLogText(filePath, string.Concat("Text G Value = ", cc.FetchIDefaultValue(driver, tst.watermark_Text_G)));
                tglog.AppendLogText(filePath, string.Concat("Text B Value = ", cc.FetchIDefaultValue(driver, tst.watermark_Text_B)));
                tglog.AppendLogText(filePath, string.Concat("Text A Value = ", cc.FetchIDefaultValue(driver, tst.watermark_Text_A)));
                tglog.AppendLogText(filePath, string.Concat("Text Hex Value = ", originalHex));

                cc.EnterText(driver, "31", tst.watermark_Text_R);
                Thread.Sleep(3000);
                cc.EnterText(driver, "219", tst.watermark_Text_G);
                Thread.Sleep(3000);
                cc.EnterText(driver, "219", tst.watermark_Text_B);
                Thread.Sleep(3000);
                cc.EnterText(driver, "90", tst.watermark_Text_A);
                Thread.Sleep(3000);

                tglog.AppendLogText(filePath, string.Concat("Text R Value = ", cc.FetchIDefaultValue(driver, tst.watermark_Text_R)));
                tglog.AppendLogText(filePath, string.Concat("Text G Value = ", cc.FetchIDefaultValue(driver, tst.watermark_Text_G)));
                tglog.AppendLogText(filePath, string.Concat("Text B Value = ", cc.FetchIDefaultValue(driver, tst.watermark_Text_B)));
                tglog.AppendLogText(filePath, string.Concat("Text A Value = ", cc.FetchIDefaultValue(driver, tst.watermark_Text_A)));
                tglog.AppendLogText(filePath, string.Concat("Text Hex Value = ", cc.FetchIDefaultValue(driver, tst.watermark_Text_Hex)));

                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Watermark Change Font ", ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);

                cc.ClickButton(driver, tst.watermarkTextColor2);
                Thread.Sleep(2000);
                cc.ScrollUP(driver);

                //Finish and Teardown
                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template Changes.....OK");
                Thread.Sleep(5000);

                tst.ExitDesigner(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);

                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                passFail = 0;

                Thread.Sleep(5000);
            }
            catch (Exception e)
            {
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Finish and Teardown
                tst.ExitDesigner(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        [TestMethod, TestCategory("R2 - Designer")]
        public void Watermark_TextProperties_ChangeTransformText()
        {
            string templateName = "";
            string imagePath2 = "";
            string imagePath1 = "";
            //string text = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 10614862;
                testName = "Watermark_TextProperties_ChangeTransformText_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14927131;
                    testName = "Watermark_TextProperties_ChangeTransformText_Stage";
                }
                else
                {
                    //instance = 13109724;
                    testName = "Watermark_TextProperties_ChangeTransformText_Prod";
                }
            }
            passFail = 1;  //reset before test
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Watermark-DO NOT USE";
            //text = "Test Static Text.....Staging";
            CreateLogFile();

            //setup
            CreateTemplateUnderTest(templateName);

            Thread.Sleep(8000);
            tglog.AppendLogText(filePath, "Opened a Security Template ..... OK");

            //Test
            try
            {
                tst.AddWatermark(driver);
                tglog.AppendLogText(filePath, "Added Watermark.....OK");

                //cc.ClickButton(driver, tst.watermarkEditButton);
                //Thread.Sleep(1000);

                //cc.EnterText(driver, text, tst.watermark_InputText);
                //cc.ClickButton(driver, tst.templateTextSave);
                //tglog.AppendLogText(filePath, "Enter Static Text in Watermark.....OK");
                //Thread.Sleep(3000);

                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Watermark Change Text Transform ", ScreenshotImageFormat.Png, 0);
                Thread.Sleep(3000);

                cc.SelectDropDownByElement(driver, tst.watermark_Transform, "ToUpper");
                tglog.AppendLogText(filePath, string.Concat("Watermark Text Transform = ", cc.FetchSelectedOption(driver, tst.watermark_Transform)));

                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Watermark Change Text Transform ", ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);

                //Finish and Teardown
                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template Changes.....OK");
                Thread.Sleep(5000);

                tst.ExitDesigner(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);

                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                passFail = 0;

                Thread.Sleep(5000);
            }
            catch (Exception e)
            {
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Finish and Teardown
                tst.ExitDesigner(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        [TestMethod, TestCategory("R2 - Designer")]
        public void Watermark_TextProperties_EnterStaticText()
        {
            string imagePath2 = "";
            string imagePath1 = "";
            string originalText = "";
            string templateName = "";
            string text = "";
            string newText = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 10614856;
                testName = "Watermark_TextProperties_EnterStaticText_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14927186;
                    testName = "Watermark_TextProperties_EnterStaticText_Stage";
                }
                else
                {
                    //instance = 13109715;
                    testName = "Watermark_TextProperties_EnterStaticText_Prod";
                }
            }
            passFail = 1;  //reset before test
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Watermark-DO NOT USE";
            text = "Test Static Text.....Staging";
            CreateLogFile();

            //setup
            CreateTemplateUnderTest(templateName);

            Thread.Sleep(8000);
            tglog.AppendLogText(filePath, "Opened a Security Template ..... OK");

            //Test
            try
            {
                tst.AddWatermark(driver);
                tglog.AppendLogText(filePath, "Added Watermark.....OK");

                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Watermark Static Text ", ScreenshotImageFormat.Png, 0);
                Thread.Sleep(3000);

                cc.ClickButton(driver, tst.watermarkEditButton);
                Thread.Sleep(1000);

                originalText = cc.FetchIDefaultValue(driver, tst.watermark_InputText);
                cc.EnterText(driver, text, tst.watermark_InputText);
                newText = cc.FetchIDefaultValue(driver, tst.watermark_InputText);
                cc.ClickButton(driver, tst.templateTextSave);
                tglog.AppendLogText(filePath, "Enter Static Text in Watermark.....OK");
                Thread.Sleep(3000);

                // verify 
                cc.ClickButton(driver, tst.watermarkEditButton);
                Thread.Sleep(1000);

                StringAssert.Contains((newText == cc.FetchIDefaultValue(driver, tst.watermark_InputText)).ToString(), true.ToString());
                tglog.AppendLogText(filePath, string.Concat("'", newText, "'", " is not the same as ", "'", originalText, "'"));

                cc.ClickButton(driver, tst.templateTextCancel);
                Thread.Sleep(3000);

                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Watermark Static Text ", ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);

                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                passFail = 0;

                //Finish and Teardown
                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template Changes.....OK");
                Thread.Sleep(5000);

                tst.ExitDesigner(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }
            catch (Exception e)
            {
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Finish and Teardown
                tst.ExitDesigner(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        [TestMethod, TestCategory("R2 - Designer")]
        public void Watermark_TextProperties_SelectJobMetadata()
        {
            string imagePath2 = "";
            string imagePath1 = "";
            string originalText = "";
            string newText = "";
            string templateName = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 10614855;
                testName = "Watermark_TextProperties_SelectJobMetadata_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14927187;
                    testName = "Watermark_TextProperties_SelectJobMetadata_Stage";
                }
                else
                {
                    //instance = 13109716;
                    testName = "Watermark_TextProperties_SelectJobMetadata_Prod";
                }
            }
            passFail = 1;  //reset before test
            imagePath = string.Concat(filePath, "Test Files\\");
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Watermark-DO NOT USE";
            CreateLogFile();

            //setup
            CreateTemplateUnderTest(templateName);

            Thread.Sleep(8000);
            tglog.AppendLogText(filePath, "Opened a Security Template ..... OK");

            //Test
            try
            {
                tst.AddWatermark(driver);
                tglog.AppendLogText(filePath, "Added Watermark.....OK");

                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Watermark Static Text ", ScreenshotImageFormat.Png, 0);
                Thread.Sleep(3000);

                cc.ClickButton(driver, tst.watermarkEditButton);
                Thread.Sleep(3000);

                originalText = cc.FetchIDefaultValue(driver, tst.watermark_InputText);
                tst.ClickMetadataButton(driver, tst.templateMetadataList, "Serial Number");
                newText = cc.FetchIDefaultValue(driver, tst.watermark_InputText);
                cc.ClickButton(driver, tst.templateTextSave);
                tglog.AppendLogText(filePath, "Enter Job Metadata in Watermark.....OK");
                Thread.Sleep(3000);

                // verify 
                cc.ClickButton(driver, tst.watermarkEditButton);
                Thread.Sleep(1000);

                StringAssert.Contains((newText == cc.FetchIDefaultValue(driver, tst.watermark_InputText)).ToString(), true.ToString());
                tglog.AppendLogText(filePath, string.Concat("'", newText, "'", " is not the same as ", "'", originalText, "'"));

                cc.ClickButton(driver, tst.templateTextCancel);
                Thread.Sleep(3000);

                //Finish and Teardown
                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Watermark Static Text ", ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);

                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                passFail = 0;

                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template Changes.....OK");
                Thread.Sleep(5000);

                tst.ExitDesigner(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }
            catch (Exception e)
            {
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Finish and Teardown
                tst.ExitDesigner(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        [TestMethod, TestCategory("R2 - Designer")]
        public void Watermark_TextProperties_StaticTextJobMetadata()
        {
            string imagePath2 = "";
            string imagePath1 = "";
            string originalText = "";
            string templateName = "";
            string text = "";
            string newText = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 10614856;
                testName = "Watermark_TextProperties_StaticTextJobMetadata_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14927188;
                    testName = "Watermark_TextProperties_StaticTextJobMetadata_Stage";
                }
                else
                {
                    //instance = 13109718;
                    testName = "Watermark_TextProperties_StaticTextJobMetadata_Prod";
                }
            }
            passFail = 1;  //reset before test
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Watermark-DO NOT USE";
            text = "Top Secret";
            CreateLogFile();

            //setup
            CreateTemplateUnderTest(templateName);

            Thread.Sleep(8000);
            tglog.AppendLogText(filePath, "Opened a Security Template ..... OK");

            //Test
            try
            {
                tst.AddWatermark(driver);
                tglog.AppendLogText(filePath, "Added Watermark.....OK");

                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Watermark Static Text and Job Metadata", ScreenshotImageFormat.Png, 0);
                Thread.Sleep(3000);

                cc.ClickButton(driver, tst.watermarkEditButton);
                Thread.Sleep(1000);

                originalText = cc.FetchIDefaultValue(driver, tst.watermark_InputText);
                cc.EnterText(driver, text, tst.watermark_InputText);
                tst.ClickMetadataButton(driver, tst.templateMetadataList, "Serial Number");
                newText = cc.FetchIDefaultValue(driver, tst.watermark_InputText);
                cc.ClickButton(driver, tst.templateTextSave);
                tglog.AppendLogText(filePath, "Enter Static Text and Job Metadata in Watermark.....OK");
                Thread.Sleep(3000);

                // verify 
                cc.ClickButton(driver, tst.watermarkEditButton);
                Thread.Sleep(1000);

                StringAssert.Contains((newText == cc.FetchIDefaultValue(driver, tst.watermark_InputText)).ToString(), true.ToString());
                tglog.AppendLogText(filePath, string.Concat("'", newText, "'", " is not the same as ", "'", originalText, "'"));

                cc.ClickButton(driver, tst.templateTextCancel);
                Thread.Sleep(3000);

                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Watermark Static Text and Job Metadata", ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);

                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                passFail = 0;

                //Finish and Teardown
                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template Changes.....OK");
                Thread.Sleep(5000);

                tst.ExitDesigner(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }
            catch (Exception e)
            {
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Finish and Teardown
                tst.ExitDesigner(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        [TestMethod, TestCategory("R2 - Designer")]
        public void Watermark_Typography_ChangeProportionalSize()
        {
            string templateName = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 10614851;
                testName = "Watermark_Typography_ChangeProportionalSize_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14927135;
                    testName = "Watermark_Typography_ChangeProportionalSize_Stage";
                }
                else
                {
                    //instance = 13109730;
                    testName = "Watermark_Typography_ChangeProportionalSize_Prod";
                }
            }
            passFail = 1;  //reset before test
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Watermark-DO NOT USE";
            CreateLogFile();

            //setup
            CreateTemplateUnderTest(templateName);

            Thread.Sleep(8000);
            tglog.AppendLogText(filePath, "Opened a Security Template ..... OK");

            try
            {
                //Test
                tst.AddWatermark(driver);
                tglog.AppendLogText(filePath, "Added Watermark.....OK");

                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Watermark Change Proportional Size ", ScreenshotImageFormat.Png, 0);
                Thread.Sleep(3000);

                cc.MoveSlider(driver, tst.watermarkRotation, 180);
                Thread.Sleep(2000);
                cc.ClickButton(driver, tst.watermarkProportionalSize);
                tglog.AppendLogText(filePath, "Changed Watermark Proportional Size.....OK");

                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Watermark Change Proportional Size ", ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);

                //Finish and Teardown
                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template Changes.....OK");
                Thread.Sleep(5000);

                tst.ExitDesigner(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);

                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                passFail = 0;

            }
            catch (Exception e)
            {
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Finish and Teardown
                tst.ExitDesigner2(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        [TestMethod, TestCategory("R2 - Designer")]
        public void Watermark_Typography_RotateCenter()
        {
            string templateName = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 10614851;
                testName = "Watermark_Typography_RotateCenter_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14927133;
                    testName = "Watermark_Typography_RotateCenter_Stage";
                }
                else
                {
                    //instance = 13109730;
                    testName = "Watermark_Typography_RotateCenter_Prod";
                }
            }
            passFail = 1;  //reset before test
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Watermark-DO NOT USE";
            CreateLogFile();

            //setup
            CreateTemplateUnderTest(templateName);

            Thread.Sleep(8000);
            tglog.AppendLogText(filePath, "Opened a Security Template ..... OK");

            try
            {
                //Test
                tst.AddWatermark(driver);
                tglog.AppendLogText(filePath, "Added Watermark.....OK");

                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Watermark Center Rotation ", ScreenshotImageFormat.Png, 0);
                Thread.Sleep(3000);

                cc.ClickButton(driver, tst.watermarkRotateCenter);
                tglog.AppendLogText(filePath, "Changed Center Rotation.....OK");

                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Watermark Center Rotation ", ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);

                //Finish and Teardown
                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template Changes.....OK");
                Thread.Sleep(5000);

                tst.ExitDesigner(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);

                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                passFail = 0;

            }
            catch (Exception e)
            {
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Finish and Teardown
                tst.ExitDesigner2(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        [TestMethod, TestCategory("R2 - Designer")]
        public void Watermark_Typography_RotateLeft()
        {
            string templateName = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 10614851;
                testName = "Watermark_Typography_RotateLeft_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14927136;
                    testName = "Watermark_Typography_RotateLeft_Stage";
                }
                else
                {
                    //instance = 13109730;
                    testName = "Watermark_Typography_RotateLeft_Prod";
                }
            }
            passFail = 1;  //reset before test
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Watermark-DO NOT USE";
            CreateLogFile();

            //setup
            CreateTemplateUnderTest(templateName);

            Thread.Sleep(8000);
            tglog.AppendLogText(filePath, "Opened a Security Template ..... OK");

            try
            {
                //Test
                tst.AddWatermark(driver);
                tglog.AppendLogText(filePath, "Added Watermark.....OK");

                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Watermark Left Rotation ", ScreenshotImageFormat.Png, 0);
                Thread.Sleep(3000);

                cc.ClickButton(driver, tst.watermarkRotateLeft);
                tglog.AppendLogText(filePath, "Changed Left Rotation.....OK");

                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Watermark Left Rotation ", ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);

                //Finish and Teardown
                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template Changes.....OK");
                Thread.Sleep(5000);

                tst.ExitDesigner(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);

                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                passFail = 0;

            }
            catch (Exception e)
            {
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Finish and Teardown
                tst.ExitDesigner2(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        [TestMethod, TestCategory("R2 - Designer")]
        public void Watermark_Typography_RotateRight()
        {
            string templateName = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 10614851;
                testName = "Watermark_Typography_RotateRight_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14927132;
                    testName = "Watermark_Typography_RotateRight_Stage";
                }
                else
                {
                    //instance = 13109730;
                    testName = "Watermark_Typography_RotateRight_Prod";
                }
            }
            passFail = 1;  //reset before test
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Watermark-DO NOT USE";
            CreateLogFile();

            //setup
            CreateTemplateUnderTest(templateName);

            Thread.Sleep(8000);
            tglog.AppendLogText(filePath, "Opened a Security Template ..... OK");

            try
            {
                //Test
                tst.AddWatermark(driver);
                tglog.AppendLogText(filePath, "Added Watermark.....OK");

                cc.ClickButton(driver, tst.watermarkRotateCenter);
                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Watermark Right Rotation ", ScreenshotImageFormat.Png, 0);
                tglog.AppendLogText(filePath, "Changed Center Rotation.....OK");
                Thread.Sleep(3000);

                cc.ClickButton(driver, tst.watermarkRotateRight);
                tglog.AppendLogText(filePath, "Changed Right Rotation.....OK");

                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Watermark Right Rotation ", ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);

                //Finish and Teardown
                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template Changes.....OK");
                Thread.Sleep(5000);

                tst.ExitDesigner(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);

                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                passFail = 0;

            }
            catch (Exception e)
            {
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Finish and Teardown
                tst.ExitDesigner2(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        [TestMethod, TestCategory("R2 - Designer")]
        public void Watermark_Typography_ChangeRotation()
        {
            string templateName = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 10614851;
                testName = "Watermark_Typography_ChangeRotation_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14927134;
                    testName = "Watermark_Typography_ChangeRotation_Stage";
                }
                else
                {
                    //instance = 13109730;
                    testName = "Watermark_Typography_ChangeRotation_Prod";
                }
            }
            passFail = 1;  //reset before test
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Watermark-DO NOT USE";
            CreateLogFile();

            //setup
            CreateTemplateUnderTest(templateName);

            Thread.Sleep(8000);
            tglog.AppendLogText(filePath, "Opened a Security Template ..... OK");

            try
            {
                //Test
                tst.AddWatermark(driver);
                tglog.AppendLogText(filePath, "Added Watermark.....OK");

                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Watermark Change Rotation ", ScreenshotImageFormat.Png, 0);
                Thread.Sleep(3000);

                cc.MoveSlider(driver, tst.watermarkRotation, 90);
                tglog.AppendLogText(filePath, "Changed Watermark Rotation.....OK");

                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Watermark Change Rotation ", ScreenshotImageFormat.Png, 1);
                Thread.Sleep(3000);

                //Finish and Teardown
                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template Changes.....OK");
                Thread.Sleep(5000);

                tst.ExitDesigner(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);

                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());
                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                passFail = 0;

            }
            catch (Exception e)
            {
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Finish and Teardown
                tst.ExitDesigner2(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }
        #endregion  

        #region Methods
        private void CreateLogFile()
        {
            tglog.CreateLogFile(filePath, string.Concat(Environment.NewLine, "----------- Start testing using ", brow, " -----------", Environment.NewLine));
            tglog.AppendLogText(filePath, string.Concat("Set up ", brow, " ..... OK"));
            tglog.AppendLogText(filePath, string.Concat("URL = ", baseURL));
            tglog.AppendLogText(filePath, string.Concat("Envionment = ", qaDev));
            tglog.AppendLogText(filePath, string.Concat("User = ", user));
            tglog.AppendLogText(filePath, "Opened SecureDocs in browser ..... OK");
            tglog.AppendLogText(filePath, string.Concat("Build Version: ", build));
            tglog.AppendLogText(filePath, "Logged into SecureDocs ..... OK");
            tglog.AppendLogText(filePath, string.Concat("Instance = ", instance));
            tglog.AppendLogText(filePath, string.Concat("FilePath = ", filePath));
            tglog.AppendLogText(filePath, "Opened SecureDocs Dashboard ..... OK");
        }

        private void CreateTemplateUnderTest(string text)
        {
            OpenSecurityTemplates();
            cst = st.OpenCreateTemplate(driver);
            //tglog.AppendLogText(filePath, driver.Title);

            tglog.AppendLogText(filePath, "Opened New Security Template popup ..... OK");

            tst = cst.OpenTemplate(driver);
            driver.SwitchTo().Window(baseWindowHandle);
            tglog.AppendLogText(filePath, "Opened New Security Template ..... OK");

            baseWindowHandle = driver.CurrentWindowHandle;

            Thread.Sleep(5000);
            nt = tst.OpenNameTemplate(driver);
            Thread.Sleep(3000);

            nt.NameTemplate(driver, text);
            Thread.Sleep(8000);
        }

        private void OpenSecurityTemplates()
        {
            try
            {
                st = mp.OpenSecurityTemplates(driver);
                //tglog.AppendLogText(filePath, driver.Title);

                //StringAssert.Contains(cc.FetchInnerHTML(driver, st.breadcrumb), "Security Templates");
                passFail = 0;
                tglog.AppendLogText(filePath, "Opened Security Templates ..... OK");

            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        private void RemoveTemplateUnderTest(string text)
        {
            string ele = "";
            string delBtn = "";
            int itemCount = 0;
            string templateName = "";

            itemCount = Convert.ToInt16(cc.FetchChildCount(driver, st.templateGrid));
            for (int x = 1; x <= itemCount; x++)
            {
                delBtn = string.Concat(st.templateGrid, "/div[", x.ToString(), "]/div[1]/div/a[3]");
                ele = string.Concat(st.templateGrid, "/div[", x.ToString(), "]/div[2]/a");
                templateName = cc.FetchInnerHTML(driver, ele);

                if (templateName == text)
                {
                    rst = st.RemoveTemplate(driver, delBtn);

                    StringAssert.Contains(string.Concat("Delete Template '", templateName, "'"), cc.FetchInnerHTML(driver, rst.header));
                    tglog.AppendLogText(filePath, "Opened Remove Security Template..... OK");

                    st = rst.SecurityTemplateYES(driver, "Yes");
                    tglog.AppendLogText(filePath, string.Concat("Removed template ", templateName));
                    break;
                }
            }
        }

        private void SendToPractitest()
        {
            if (qaDev.ToUpper() == "DEV")
            {
                ptc.PT_Post_Specific_Run(filePath, passFail, resultMsg, instance, project, brow, "Development");
            }
            else
            {
                if (qaDev.ToUpper() == "STAGE")
                {
                    ptc.PT_Post_Specific_Run(filePath, passFail, resultMsg, instance, project, brow, "Stage");
                }
                else
                {
                    ptc.PT_Post_Specific_Run(filePath, passFail, resultMsg, instance, project, brow, "Production");
                }
            }
        }

        private void SetupDriver()
        {
            if ((brow.ToLower() == "ie"))
            {
                this.driver = new InternetExplorerDriver("C:\\Selenium");
            }

            if ((brow.ToLower() == "chrome"))
            {
                this.driver = new ChromeDriver(@"C:\Selenium");
            }

            //if ((brow.ToLower() == "firefox"))
            //{
            //    this.driver = new FirefoxDriver();
            //}

        }

        private void SetupCaptureText()
        {
            cc.ClickButton(driver, tst.captureElements);
            tglog.AppendLogText(filePath, "Exposed Security Elements ..... OK");

            cc.ClickButton(driver, tst.captureText);
            tglog.AppendLogText(filePath, "Added Data Capture To Template ..... OK");
        }

        private void SetUpEnvironVars()
        {
            if (qaDev.ToLower() == dev)
            {
                filePath = string.Concat(filePath, "Dev\\");
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    filePath = string.Concat(filePath, "Stage\\");
                }
                else
                {
                    filePath = string.Concat(filePath, "Prod\\");
                }
            }
        }

        private void UploadImage(string filename)
        {
            cc.ClickButton(driver, tst.imageWatermarkUploadImage);
            Thread.Sleep(1000);
            tglog.AppendLogText(filePath, "Clicked Upload Image");

            cc.ClickButton(driver, tst.selectImageArea);
            Thread.Sleep(1000);
            tglog.AppendLogText(filePath, "Click Select File Area");

            System.Windows.Forms.SendKeys.SendWait(string.Concat(imageFilesPath, filename));
            System.Windows.Forms.SendKeys.SendWait("{ENTER}");
            tglog.AppendLogText(filePath, "Uploaded File ..... OK");
            Thread.Sleep(3000);
        }
        #endregion

        //--------------------------------------------------------------------------------------------
        #region Additional test attributes
        [TestInitialize()]
        public void MyTestInitialize()
        {
            cc = new SecureRx.Common.Common_Code(driver);

            brow = ConfigurationManager.AppSettings["browser"];
            baseURL = ConfigurationManager.AppSettings["DocrityURL"];
            project = ConfigurationManager.AppSettings["project"];
            qaDev = ConfigurationManager.AppSettings["testEnv"];
            user = ConfigurationManager.AppSettings["testUser"].ToUpper();
            string pswd = ConfigurationManager.AppSettings["TestPswd"];

            SetupDriver();
            SetUpEnvironVars();

            lp = new SecureRx.Page_Elements.LoginPage(driver, baseURL);
            build = lp.FetchBuildVersion(driver);
            mp = lp.SDLogin(user, pswd, driver);

            //Did Home Menu open
            if (mp == null)
            {
                throw new NoSuchWindowException("SecureRx home page was not displayed!");
            }
            else
            {
                passFail = 0;
            }
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {

            driver.SwitchTo().Window(baseWindowHandle);
            Thread.Sleep(1000);

            try
            {
                mp.LogoutandCloseBrowser(driver);
                tglog.AppendLogText(filePath, "Logged out and Closed browser..... OK");
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }

            if (passFail != 0)
            {
                resultMsg = string.Concat(testName, " ..... FAILED!!!!! Please refer to log file");
                //driver.Close();
            }
            else
            {
                resultMsg = string.Concat(testName, " ..... PASSED!!!!!");
            }

            SendToPractitest();

            tglog.AppendLogText(filePath, "Close SecureRx");
            tglog.AppendLogText(filePath, "\n Test Finished \n");
        }

        #endregion

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;
    }
}