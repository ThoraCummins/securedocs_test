﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;

namespace SecureRx.Tests
{

    [TestClass]
    public class DocumentLog_Tests
    {
        #region libraries

        SecureRx.Page_Elements.MainPage mp = null;
        SecureRx.Page_Elements.LoginPage lp = null;

       SecureRx.Page_Elements.Reports.DocumentLogs dl = null;
       SecureRx.Page_Elements.Reports.Document_Logs_Details dld = null;

        SecureRx.Common.Common_Code cc = null;
        SecureRx.Common.PT_Client ptc = new Common.PT_Client();
        SecureRx.Common.TGLogging tglog = new Common.TGLogging();
        #endregion

        //Vars
        private string baseURL = "";
        private string baseWindowHandle = "";
        private string beta = "beta";
        private string brow = "";
        private string build = "";
        private string dev = "dev";
        private IWebDriver driver = null;
        private string filePath = "C:\\Test Logs\\SecureRx\\";
        private int instance = 0;
        private int passFail = 1;
        private string project = "";
        private string qa = "qa";
        private string qaDev = "";
        private string resultMsg = "";
        private string testName = "";
        private string user = "";

//        [TestMethod, TestCategory("Activity - Regression")]
//        public void Activity_ActivityList_VerifyActivityData()
//        {
//            string cellOne = "";
//            string copies = "";
//            string createdDate = "";
//            string docName = "";
//            string inputAgent = "";
//            string pages = "";
//            string processTime = "";
//            int rows = 0;
//            string serialNumber = "";
//            string userName = "";

//            if (qaDev.ToLower() == dev)
//            {
//                instance = 11117041;
//                testName = "Activity_ActivityList_VerifyActivityData_Dev";
//            }
//            else
//            {
//                if (qaDev.ToLower() == qa)
//                {
//                    instance = 11507427;
//                    testName = "Activity_ActivityList_VerifyActivityData_QA";
//                }
//                else
//                //{
//                //    if (qaDev.ToLower() == beta)
//                //    {
//                //        //instance = 4748406;
//                //        testName = "Activity_ActivityList_VerifyActivityData_Beta";
//                //    }
//                //    else
//                    {
//                        instance = 11263085;
//                        testName = "Activity_ActivityList_VerifyActivityData_Prod";
//                    }
//                //}
//            }
//            passFail = 1;  //reset before test
//            filePath = string.Concat(filePath, testName, ".txt");

//            CreateLogFile();

//            try
//            {
//                act = mp.OpenActivity(driver);
//                Thread.Sleep(3000)
//;                //tglog.AppendLogText(filePath, driver.Title);

//                StringAssert.Contains(cc.FetchInnerHTML(driver, act.Header), "Activity");
//                tglog.AppendLogText(filePath, "Opened Activity Page ..... OK");

//                //Date Change
//                cc.ClickButton(driver, act.calendarElement);
//                cc.EnterText(driver, "06/21/2018", act.startDate);
//                cc.EnterText(driver, "06/21/2018", act.endDate);

//                tglog.AppendLogText(filePath, string.Concat("Start date = ", cc.FetchTextValue(driver, act.startDate), " / End Date = ", cc.FetchTextValue(driver, act.endDate)));
//                Thread.Sleep(3000);
//                cc.ClickButton(driver, act.calendarElement);
//                Thread.Sleep(5000);

//                rows = Convert.ToInt16(cc.FetchTableRows(driver, act.printLogTable));
//                tglog.AppendLogText(filePath, string.Concat("Row Count is ", rows.ToString(), Environment.NewLine));

//                for (int x = 1; x <= rows; x++)
//                {
//                    cellOne = cc.FetchCellTextValue(driver, "1", x.ToString(), act.printLogTable);
//                    cellOne = cellOne.Replace("\r\n", "");
//                    serialNumber = cellOne.Substring(0, cellOne.IndexOf(" "));
//                    docName = cellOne.Substring(cellOne.IndexOf(" ") + 1, cellOne.IndexOf(".pdf") - cellOne.IndexOf(" ") + 3);

//                    inputAgent = cc.FetchCellValue(driver, "2", x.ToString(), act.printLogTable);
//                    userName = cc.FetchCellValue(driver, "3", x.ToString(), act.printLogTable);
//                    createdDate = cc.FetchCellValue(driver, "4", x.ToString(), act.printLogTable);
//                    processTime = cc.FetchCellValue(driver, "5", x.ToString(), act.printLogTable);
//                    pages = cc.FetchCellValue(driver, "6", x.ToString(), act.printLogTable);
//                    copies = cc.FetchCellValue(driver, "7", x.ToString(), act.printLogTable);

//                     //tglog.AppendLogText(filePath, cellOne);

//                    //test
//                    actd = act.OpenSpecificPrintLog(driver, x.ToString());
//                    Thread.Sleep(3000);

//                    tglog.AppendLogText(filePath, string.Concat("serial number = ", serialNumber, " /  doc name = ", docName));
//                    StringAssert.Contains(cc.FetchInnerHTML(driver, actd.serialNumber), serialNumber);
//                    StringAssert.Contains(cc.FetchInnerHTML(driver, actd.documentName), docName);

//                   tglog.AppendLogText(filePath, string.Concat("created on ", createdDate, " /  Process Time is ", processTime));
//                   StringAssert.Contains(cc.FetchInnerHTML(driver, actd.processTime), processTime);

//                    tglog.AppendLogText(filePath, string.Concat("Input/Agent = ", inputAgent, " / user = ", userName));
//                    StringAssert.Contains(cc.FetchInnerHTML(driver, actd.printAgent), inputAgent);
//                    StringAssert.Contains(cc.FetchInnerHTML(driver, actd.userName), userName);

//                    tglog.AppendLogText(filePath, string.Concat(" pages = ", pages, " / copies  = ", copies, Environment.NewLine));
//                    StringAssert.Contains(cc.FetchInnerHTML(driver, actd.pages), pages);
//                    StringAssert.Contains(cc.FetchInnerHTML(driver, actd.copies), copies);

//                    cc.ClickButton(driver, actd.backToActivity);
//                    Thread.Sleep(3000);
//                }
//                passFail = 0;
//            }
//            catch (Exception e)
//            {
//                passFail = 1;
//                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
//            }
//        }

//        [TestMethod, TestCategory("Activity - Regression")]
//        public void Activity_DateRange_ClickLast30Days()
//        {
//            if (qaDev.ToLower() == dev)
//            {
//                instance = 11117034;
//                testName = "Activity_DateRange_ClickLast30Days_Dev";
//            }
//            else
//            {
//                if (qaDev.ToLower() == qa)
//                {
//                    instance = 11507420;
//                    testName = "Activity_DateRange_ClickLast30Days_QA";
//                }
//                else
//                //{
//                //    if (qaDev.ToLower() == beta)
//                //    {
//                //        //instance = 4748406;
//                //        testName = "Activity_DateRange_ClickLast30Days_Beta";
//                //    }
//                //    else
//                    {
//                        //if (qaDev.ToLower() == prod)
//                        //{
//                        instance = 11263078;
//                        testName = "Activity_DateRange_ClickLast30Days_Prod";
//                        //}
//                    }
//                //}
//            }
//            passFail = 1;  //reset before test
//            filePath = string.Concat(filePath, testName, ".txt");

//            CreateLogFile();

//            try
//            {
//                act = mp.OpenActivity(driver);
//                Thread.Sleep(3000)
//;                //tglog.AppendLogText(filePath, driver.Title);

//                StringAssert.Contains(cc.FetchInnerHTML(driver, act.Header), "Activity");
//                tglog.AppendLogText(filePath, "Opened Activity Page ..... OK");

//                //test
//                //Date Change
//                cc.ClickButton(driver, act.calendarElement);
//                cc.ClickButton(driver, act.last30Days);
//                Thread.Sleep(3000);
//                cc.ClickButton(driver, act.calendarElement);

//                StringAssert.Contains(DateTime.Today.AddDays(-29).ToString("MM/dd/yyyy"), cc.FetchTextValue(driver, act.startDate));
//                StringAssert.Contains(DateTime.Today.ToString("MM/dd/yyyy"), cc.FetchTextValue(driver, act.endDate));
//                tglog.AppendLogText(filePath, string.Concat("Start date = ", cc.FetchTextValue(driver, act.startDate), " / End Date = ", cc.FetchTextValue(driver, act.endDate)));
//                passFail = 0;

//            }
//            catch (Exception e)
//            {
//                passFail = 1;
//                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
//            }
//        }

//        [TestMethod, TestCategory("Activity - Regression")]
//        public void Activity_DateRange_ClickLast7Days()
//        {
//            if (qaDev.ToLower() == dev)
//            {
//                instance = 11117033;
//                testName = "Activity_DateRange_ClickLast7Days_Dev";
//            }
//            else
//            {
//                if (qaDev.ToLower() == qa)
//                {
//                    instance = 11507419;
//                    testName = "Activity_DateRange_ClickLast7Days_QA";
//                }
//                else
//                //{
//                //    if (qaDev.ToLower() == beta)
//                //    {
//                //        //instance = 4748406;
//                //        testName = "Activity_DateRange_ClickLast7Days_Beta";
//                //    }
//                //    else
//                    {
//                        //if (qaDev.ToLower() == prod)
//                        //{
//                        instance = 11263077;
//                        testName = "Activity_DateRange_ClickLast7Days_Prod";
//                        //}
//                    }
//                //}
//            }
//            passFail = 1;  //reset before test
//            filePath = string.Concat(filePath, testName, ".txt");

//            CreateLogFile();

//            try
//            {
//                act = mp.OpenActivity(driver);
//                Thread.Sleep(3000)
//;                //tglog.AppendLogText(filePath, driver.Title);

//                StringAssert.Contains(cc.FetchInnerHTML(driver, act.Header), "Activity");
//                tglog.AppendLogText(filePath, "Opened Activity Page ..... OK");

//                //test
//                //Date Change
//                cc.ClickButton(driver, act.calendarElement);
//                cc.ClickButton(driver, act.last7Days);
//                Thread.Sleep(3000);
//                cc.ClickButton(driver, act.calendarElement);

//                StringAssert.Contains(DateTime.Today.AddDays(-6).ToString("MM/dd/yyyy"), cc.FetchTextValue(driver, act.startDate));
//                StringAssert.Contains(DateTime.Today.ToString("MM/dd/yyyy"), cc.FetchTextValue(driver, act.endDate));
//                tglog.AppendLogText(filePath, string.Concat("Start date = ", cc.FetchTextValue(driver, act.startDate), " / End Date = ", cc.FetchTextValue(driver, act.endDate)));
//                passFail = 0;

//            }
//            catch (Exception e)
//            {
//                passFail = 1;
//                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
//            }
//        }

//        [TestMethod, TestCategory("Activity - Regression")]
//        public void Activity_DateRange_ClickLastMonth()
//        {
//            string testEnd = ""; ;
//            string testStart = "";

//            if (qaDev.ToLower() == dev)
//            {
//                instance = 11117036;
//                testName = "Activity_DateRange_ClickLastMonth_Dev";
//            }
//            else
//            {
//                if (qaDev.ToLower() == qa)
//                {
//                    instance = 11507422;
//                    testName = "Activity_DateRange_ClickLastMonth_QA";
//                }
//                else
//                //{
//                //    if (qaDev.ToLower() == beta)
//                //    {
//                //        //instance = 4748406;
//                //        testName = "Activity_DateRange_ClickLastMonth_Beta";
//                //    }
//                //    else
//                    {
//                        instance = 11263080;
//                        testName = "Activity_DateRange_ClickLastMonth_Prod";
//                    }
//                //}
//            }
//            passFail = 1;  //reset before test
//            filePath = string.Concat(filePath, testName, ".txt");

//            CreateLogFile();

//            try
//            {
//                act = mp.OpenActivity(driver);
//                Thread.Sleep(3000)
//;                //tglog.AppendLogText(filePath, driver.Title);

//                StringAssert.Contains(cc.FetchInnerHTML(driver, act.Header), "Activity");
//                tglog.AppendLogText(filePath, "Opened Activity Page ..... OK");

//                //test
//                //Date Change
//                cc.ClickButton(driver, act.calendarElement);
//                cc.ClickButton(driver, act.lastMonth);
//                Thread.Sleep(3000);
//                cc.ClickButton(driver, act.calendarElement);

//                testStart = string.Concat(((DateTime.Today.Month)-1).ToString("00"), "/01/", DateTime.Today.Year.ToString());
//                testEnd = string.Concat(((DateTime.Today.Month)-1).ToString("00"), "/", cc.DaysInMonth(2018,(DateTime.Today.Month)-1), "/", DateTime.Today.Year.ToString());

//                StringAssert.Contains(testStart, cc.FetchTextValue(driver, act.startDate));
//                StringAssert.Contains(testEnd, cc.FetchTextValue(driver, act.endDate));
//                tglog.AppendLogText(filePath, string.Concat("Start date = ", cc.FetchTextValue(driver, act.startDate), " / End Date = ", cc.FetchTextValue(driver, act.endDate)));
//                passFail = 0;

//            }
//            catch (Exception e)
//            {
//                passFail = 1;
//                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
//            }
//        }

        [TestMethod, TestCategory("Under Review - Document Logs - Regression")]
        public void DocumentLogs_DateRange_ClickToday()
        {
            if (qaDev.ToLower() == dev)
            {
                instance = 11117031;
                testName = "DocumentLogs_DateRange_ClickToday_Dev";
            }
            else
            {
                if (qaDev.ToLower() == qa)
                {
                    instance = 11507417;
                    testName = "DocumentLogs_DateRange_ClickToday_QA";
                }
                else
                //{
                //    if (qaDev.ToLower() == beta)
                //    {
                //        //instance = 4748406;
                //        testName = "DocumentLogs_DateRange_ClickToday_Beta";
                //    }
                //    else
                {
                    //if (qaDev.ToLower() == prod)
                    //{
                    instance = 11263075;
                        testName = "DocumentLogs_DateRange_ClickToday_Prod";
                        //}
                    }
                //}
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                dl = mp.OpenDocumentLogs(driver);
                Thread.Sleep(3000)
;                //tglog.AppendLogText(filePath, driver.Title);

                StringAssert.Contains(cc.FetchInnerHTML(driver, dl.header), "Document Logs");
                tglog.AppendLogText(filePath, "Opened Document Logs Page ..... OK");

                //test
                //Date Change
                cc.ClickButton(driver, dl.calendarElement);
                cc.ClickButton(driver, dl.toDay);
                Thread.Sleep(3000);
                cc.ClickButton(driver, dl.calendarElement);

                StringAssert.Contains(DateTime.Today .ToString("MM/dd/yyyy"), cc.FetchTextValue(driver, dl.startDate));
                StringAssert.Contains(DateTime.Today.ToString("MM/dd/yyyy"), cc.FetchTextValue(driver, dl.endDate));
                tglog.AppendLogText(filePath, string.Concat("Start date = ", cc.FetchTextValue(driver,dl.startDate), " / End Date = ", cc.FetchTextValue(driver,dl.endDate)));
                passFail = 0;

            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        //        [TestMethod, TestCategory("Activity - Regression")]
        //        public void Activity_DateRange_ClickThisMonth()
        //        {
        //            string testEnd = ""; ;
        //            string testStart = "";

        //            if (qaDev.ToLower() == dev)
        //            {
        //                instance = 11117035;
        //                testName = "Activity_DateRange_ClickThisMonth_Dev";
        //            }
        //            else
        //            {
        //                if (qaDev.ToLower() == qa)
        //                {
        //                    instance = 11507421;
        //                    testName = "Activity_DateRange_ClickThisMonth_QA";
        //                }
        //                else
        //                //{
        //                //    if (qaDev.ToLower() == beta)
        //                //    {
        //                //        //instance = 4748406;
        //                //        testName = "Activity_DateRange_ClickThisMonth_Beta";
        //                //    }
        //                //    else
        //                    {
        //                        instance = 11263079;
        //                        testName = "Activity_DateRange_ClickThisMonth_Prod";
        //                    }
        //                //}
        //            }
        //            passFail = 1;  //reset before test
        //            filePath = string.Concat(filePath, testName, ".txt");

        //            CreateLogFile();

        //            try
        //            {
        //                act = mp.OpenActivity(driver);
        //                Thread.Sleep(3000)
        //;                //tglog.AppendLogText(filePath, driver.Title);

        //                StringAssert.Contains(cc.FetchInnerHTML(driver, act.Header), "Activity");
        //                tglog.AppendLogText(filePath, "Opened Activity Page ..... OK");

        //                //test
        //                //Date Change
        //                cc.ClickButton(driver, act.calendarElement);
        //                cc.ClickButton(driver, act.thisMonth);
        //                Thread.Sleep(3000);
        //                cc.ClickButton(driver, act.calendarElement);

        //                testStart = string.Concat(DateTime.Today.Month.ToString("00"), "/01/", DateTime.Today.Year.ToString());
        //                testEnd = string.Concat(DateTime.Today.Month.ToString("00"), "/",cc.DaysInMonth(2018, DateTime.Today.Month),"/", DateTime.Today.Year.ToString());

        //                StringAssert.Contains(testStart, cc.FetchTextValue(driver, act.startDate));
        //                StringAssert.Contains(testEnd, cc.FetchTextValue(driver, act.endDate));
        //                tglog.AppendLogText(filePath, string.Concat("Start date = ", cc.FetchTextValue(driver, act.startDate), " / End Date = ", cc.FetchTextValue(driver, act.endDate)));
        //                passFail = 0;

        //            }
        //            catch (Exception e)
        //            {
        //                passFail = 1;
        //                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
        //            }
        //        }

        [TestMethod, TestCategory("Under Review - Document Logs - Regression")]
        public void DocumentLogs_DateRange_ClickYesterday()
        {
            if (qaDev.ToLower() == dev)
            {
                instance = 11117032;
                testName = "DocumentLogs_DateRange_ClickYesterday_Dev";
            }
            else
            {
                if (qaDev.ToLower() == qa)
                {
                    instance = 11507418;
                    testName = "DocumentLogs_DateRange_ClickYesterday_QA";
                }
                else
                //{
                //    if (qaDev.ToLower() == beta)
                //    {
                //        //instance = 4748406;
                //        testName = "DocumentLogs_DateRange_ClickYesterday_Beta";
                //    }
                //    else
                {
                    //if (qaDev.ToLower() == prod)
                    //{
                    instance = 11263076;
                    testName = "DocumentLogs_DateRange_ClickYesterday_Prod";
                    //}
                }
                //}
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                dl = mp.OpenDocumentLogs(driver);
                Thread.Sleep(3000)
;                //tglog.AppendLogText(filePath, driver.Title);

                StringAssert.Contains(cc.FetchInnerHTML(driver, dl.header), "Document Logs");
                tglog.AppendLogText(filePath, "Opened Document Logs Page ..... OK");

                //test
                //Date Change
                cc.ClickButton(driver, dl.calendarElement);
                cc.ClickButton(driver, dl.yesterDay);
                Thread.Sleep(3000);
                cc.ClickButton(driver, dl.calendarElement);

                StringAssert.Contains(DateTime.Today.AddDays(-1).ToString("MM/dd/yyyy"), cc.FetchTextValue(driver, dl.startDate));
                StringAssert.Contains(DateTime.Today.AddDays(-1).ToString("MM/dd/yyyy"), cc.FetchTextValue(driver, dl.endDate));
                tglog.AppendLogText(filePath, string.Concat("Start date = ", cc.FetchTextValue(driver, dl.startDate), " / End Date = ", cc.FetchTextValue(driver, dl.endDate)));
                passFail = 0;

            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        //        [TestMethod, TestCategory("Activity - Regression")]
        //        public void Activity_Activitydetail_DownloadDocument()
        //        {
        //            string cellOne = "";
        //            string downloadFile = "C:\\Users\\Thora\\Downloads\\";
        //            string endDate = "";
        //            int rows = 0;
        //            string serialNumber = "";
        //            string startDate = "";

        //            if (qaDev.ToLower() == dev)
        //            {
        //                instance = 11117042;
        //                testName = "Activity_Activitydetail_DownloadDocument_Dev";
        //                startDate = "08/24/2018";
        //                endDate = "08/30/2018";
        //            }
        //            else
        //            {
        //                if (qaDev.ToLower() == qa)
        //                {
        //                    instance = 11507428;
        //                    testName = "Activity_Activitydetail_DownloadDocument_QA";
        //                    startDate = "08/24/2018";
        //                    endDate = "08/30/2018";
        //                }
        //                else
        //                //{
        //                //    if (qaDev.ToLower() == beta)
        //                //    {
        //                //        //instance = 4748406;
        //                //        testName = "Activity_Activitydetail_DownloadDocument_Beta";
        //                //    }
        //                //    else
        //                    {
        //                        instance = 11263086;
        //                        testName = "Activity_Activitydetail_DownloadDocument_Prod";
        //                        startDate = "07/16/2018";
        //                        endDate = "07/16/2018";
        //                    }
        //                //}
        //            }
        //            passFail = 1;  //reset before test
        //            filePath = string.Concat(filePath, testName, ".txt");

        //            CreateLogFile();

        //            try
        //            {
        //                act = mp.OpenActivity(driver);
        //                Thread.Sleep(3000)
        //;                //tglog.AppendLogText(filePath, driver.Title);

        //                StringAssert.Contains(cc.FetchInnerHTML(driver, act.Header), "Activity");
        //                tglog.AppendLogText(filePath, "Opened Activity Page ..... OK");

        //                //Date Change
        //                cc.ClickButton(driver, act.calendarElement);
        //                cc.EnterText(driver,startDate, act.startDate);
        //                cc.EnterText(driver,endDate, act.endDate);

        //                tglog.AppendLogText(filePath, string.Concat("Start date = ", cc.FetchTextValue(driver, act.startDate), " / End Date = ", cc.FetchTextValue(driver, act.endDate)));
        //                Thread.Sleep(3000);
        //                cc.ClickButton(driver, act.calendarElement);
        //                Thread.Sleep(3000);

        //                rows = Convert.ToInt16(cc.FetchTableRows(driver, act.printLogTable));
        //                tglog.AppendLogText(filePath, string.Concat("Row Count is ", rows.ToString(), Environment.NewLine));

        //                cellOne = cc.FetchCellTextValue(driver, "1", "1", act.printLogTable);
        //                cellOne = cellOne.Replace("\r\n", "");
        //                serialNumber = cellOne.Substring(0, cellOne.IndexOf(" "));
        //                downloadFile = string.Concat(downloadFile, serialNumber, ".pdf");

        //                actd = act.OpenActivityDetail(driver);
        //                Thread.Sleep(3000);

        //                //file exists? Remove it
        //                if (File.Exists(downloadFile))
        //                {
        //                    File.Delete(downloadFile);
        //                    Thread.Sleep(3000);
        //                    tglog.AppendLogText(filePath, string.Concat("File, ", downloadFile, " was deleted", Environment.NewLine));
        //                }

        //                // test
        //                cc.ClickButton(driver, actd.downloadDocument);
        //                Thread.Sleep(4000);

        //                if (File.Exists(downloadFile))
        //                {
        //                    tglog.AppendLogText(filePath, string.Concat("File, ", downloadFile, " was created"));
        //                    passFail = 0;
        //                }
        //                else
        //                {
        //                    tglog.AppendLogText(filePath, string.Concat("File, ", downloadFile, " was NOT created"));
        //                    passFail = 1;
        //                }

        //                cc.ClickButton(driver, actd.backToActivity);
        //                Thread.Sleep(3000);
        //            }
        //            catch (Exception e)
        //            {
        //                passFail = 1;
        //                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
        //            }
        //        }

        //        [TestMethod, TestCategory("Activity - Regression")]
        //        public void Activity_Export_ClickExportButton()
        //        {
        //            string downloadFile = "C:\\Users\\Thora\\Downloads\\printlogs.csv";
        //            string endDate = "";
        //            string pageNumb = "";
        //            string recCount = "";
        //            int rows = 0;
        //            string startDate = "";
        //            string totPages = "";

        //            if (qaDev.ToLower() == dev)
        //            {
        //                instance = 11117040;
        //                testName = "Activity_Export_ClickExportButton_Dev";
        //                startDate = "07/26/2018";
        //                endDate = "07/26/2018";
        //            }
        //            else
        //            {
        //                if (qaDev.ToLower() == qa)
        //                {
        //                    instance = 11507426;
        //                    testName = "Activity_Export_ClickExportButton_QA";
        //                    startDate = "07/26/2018";
        //                    endDate = "07/26/2018";
        //                }
        //                else
        //                //{
        //                //    if (qaDev.ToLower() == beta)
        //                //    {
        //                //        //instance = 4748408;
        //                //        testName = "Activity_Export_ClickExportButton_Beta";
        //                //    }
        //                    //else
        //                    {
        //                        instance = 11263084;
        //                        testName = "Activity_Export_ClickExportButton_Prod";
        //                        startDate = "06/21/2018";
        //                        endDate = "06/27/2018";
        //                    }
        //                //}
        //            }
        //            passFail = 1;  //reset before test
        //            filePath = string.Concat(filePath, testName, ".txt");

        //            CreateLogFile();
        //            //tglog.AppendLogText(filePath, driver.Title);

        //            try
        //            {
        //                //setup
        //                act = mp.OpenActivity(driver);
        //                baseWindowHandle = driver.CurrentWindowHandle;

        //                cc.ClickButton(driver, act.calendarElement);
        //                cc.ClickButton(driver, act.customRange);
        //                cc.EnterText(driver, startDate, act.startDate);
        //                cc.EnterText(driver, endDate, act.endDate);
        //                cc.ClickButton(driver, act.applyButton);


        //                pageNumb = cc.FetchInnerHTML(driver, act.pageNumbers);
        //                tglog.AppendLogText(filePath, pageNumb);
        //                rows = Convert.ToInt16(cc.FetchTableRows(driver, act.printLogTable));
        //                totPages = pageNumb.Substring(pageNumb.IndexOf(" of ") + 4, pageNumb.IndexOf(" (") - (pageNumb.IndexOf(" of ") + 4));
        //                recCount = pageNumb.Substring(pageNumb.IndexOf("(") + 1, pageNumb.IndexOf(" records)") - (pageNumb.IndexOf("(") + 1));

        //                tglog.AppendLogText(filePath, string.Concat("Record Count is ", recCount, Environment.NewLine, Environment.NewLine));

        //                //file exists?
        //                if (File.Exists(downloadFile))
        //                {
        //                    File.Delete(downloadFile);
        //                    Thread.Sleep(3000);
        //                }

        //                cc.ClickButton(driver, act.exportBtn);
        //                Thread.Sleep(10000);
        //                passFail = 2;

        //                tglog.AppendLogText(filePath,"IMPORTANT!!! This test is not complete. Please verify the correct number of records are contained in the exported file.");
        //            }
        //            catch (Exception e)
        //            {
        //                passFail = 1;
        //                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
        //            }
        //        }

        //        [TestMethod, TestCategory("Activity - Regression")]
        //        public void Activity_Paging_PageThroughActivityList()
        //        {
        //            string endDate = "";
        //            string page = "";
        //            string pageNumb = "";
        //            int rows = 0;
        //            string startDate = "";
        //            string totPages = "";

        //            if (qaDev.ToLower() == dev)
        //            {
        //                instance = 11117135;
        //                testName = "Activity_Paging_PageThroughActivityList_Dev";
        //                startDate = "10/01/2017";
        //                endDate = "10/31/2017";
        //            }
        //            else
        //            {
        //                if (qaDev.ToLower() == qa)
        //                {
        //                    instance = 11507429;
        //                    testName = "Activity_Paging_PageThroughActivityList_QA";
        //                    startDate = "10/01/2017";
        //                    endDate = "10/31/2017";
        //                }
        //                else
        //                //{
        //                //    if (qaDev.ToLower() == beta)
        //                //    {
        //                //        //instance = 4748408;
        //                //        testName = "Activity_Paging_PageThroughActivityList_Beta";
        //                //    }
        //                //    else
        //                    {
        //                        instance = 11263087;
        //                        testName = "Activity_Paging_PageThroughActivityList_Prod";
        //                        startDate = "06/25/2018";
        //                        endDate = "06/27/2018";
        //                    }
        //                //}
        //            }
        //            passFail = 1;  //reset before test
        //            filePath = string.Concat(filePath, testName, ".txt");

        //            CreateLogFile();
        //            //tglog.AppendLogText(filePath, driver.Title);

        //            try
        //            {
        //                //setup
        //                act = mp.OpenActivity(driver);
        //                baseWindowHandle = driver.CurrentWindowHandle;

        //                cc.ClickButton(driver, act.calendarElement);
        //                cc.ClickButton(driver, act.customRange);
        //                cc.EnterText(driver, startDate, act.startDate);
        //                cc.EnterText(driver, endDate, act.endDate);
        //                cc.ClickButton(driver, act.applyButton);

        //                //test
        //                pageNumb = cc.FetchInnerHTML(driver, act.pageNumbers);
        //                tglog.AppendLogText(filePath, pageNumb);
        //                rows = Convert.ToInt16(cc.FetchTableRows(driver, act.printLogTable));
        //                totPages = pageNumb.Substring(pageNumb.IndexOf(" of ") + 4, pageNumb.IndexOf(" (") - (pageNumb.IndexOf(" of ") + 4));

        //                for (int tot = 1; tot <= Convert.ToInt16(totPages); tot++)
        //                {
        //                    //paging forward test
        //                    page = pageNumb.Substring(pageNumb.IndexOf("page ") +5, pageNumb.IndexOf(" of ") - (pageNumb.IndexOf("page ") + 5));
        //                    StringAssert.Contains(page, tot.ToString());
        //                    tglog.AppendLogText(filePath, string.Concat("Page ", page, "..... OK"));

        //                    if (tot+1 > Convert.ToInt16(totPages))
        //                    {
        //                        break;
        //                    }

        //                    cc.ClickButton(driver, act.nextBtn);
        //                    Thread.Sleep(3000);
        //                    pageNumb = cc.FetchInnerHTML(driver, act.pageNumbers);
        //                }

        //                // last page test
        //                StringAssert.Contains(pageNumb, totPages.ToString());
        //                passFail = 0;
        //                tglog.AppendLogText(filePath, string.Concat("Paged through all ", totPages, ".....OK", Environment.NewLine));

        //                //paging backward test
        //                for (int tot = Convert.ToInt16(totPages); tot >= 1; tot--)
        //                {
        //                    page = pageNumb.Substring(pageNumb.IndexOf("page ") + 5, pageNumb.IndexOf(" of ") - (pageNumb.IndexOf("page ") + 5));
        //                    StringAssert.Contains(page, tot.ToString());
        //                    tglog.AppendLogText(filePath, string.Concat("Page ", page, "..... OK"));

        //                    if (tot - 1 < 1)
        //                    {
        //                        break;
        //                    }

        //                    cc.ClickButton(driver, act.prevBtn);
        //                    Thread.Sleep(3000);
        //                    pageNumb = cc.FetchInnerHTML(driver, act.pageNumbers);
        //                }

        //                // First page test
        //                StringAssert.Contains(pageNumb, totPages.ToString());
        //                passFail = 0;
        //                tglog.AppendLogText(filePath, string.Concat("Paged through all ", totPages, ".....OK", Environment.NewLine));

        //            }
        //            catch (Exception e)
        //            {
        //                passFail = 1;
        //                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
        //            }
        //        }

        //        [TestMethod, TestCategory("Activity - Regression")]
        //        public void Activity_Search_SearchByPartialSerialNumber()
        //        {
        //            string pageNumb = "";
        //            string recCount = "";
        //            int rows = 0;
        //            string serialNumber = "";
        //            string totPages = "";

        //            if (qaDev.ToLower() == dev)
        //            {
        //                instance = 11117039;
        //                testName = "Activity_Search_SearchByPartialSerialNumber_Dev";
        //                serialNumber = "652";
        //            }
        //            else
        //            {
        //                if (qaDev.ToLower() == qa)
        //                {
        //                    instance = 11507425;
        //                    testName = "Activity_Search_SearchByPartialSerialNumber_QA";
        //                    serialNumber = "652";
        //                }
        //                else
        //                //{
        //                //    if (qaDev.ToLower() == beta)
        //                //    {
        //                //        //instance = 4748406;
        //                //        testName = "Activity_Search_SearchByPartialSerialNumber_Beta";
        //                //    }
        //                //    else
        //                    {
        //                        instance = 11263083;
        //                        testName = "Activity_Search_SearchByPartialSerialNumber_Prod";
        //                        serialNumber = "9WQH";
        //                    }
        //                //}
        //            }
        //            passFail = 1;  //reset before test
        //            filePath = string.Concat(filePath, testName, ".txt");

        //            CreateLogFile();

        //            try
        //            {
        //                act = mp.OpenActivity(driver);
        //                Thread.Sleep(3000)
        //;                //tglog.AppendLogText(filePath, driver.Title);

        //                StringAssert.Contains(cc.FetchInnerHTML(driver, act.Header), "Activity");
        //                passFail = 0;
        //                tglog.AppendLogText(filePath, "Opened Activity Page ..... OK");

        //                //Date Change
        //                cc.ClickButton(driver, act.calendarElement);
        //                cc.ClickButton(driver, act.customRange);
        //                cc.EnterText(driver, "06/01/2017", act.startDate);
        //                cc.EnterText(driver, "06/30/2018", act.endDate);
        //                cc.ClickButton(driver, act.applyButton);
        //                Thread.Sleep(3000);

        //                pageNumb = cc.FetchInnerHTML(driver, act.pageNumbers);
        //                recCount = pageNumb.Substring(pageNumb.IndexOf("(") + 1, pageNumb.IndexOf(" records)") - (pageNumb.IndexOf("(") + 1));
        //                TestContext.WriteLine(totPages);

        //                //test
        //                cc.EnterText(driver, serialNumber, act.searchText);
        //                cc.ClickButton(driver, act.searchBtn);
        //                Thread.Sleep(3000);

        //                //verify
        //                pageNumb = cc.FetchInnerHTML(driver, act.pageNumbers);
        //                StringAssert.Contains("True", (Convert.ToInt16(recCount) >= Convert.ToInt16(pageNumb.Substring(pageNumb.IndexOf("(") + 1, pageNumb.IndexOf(" records)") - (pageNumb.IndexOf("(") + 1)))).ToString());
        //                tglog.AppendLogText(filePath, string.Concat("Original Record Count = ", recCount, " / Search Record Count = ", pageNumb.Substring(pageNumb.IndexOf("(") + 1, pageNumb.IndexOf(" records)") - (pageNumb.IndexOf("(") + 1))));

        //                rows = Convert.ToInt16(cc.FetchTableRows(driver, act.printLogTable));
        //                totPages = pageNumb.Substring(pageNumb.IndexOf(" of ") + 4, pageNumb.IndexOf(" (") - (pageNumb.IndexOf(" of ") + 4));

        //                for (int tot = 1; tot <= Convert.ToInt16(totPages); tot++)
        //                {
        //                    for (int x = 1; x <= rows; x++)
        //                    {
        //                        StringAssert.Contains(cc.FetchCellValue(driver, "1", x.ToString(), act.printLogTable),serialNumber);
        //                    }
        //                    tglog.AppendLogText(filePath, string.Concat("Page ", tot.ToString(), "..... OK!"));
        //                }
        //            }
        //            catch (Exception e)
        //            {
        //                passFail = 1;
        //                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
        //            }
        //        }

        //        [TestMethod, TestCategory("Activity - Regression")]
        //        public void Activity_Search_SearchBySerialNumber()
        //        {
        //            string pageNumb = "";
        //            string recCount = "";
        //            string serialNumber = "";
        //            string totPages = "";

        //            if (qaDev.ToLower() == dev)
        //            {
        //                instance = 11117038;
        //                testName = "Activity_Search_SearchBySerialNumber_Dev";
        //                serialNumber = "652mfea2a";
        //            }
        //            else
        //            {
        //                if (qaDev.ToLower() == qa)
        //                {
        //                    instance = 11507424;
        //                    testName = "Activity_Search_SearchBySerialNumber_QA";
        //                    serialNumber = "652mfea2a";
        //                }
        //                else
        //                //{
        //                //    if (qaDev.ToLower() == beta)
        //                //    {
        //                //        //instance = 4748406;
        //                //        testName = "Activity_Search_SearchBySerialNumber_Beta";
        //                //    }
        //                //    else
        //                    {
        //                        instance = 11263082;
        //                        testName = "Activity_Search_SearchBySerialNumber_Prod";
        //                        serialNumber = "VEVPD2MJD";
        //                    }
        //                //}
        //            }
        //            passFail = 1;  //reset before test
        //            filePath = string.Concat(filePath, testName, ".txt");

        //            CreateLogFile();

        //            try
        //            {
        //                act = mp.OpenActivity(driver);
        //                Thread.Sleep(3000)
        //;                //tglog.AppendLogText(filePath, driver.Title);

        //                StringAssert.Contains(cc.FetchInnerHTML(driver, act.Header), "Activity");
        //                passFail = 0;
        //                tglog.AppendLogText(filePath, "Opened Activity Page ..... OK");

        //                //Date Change
        //                cc.ClickButton(driver, act.calendarElement);
        //                cc.ClickButton(driver, act.customRange);
        //                cc.EnterText(driver, "06/01/2017", act.startDate);
        //                cc.EnterText(driver, "06/30/2018", act.endDate);
        //                cc.ClickButton(driver, act.applyButton);
        //                Thread.Sleep(3000);

        //                pageNumb = cc.FetchInnerHTML(driver, act.pageNumbers);
        //                recCount = pageNumb.Substring(pageNumb.IndexOf("(") + 1, pageNumb.IndexOf(" records)") - (pageNumb.IndexOf("(") + 1));
        //                TestContext.WriteLine(totPages);

        //                //test
        //                cc.EnterText(driver, serialNumber, act.searchText);
        //                cc.ClickButton(driver, act.searchBtn);
        //                Thread.Sleep(3000);

        //                //verify
        //                pageNumb = cc.FetchInnerHTML(driver, act.pageNumbers);
        //                StringAssert.Contains("True", (Convert.ToInt16(pageNumb.Substring(pageNumb.IndexOf("(") + 1, pageNumb.IndexOf(" records)") - (pageNumb.IndexOf("(") + 1))) == 1).ToString());
        //                tglog.AppendLogText(filePath, string.Concat("Original Record Count = ", recCount, " / Search Record Count = ", pageNumb.Substring(pageNumb.IndexOf("(") + 1, pageNumb.IndexOf(" records)") - (pageNumb.IndexOf("(") + 1))));
        //            }
        //            catch (Exception e)
        //            {
        //                passFail = 1;
        //                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
        //            }
        //        }

        //        [TestMethod, TestCategory("Activity - Regression")]
        //        public void Activity_Search_SearchByUser()
        //        {
        //            string pageNumb = "";
        //            string recCount = "";
        //            int rows = 0;
        //            string totPages = "";

        //            if (qaDev.ToLower() == dev)
        //            {
        //                instance = 11117037;
        //                testName = "Activity_Search_SearchByUser_Dev";
        //            }
        //            else
        //            {
        //                if (qaDev.ToLower() == qa)
        //                {
        //                    instance = 11507423;
        //                    testName = "Activity_Search_SearchByUser_QA";
        //                }
        //                else
        //                //{
        //                //    if (qaDev.ToLower() == beta)
        //                //    {
        //                //        //instance = 4748406;
        //                //        testName = "Activity_Search_SearchByUser_Beta";
        //                //    }
        //                //    else
        //                    {
        //                        instance = 11263081;
        //                        testName = "Activity_Search_SearchByUser_Prod";
        //                    }
        //                //}
        //            }
        //            passFail = 1;  //reset before test
        //            filePath = string.Concat(filePath, testName, ".txt");

        //            CreateLogFile();

        //            try
        //            {
        //                act = mp.OpenActivity(driver);
        //                Thread.Sleep(3000)
        //;                //tglog.AppendLogText(filePath, driver.Title);

        //                StringAssert.Contains(cc.FetchInnerHTML(driver, act.Header), "Activity");
        //                passFail = 0;
        //                tglog.AppendLogText(filePath, "Opened Activity Page ..... OK");

        //                //Date Change
        //                cc.ClickButton(driver, act.calendarElement);
        //                cc.ClickButton(driver, act.customRange);
        //                cc.EnterText(driver, "06/01/2017", act.startDate);
        //                cc.EnterText(driver, "06/30/2018", act.endDate);
        //                cc.ClickButton(driver, act.applyButton);
        //                Thread.Sleep(3000);

        //                pageNumb = cc.FetchInnerHTML(driver, act.pageNumbers);
        //                recCount = pageNumb.Substring(pageNumb.IndexOf("(") + 1, pageNumb.IndexOf(" records)") - (pageNumb.IndexOf("(") + 1));
        //                TestContext.WriteLine(totPages);

        //                //test
        //                cc.EnterText(driver, "User", act.searchText);
        //                cc.ClickButton(driver, act.searchBtn);
        //                Thread.Sleep(3000);

        //                //verify
        //                pageNumb = cc.FetchInnerHTML(driver, act.pageNumbers);
        //                StringAssert.Contains("True", (Convert.ToInt16(recCount) >= Convert.ToInt16(pageNumb.Substring(pageNumb.IndexOf("(") + 1, pageNumb.IndexOf(" records)") - (pageNumb.IndexOf("(") + 1)))).ToString());
        //                tglog.AppendLogText(filePath, string.Concat("Original Record Count = ", recCount, " / Search Record Count = ", pageNumb.Substring(pageNumb.IndexOf("(") + 1, pageNumb.IndexOf(" records)") - (pageNumb.IndexOf("(") + 1))));

        //                rows = Convert.ToInt16(cc.FetchTableRows(driver, act.printLogTable));
        //                totPages = pageNumb.Substring(pageNumb.IndexOf(" of ") + 4, pageNumb.IndexOf(" (") - (pageNumb.IndexOf(" of ") + 4));

        //                for (int tot = 1; tot <= Convert.ToInt16(totPages); tot++)
        //                {
        //                    for (int x = 1; x <= rows; x++)
        //                    {
        //                        StringAssert.Contains(cc.FetchCellValue(driver, "3", x.ToString(), act.printLogTable), "User");
        //                    }
        //                    tglog.AppendLogText(filePath, string.Concat("Page ", tot.ToString(), "..... OK!"));
        //                }
        //            }
        //            catch (Exception e)
        //            {
        //                passFail = 1;
        //                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
        //            }
        //        }

        #region Methods
        private void SetupDriver()
        {
            if ((brow.ToLower() == "ie"))
            {
                this.driver = new InternetExplorerDriver("C:\\Selenium");
            }

            if ((brow.ToLower() == "chrome"))
            {
                this.driver = new ChromeDriver(@"C:\Selenium");
            }

            //if ((brow.ToLower() == "firefox"))
            //{
            //    this.driver = new FirefoxDriver();
            //}

        }

        private void CreateLogFile()
        {
            tglog.CreateLogFile(filePath, string.Concat(Environment.NewLine, "----------- Start testing using ", brow, " -----------", Environment.NewLine));
            tglog.AppendLogText(filePath, string.Concat("Set up ", brow, " ..... OK"));
            tglog.AppendLogText(filePath, string.Concat("URL = ", baseURL));
            tglog.AppendLogText(filePath, string.Concat("Envionment = ", qaDev));
            tglog.AppendLogText(filePath, string.Concat("User = ", user));
            tglog.AppendLogText(filePath, "Opened SecureDocs in browser ..... OK");
            tglog.AppendLogText(filePath, string.Concat("Build Version: ", build));
            tglog.AppendLogText(filePath, "Logged into SecureDocs ..... OK");
            tglog.AppendLogText(filePath, string.Concat("Instance = ", instance));
            tglog.AppendLogText(filePath, string.Concat("FilePath = ", filePath));
            tglog.AppendLogText(filePath, "Opened SecureDocs Dashboard ..... OK");
        }

        //private void OpenPrintAgents()
        //{
        //    try
        //    {
        //        spa = mp.OpenPrintAgent(driver);
        //        //tglog.AppendLogText(filePath, driver.Title);

        //        StringAssert.Contains(cc.FetchInnerHTML(driver, spa.Header), "Print Agents");
        //        passFail = 0;
        //        tglog.AppendLogText(filePath, "Opened Secure Print Agents ..... OK");

        //    }
        //    catch (Exception e)
        //    {
        //        passFail = 1;
        //        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
        //    }
        //}

        //private void OpenSecurityTemplates()
        //{
        //    try
        //    {
        //        st = mp.OpenSecurityTemplates(driver);
        //        //tglog.AppendLogText(filePath, driver.Title);

        //        StringAssert.Contains(cc.FetchInnerHTML(driver, st.Header), "Security Templates");
        //        //passFail = 0;
        //        tglog.AppendLogText(filePath, "Opened Security Templates ..... OK");

        //    }
        //    catch (Exception e)
        //    {
        //        passFail = 1;
        //        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
        //    }
        //}

        private void SendToPractitest()
        {
            if (qaDev.ToUpper() == "DEV")
            {
                ptc.PT_Post_Specific_Run(filePath, passFail, resultMsg, instance, project, brow, "Development");
            }
            else
            {
                if (qaDev.ToUpper() == "QA")
                {
                    ptc.PT_Post_Specific_Run(filePath,passFail, resultMsg, instance, project, brow, "QA");
                }
                else
                {
                    if (qaDev.ToUpper() == "BETA")
                    {
                        ptc.PT_Post_Specific_Run(filePath, passFail, resultMsg, instance, project, brow, "Beta");
                    }
                    else
                    {
                        ptc.PT_Post_Specific_Run(filePath, passFail, resultMsg, instance, project, brow, "Production");
                    }
                }
            }
        }

        private void SetUpEnvironVars()
        {
            if (qaDev.ToLower() == dev)
            {
                filePath = string.Concat(filePath, "Dev\\");
            }
            else
            {
                if (qaDev.ToLower() == qa)
                {
                    filePath = string.Concat(filePath, "QA\\");
                }
                else
                {
                    if (qaDev.ToLower() == beta)
                    {
                        filePath = string.Concat(filePath, "Beta\\");
                    }
                    else
                    {
                        filePath = string.Concat(filePath, "Prod\\");
                    }
                }
            }
        }
        #endregion

        //--------------------------------------------------------------------------------------------
        #region Additional test attributes
        [TestInitialize()]
        public void MyTestInitialize()
        {
            cc = new Common.Common_Code(driver);

            brow = ConfigurationManager.AppSettings["browser"];
            baseURL = ConfigurationManager.AppSettings["DocrityURL"];
            project = ConfigurationManager.AppSettings["project"];
            qaDev = ConfigurationManager.AppSettings["testEnv"];
            user = ConfigurationManager.AppSettings["testUser"].ToUpper();
            string pswd = ConfigurationManager.AppSettings["TestPswd"];

            SetupDriver();
            SetUpEnvironVars();

            lp = new SecureRx.Page_Elements.LoginPage(driver, baseURL);
            build = lp.FetchBuildVersion(driver);
            mp = lp.SDLogin(user, pswd, driver);

            //Did Home Menu open
            if (mp == null)
            {
                throw new NoSuchWindowException("Docrity home page was not displayed!");
            }
            else
            {
                passFail = 0;
            }
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {

            driver.SwitchTo().Window(baseWindowHandle);
            Thread.Sleep(1000);

            try
            {
                mp.LogoutandCloseBrowser(driver);
                tglog.AppendLogText(filePath, "Logged out and Closed browser..... OK");
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }

            if (passFail != 0)
            {
                resultMsg = string.Concat(testName, " ..... FAILED!!!!! Please refer to log file");
                //driver.Close();
            }
            else
            {
                resultMsg = string.Concat(testName, " ..... PASSED!!!!!");
            }

            SendToPractitest();

            tglog.AppendLogText(filePath, "Close Docrity");
            tglog.AppendLogText(filePath, "\n Test Finished \n");
        }

        #endregion

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;
    }
}