﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Data;
using System.IO;
using System.Net;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;

namespace SecureRx.Tests
{
    [TestClass]
    public class Signature_Tests
    {
        #region libraries
        SecureRx.Page_Elements.MainPage mp = null;
        SecureRx.Page_Elements.LoginPage lp = null;

        SecureRx.Page_Elements.Templates.Name_Template nt = null;
        SecureRx.Page_Elements.Templates.SecurityTemplates st = null;
        SecureRx.Page_Elements.Templates.Create_Security_Templates cst = null;
        SecureRx.Page_Elements.Templates.Remove_Security_Template rst = null;
        SecureRx.Page_Elements.Templates.Signature sst = null;
        SecureRx.Page_Elements.Templates.Template tst = null;

        SecureRx.Common.Common_Code cc = null;
        SecureRx.Common.PT_Client ptc = new SecureRx.Common.PT_Client();
        SecureRx.Common.TGLogging tglog = new SecureRx.Common.TGLogging();
        #endregion

        //Vars
        private string baseURL = "";
        private string baseWindowHandle = "";
        private string brow = "";
        private string build = "";
        private string dev = "dev";
        private IWebDriver driver = null;
        private string filePath = "C:\\Test Logs\\SecureRx\\";
        private string imagePath = "";
        public string imagePath2 = "";
        public string imagePath1 = "";
        private string imageFilesPath = @"C:\SecureRx\Images\";
        private int instance = 0;
        private int passFail = 1;
        private string project = "4896";
        private string qaDev = "";
        private string resultMsg = "";
        private string staging = "stage";
        private string testName = "";
        private string user = "";

        #region Image Watermark Tests
        [TestMethod, TestCategory("R2 - Designer")]
        public void Initial_Position_MoveInitial()
        {
            string templateName = "";
            string testValu = "";
            string testValu2 = "";
            string imagePath2 = "";
            string imagePath1 = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 10614850;
                testName = "Initial_Position_MoveInitial_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14691107;
                    testName = "Initial_Position_MoveInitial_Stage";
                }
                else
                {
                    //instance = 13109711;
                    testName = "Initial_Position_MoveInitial_Prod";
                }
            }
            passFail = 1;  //reset before test
            testValu = "561";
            testValu2 = "486";
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Initial-DO NOT USE";
            CreateLogFile();

            //setup
            CreateTemplateUnderTest(templateName);

            Thread.Sleep(2000);
            tglog.AppendLogText(filePath, "Opened a Security Template ..... OK");

            //Test
            try
            {
                sst = tst.OpenInitial(driver);
                tglog.AppendLogText(filePath, "Open Initial Modal.....OK");
                Thread.Sleep(500);

                sst.EnterSignature(driver, "jaa");
                tglog.AppendLogText(filePath, "Entered New Initials...OK");
                Thread.Sleep(500);

                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Initials X and Y ", ScreenshotImageFormat.Png, 0);
                tglog.AppendLogText(filePath, string.Concat("X = ", cc.FetchIDefaultValue(driver, tst.troyMark_X), " / Y = ", cc.FetchIDefaultValue(driver, tst.troyMark_Y)));
                Thread.Sleep(500);

                cc.EnterText(driver, testValu2, tst.troyMark_X);
                tglog.AppendLogText(filePath, string.Concat("Initials X Coordinate = ", testValu2));

                cc.EnterText(driver, testValu, tst.troyMark_Y);
                tglog.AppendLogText(filePath, string.Concat("Initials Y Coordinate = ", testValu));

                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Initials X and Y ", ScreenshotImageFormat.Png, 1);
                Thread.Sleep(1000);

                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());

                //Finish and Teardown
                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template Changes.....OK");
                Thread.Sleep(1000);

                tst.ExitDesigner(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(500);
                passFail = 0;
            }
            catch (Exception e)
            {
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Finish and Teardown
                tst.ExitDesigner2(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(500);
            }

            Thread.Sleep(500);
        }

        [TestMethod, TestCategory("R2 - Designer")]
        public void Initial_Position_ResizeInitial()
        {
            string templateName = "";
            string testValu = "";
            string testValu2 = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 10614851;
                testName = "Initial_Position_ResizeInitial_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14691108;
                    testName = "Initial_Position_ResizeInitial_Stage";
                }
                else
                {
                    //instance = 13109712;
                    testName = "Initial_Position_ResizeInitial_Prod";
                }
            }
            passFail = 1;  //reset before test
            testValu = "200";
            testValu2 = "200";
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Initial-DO NOT USE";
            CreateLogFile();

            //setup
            CreateTemplateUnderTest(templateName);

            Thread.Sleep(8000);
            tglog.AppendLogText(filePath, "Opened a Security Template ..... OK");

            try
            {
                //Test
                sst = tst.OpenInitial(driver);
                tglog.AppendLogText(filePath, "Open Initial Modal.....OK");
                Thread.Sleep(500);

                sst.EnterSignature(driver, "jaa");
                tglog.AppendLogText(filePath, "Entered New Initial...OK");
                Thread.Sleep(500);

                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Initial Height and Width ", ScreenshotImageFormat.Png, 0);
                tglog.AppendLogText(filePath, string.Concat("X = ", cc.FetchIDefaultValue(driver, tst.troyMark_X), " / Y = ", cc.FetchIDefaultValue(driver, tst.troyMark_Y)));
                Thread.Sleep(500);

                cc.EnterText(driver, testValu2, tst.troyMark_Height);
                tglog.AppendLogText(filePath, string.Concat("Initia; Height = ", testValu2));

                cc.EnterText(driver, testValu, tst.troyMark_Width);
                tglog.AppendLogText(filePath, string.Concat("Initial Width = ", testValu));

                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Initial Height and Width ", ScreenshotImageFormat.Png, 1);
                Thread.Sleep(1000);

                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());

                //Finish and Teardown
                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template Changes.....OK");
                Thread.Sleep(1000);

                tst.ExitDesigner(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(500);
                passFail = 0;
            }
            catch (Exception e)
            {
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Finish and Teardown
                tst.ExitDesigner2(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        [TestMethod, TestCategory("R2 - Designer")]
        public void Initial_Write_CancelWrite()
        {
            string templateName = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 10614851;
                testName = " Initial_Write_CancelWrite_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14691109;
                    testName = " Initial_Write_CancelWrite_Stage";
                }
                else
                {
                    //instance = 13109712;
                    testName = " Initial_Write_CancelWrite_Prod";
                }
            }
            passFail = 1;  //reset before test
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Initial-DO NOT USE";
            CreateLogFile();

            //setup
            CreateTemplateUnderTest(templateName);

            Thread.Sleep(8000);
            tglog.AppendLogText(filePath, "Opened a Security Template ..... OK");

            try
            {
                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Cancel Initial ", ScreenshotImageFormat.Png, 0);
                tglog.AppendLogText(filePath, string.Concat("X = ", cc.FetchIDefaultValue(driver, tst.troyMark_X), " / Y = ", cc.FetchIDefaultValue(driver, tst.troyMark_Y)));
                Thread.Sleep(500);

                //Test
                sst = tst.OpenInitial(driver);
                tglog.AppendLogText(filePath, "Open Initial Modal.....OK");
                Thread.Sleep(500);

                sst.CancelSignature(driver, "jaa");
                tglog.AppendLogText(filePath, "Cancel New Initial...OK");
                Thread.Sleep(500);

                //Finish and Teardown
                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template Changes.....OK");
                Thread.Sleep(1000);

                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Cancel Initial ", ScreenshotImageFormat.Png, 1);
                Thread.Sleep(1000);

                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), true.ToString());

                tst.ExitDesigner(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(500);
                passFail = 0;
            }
            catch (Exception e)
            {
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Finish and Teardown
                tst.ExitDesigner2(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        #endregion

        #region Watermark Tests
        [TestMethod, TestCategory("R2 - Designer")]
        public void Signature_Position_MoveSignature()
        {
            string templateName = "";
            string testValu = "";
            string testValu2 = "";
            string imagePath2 = "";
            string imagePath1 = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 10614850;
                testName = "Signature_Position_MoveSignature_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14691104;
                    testName = "Signature_Position_MoveSignature_Stage";
                }
                else
                {
                    //instance = 13109711;
                    testName = "Signature_Position_MoveSignature_Prod";
                }
            }
            passFail = 1;  //reset before test
            testValu = "561";
            testValu2 = "486";
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Signature-DO NOT USE";
            CreateLogFile();

            //setup
            CreateTemplateUnderTest(templateName);

            Thread.Sleep(2000);
            tglog.AppendLogText(filePath, "Opened a Security Template ..... OK");

            //Test
            try
            {
                sst = tst.OpenSignature(driver);
                tglog.AppendLogText(filePath, "Open Signature Modal.....OK");
                Thread.Sleep(500);

                sst.EnterSignature(driver, "Judi Anderson");
                tglog.AppendLogText(filePath, "Entered New Signature...OK");
                Thread.Sleep(500);

                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Signature X and Y ", ScreenshotImageFormat.Png, 0);
                tglog.AppendLogText(filePath, string.Concat("X = ", cc.FetchIDefaultValue(driver, tst.troyMark_X), " / Y = ", cc.FetchIDefaultValue(driver, tst.troyMark_Y)));
                Thread.Sleep(500);

                cc.EnterText(driver, testValu2, tst.troyMark_X);
                tglog.AppendLogText(filePath, string.Concat("Signature X Coordinate = ", testValu2));

                cc.EnterText(driver, testValu, tst.troyMark_Y);
                tglog.AppendLogText(filePath, string.Concat("Signature Y Coordinate = ", testValu));

                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Signature X and Y ", ScreenshotImageFormat.Png, 1);
                Thread.Sleep(1000);

                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());

                //Finish and Teardown
                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template Changes.....OK");
                Thread.Sleep(1000);

                tst.ExitDesigner(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(500);
                passFail = 0;
            }
            catch (Exception e)
            {
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Finish and Teardown
                tst.ExitDesigner2(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(500);
            }

            Thread.Sleep(500);
        }

        [TestMethod, TestCategory("R2 - Designer")]
        public void Signature_Position_ResizeSignature()
        {
            string templateName = "";
            string testValu = "";
            string testValu2 = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 10614851;
                testName = "Signature_Position_ResizeSignature_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14691105;
                    testName = "Signature_Position_ResizeSignature_Stage";
                }
                else
                {
                    //instance = 13109712;
                    testName = "Signature_Position_ResizeSignature_Prod";
                }
            }
            passFail = 1;  //reset before test
            testValu = "200";
            testValu2 = "200";
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Signature-DO NOT USE";
            CreateLogFile();

            //setup
            CreateTemplateUnderTest(templateName);

            Thread.Sleep(8000);
            tglog.AppendLogText(filePath, "Opened a Security Template ..... OK");

            try
            {
                //Test
                sst = tst.OpenSignature(driver);
                tglog.AppendLogText(filePath, "Open Signature Modal.....OK");
                Thread.Sleep(500);

                sst.EnterSignature(driver, "Judi Anderson");
                tglog.AppendLogText(filePath, "Entered New Signature...OK");
                Thread.Sleep(500);

                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Signature Height and Width ", ScreenshotImageFormat.Png, 0);
                tglog.AppendLogText(filePath, string.Concat("X = ", cc.FetchIDefaultValue(driver, tst.troyMark_X), " / Y = ", cc.FetchIDefaultValue(driver, tst.troyMark_Y)));
                Thread.Sleep(500);

                cc.EnterText(driver, testValu2, tst.troyMark_Height);
                tglog.AppendLogText(filePath, string.Concat("Signature Height = ", testValu2));

                cc.EnterText(driver, testValu, tst.troyMark_Width);
                tglog.AppendLogText(filePath, string.Concat("Signature Width = ", testValu));

                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Signature Height and Width ", ScreenshotImageFormat.Png, 1);
                Thread.Sleep(1000);

                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), false.ToString());

                //Finish and Teardown
                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template Changes.....OK");
                Thread.Sleep(1000);

                tst.ExitDesigner(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(500);
                passFail = 0;
            }
            catch (Exception e)
            {
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Finish and Teardown
                tst.ExitDesigner2(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        [TestMethod, TestCategory("R2 - Designer")]
        public void Signature_Write_CancelWrite()
        {
            string templateName = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 10614851;
                testName = "Signature_Write_CancelWrite_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14691106;
                    testName = "Signature_Write_CancelWrite_Stage";
                }
                else
                {
                    //instance = 13109712;
                    testName = "Signature_Write_CancelWrite_Prod";
                }
            }
            passFail = 1;  //reset before test
            imagePath = string.Concat(filePath, "Test Files\\");
            imagePath1 = imagePath;
            imagePath2 = imagePath;
            filePath = string.Concat(filePath, testName, ".txt");
            templateName = "Signature-DO NOT USE";
            CreateLogFile();

            //setup
            CreateTemplateUnderTest(templateName);

            Thread.Sleep(8000);
            tglog.AppendLogText(filePath, "Opened a Security Template ..... OK");

            try
            {
                imagePath1 = cc.TakeScreenShotWithDate(driver, imagePath, "Cancel Signature ", ScreenshotImageFormat.Png, 0);
                tglog.AppendLogText(filePath, string.Concat("X = ", cc.FetchIDefaultValue(driver, tst.troyMark_X), " / Y = ", cc.FetchIDefaultValue(driver, tst.troyMark_Y)));
                Thread.Sleep(500);

                //Test
                sst = tst.OpenSignature(driver);
                tglog.AppendLogText(filePath, "Open Signature Modal.....OK");
                Thread.Sleep(500);

                sst.CancelSignature(driver, "Judi Anderson");
                tglog.AppendLogText(filePath, "Cancel New Signature...OK");
                Thread.Sleep(500);

                //Finish and Teardown
                cc.ClickButton(driver, tst.saveButton);
                tglog.AppendLogText(filePath, "Saved Template Changes.....OK");
                Thread.Sleep(1000);

                imagePath2 = cc.TakeScreenShotWithDate(driver, imagePath, "Cancel Signature ", ScreenshotImageFormat.Png, 1);
                Thread.Sleep(1000);

                tglog.AppendLogText(filePath, string.Concat("Images Comparison = ", cc.CompareImages(imagePath1, imagePath2).ToString()));
                StringAssert.Contains(cc.CompareImages(imagePath1, imagePath2).ToString(), true.ToString());

                tst.ExitDesigner(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(500);
                passFail = 0;
            }
            catch (Exception e)
            {
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));

                //Finish and Teardown
                tst.ExitDesigner2(driver);
                tglog.AppendLogText(filePath, "Closed Template ..... OK");

                RemoveTemplateUnderTest(templateName);
                Thread.Sleep(5000);
            }

            Thread.Sleep(5000);
        }

        #endregion  

        #region Methods
        private void CreateLogFile()
        {
            tglog.CreateLogFile(filePath, string.Concat(Environment.NewLine, "----------- Start testing using ", brow, " -----------", Environment.NewLine));
            tglog.AppendLogText(filePath, string.Concat("Set up ", brow, " ..... OK"));
            tglog.AppendLogText(filePath, string.Concat("URL = ", baseURL));
            tglog.AppendLogText(filePath, string.Concat("Envionment = ", qaDev));
            tglog.AppendLogText(filePath, string.Concat("User = ", user));
            tglog.AppendLogText(filePath, "Opened SecureDocs in browser ..... OK");
            tglog.AppendLogText(filePath, string.Concat("Build Version: ", build));
            tglog.AppendLogText(filePath, "Logged into SecureDocs ..... OK");
            tglog.AppendLogText(filePath, string.Concat("Instance = ", instance));
            tglog.AppendLogText(filePath, string.Concat("FilePath = ", filePath));
            tglog.AppendLogText(filePath, "Opened SecureDocs Dashboard ..... OK");
        }

        private void CreateTemplateUnderTest(string text)
        {
            OpenSecurityTemplates();
            cst = st.OpenCreateTemplate(driver);

            tglog.AppendLogText(filePath, "Opened New Security Template popup ..... OK");

            tst = cst.OpenTemplate(driver);
            driver.SwitchTo().Window(baseWindowHandle);
            tglog.AppendLogText(filePath, "Opened New Security Template ..... OK");

            baseWindowHandle = driver.CurrentWindowHandle;

            Thread.Sleep(500);
            nt = tst.OpenNameTemplate(driver);
            Thread.Sleep(500);

            nt.NameTemplate(driver, text);
            Thread.Sleep(500);
        }

        private void OpenSecurityTemplates()
        {
            try
            {
                st = mp.OpenSecurityTemplates(driver);
                passFail = 0;
                tglog.AppendLogText(filePath, "Opened Security Templates ..... OK");

            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        private void RemoveTemplateUnderTest(string text)
        {
            string ele = "";
            string delBtn = "";
            int itemCount = 0;
            string templateName = "";

            itemCount = Convert.ToInt16(cc.FetchChildCount(driver, st.templateGrid));
            for (int x = 1; x <= itemCount; x++)
            {
                delBtn = string.Concat(st.templateGrid, "/div[", x.ToString(), "]/div[1]/div/a[3]");
                ele = string.Concat(st.templateGrid, "/div[", x.ToString(), "]/div[2]/a");
                templateName = cc.FetchInnerHTML(driver, ele);

                if (templateName == text)
                {
                    rst = st.RemoveTemplate(driver, delBtn);

                    StringAssert.Contains(string.Concat("Delete Template '", templateName, "'"), cc.FetchInnerHTML(driver, rst.header));
                    tglog.AppendLogText(filePath, "Opened Remove Security Template..... OK");

                    st = rst.SecurityTemplateYES(driver, "Yes");
                    tglog.AppendLogText(filePath, string.Concat("Removed template ", templateName));
                    break;
                }
            }
        }

        private void SendToPractitest()
        {
            if (qaDev.ToUpper() == "DEV")
            {
                ptc.PT_Post_Specific_Run(filePath, passFail, resultMsg, instance, project, brow, "Development");
            }
            else
            {
                if (qaDev.ToUpper() == "STAGE")
                {
                    ptc.PT_Post_Specific_Run(filePath, passFail, resultMsg, instance, project, brow, "Stage");
                }
                else
                {
                    ptc.PT_Post_Specific_Run(filePath, passFail, resultMsg, instance, project, brow, "Production");
                }
            }
        }

        private void SetupDriver()
        {
            if ((brow.ToLower() == "ie"))
            {
                this.driver = new InternetExplorerDriver("C:\\Selenium");
            }

            if ((brow.ToLower() == "chrome"))
            {
                this.driver = new ChromeDriver(@"C:\Selenium");
            }

            //if ((brow.ToLower() == "firefox"))
            //{
            //    this.driver = new FirefoxDriver();
            //}

        }

        private void SetUpEnvironVars()
        {
            if (qaDev.ToLower() == dev)
            {
                filePath = string.Concat(filePath, "Dev\\");
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    filePath = string.Concat(filePath, "Stage\\");
                }
                else
                {
                    filePath = string.Concat(filePath, "Prod\\");
                }
            }
        }

        #endregion

        //--------------------------------------------------------------------------------------------
        #region Additional test attributes
        [TestInitialize()]
        public void MyTestInitialize()
        {
            cc = new SecureRx.Common.Common_Code(driver);

            brow = ConfigurationManager.AppSettings["browser"];
            baseURL = ConfigurationManager.AppSettings["DocrityURL"];
            project = ConfigurationManager.AppSettings["project"];
            qaDev = ConfigurationManager.AppSettings["testEnv"];
            user = ConfigurationManager.AppSettings["testUser"].ToUpper();
            string pswd = ConfigurationManager.AppSettings["TestPswd"];

            SetupDriver();
            SetUpEnvironVars();

            lp = new SecureRx.Page_Elements.LoginPage(driver, baseURL);
            build = lp.FetchBuildVersion(driver);
            mp = lp.SDLogin(user, pswd, driver);

            //Did Home Menu open
            if (mp == null)
            {
                throw new NoSuchWindowException("SecureRx home page was not displayed!");
            }
            else
            {
                passFail = 0;
            }
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {

            driver.SwitchTo().Window(baseWindowHandle);
            Thread.Sleep(1000);

            try
            {
                mp.LogoutandCloseBrowser(driver);
                tglog.AppendLogText(filePath, "Logged out and Closed browser..... OK");
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }

            if (passFail != 0)
            {
                resultMsg = string.Concat(testName, " ..... FAILED!!!!! Please refer to log file");
                //driver.Close();
            }
            else
            {
                resultMsg = string.Concat(testName, " ..... PASSED!!!!!");
            }

            SendToPractitest();

            tglog.AppendLogText(filePath, "Close SecureRx");
            tglog.AppendLogText(filePath, "\n Test Finished \n");
        }

        #endregion

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;
    }
}
