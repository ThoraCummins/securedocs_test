﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;

namespace SecureRx.Tests
{
    [TestClass]
    public class Dashboard_Tests
    {
    #region libraries

        SecureRx.Page_Elements.MainPage mp = null;
        SecureRx.Page_Elements.LoginPage lp = null;

        SecureRx.Page_Elements.PrintAgents.Print_Agents_Page spa = null;
        SecureRx.Page_Elements.Activity.Activity_Detail actd = null;
        SecureRx.Page_Elements.Reports.DocumentLogs dl = null;
        SecureRx.Page_Elements.Templates.SecurityTemplates st = null;

        SecureRx.Common.Common_Code cc = null;
        SecureRx.Common.PT_Client ptc = new SecureRx.Common.PT_Client();
        SecureRx.Common.TGLogging tglog = new SecureRx.Common.TGLogging();
        #endregion

        //Vars
        private string baseURL = "";
        private string baseWindowHandle = "";
        //private string beta = "beta";
        private string brow = "";
        private string build = "";
        private string dev = "dev";
        private IWebDriver driver = null;
        private string filePath = "C:\\Test Logs\\SecureRx\\";
        private int instance = 0;
        private int passFail = 1;
        private string project = "";
        //private string qa = "qa";
        private string qaDev = "";
        private string resultMsg = "";
        private string staging = "stage";
        private string testName = "";
        private string user = "";

        [TestMethod, TestCategory("R1 - Dashboard")]
        public void R1_Dashboard_InputsAgents_VerifyAgentsCount()
        {
            string agentstCount = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 12632686;
                testName = "Dashboard_InputsAgents_VerifyAgentsCount_Dev";
            }
            else
            {
                if (qaDev.ToLower() ==staging)
                {
                    instance = 14591697;
                    testName = "Dashboard_InputsAgents_VerifyAgentsCount_Stage";
                }
                else
                {
                    instance = 14823110;
                    testName = "Dashboard_InputsAgents_VerifyAgentsCount_Prod";
                }
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();
            //tglog.AppendLogText(filePath, driver.Title);

            try
            {
                //setup
                agentstCount = cc.FetchInnerHTML(driver, mp.agentsCount);

                OpenPrintAgents();
                baseWindowHandle = driver.CurrentWindowHandle;

                //test
                StringAssert.Contains(agentstCount, cc.FetchChildCount(driver, spa.agentTable));
                passFail = 0;
                tglog.AppendLogText(filePath, string.Concat("Dashboard Agents count, ", agentstCount, " is same as Agents Count, ", cc.FetchChildCount(driver, spa.agentTable)));

            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        [TestMethod, TestCategory("R2 - Dashboard")]
        public void Dashboard_Activity_VerifyPagesSecuredCount()
        {
            string downloadFile = "C:\\Users\\Thora\\Downloads\\printlogs.csv";
            string endDate = "";
            string pageCount = "";
            string pageNumb = "";
            string recCount = "";
            int rows = 0;
            string startDate = "";
            //string totPages = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 12632685;
                testName = "Dashboard_Activity_VerifyPagesSecuredCount_Dev";
                startDate = "05/01/2017";
                endDate = DateTime.Today.AddDays(1).ToString();
            }
            else
            {
                if (qaDev.ToLower() ==staging)
                {
                    instance = 14591761;
                    testName = "Dashboard_Activity_VerifyPagesSecuredCount_Stage";
                    startDate = "05/01/2017";
                    endDate = DateTime.Today.AddDays(1).ToString();
                }
                else
                {
                    instance = 12982291;
                    testName = "Dashboard_Activity_VerifyPagesSecuredCount_Prod";
                    startDate = "05/01/2017";
                    endDate = DateTime.Today.AddDays(1).ToString();
                }
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();
            //tglog.AppendLogText(filePath, driver.Title);

            try
            {
                //setup
                pageCount = cc.FetchInnerHTML(driver, mp.pagesSecured);
                tglog.AppendLogText(filePath, string.Concat("Pages Secured Count is ", pageCount));

                dl = mp.OpenDocumentLogs(driver);
                baseWindowHandle = driver.CurrentWindowHandle;

                cc.ClickButton(driver, dl.calendarElement);
                cc.ClickButton(driver,dl.customRange);
                cc.EnterText(driver, startDate, dl.startDate);
                cc.EnterText(driver, endDate, dl.endDate);
                cc.ClickButton(driver, dl.applyBtn);
                Thread.Sleep(100);

                pageNumb = cc.FetchInnerHTML(driver, dl.pageNumbers);
                tglog.AppendLogText(filePath, pageNumb);
                rows = Convert.ToInt16(cc.FetchTableRows(driver, dl.tablegrid));
                recCount = cc.FetchChildCount(driver, dl.tablegrid);

                tglog.AppendLogText(filePath, string.Concat("Record Count is ", recCount, Environment.NewLine, Environment.NewLine));

                //file exists?
                if (File.Exists(downloadFile))
                {
                    File.Delete(downloadFile);
                }
                Thread.Sleep(100);

                //test
                cc.ClickButton(driver,dl.exportBtn);
                Thread.Sleep(5000);

                tglog.AppendLogText(filePath, "IMPORTANT!!! This test is not complete. Please verify the total page count in file is same as pages secured count on Dashboard.");
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        [TestMethod, TestCategory("R2 - Dashboard")]
        public void Dashboard_Activity_VerifyRecentActivity()
        {
            string cellOne = "";
            string copies = "";
            string createdDate = "";
            string docName = "";
            string inputAgent = "";
            string pages = "";
            string processTime = "";
            int rows = 0;
            string serialNumber = "";
            string userName = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 11117146;
                testName = "Dashboard_Activity_VerifyRecentActivity_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14591762;
                    testName = "Dashboard_Activity_VerifyRecentActivity_Stage";
                }
                else
                {
                    instance = 12982294;
                    testName = "Dashboard_Activity_VerifyRecentActivity_Prod";
                }
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();
            //tglog.AppendLogText(filePath, driver.Title);

            try
            {
                //setup
                rows = Convert.ToInt16(cc.FetchTableRows(driver, mp.printLogTable));
                tglog.AppendLogText(filePath, string.Concat("Row Count is ", rows.ToString()));

                for (int x = 1; x <= rows; x++)
                {
                    cellOne = cc.FetchCellTextValue(driver, "1", x.ToString(), mp.printLogTable);
                    cellOne = cellOne.Replace("\r\n", "");
                    TestContext.WriteLine(string.Concat("Cellone = ", cellOne));
                    serialNumber = cellOne.Substring(0, 16);
                    try
                    {
                        docName = cellOne.Substring(17, (cellOne.IndexOf(".pdf")-17)+4);
                    }
                    catch
                    {
                        try
                        {
                            docName = cellOne.Substring(17, (cellOne.IndexOf(".xlsx") -17) + 4);
                        }
                        catch
                        {
                            docName = "Not a PDF File";
                        }
                    }

                    inputAgent = cc.FetchCellValue(driver, "2", x.ToString(), mp.printLogTable);
                    userName = cc.FetchCellValue(driver, "3", x.ToString(), mp.printLogTable);
                    createdDate = cc.FetchCellValue(driver, "4", x.ToString(), mp.printLogTable);
                    processTime = cc.FetchCellValue(driver, "5", x.ToString(), mp.printLogTable);
                    pages = cc.FetchCellValue(driver, "6", x.ToString(), mp.printLogTable);
                    copies = cc.FetchCellValue(driver, "7", x.ToString(), mp.printLogTable);

                    tglog.AppendLogText(filePath, cellOne);

                    // test
                    actd = mp.OpenSpecificDocumentLog(driver, x.ToString());
                    Thread.Sleep(1000);

                    try
                    {
                        StringAssert.Contains(cc.FetchInnerHTML(driver, actd.serialNumber), serialNumber);
                        StringAssert.Contains(cc.FetchInnerText(driver, actd.documentName.Trim()), docName);
                        tglog.AppendLogText(filePath, string.Concat("serial number = ", serialNumber, " /  doc name = ", docName));

                        StringAssert.Contains(cc.FetchInnerHTML(driver, actd.processTime).Replace(" ms)", ""), processTime);
                        tglog.AppendLogText(filePath, string.Concat("created on ", createdDate, " /  Process Time is ", processTime));

                        StringAssert.Contains(cc.FetchInnerHTML(driver, actd.userName), userName);
                        tglog.AppendLogText(filePath, string.Concat("Input/Agent = ", inputAgent, " / user = ", userName));

                        StringAssert.Contains(cc.FetchInnerHTML(driver, actd.pages), pages);
                        StringAssert.Contains(cc.FetchInnerHTML(driver, actd.copies), copies);
                        tglog.AppendLogText(filePath, string.Concat(" pages = ", pages, " / copies  = ", copies));

                        passFail = 0;
                    }
                    catch
                    {
                        tglog.AppendLogText(filePath, string.Concat(cellOne, " ", docName, " ..... FAILED! Please reveiw log!"));
                        tglog.AppendLogText(filePath, string.Concat("created on ", createdDate, " /  Process Time is ", processTime));
                        tglog.AppendLogText(filePath, string.Concat("Input/Agent = ", inputAgent, " / user = ", userName));
                        tglog.AppendLogText(filePath, string.Concat(" pages = ", pages, " / copies  = ", copies));

                        passFail = 1;
                    }

                    cc.ClickButton(driver, actd.dashboardLink);
                }
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        [TestMethod, TestCategory("R1 - Dashboard")]
        public void R1_Dashboard_Reports_VerifySecuredPrescriptionsCount()
        {
            string docCount = "";
            int len = 0;
            string pageNumb = "";
            string recCount = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 12632684;
                testName = "Dashboard_Reports_VerifySecuredPrescriptionsCount_Dev";
            }
            else
            {
                if (qaDev.ToLower() ==staging)
                {
                    instance = 14591696;
                    testName = "Dashboard_Reports_VerifySecuredPrescriptionsCount_Stage";
                }
                else
                {
                    //instance = 14823109;
                    testName = "Dashboard_Reports_VerifySecuredPrescriptionsCount_Prod";
                }
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();
            //tglog.AppendLogText(filePath, driver.Title);

            try
            {
                //setup
                docCount = cc.FetchInnerHTML(driver, mp.securedDocumentsCount);

                dl = mp.OpenDocumentLogs(driver);
                tglog.AppendLogText(filePath, "Opened /Reports/DocumentLogs.....");
                baseWindowHandle = driver.CurrentWindowHandle;
                Thread.Sleep(500);

                cc.ClickButton(driver, dl.calendarElement);
                cc.ClickButton(driver, dl.customRange);
                cc.EnterText(driver, "05/01/2015", dl.startDate);
                cc.EnterText(driver, (DateTime.Today.AddDays(1)).ToString(), dl.endDate);
                cc.ClickButton(driver, dl.applyBtn);
                tglog.AppendLogText(filePath, "Set Test Date Range.....");
                Thread.Sleep(1000);

                //test
                cc.ScrollPage(driver);
                pageNumb = cc.FetchInnerHTML(driver, dl.pageNumbers);
                len = (pageNumb.IndexOf(")") - pageNumb.IndexOf("("))-9;
                recCount = pageNumb.Substring(pageNumb.IndexOf("(") + 1,len);

                cc.ScrollUP(driver);
                StringAssert.Contains(docCount, recCount);
                passFail = 0;
                tglog.AppendLogText(filePath, string.Concat("Dashboard Secured Documents count, ", docCount, " is same as Secured Documents Count, ", recCount));

            }
            catch (Exception e)
            {
                cc.ScrollUP(driver);
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        [TestMethod, TestCategory("R1 - Dashboard")]
        public void R1_Dashboard_SecurityTemplates_VerifyTemplateCount()
        {
            string stCount = "";

            if (qaDev.ToLower() == dev)
            {
                //instance = 12632687;
                testName = "Dashboard_SecurityTemplates_VerifyTemplateCount_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 14591698;
                    testName = "Dashboard_SecurityTemplates_VerifyTemplateCount_Stage";
                }
                else
                {
                    instance = 14823111;
                    testName = "Dashboard_SecurityTemplates_VerifyTemplateCount_Prod";
                }
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();
            //tglog.AppendLogText(filePath, driver.Title);

            try
            {
                //setup
                stCount = cc.FetchInnerHTML(driver, mp.securityTemplateCount);

                OpenSecurityTemplates();
                baseWindowHandle = driver.CurrentWindowHandle;

                //test
                StringAssert.Contains(stCount, cc.FetchChildCount(driver, st.templates));
                passFail = 0;
                tglog.AppendLogText(filePath, string.Concat("Dashboard Security Template count, ", stCount, " is same as Templates Count, ", cc.FetchChildCount(driver, st.templates)));

            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        #region Methods
        private void SetupDriver()
        {
            if ((brow.ToLower() == "ie"))
            {
                this.driver = new InternetExplorerDriver("C:\\Selenium");
            }

            if ((brow.ToLower() == "chrome"))
            {
                this.driver = new ChromeDriver(@"C:\Selenium");
            }

            //if ((brow.ToLower() == "firefox"))
            //{
            //    this.driver = new FirefoxDriver();
            //}

        }

        private void CreateLogFile()
        {
            tglog.CreateLogFile(filePath, string.Concat(Environment.NewLine, "----------- Start testing using ", brow, " -----------", Environment.NewLine));
            tglog.AppendLogText(filePath, string.Concat("Set up ", brow, " ..... OK"));
            tglog.AppendLogText(filePath, string.Concat("URL = ", baseURL));
            tglog.AppendLogText(filePath, string.Concat("Envionment = ", qaDev));
            tglog.AppendLogText(filePath, string.Concat("User = ", user));
            tglog.AppendLogText(filePath, "Opened SecureDocs in browser ..... OK");
            tglog.AppendLogText(filePath, string.Concat("Build Version: ", build));
            tglog.AppendLogText(filePath, "Logged into SecureDocs ..... OK");
            tglog.AppendLogText(filePath, string.Concat("Instance = ", instance));
            tglog.AppendLogText(filePath, string.Concat("FilePath = ", filePath));
            tglog.AppendLogText(filePath, "Opened SecureDocs Dashboard ..... OK");
        }

        private void OpenPrintAgents()
        {
            try
            {
                spa = mp.OpenPrintAgent(driver);
                //tglog.AppendLogText(filePath, driver.Title);

                StringAssert.Contains(cc.FetchInnerHTML(driver, spa.Header), "Print Agents");
                passFail = 0;
                tglog.AppendLogText(filePath, "Opened Secure Print Agents ..... OK");

            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        private void OpenSecurityTemplates()
        {
            try
            {
                st = mp.OpenSecurityTemplates(driver);

                //StringAssert.Contains(cc.FetchInnerHTML(driver, st.Header), "Security Templates");
                tglog.AppendLogText(filePath, "Opened Security Templates ..... OK");

            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        private void SendToPractitest()
        {
            if (qaDev.ToUpper() == "STAGE")
            {
                ptc.PT_Post_Specific_Run(filePath, passFail, resultMsg, instance, project, brow, "Stage");
            }
            else
            {
                ptc.PT_Post_Specific_Run(filePath, passFail, resultMsg, instance, project, brow, "Production");
            }
        }

        private void SetUpEnvironVars()
        {
            if (qaDev.ToLower() == dev)
            {
                filePath = string.Concat(filePath, "Dev\\");
            }
            else
                if (qaDev.ToLower() == staging)
            {
                filePath = string.Concat(filePath, "Stage\\");
            }
            else
            {
                filePath = string.Concat(filePath, "Prod\\");
            }
        }
        #endregion

        //--------------------------------------------------------------------------------------------
        #region Additional test attributes
        [TestInitialize()]
        public void MyTestInitialize()
        {
            cc = new Common.Common_Code(driver);

            brow = ConfigurationManager.AppSettings["browser"];
            baseURL = ConfigurationManager.AppSettings["DocrityURL"];
            project = ConfigurationManager.AppSettings["project"];
            qaDev = ConfigurationManager.AppSettings["testEnv"];
            user = ConfigurationManager.AppSettings["testUser"].ToUpper();
            string pswd = ConfigurationManager.AppSettings["TestPswd"];

            SetupDriver();
            SetUpEnvironVars();

            lp = new SecureRx.Page_Elements.LoginPage(driver, baseURL);
            build = lp.FetchBuildVersion(driver);
            mp = lp.SDLogin(user, pswd, driver);

            //Did Home Menu open
            if (mp == null)
            {
                throw new NoSuchWindowException("Docrity home page was not displayed!");
            }
            else
            {
                passFail = 0;
            }
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {

            driver.SwitchTo().Window(baseWindowHandle);
            Thread.Sleep(1000);

            try
            {
                mp.LogoutandCloseBrowser(driver);
                tglog.AppendLogText(filePath, "Logged out and Closed browser..... OK");
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }

            if (passFail != 0)
            {
                resultMsg = string.Concat(testName, " ..... FAILED!!!!! Please refer to log file");
                //driver.Close();
            }
            else
            {
                resultMsg = string.Concat(testName, " ..... PASSED!!!!!");
            }

            SendToPractitest();

            tglog.AppendLogText(filePath, "Close Docrity");
            tglog.AppendLogText(filePath, "\n Test Finished \n");
        }

        #endregion

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;
    }
}