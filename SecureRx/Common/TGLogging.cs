﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Input;
using System.CodeDom.Compiler;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;

namespace SecureRx.Common
{
    public class TGLogging
    {
        public void AppendLogText(string filePath, string logText)
        {
            using (StreamWriter sw = File.AppendText(filePath))
            {
                DateTime dt = DateTime.Now;

                sw.WriteLine(string.Concat(dt.ToString(), " ", logText));
            }
        }

        public void CreateLogFile(string filePath, string logText)
        {
            DateTime dt = DateTime.Now;
            logText = string.Concat(dt, " ", logText);

            if (File.Exists(filePath))
            {
                File.Delete(filePath);
                Thread.Sleep(3000);
                CreateTextFile(filePath, logText);
            }
            else
            {
                CreateTextFile(filePath, logText);
            }
        }

        public void CreateTestFiles(string oldPathName, string NewPathName)
        {
            System.IO.File.Copy(oldPathName, NewPathName);
        }

        private void CreateTextFile(string filePath, string fileText)
        {
            if (!File.Exists(filePath))
            {
                DateTime dt = DateTime.Now;
                File.WriteAllText(@filePath, fileText);
            }
        }

    }
}
