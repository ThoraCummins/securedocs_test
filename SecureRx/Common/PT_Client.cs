﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace SecureRx.Common
{
    public class PT_Client
    {
        private string authInfo = "";
        private string devEmail = ConfigurationManager.AppSettings["devEmail"];
        private string specificRunURL = ConfigurationManager.AppSettings["runURL"];
        private string token = ConfigurationManager.AppSettings["config"];

        SecureRx.Common.TGLogging tglog = new TGLogging();

        public void PT_Post_Specific_Instance(string filePath, int instance, string project, string browser, string env)
        {
            string json = "";
            specificRunURL = string.Concat(specificRunURL, project, "/instances/", instance.ToString(), ".json");
            tglog.AppendLogText(filePath, specificRunURL);
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(specificRunURL);
                authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(devEmail + ":" + token));
                request.Headers["Authorization"] = "Basic " + authInfo;
                request.ContentType = "application/json";
                request.Method = "POST";
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Request Headers: ", request.Headers));
                //tglog.AppendLogText(filePath, "{\"data\": {\"type\": \"instances\", \"attributes\": {\"instance-id\": " + instance.ToString() + ", \"exit-code\": " + passFail.ToString() + ",\"automated-execution-output\":  \"" + msg.Trim() + "\" }");
                //tglog.AppendLogText(filePath, ", \"custom-fields\":{\"---f-25406\": \"" + browser + "\" }}}");

                //if (passFail == 0)
                //{
                json = "{\"data\": {\"type\": \"instances\", \"attributes\": {\"instance-id\": " + instance.ToString() + "\"" +
                ", \"custom-fields\":{\"---f-25406\": \"" + browser + "\", \"---f-26029\": \"" + env + "\" }}}}";
                tglog.AppendLogText(filePath, json);
                //}
                //else
                //{
                //    json = "{\"data\": {\"type\": \"instances\", \"attributes\": {\"instance-id\": " + instance.ToString() + ", \"exit-code\": " + passFail.ToString() + ",\"automated-execution-output\":  \"" + msg.Trim() + "\" }" +
                //    ", \"files\": {\"data\": [{\"filename\": \"" + Path.GetFileName(filePath) + "\", \"content_encoded\": \"" + Convert.ToBase64String(File.ReadAllBytes(filePath)) + "\" }]} }  }";
                //}

                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    streamWriter.Write(json);
                }

                //var response = request.GetResponse();
                //tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Response Headers", Environment.NewLine, response.Headers.ToString()));
                //// get stream associated with the response
                //Stream receiveStream = response.GetResponseStream();

                //// pipes stream to higher level stread reader with the required encoding format
                //StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);
                //tglog.AppendLogText(filePath, "Response Stream received.");
                //tglog.AppendLogText(filePath, readStream.ReadToEnd());

                //response.Close();
                //readStream.Close();
            }
            catch (WebException e)
            {
                printResponse((HttpWebResponse)e.Response, filePath);
            }
        }

        public void PT_Post_Specific_Run(string filePath, int passFail, string msg, int instance, string project, string browser, string env)
        {
            string json = "";
            specificRunURL = string.Concat(specificRunURL, project, "/runs.json");

            try
            {
                var request = (HttpWebRequest)WebRequest.Create(specificRunURL);
                authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(devEmail + ":" + token));
                request.Headers["Authorization"] = "Basic " + authInfo;
                request.ContentType = "application/json";
                request.Method = "POST";
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Request Headers: ", request.Headers));
                tglog.AppendLogText(filePath, "{\"data\": {\"type\": \"instances\", \"attributes\": {\"instance-id\": " + instance.ToString() + ", \"exit-code\": " + passFail.ToString() + ",\"automated-execution-output\":  \"" + msg.Trim() + "\" }");
                tglog.AppendLogText(filePath, ", \"custom-fields\":{\"---f-25406\": \"" + browser + "\" }}}");

                json = "{\"data\": {\"type\": \"instances\", \"attributes\": {\"instance-id\": " + instance.ToString() + ", \"exit-code\": " + passFail.ToString() + ",\"automated-execution-output\":  \"" + msg.Trim() + "\" }" +
                ", \"custom-fields\":{\"---f-25406\": \"" + browser + "\", \"---f-26029\": \"" + env + "\" }" + //}}}";
                ", \"files\": {\"data\": [{\"filename\": \"" + Path.GetFileName(filePath) + "\", \"content_encoded\": \"" + Convert.ToBase64String(File.ReadAllBytes(filePath)) + "\" }]} }  }";
                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    streamWriter.Write(json);
                }

                var response = request.GetResponse();
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Response Headers", Environment.NewLine, response.Headers.ToString()));
                // get stream associated with the response
                Stream receiveStream = response.GetResponseStream();

                // pipes stream to higher level stread reader with the required encoding format
                StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);
                tglog.AppendLogText(filePath, "Response Stream received.");
                tglog.AppendLogText(filePath, readStream.ReadToEnd());

                response.Close();
                readStream.Close();
            }
            catch (WebException e)
            {
                printResponse((HttpWebResponse)e.Response, filePath);
            }
        }

        //public void PT_Post_Specific_Run(string filePath, string passFail, string msg, int instance, string project, string browser, string env)
        //{
        //    string json = "";
        //    specificRunURL = string.Concat(specificRunURL, project, "/runs.json");

        //    try
        //    {
        //        var request = (HttpWebRequest)WebRequest.Create(specificRunURL);
        //        authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(devEmail + ":" + token));
        //        request.Headers["Authorization"] = "Basic " + authInfo;
        //        request.ContentType = "application/json";
        //        request.Method = "POST";
        //        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Request Headers: ", request.Headers));
        //        tglog.AppendLogText(filePath, "{\"data\": {\"type\": \"instances\", \"attributes\": {\"instance-id\": " + instance.ToString() + ", \"status\": " + passFail + ",\"automated-execution-output\":  \"" + msg.Trim() + "\" }");
        //        tglog.AppendLogText(filePath, ", \"custom-fields\":{\"---f-25406\": \"" + browser + "\" }}}");

        //        //json = "{\"data\": {\"type\": \"instances\", \"attributes\": {\"instance-id\": " + instance.ToString() + ", \"status\": " + passFail + ",\"automated-execution-output\":  \"" + msg.Trim() + "\" }" +
        //        //", \"custom-fields\":{\"---f-25406\": \"" + browser + "\", \"---f-26029\": \"" + env + "\" }" + //}}}";
        //        //", \"files\": {\"data\": [{\"filename\": \"" + Path.GetFileName(filePath) + "\", \"content_encoded\": \"" + Convert.ToBase64String(File.ReadAllBytes(filePath)) + "\" }]} }  }";
        //        json = "{\"data\": {\"type\": \"instances\", \"attributes\": {\"instance-id\": " + instance.ToString() +  ",\"automated-execution-output\":  \"" + msg.Trim() + "\" }" +
        //        ", \"custom-fields\":{\"---f-25406\": \"" + browser + "\", \"---f-26029\": \"" + env + "\" }" + //}}}";
        //        ", \"status\": " + passFail + ", \"files\": {\"data\": [{\"filename\": \"" + Path.GetFileName(filePath) + "\", \"content_encoded\": \"" + Convert.ToBase64String(File.ReadAllBytes(filePath)) + "\" }]} }  }";               using (var streamWriter = new StreamWriter(request.GetRequestStream()))
        //        {
        //            streamWriter.Write(json);
        //        }

        //        var response = request.GetResponse();
        //        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Response Headers", Environment.NewLine, response.Headers.ToString()));
        //        // get stream associated with the response
        //        Stream receiveStream = response.GetResponseStream();

        //        // pipes stream to higher level stread reader with the required encoding format
        //        StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);
        //        tglog.AppendLogText(filePath, "Response Stream received.");
        //        tglog.AppendLogText(filePath, readStream.ReadToEnd());

        //        response.Close();
        //        readStream.Close();
        //    }
        //    catch (WebException e)
        //    {
        //        printResponse((HttpWebResponse)e.Response, filePath);
        //    }
        //}

        public void PT_Post_Specific_Run(string filePath, string imagePath, int passFail, string msg, int instance, string project, string browser, string env)
        {
            string json = "";
            specificRunURL = string.Concat(specificRunURL, project, "/runs.json");

            try
            {
                var request = (HttpWebRequest)WebRequest.Create(specificRunURL);
                authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(devEmail + ":" + token));
                request.Headers["Authorization"] = "Basic " + authInfo;
                request.ContentType = "application/json";
                request.Method = "POST";
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Request Headers: ", request.Headers));
                tglog.AppendLogText(filePath, "{\"data\": {\"type\": \"instances\", \"attributes\": {\"instance-id\": " + instance.ToString() + ", \"exit-code\": " + passFail.ToString() + ",\"automated-execution-output\":  \"" + msg.Trim() + "\" }");
                tglog.AppendLogText(filePath, ", \"custom-fields\":{\"---f-25406\": \"" + browser + "\" }}}");

                json = "{\"data\": {\"type\": \"instances\", \"attributes\": {\"instance-id\": " + instance.ToString() + ", \"exit-code\": " + passFail.ToString() + ",\"automated-execution-output\":  \"" + msg.Trim() + "\" }" +
                ", \"custom-fields\":{\"---f-25406\": \"" + browser + "\", \"---f-26029\": \"" + env + "\" }" +
                ", \"files\": {\"data\": [{\"filename\": \"" + Path.GetFileName(filePath) + "\", \"content_encoded\": \"" + Convert.ToBase64String(File.ReadAllBytes(filePath)) +
                "\" }]} }  }";

                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    streamWriter.Write(json);
                }

                var response = request.GetResponse();
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Response Headers", Environment.NewLine, response.Headers.ToString()));
                // get stream associated with the response
                Stream receiveStream = response.GetResponseStream();

                // pipes stream to higher level stread reader with the required encoding format
                StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);
                tglog.AppendLogText(filePath, "Response Stream received.");
                tglog.AppendLogText(filePath, readStream.ReadToEnd());

                response.Close();
                readStream.Close();
            }
            catch (WebException e)
            {
                printResponse((HttpWebResponse)e.Response, filePath);
            }
        }

        private void printResponse(HttpWebResponse r, string filePath)
        {

            tglog.AppendLogText(filePath, string.Concat("Status Code: ", r.StatusCode.ToString()));
            tglog.AppendLogText(filePath, string.Concat("Status Description: ", r.StatusDescription.ToString()));
            tglog.AppendLogText(filePath, readResponseString(r, filePath));
        }

        private string readResponseString(HttpWebResponse r, string filePath)
        {
            StreamReader reader = new StreamReader(r.GetResponseStream(), Encoding.GetEncoding("utf-8"));
            StringBuilder sb = new StringBuilder();
            Char[] read = new Char[256];
            int count = reader.Read(read, 0, 256);

            while (count > 0)
            {
                sb.Append(read, 0, count);
                count = reader.Read(read, 0, 256);
            }

            reader.Close();
            return sb.ToString();
        }

    }
}

