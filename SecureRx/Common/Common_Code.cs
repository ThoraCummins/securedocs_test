﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Input;
using System.CodeDom.Compiler;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Interactions.Internal;
using OpenQA.Selenium.Support.UI;
using SecureRx.Common;

namespace SecureRx.Common
{
    public class Common_Code
    {
    private By elementLocator = null;
    private By tableLocator = null;
    private int timeoutInMilliseconds = 500;
    //private int timeoutInSeconds = 60;

        public Common_Code(IWebDriver driver)
        {
        }

        //------------------------------------------------------------------------Selenium Methods-----------------------------------------------------------------

        //public void AddToList(IWebDriver driver, string element)
        //{
        //    List<IWebElement> displayedOptions = null;
        //    displayedOptions = driver.FindElements(By.XPath(element));
        //    for (WebElement option : displayedOptions)
        //    {
        //        if (option.displayed)
        //        {
        //            displayedOptions.Add(option);
        //        }
        //    }
        //}

        public void ClickButton(IWebDriver driver, string element)
        {
            IWebElement ele = null;

            elementLocator = By.XPath(element);
            ele = WebDriverExtensions.FindElementInMilliseconds(driver, elementLocator, timeoutInMilliseconds);
            ele.Click();
        }

        //public void DragandDrop(IWebDriver driver, string from, string to)
        //{
        //    By drag = By.XPath(from);
        //    By drop = By.XPath(to);

        //    Actions act = new Actions(driver);

        //    act.ClickAndHold(driver.FindElement(drag));
        //    Thread.Sleep(1000);
        //    act.MoveToElement(driver.FindElement(drop));
        //    act.Build();
        //    act.Perform();
        //}

        //public void DragandDropByOffset(IWebDriver driver, string from, int x, int y)
        //{
        //    By drag = By.XPath(from);

        //    Actions act = new Actions(driver);

        //    act.DragAndDropToOffset(driver.FindElement(drag), x,y).Build().Perform();

        //}

        public void EnterText(IWebDriver driver, string txt, string element)
        {
            IWebElement ele = null;

            elementLocator = By.XPath(element);
            ele = WebDriverExtensions.FindElementInMilliseconds(driver, elementLocator, timeoutInMilliseconds);

            ele.Click();
            ele.Clear();
            ele.SendKeys(txt);
        }

        public void EnterText_Clickable(IWebDriver driver, string txt, string element)
        {
            IWebElement ele = null;

            elementLocator = By.XPath(element);
            ele = WebDriverExtensions.FindElementClickable(driver, elementLocator, timeoutInMilliseconds);

            ele.Click();
            ele.Clear();
            ele.SendKeys(txt);
        }

        //public string FetchArray(IWebDriver driver, string element, int item)
        //{
        //    elementLocator = By.XPath(string.Concat(element,"option[",item.ToString(),"]"));
        //    SelectElement dropdown = null;
        //    IWebElement elementList = null;

        //    try
        //    {
        //        elementLocator = By.XPath(element);
        //        elementList = driver.FindElement(elementLocator);

        //        //return driver.FindElement(elementLocator).GetAttribute("innerHTML");
        //    }
        //    catch
        //    {
        //        return "";
        //    }
        //}

        public string FetchCellValue(IWebDriver driver, string col, string row, string table)
        {
            IWebElement ele = null;
            string machineName = "";

            tableLocator = By.XPath(string.Concat(table, "/tr[", row, "]/td[", col, "]"));
            ele = WebDriverExtensions.FindElementInMilliseconds(driver, tableLocator, timeoutInMilliseconds);

            machineName = ele.GetAttribute("innerHTML");
            return machineName;
        }

        public string FetchCellTextValue(IWebDriver driver, string col, string row, string table)
        {
            IWebElement ele = null;
            string machineName = "";

            tableLocator = By.XPath(string.Concat(table, "/tr[", row, "]/td[", col, "]"));
            ele = WebDriverExtensions.FindElementInMilliseconds(driver, tableLocator, timeoutInMilliseconds);
            machineName = ele.GetAttribute("innerText");
            return machineName;
        }

        public string FetchChecked(IWebDriver driver, string element)
        {
            IWebElement check = null;
            elementLocator = By.XPath(element);
            check = WebDriverExtensions.FindElementInMilliseconds(driver, elementLocator, timeoutInMilliseconds); 

            if (check.GetAttribute("checked") != null)
            {
                return true.ToString();
            }
            else
            {
                return false.ToString();
            }
        }

        public string FetchChildCount(IWebDriver driver, string item)
        {
            IWebElement ele = null;
            string row = "";

            elementLocator = By.XPath(item);
            ele = WebDriverExtensions.FindElementInMilliseconds(driver, elementLocator, timeoutInMilliseconds);
            row = ele.GetAttribute("childElementCount");
            return row;
        }

        public string FetchClass(IWebDriver driver, string element)
        {
            IWebElement ele = null;
            elementLocator = By.XPath(element);

            try
            {
                ele = WebDriverExtensions.FindElementInMilliseconds(driver, elementLocator, timeoutInMilliseconds);
                return ele.GetAttribute("className");
            }
            catch
            {
                return "";
            }
        }

        public string FetchIDefaultValue(IWebDriver driver, string element)
        {
            IWebElement ele = null;
            elementLocator = By.XPath(element);

            try
            {
                ele = WebDriverExtensions.FindElementInMilliseconds(driver, elementLocator, timeoutInMilliseconds);
                return ele.GetAttribute("defaultValue");
            }
            catch
            {
                return "";
            }
        }

        public string FetchInnerHTML(IWebDriver driver, string element)
        {
            IWebElement ele = null;
            elementLocator = By.XPath(element);

            try
            {
                ele = WebDriverExtensions.FindElementInMilliseconds(driver, elementLocator, timeoutInMilliseconds);
                return ele.GetAttribute("innerHTML");
            }
            catch
            {
                return "";
            }
        }

        public string FetchInnerText(IWebDriver driver, string element)
        {
            IWebElement ele = null;
            elementLocator = By.XPath(element);

            try
            {
                ele = WebDriverExtensions.FindElementInMilliseconds(driver, elementLocator, timeoutInMilliseconds);
                return ele.GetAttribute("innerText");
            }
            catch
            {
                return "";
            }
        }

        public string FetchPlaceholder(IWebDriver driver, string element)
        {
            IWebElement ele = null;
            elementLocator = By.XPath(element);

            try
            {
                ele = WebDriverExtensions.FindElementInMilliseconds(driver, elementLocator, timeoutInMilliseconds);
                return ele.GetAttribute("placeholder");
            }
            catch
            {
                return "";
            }
        }

        public string FetchSelectedOption(IWebDriver driver, string element)
        {
            SelectElement dropdown = null;
            IWebElement option = null;
            IWebElement elementList = null;

            elementLocator = By.XPath(element);
            elementList = WebDriverExtensions.FindElementInMilliseconds(driver, elementLocator, timeoutInMilliseconds);
            dropdown = new SelectElement(elementList);
            option = dropdown.SelectedOption;
           
            return option.Text;
        }

        public string FetchTableRows(IWebDriver driver, string table)
        {
            IWebElement ele = null;
            string row = "";

            tableLocator = By.XPath(table);
            ele = WebDriverExtensions.FindElementInMilliseconds(driver, tableLocator, timeoutInMilliseconds);
            row = ele.GetAttribute("childElementCount");
            return row;
        }

        public string FetchTextValue(IWebDriver driver, string element)
        {
            IWebElement ele = null;
            elementLocator = By.XPath(element);
            ele = WebDriverExtensions.FindElementInMilliseconds(driver, elementLocator, timeoutInMilliseconds);

            return ele.GetAttribute("value");
        }

        public void MoveSlider(IWebDriver driver, string element, int point)
        {
            IWebElement slider = null;
            elementLocator = By.XPath(element);
            slider = driver.FindElement(elementLocator);

            Actions move = new Actions(driver);
            move.DragAndDropToOffset(slider, point, 0).Build().Perform();
        }

        public void RefreshPage(IWebDriver driver)
        {
            driver.Navigate().Refresh();
        }

        public void SelectDropDownByElement(IWebDriver driver, string element, string text)
        {
            SelectElement dropdown = null;
            IWebElement elementList = null;

            elementLocator = By.XPath(element);

            elementList = WebDriverExtensions.FindElementInMilliseconds(driver, elementLocator, timeoutInMilliseconds);
            dropdown = new SelectElement(elementList);
            dropdown.SelectByText(text);
        }

        public void SelectDropDownByOption(IWebDriver driver, string element, int valu)
        {
            SelectElement dropdown = null;
            IWebElement elementList = null;

            elementLocator = By.XPath(element);

            elementList = WebDriverExtensions.FindElementInMilliseconds(driver, elementLocator, timeoutInMilliseconds);
            dropdown = new SelectElement(elementList);
            dropdown.SelectByIndex(valu);
        }

        public void SwitchToiFrame(IWebDriver driver)
        {
            List<IWebElement> frames = new List<IWebElement>(driver.FindElements(By.TagName("IFRAME")));

            for (int i = 0; i < frames.Count; i++)
            {
                if (frames.Count == 1)
                {
                    driver.SwitchTo().Frame(i);
                }
            }
        }

        // --------------------------------------------------------Misc. Methods----------------------------------------------------------------------------
        public string AcceptAssertOK(IWebDriver driver)
        {
            IAlert alert = null;

            try //is alert displayed
            {
                alert = driver.SwitchTo().Alert();
                alert.Accept();
                return "";
            }
            catch
            {
                return "Alert was not displayed";
            }
        }

        public string CancelAssert(IWebDriver driver)
        {
            IAlert alert = null;

            try //is alert displayed
            {
                alert = driver.SwitchTo().Alert();
                alert.Dismiss();
                return "";
            }
            catch
            {
                return "Alert was not displayed";
            }
        }

        //public bool CompareImages(string filename, string filename2, string SaveName, string imagePath)
        //{
        //    bool flag = true;
        //    Bitmap img1 = new Bitmap(filename);
        //    string img1_ref = "";
        //    Bitmap img2 = new Bitmap(filename2);
        //    string img2_ref = "";
        //    StringBuilder TimeAndDate = new StringBuilder(DateTime.Now.ToString());
        //    TimeAndDate.Replace("/", "_");
        //    TimeAndDate.Replace(":", "_");

        //    if (img1.Width == img2.Width && img1.Height == img2.Height)
        //    {
        //        for (int w = 0; w < img1.Width; w++)  // Image 1 Width
        //        {
        //            for (int h = 0; h < img1.Height; h++)  // Image 1 Height
        //            {
        //                img1_ref = img1.GetPixel(w, h).ToString();
        //                img2_ref = img2.GetPixel(w, h).ToString();

        //                if (img1_ref != img2_ref)
        //                {
        //                    img2.SetPixel(w, h, Color.OrangeRed);
        //                    img2.Save(string.Concat(imagePath, SaveName, TimeAndDate.ToString(), "_", " Marked.png"));
        //                    flag = false;
        //                    //break;
        //                }
        //            }
        //        }
        //    }
        //    else
        //    {
        //        flag = false;
        //    }
        //    return flag;
        //}

        public bool CompareImages(string filename, string filename2)
        {
            bool flag = true;
            Bitmap img1 = new Bitmap(filename);
            string img1_ref = "";
            Bitmap img2 = new Bitmap(filename2);
            string img2_ref = "";
            StringBuilder TimeAndDate = new StringBuilder(DateTime.Now.ToString());
            TimeAndDate.Replace("/", "_");
            TimeAndDate.Replace(":", "_");

            if (img1.Width == img2.Width && img1.Height == img2.Height)
            {
                for (int w = 0; w < img1.Width; w++)  // Image 1 Width
                {
                    for (int h = 0; h < img1.Height; h++)  // Image 1 Height
                    {
                        img1_ref = img1.GetPixel(w, h).ToString();
                        img2_ref = img2.GetPixel(w, h).ToString();

                        if (img1_ref != img2_ref)
                        {
                            flag = false;
                            break;
                        }
                    }
                }
            }
            else
            {
                flag = false;
            }
            return flag;
        }

        public string DaysInMonth(int year, int month)
        {
            int daysInMth = System.DateTime.DaysInMonth(year, month);
            return daysInMth.ToString();
        }

        public void ScrollPage(IWebDriver driver)
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            js.ExecuteScript("window.scrollTo(0, document.body.scrollHeight)");
        }

        public void ScrollUP(IWebDriver driver)
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            js.ExecuteScript("window.scrollTo(0, -document.body.scrollHeight)");
        }

        public string TakeScreenShotWithDate(IWebDriver driver, string Path, string fileName, ScreenshotImageFormat Format, int screenCounter)
        {
            //screenshot of all scree
            string pathName = "";
            StringBuilder TimeAndDate = new StringBuilder(DateTime.Now.ToString());
            TimeAndDate.Replace("/", "_");
            TimeAndDate.Replace(":", "_");

            screenCounter++; // Count number of screenshots taken
            DirectoryInfo validation = new DirectoryInfo(Path);  //System IO Object
            if (validation.Exists == true) // Capture Screen if path is available
            {
                pathName = string.Concat(Path, fileName, TimeAndDate.ToString(), "_", screenCounter.ToString(), ".", Format);

                ((ITakesScreenshot)driver).GetScreenshot().SaveAsFile(pathName, Format);
            }
            return pathName;
        }

        public void UploadFile(string filePath, string file)
        {
            System.Windows.Forms.SendKeys.SendWait(string.Concat(filePath, file));
            Thread.Sleep(3000);
            System.Windows.Forms.SendKeys.SendWait("{ENTER}");
        }
    }
}   

