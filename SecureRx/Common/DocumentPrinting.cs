﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Microsoft.Office.Interop.Word;
using System.Drawing;
using System.Drawing.Printing;
using System.Diagnostics;
using System.Windows.Input;
using System.CodeDom.Compiler;
using System.IO;
using System.Text.RegularExpressions;
using iTextSharp;


namespace SecureRx.Common
{
    public class DocumentPrinting
    {
        public DocumentPrinting()
        {
        }
        public void PrintPDF(string PDFname, string printer)
        {
            ProcessStartInfo info = new ProcessStartInfo();
            info.Verb = "PrintTo";
            info.Arguments = "\"" + printer + "\"";
            info.FileName = PDFname;
            info.CreateNoWindow = true;
            info.WindowStyle = ProcessWindowStyle.Hidden;

            Process p = new Process();
            p.StartInfo = info;
            p.Start();

            p.WaitForInputIdle();
            System.Threading.Thread.Sleep(3000);
            if (false == p.CloseMainWindow())
                p.Kill();

        }

        public void PrintWord(string filename, string printer)
        {
            Application apps = new Application();
            Document doc = null;

            try
            {
                doc = apps.Documents.Open(filename);
                apps.ActivePrinter = printer;
                apps.ActiveDocument.PrintOut(true);

                doc.Close();

                while (apps.BackgroundSavingStatus > 0)
                {
                    System.Threading.Thread.Sleep(250);
                }

                apps.Quit();
            }
            catch
            {
                doc.Close();
                apps.Quit();
            }
            finally
            {
                doc.Close();
                apps.Quit();
            }
        }

    }
}
