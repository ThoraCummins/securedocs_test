﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Interactions.Internal;
using OpenQA.Selenium.Support.UI;

namespace SecureRx.Common
{
    public static class WebDriverExtensions
    {

        public static IWebElement FindElementClickable(this IWebDriver driver, By by, int timeOut)
        {
            if (timeOut > 0)
            {
                var wait = new WebDriverWait(driver, TimeSpan.FromMilliseconds(timeOut));
                return wait.Until(drv => drv.FindElementClickable(by, timeOut));
            }
            return driver.FindElement(by);
        }
        public static IWebElement FindElementInSeconds (this IWebDriver driver, By by, int timeOut )
        {
            if (timeOut > 0)
            {
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeOut));
                return wait.Until(drv => drv.FindElement(by));
            }
            return driver.FindElement(by);
        }

        public static IWebElement FindElementInMilliseconds(this IWebDriver driver, By by, int timeOut)
        {
            if (timeOut > 0)
            {
                var wait = new WebDriverWait(driver, TimeSpan.FromMilliseconds(timeOut));
                return wait.Until(drv => drv.FindElement(by));
            }
            return driver.FindElement(by);
        }

    }
}
