﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Interactions.Internal;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using SecureRx;
using SecureRx.Common;
using SecureRx.Page_Elements;

namespace LoadandPerformance.LimitationTests
{
    [TestClass]
    public class Settings_Tests
    {
        #region libraries
        SecureRx.Page_Elements.MainPage mp = null;
        SecureRx.Page_Elements.LoginPage lp = null;

        //SecureRx.Page_Elements.PrintAgents.Bulk_Add bpa = null;
        SecureRx.Page_Elements.PrintAgents.Create_Print_Agent cspa = null;
        SecureRx.Page_Elements.PrintAgents.Print_Agents_Page spa = null;

        SecureRx.Common.Common_Code cc = null;
        SecureRx.Common.DocumentPrinting dp = new DocumentPrinting();
        SecureRx.Common.PT_Client ptc = new SecureRx.Common.PT_Client();
        SecureRx.Common.TGLogging tglog = new SecureRx.Common.TGLogging();
        #endregion

        //Vars
        private string baseURL = "";
        private string baseWindowHandle = "";
        private string brow = "";
        private string build = "";
        private string dev = "dev";
        private IWebDriver driver = null;
        private string filePath = "C:\\Test Logs\\Docrity\\";
        private int instance = 0;
        private int itemCnt = 0;
        private string outputFilePath = "C:\\PDF Files\\";
        private int passFail = 1;
        private string printClient = "";
        private string printerName = "";
        private string project = "";
        private string qaDev = "";
        private int rows = 0;
        private string resultMsg = "";
        private string staging = "stage";
        private DateTime startTest = DateTime.Now;
        private DateTime stopTest = DateTime.Now;
        private string testName = "";
        public string today = DateTime.Now.ToString("d");
        private string user = "";

        //-------------------------------------------------------------------------------------------

        #region Print Agent
        //[TestMethod, TestCategory("Limitation - Print Agents")]
        //public void PrintAgent_Added600PrintAgentsToServer()
        //{
        //    string added = "";
        //    string msg = "";
        //    string printAgent = "";
        //    string PrintAgents_600 = "600 PrintAgents.csv";
        //    int rows = 0;

        //    if (qaDev.ToLower() == dev)
        //    {
        //        //instance = 11128193;
        //        testName = "PrintAgent_Added600PrintAgentsToServer_Dev";
        //    }
        //    else
        //    {
        //        if (qaDev.ToLower() == staging)
        //        {
        //            //instance = 13708311;
        //            testName = "PrintAgent_Added600PrintAgentsToServer_Stage";
        //        }
        //        else
        //        {
        //            //instance = 11263397;
        //            testName = "PrintAgent_Added600PrintAgentsToServer_Prod";
        //        }
        //    }
        //    passFail = 1;  //reset before test
        //    filePath = string.Concat(filePath, testName, ".txt");

        //    CreateLogFile();

        //    try
        //    {
        //        //setup
        //        OpenPrintAgents();
        //        StringAssert.Contains(cc.FetchInnerHTML(driver, spa.Header), "Print Agents");
        //        tglog.AppendLogText(filePath, "Opened Secure Print Agents ..... OK");

        //        bpa = spa.BulkAdd(driver);
        //        tglog.AppendLogText(filePath, "Opened Bulk Add page..... OK");

        //        cc.ClickButton(driver, bpa.chooseFileBtn);
        //        cc.UploadFile(uploadFilePath,PrintAgents_600);
        //        tglog.AppendLogText(filePath, "Selected File ..... OK");
        //        Thread.Sleep(3000);

        //        startTest = DateTime.Now;
        //        tglog.AppendLogText(filePath, string.Concat("Start Setup at ", startTest));

        //        cc.ClickButton(driver, bpa.upload);
        //        Thread.Sleep(5000);
        //        stopTest = DateTime.Now;
        //        tglog.AppendLogText(filePath, string.Concat("Stop at ", stopTest));
        //        tglog.AppendLogText(filePath, "Upload Complete ..... OK");

        //        rows = Convert.ToInt16(cc.FetchTableRows(driver, bpa.tableGrid));
        //        for (int x = 0; x<= rows;x++)
        //        {
        //            printAgent = cc.FetchInnerHTML(driver, string.Concat(bpa.tableGrid, "/tr[", x.ToString(), "]/td[1]/a"));
        //            added = cc.FetchInnerHTML(driver, string.Concat(bpa.tableGrid, "/tr[", x.ToString(), "]/td[2]"));
        //            msg = cc.FetchInnerHTML(driver, string.Concat(bpa.tableGrid, "/tr[", x.ToString(), "]/td[3]"));
        //            tglog.AppendLogText(filePath, string.Concat("Print Agent = ",printAgent, "/ Added = ", added, " / Message = ", msg));
        //        }

        //        runtimeSeconds = stopTest.Subtract(startTest).TotalSeconds;

        //        tglog.AppendLogText(filePath, string.Concat("End Test ", stopTest.ToString()));
        //        tglog.AppendLogText(filePath, string.Concat("Time to Add 600 Print agents is ", stopTest.Subtract(startTest).ToString()));
        //        tglog.AppendLogText(filePath, string.Concat("Run time in seconds ", runtimeSeconds.ToString()));

        //    }
        //    catch (Exception e)
        //    {
        //        passFail = 1;
        //        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
        //    }
        //}

        [TestMethod, TestCategory("Limitation - Print Agents")]
        public void PrintAgent_RemoveBulkPrintAgents()
        {
            string printAgent = "";
            int rows = 0;
            string testAgent = "Limit Test";
            //string testFile = "C:\\Test Files\\Hygieia_Amoxicillin_Prescription_input_Secured.pdf";
            testName = " PrintAgent_Remove600PrintAgentsToServer_QA";
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                //setup
                OpenPrintAgents();

                rows = Convert.ToInt16(cc.FetchTableRows(driver,spa.agentTable));

                //test document to print agent
                //for (int x = 1; x <= rows; x++)
                //{
                //    printAgent = cc.FetchInnerHTML(driver, string.Concat(spa.agentTable, "/tr[", x.ToString(), "]/td[1]/a"));

                //    if (cc.FetchCellValue(driver, "1", x.ToString(), spa.agentTable).Contains(testAgent))
                //    {
                //        dp.PrintPDF(testFile, printAgent);
                //        tglog.AppendLogText(filePath, string.Concat("Document ", testFile, " printed on agent ", printAgent));
                //    }
                //}

                //remove print agent
                for (int x = 1; x <=rows; x++)
                {
                    printAgent = cc.FetchInnerHTML(driver, string.Concat(spa.agentTable, "/tr[", x.ToString(), "]/td[1]/a"));

                    if (cc.FetchCellValue(driver, "1", x.ToString(), spa.agentTable).Contains(testAgent))
                    {
                        cc.ClickButton(driver, string.Concat(spa.agentTable, "/tr[", x.ToString(), "]/td[5]/form/button"));
                        if (cc.AcceptAssertOK(driver) == "")
                        {
                            driver.SwitchTo().Window(baseWindowHandle);
                            tglog.AppendLogText(filePath, string.Concat("Selected print agent ", printAgent, " was removed.....OK"));

                            cc.RefreshPage(driver);
                            Thread.Sleep(2000);
                            rows = Convert.ToInt16(cc.FetchTableRows(driver, spa.agentTable));
                            x = 1;
                        }
                        else
                        {
                            passFail = 1;
                            tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Alert was not displayed!", Environment.NewLine));
                        }
                    }
                }
                passFail = 0;
                Thread.Sleep(3000);
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        #endregion

        //-------------------------------------------------------------------------------------------
        #region Methods
        private void AddPrintAgent()
        {
            cspa = spa.CreatePrintAgent(driver);
            StringAssert.Contains(cc.FetchInnerHTML(driver, cspa.Header), "Create Print Agent");
            tglog.AppendLogText(filePath, "Opened Create Print Agent ..... OK");
            Thread.Sleep(3000);

            printerName = "VPN Test";
            cc.EnterText(driver, printerName, cspa.VPN);
            cc.ScrollPage(driver);
            Thread.Sleep(2000);
            printClient = cc.FetchTextValue(driver, cspa.printClient);
            cc.EnterText(driver, outputFilePath, cspa.xpsFile2);

            tglog.AppendLogText(filePath, string.Concat("Printer Name = ", printerName));
            tglog.AppendLogText(filePath, string.Concat("Print Client = ", printClient));
            tglog.AppendLogText(filePath, string.Concat("XPS File Path = ", outputFilePath));
            Thread.Sleep(2000);

            cc.ClickButton(driver, cspa.saveBtn);
        }

        private void CreateLogFile()
        {
            tglog.CreateLogFile(filePath, string.Concat(Environment.NewLine, "----------- Start testing using ", brow, " -----------", Environment.NewLine));
            tglog.AppendLogText(filePath, string.Concat("Set up ", brow, " ..... OK"));
            tglog.AppendLogText(filePath, string.Concat("URL = ", baseURL));
            tglog.AppendLogText(filePath, string.Concat("Envionment = ", qaDev));
            tglog.AppendLogText(filePath, string.Concat("User = ", user));
            tglog.AppendLogText(filePath, "Opened SecureRx in browser ..... OK");
            tglog.AppendLogText(filePath, string.Concat("Build Version: ", build));
            tglog.AppendLogText(filePath, "Logged into SecureDocs ..... OK");
            tglog.AppendLogText(filePath, string.Concat("Instance = ", instance));
            tglog.AppendLogText(filePath, string.Concat("FilePath = ", filePath));
            tglog.AppendLogText(filePath, "Opened SecureDocs Dashboard ..... OK");
        }

        private void OpenPrintAgents()
        {
            try
            {
                spa = mp.OpenPrintAgent(driver);
                //tglog.AppendLogText(filePath, driver.Title);

                StringAssert.Contains(cc.FetchInnerHTML(driver, spa.Header), "Print Agents");
                passFail = 0;
                tglog.AppendLogText(filePath, "Opened Secure Print Agents ..... OK");

            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        private void RemovePrintAgent()
        {
            rows = Convert.ToInt16(cc.FetchTableRows(driver, spa.agentTable2));

            for (int x = 1; x <= rows; x++)
            {
                itemCnt = Convert.ToInt16(cc.FetchChildCount(driver, spa.agentTable2));
                if (cc.FetchCellValue(driver, "1", x.ToString(), spa.agentTable2).Contains("VPN Test"))
                {
                    cc.ClickButton(driver, string.Concat(spa.agentTable2, "/tr[", x.ToString(), "]/td[5]/form/button"));
                    Thread.Sleep(3000);
                }
            }
        }

        private void RemovePrintAgent(int rows)
        {
            for (int x = 1; x <= rows; x++)
            {
                itemCnt = Convert.ToInt16(cc.FetchChildCount(driver, spa.agentTable));
                if (cc.FetchCellValue(driver, "1", x.ToString(), spa.agentTable).Contains("VPN Test"))
                {
                    cc.ClickButton(driver, string.Concat(spa.agentTable, "/tr[", x.ToString(), "]/td[5]/form/button"));
                    Thread.Sleep(3000);
                }
            }
        }

        private void SetupDriver()
        {
            if ((brow.ToLower() == "ie"))
            {
                this.driver = new InternetExplorerDriver("C:\\Selenium");
            }

            if ((brow.ToLower() == "chrome"))
            {
                this.driver = new ChromeDriver(@"C:\Selenium");
            }

            if ((brow.ToLower() == "firefox"))
            {
                this.driver = new FirefoxDriver();
            }

        }

        private void SetUpEnvironVars()
        {
            if (qaDev.ToLower() == dev)
            {
                filePath = string.Concat(filePath, "Dev\\");
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    filePath = string.Concat(filePath, "Stage\\");
                }
                else
                {
                    filePath = string.Concat(filePath, "Prod\\");
                }
            }
        }

        private void SendToPractitest()
        {
            //if (qaDev.ToUpper() == "DEV")
            //{
            //    ptc.PT_Post_Specific_Run(filePath, passFail, resultMsg, instance, project, brow, "Development");
            //}
            //else
            //{
            //    if (qaDev.ToUpper() == "STAGE")
            //    {
            //        ptc.PT_Post_Specific_Run(filePath, passFail, resultMsg, instance, project, brow, "Stage");
            //    }
            //    else
            //    {
            //        ptc.PT_Post_Specific_Run(filePath, passFail, resultMsg, instance, project, brow, "Production");
            //    }
            //}
        }
        #endregion

        //--------------------------------------------------------------------------------------------
        #region Additional test attributes
        [TestInitialize()]
        public void MyTestInitialize()
        {
            cc = new SecureRx.Common.Common_Code(driver);

            brow = ConfigurationManager.AppSettings["browser"];
            baseURL = ConfigurationManager.AppSettings["DocrityURL"];
            project = ConfigurationManager.AppSettings["project"];
            qaDev = ConfigurationManager.AppSettings["testEnv"];
            user = ConfigurationManager.AppSettings["testUser"].ToUpper();
            string pswd = ConfigurationManager.AppSettings["TestPswd"];

            SetupDriver();
            SetUpEnvironVars();

            lp = new SecureRx.Page_Elements.LoginPage(driver, baseURL);
            build = lp.FetchBuildVersion(driver);
            mp = lp.SDLogin(user, pswd, driver);

            //Did Home Menu open
            if (mp == null)
            {
                throw new NoSuchWindowException("Docrity home page was not displayed!");
            }
            else
            {
                passFail = 0;
            }
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {

            driver.SwitchTo().Window(baseWindowHandle);
            Thread.Sleep(1000);

            try
            {
                mp.LogoutandCloseBrowser(driver);
                tglog.AppendLogText(filePath, "Logged out and Closed browser..... OK");
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }

            if (passFail != 0)
            {
                resultMsg = string.Concat(testName, " ..... FAILED!!!!! Please refer to log file");
                //driver.Close();
            }
            else
            {
                resultMsg = string.Concat(testName, " ..... PASSED!!!!!");
            }

            //SendToPractitest();

            tglog.AppendLogText(filePath, "Close Docrity");
            tglog.AppendLogText(filePath, "\n Test Finished \n");
        }

        #endregion

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;
    }
}