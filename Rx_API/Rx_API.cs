﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace Rx_API
{
    public class Rx_API
    {
        private string specificURL = "https://admin.docrity.com/connect/token";

        public string Get_Auth_Token()
        {
            var client = new RestClient(specificURL);
            var request = new RestRequest(Method.POST);
            request.AddHeader("Cache-Control", "no-cache");
            request.AddHeader("content-type", "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW");
            request.AddParameter("multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW", "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"grant_type\"\r\n\r\nclient_credentials\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"client_id\"\r\n\r\nb04c3fdfe4ba41608291b5982d43e21f\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"client_secret\"\r\n\r\nZgqNn+jKYciAsrW4pSppdvrgy3xQTTRXvRtGtFX/ftA=\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"scope\"\r\n\r\napi\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);

            return response.Content.ToString();
        }

        public string Get_SecureRxAuthURL(string token, string fn, string tn)
        {
            var client = new RestClient("https://admin.docrity.com/api/document/processtemplate");
            var jSon = "";
            var request = new RestRequest(Method.POST);
            request.AddHeader("Postman-Token", "f317e27b-36e4-41dc-9110-39c87ea9c086");
            request.AddHeader("Cache-Control", "no-cache");
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("Authorization", "Bearer " + token);
            //request.AddParameter("undefined", "{\n\t\"Fields\": {\n\t\t\"SalesOrderNumber\":\"123456789\",\n\t\t\"DateCode\":\"2018\",\n\t\t\"ProductModelNumber\":\"ABC-1234\"\n\t}\n}", ParameterType.RequestBody);
            request.AddParameter("undefined", "{\n\t\"Fields\": {\n\t\t\"file\":\"" + fn + "\",\n\t\t\"templateName\":\"" +tn+"\",\n\t}\n}", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);

            jSon = response.StatusCode.ToString();
            return jSon;
        }
    }
}
