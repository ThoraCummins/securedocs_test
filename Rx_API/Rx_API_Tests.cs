﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Rx_API
{
    [TestClass]

    public class Rx_API_Tests
    {
        string fileName = "C:\\Test Files\\Hygieia_Amoxicillin_Prescription_input_Secured.pdf";
        string templateName = "Sample Rx";

       Rx_API api = new Rx_API();

        [TestMethod, TestCategory("Rx API Testing")]
        public void FetchAuthToken()
        {
            var bearerToken = "";

            bearerToken = api.Get_Auth_Token();
            TestContext.WriteLine(bearerToken);
        }

        [TestMethod, TestCategory("Rx API Testing")]
        public void ProcessTemplate_TravisTest()
        {
            string accessToken = "";
            int accTok = 0;
            string arr = "";
            var bearerToken = "";
            int len = 0;
            int expire = 0;
            int tokType = 0;

            bearerToken = api.Get_Auth_Token();
            len = bearerToken.Length;

            TestContext.WriteLine(string.Concat(bearerToken, Environment.NewLine));

            accTok = bearerToken.IndexOf("access_token", 1) + 15;
            expire = bearerToken.IndexOf("expires_in", accTok) + 12;
            tokType = bearerToken.IndexOf("token_type", expire) + 13;

            TestContext.WriteLine(bearerToken.Substring(accTok, expire));
            TestContext.WriteLine(bearerToken.Substring(expire));
            TestContext.WriteLine(bearerToken.Substring(tokType));

            accessToken = bearerToken.Substring(accTok, expire - accTok);

            TestContext.WriteLine(accessToken);

            arr = api.Get_SecureRxAuthURL(bearerToken, fileName, templateName);
            TestContext.WriteLine(arr);

        }

        //---------------------------------------------------------------------------------------------------------------------------------------
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;
    }
}
