﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Http.Headers;
using System.Runtime;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using SecureRx;
using SecureRx.Common;
using SecureRx.Page_Elements;

namespace SecureDocs.Tests
{
    [TestClass]
    public class SecureDocs_Smoke_Tests
    {
        #region libraries

        SecureRx.Page_Elements.MainPage mp = null;
        SecureRx.Page_Elements.LoginPage lp = null;

        SecureRx.Page_Elements.Resources.API_References ar = null;
        SecureRx.Page_Elements.Resources.Documentation doc = null;
        SecureRx.Page_Elements.Resources.Software_Downloads sd = null;

        SecureRx.Page_Elements.Authentication.Authentication_Portal auth = null;
        SecureRx.Page_Elements.PrintAgents.Print_Agents_Page spa = null;
        SecureRx.Page_Elements.PrintAgents.Bulk_Add bpa = null;
        SecureRx.Page_Elements.PrintAgents.Create_Print_Agent cspa = null;
        SecureRx.Page_Elements.PrintAgents.Print_Agent_Details dspa = null;
        SecureRx.Page_Elements.PrintAgents.Edit_Print_Agent espa = null;

        SecureRx.Page_Elements.AuthPortalActivity.AuthPortal_Activity act = null;
        SecureRx.Page_Elements.Activity.Activity_Detail actd = null;
        SecureRx.Page_Elements.Activity.Activity da = null;

        SecureRx.Page_Elements.Prescriptions.Prescriptions pre = null;
        SecureRx.Page_Elements.Prescriptions.Prescription_Details preScript = null;

        SecureRx.Page_Elements.Reports.DocumentLogs dl = null;
        SecureRx.Page_Elements.Reports.Document_Logs_Details dld = null;
        SecureRx.Page_Elements.Reports.PortalEvents pe = null;
        SecureRx.Page_Elements.Reports.Portal_Events_Details ped = null;

        SecureRx.Page_Elements.Templates.SecurityTemplates st = null;
        //SecureRx.Page_Elements.Templates.Create_Security_Templates cst = null;
        //SecureRx.Page_Elements.Templates.Name_Template nt = null;
        //SecureRx.Page_Elements.Templates.Protect_Settings pst = null;
        SecureRx.Page_Elements.Templates.Remove_Security_Template rst = null;
        //SecureRx.Page_Elements.Templates.Show_Upload sst = null;
        //SecureRx.Page_Elements.Templates.Security_Profile spt = null;
        //SecureRx.Page_Elements.Templates.Template tst = null;
        //SecureRx.Page_Elements.Templates.Template_Settings tts = null;

        SecureRx.Page_Elements.Serialization.Serialization ser = null;

        SecureRx.Page_Elements.Users.Company_Details cd = null;
        SecureRx.Page_Elements.Users.Edit_User eu = null;
        SecureRx.Page_Elements.Users.New_User nu = null;
        SecureRx.Page_Elements.Users.User_Profile up = null;
        SecureRx.Page_Elements.Users.Reset_Password rp = null;
        SecureRx.Page_Elements.Settings.Settings stt = null;

        SecureRx.Common.Common_Code cc = null;
        SecureRx.Common.PT_Client ptc = new SecureRx.Common.PT_Client();
        SecureRx.Common.TGLogging tglog = new SecureRx.Common.TGLogging();
        #endregion

        //Vars
        private string baseURL = "";
        private string baseWindowHandle = "";
        private string brow = "";
        private string build = "";
        private string dev = "dev";
        private IWebDriver driver = null;
        private string filePath = "C:\\Test Logs\\SecureDocs\\";
        private int instance = 0;
        private int passFail = 1;
        private string project = "";
        private string qaDev = "";
        private string resultMsg = "";
        private string staging = "stage";
        private string testName = "";
        private string user = "";

        //--------------------------------------------------------------------------------------------------------------------------------------
        #region Authentication Portal
        [TestMethod, TestCategory("SD: Not Ready - Smoke_Tests")]
        public void Smoke_AuthenticationPortal_OpenActivityDetails()
        {
            //string scriptName = "";
            if (qaDev.ToLower() == dev)
            {
                instance = 12480595;
                testName = "smoke_AuthenticationPortal_OpenActivityDetails_Dev";
            }
            else
                if (qaDev.ToLower() == staging)
            {
                instance = 12335667;
                testName = "smoke_AuthenticationPortal_OpenActivityDetails_Stage";
            }
            else
            {
                instance = 12411499;
                testName = "smoke_AuthenticationPortal_OpenActivityDetails_Prod";
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                act = mp.OpenAuthenticationPortAct(driver);
                //Thread.Sleep(3000);

                StringAssert.Contains(cc.FetchInnerText(driver, act.Header), "Authentication Portal Activity");
                tglog.AppendLogText(filePath, "Opened Authentication Portal Activity Page ..... OK");

                //scriptName = cc.FetchInnerHTML(driver, pre.script);

                ////test
                //preScript = pre.OpenScriptDetails(driver);
                //Thread.Sleep(3000);

                //StringAssert.Contains(cc.FetchInnerHTML(driver, preScript.header), scriptName);
                //tglog.AppendLogText(filePath, "Document Name shown is same as on Prescriptions List.....");

                //passFail = 0;
                //tglog.AppendLogText(filePath, "Opened Document Log Detail ..... OK");
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }
        #endregion

        #region Dashboard
        [TestMethod, TestCategory("SD: Smoke_Tests")]
        public void smoke_Dashboard_OpenDocumentDetails()
        {
            string cellOne = "";
            string copies = "";
            string createdDate = "";
            string docName = "";
            string inputAgent = "";
            string pages = "";
            string processTime = "";
            int rows = 0;
            string serialNumber = "";
            string userName = "";

            if (qaDev.ToLower() == dev)
            {
                instance = 12480588;
                testName = "smoke_Dashboard_OpenDocumentDetails_dev";
            }
            else
                if (qaDev.ToLower() == staging)
            {
                instance = 12335660;
                testName = "smoke_Dashboard_OpenDocumentDetails_Stage";
            }
            else
            {
                instance = 12411492;
                testName = "smoke_Dashboard_OpenDocumentDetails_Prod";
            }

            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                //setup
                rows = Convert.ToInt16(cc.FetchTableRows(driver, mp.printLogTable));
                tglog.AppendLogText(filePath, string.Concat("Row Count is ", rows.ToString()));

                for (int x = 1; x <= 1; x++)
                {
                    cellOne = cc.FetchCellTextValue(driver, "1", x.ToString(), mp.printLogTable);
                    cellOne = cellOne.Replace("\r\n", " ");
                    serialNumber = cellOne.Substring(0, cellOne.IndexOf(" "));

                    try
                    {
                        docName = cellOne.Substring(cellOne.IndexOf(" ") + 1).Trim();
                    }
                    catch
                    {
                        docName = "Not a valid File Format";
                    }

                    inputAgent = cc.FetchCellValue(driver, "2", x.ToString(), mp.printLogTable);
                    userName = cc.FetchCellValue(driver, "3", x.ToString(), mp.printLogTable);
                    createdDate = cc.FetchCellValue(driver, "4", x.ToString(), mp.printLogTable);
                    processTime = cc.FetchCellValue(driver, "5", x.ToString(), mp.printLogTable);
                    pages = cc.FetchCellValue(driver, "6", x.ToString(), mp.printLogTable);
                    copies = cc.FetchCellValue(driver, "7", x.ToString(), mp.printLogTable);

                    // test
                    actd = mp.OpenSpecificDocumentLog(driver, x.ToString());
                    Thread.Sleep(5000);

                    try
                    {
                        StringAssert.Contains(cc.FetchInnerHTML(driver, actd.serialNumber), serialNumber);
                        StringAssert.Contains(cc.FetchInnerText(driver, actd.documentName.Trim()), docName);
                        tglog.AppendLogText(filePath, string.Concat("serial number = ", serialNumber, " /  doc name = ", docName));

                        StringAssert.Contains(cc.FetchInnerHTML(driver, actd.processTime).Replace(" ms)", ""), processTime);
                        tglog.AppendLogText(filePath, string.Concat("created on ", createdDate, " /  Process Time is ", processTime));

                        StringAssert.Contains(cc.FetchInnerHTML(driver, actd.userName), userName);
                        tglog.AppendLogText(filePath, string.Concat("Input/Agent = ", inputAgent, " / user = ", userName));

                        StringAssert.Contains(cc.FetchInnerHTML(driver, actd.pages), pages);
                        StringAssert.Contains(cc.FetchInnerHTML(driver, actd.copies), copies);
                        tglog.AppendLogText(filePath, string.Concat(" pages = ", pages, " / copies  = ", copies));

                        passFail = 0;
                    }
                    catch
                    {
                        tglog.AppendLogText(filePath, string.Concat(cellOne, " ", docName, " ..... FAILED! Please reveiw log!"));
                        tglog.AppendLogText(filePath, string.Concat("created on ", createdDate, " /  Process Time is ", processTime));
                        tglog.AppendLogText(filePath, string.Concat("Input/Agent = ", inputAgent, " / user = ", userName));
                        tglog.AppendLogText(filePath, string.Concat(" pages = ", pages, " / copies  = ", copies));

                        passFail = 1;
                    }

                    cc.ClickButton(driver, actd.dashboardLink);
                }
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        [TestMethod, TestCategory("SD: Smoke_Tests")]
        public void smoke_Dashboard_ViewDocumentLogs()
        {
            if (qaDev.ToLower() == dev)
            {
                instance = 12480589;
                testName = "smoke_Dashboard_ViewDocumentLogs_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 12335661;
                    testName = "smoke_Dashboard_ViewDocumentLogs_Stage";
                }
                else
                {
                    instance = 12411493;
                    testName = "smoke_Dashboard_ViewDocumentLogs_Prod";
                }
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                da = mp.OpenActivityFromDashboard(driver);

                StringAssert.Contains(cc.FetchInnerHTML(driver, da.Header), "Document Logs");
                passFail = 0;
                tglog.AppendLogText(filePath, "Opened Document Logs From View All Activity ..... OK");
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        #endregion

        #region Prescriptions
        [TestMethod, TestCategory("SD: Smoke_Tests")]
        public void smoke_Prescriptions_OpenPrescriptionDetails()
        {
            string scriptName = "";
            if (qaDev.ToLower() == dev)
            {
                instance = 12480594;
                testName = "smoke_Prescriptions_OpenPrescriptionDetails_Dev";
            }
            else
                if (qaDev.ToLower() == staging)
            {
                instance = 12335666;
                testName = "smoke_Prescriptions_OpenPrescriptionDetails_Stage";
            }
            else
            {
                instance = 12411498;
                testName = "smoke_Prescriptions_OpenPrescriptionDetails_Prod";
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                pre = mp.OpenPrescriptions(driver);

                StringAssert.Contains(cc.FetchInnerHTML(driver, pre.header), "Prescriptions");
                tglog.AppendLogText(filePath, "Opened Prescriptions Page ..... OK");

                scriptName = cc.FetchInnerHTML(driver, pre.script);

                //test
                preScript = pre.OpenScriptDetails(driver);

                StringAssert.Contains(cc.FetchInnerHTML(driver, preScript.header), scriptName);
                tglog.AppendLogText(filePath, "Document Name shown is same as on Prescriptions List.....");

                passFail = 0;
                tglog.AppendLogText(filePath, "Opened Document Log Detail ..... OK");
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }
        #endregion

        #region Reports
        [TestMethod, TestCategory("SD: Smoke_Tests")]
        public void smoke_Reports_OpenDocumentLogDetail()
        {
            if (qaDev.ToLower() == dev)
            {
                instance = 12480590;
                testName = "smoke_Reports_OpenDocumentLogDetail_Dev";
            }
            else
                if (qaDev.ToLower() == staging)
            {
                instance = 12335662;
                testName = "smoke_Reports_OpenDocumentLogDetail_Stage";
            }
            else
            {
                instance = 12411494;
                testName = "smoke_Reports_OpenDocumentLogDetail_Prod";
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                dl = mp.OpenDocumentLogs(driver);
                Thread.Sleep(3000);

                StringAssert.Contains(cc.FetchInnerHTML(driver, dl.header), "Document Logs");
                passFail = 0;
                tglog.AppendLogText(filePath, "Opened Document Logs Page ..... OK");

                //Date Change
                cc.ClickButton(driver, dl.calendarElement);
                cc.ClickButton(driver, dl.customRange);
                cc.EnterText(driver, "06/01/2018", dl.startDate);
                cc.EnterText(driver, (DateTime.Today.AddDays(1)).ToString(), dl.endDate);
                cc.ClickButton(driver, dl.applyBtn);

                //test
                dld = dl.OpenDocumentLogDetails(driver);

                passFail = 0;
                tglog.AppendLogText(filePath, "Opened Document Log Detail ..... OK");
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        [TestMethod, TestCategory("SD: Smoke_Tests")]
        public void smoke_Reports_OpenPortalEventsDetail()
        {
            string startDate = "";
            if (qaDev.ToLower() == dev)
            {
                instance = 12480613;
                testName = "smoke_Reports_OpenPortalEventsDetail_Dev";
                startDate = "08/01/2018";
            }
            else
                if (qaDev.ToLower() == staging)
            {
                instance = 12368338;
                testName = "smoke_Reports_OpenPortalEventsDetail_Stage";
                startDate = "09/01/2018";
            }
            else
            {
                instance = 12411517;
                testName = "smoke_Reports_OpenPortalEventsDetail_Prod";
                startDate = "09/01/2018";
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                pe = mp.OpenPortalEvents(driver);
                Thread.Sleep(3000);

                StringAssert.Contains(cc.FetchInnerHTML(driver, pe.header), "Portal Events");
                tglog.AppendLogText(filePath, "Opened Document Logs Page ..... OK");

                //Date Change
                cc.ClickButton(driver, pe.calendarElement);
                cc.ClickButton(driver, pe.customRange);
                cc.EnterText(driver, startDate, pe.startDate);
                cc.EnterText(driver, (DateTime.Today.AddDays(1)).ToString(), pe.endDate);
                cc.ClickButton(driver, pe.applyBtn);

                //test
                ped = pe.OpenPortalEventsDetails(driver);
                passFail = 0;
                tglog.AppendLogText(filePath, "Opened Document Log Detail ..... OK");
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        #endregion

        #region Resources
        [TestMethod, TestCategory("SD: Smoke_Tests")]
        public void smoke_Resources_OpenAPIDocumentation()
        {
            if (qaDev.ToLower() == dev)
            {
                //instance = 12480592;
                testName = "smoke_Resources_OpenAPIDocumentation_Dev";
            }
            else
                if (qaDev.ToLower() == staging)
            {
                //instance = 12335664;
                testName = "smoke_Resources_OpenAPIDocumentation_Stage";
            }
            else
            {
                //instance = 12411496;
                testName = "smoke_Resources_OpenAPIDocumentation_Prod";
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                baseWindowHandle = driver.CurrentWindowHandle;
                ar = mp.OpenAPIReference(driver);
                passFail = 0;
                tglog.AppendLogText(filePath, "Opened API v1 Documentation ..... OK");
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        [TestMethod, TestCategory("SD: Smoke_Tests")]
        public void smoke_Resources_OpenDocumentation()
        {
            if (qaDev.ToLower() == dev)
            {
                instance = 12480591;
                testName = "smoke_Resources_OpenDocumentation_Dev";
            }
            else
                if (qaDev.ToLower() == staging)
            {
                instance = 12335663;
                testName = "smoke_Resources_OpenDocumentation_Stage";
            }
            else
            {
                instance = 12411495;
                testName = "smoke_Resources_OpenDocumentation_Prod";
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                doc = mp.OpenDocumentation(driver);

                passFail = 0;
                tglog.AppendLogText(filePath, "Opened Documentation ..... OK");
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        [TestMethod, TestCategory("SD: Smoke_Tests")]
        public void smoke_Resources_OpenSoftware()
        {
            if (qaDev.ToLower() == dev)
            {
                instance = 12480612;
                testName = "smoke_Resources_OpenSoftware_Dev";
            }
            else
                if (qaDev.ToLower() == staging)
            {
                instance = 12342761;
                testName = "smoke_Resources_OpenSoftware_Stage";
            }
            else
            {
                instance = 12411516;
                testName = "smoke_Resources_OpenSoftware_Prod";
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                sd = mp.OpenDownloads(driver);

                StringAssert.Contains(cc.FetchInnerHTML(driver, sd.Header), "Downloads");
                passFail = 0;
                tglog.AppendLogText(filePath, "Opened Downloads ..... OK");
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }
        #endregion

        #region Security Templates
        //[TestMethod, TestCategory("Smoke_Tests")]
        //public void smoke_Designer_OpenProtectSettings()
        //{
        //    if (qaDev.ToLower() == dev)
        //    {
        //        instance = 12480611;
        //        testName = "smoke_Designer_OpenProtectSettings_Dev";
        //    }
        //    else
        //    {
        //        if (qaDev.ToLower() == staging)
        //        {
        //            instance = 12335683;
        //            testName = "smoke_Designer_OpenProtectSettings_Stage";
        //        }
        //        else
        //        {
        //            instance = 12411515;
        //            testName = "smoke_Designer_OpenProtectSettings_Prod";
        //        }
        //    }
        //    passFail = 1;  //reset before test
        //    filePath = string.Concat(filePath, testName, ".txt");

        //    CreateLogFile();

        //    try
        //    {
        //        OpenSecurityTemplates();
        //        baseWindowHandle = driver.CurrentWindowHandle;

        //        tst = st.OpenTemplate(driver);
        //        //Thread.Sleep(8000);
        //        tglog.AppendLogText(filePath, "Opened a Security Template ..... OK");

        //        //***** Test
        //        pst = tst.OpenProtectSettings(driver);
        //        //Thread.Sleep(5000);

        //        StringAssert.Contains(cc.FetchInnerHTML(driver, pst.header), "Protect Settings");
        //        tglog.AppendLogText(filePath, "Protect Settings Modal Opened ..... OK");

        //        pst.CancelProtectSettings(driver);
        //        //Thread.Sleep(3000);
        //        passFail = 0;

        //        tst.ExitDesigner(driver);
        //    }
        //    catch (Exception e)
        //    {
        //        passFail = 1;
        //        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
        //    }
        //}

        //[TestMethod, TestCategory("Smoke_Tests")]
        //public void smoke_SecurityTemplates_CreateSecurityTemplate()
        //{
        //    if (qaDev.ToLower() == dev)
        //    {
        //        instance = 12480606;
        //        testName = "SMOKE_SecurityTemplates_CreateSecurityTemplate_Dev";
        //    }
        //    else
        //    {
        //        if (qaDev.ToLower() == staging)
        //        {
        //            instance = 12335678;
        //            testName = "SMOKE_SecurityTemplates_CreateSecurityTemplate_Stage";
        //        }
        //        else
        //        {
        //            instance = 12411510;
        //            testName = "SMOKE_SecurityTemplates_CreateSecurityTemplate_Prod";
        //        }
        //    }
        //    passFail = 1;  //reset before test
        //    filePath = string.Concat(filePath, testName, ".txt");

        //    CreateLogFile();

        //    try
        //    {
        //        OpenSecurityTemplates();
        //        baseWindowHandle = driver.CurrentWindowHandle;

        //        //***** Create Test
        //        cst = st.OpenCreateTemplate(driver);
        //        tglog.AppendLogText(filePath, driver.Title);

        //        StringAssert.Contains(cc.FetchInnerHTML(driver, cst.Header), "New Security Template");
        //        tglog.AppendLogText(filePath, "Opened New Security Template popup ..... OK");

        //        tst=cst.OpenTemplate(driver);
        //        driver.SwitchTo().Window(baseWindowHandle);
        //        tglog.AppendLogText(filePath, "Opened New Security Template ..... OK");

        //        //Thread.Sleep(5000);
        //        nt = tst.OpenNameTemplate(driver);
        //        //Thread.Sleep(3000);

        //        nt.NameTemplate(driver, "Template Name");
        //        //Thread.Sleep(3000);

        //        tst.ExitDesigner(driver);

        //        RemoveTemplateUnderTest("Template Name");
        //        passFail = 0;   // fail until can test designer from this path
        //    }
        //    catch (Exception e)
        //    {
        //        passFail = 1;
        //        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
        //    }
        //}

        [TestMethod, TestCategory("SD: Smoke_Tests")]
        public void smoke_SecurityTemplates_RemoveTemplateLink()
        {
            string templateName = "";

            if (qaDev.ToLower() == dev)
            {
                instance = 12480608;
                testName = "SMOKE_SecurityTemplates_RemoveTemplateLink_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 12335680;
                    testName = "SMOKE_SecurityTemplates_RemoveTemplateLink_Stage";
                }
                else
                {
                    instance = 12411512;
                    testName = "SMOKE_SecurityTemplates_RemoveTemplateLink_Prod";
                }
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                OpenSecurityTemplates();
                baseWindowHandle = driver.CurrentWindowHandle;
                templateName = cc.FetchInnerHTML(driver, st.templateName);
                tglog.AppendLogText(filePath, string.Concat("Template Name = ", templateName));

                //***** Create Test
                rst = st.RemoveTemplate(driver);

                StringAssert.Contains(string.Concat("Delete Template '", templateName, "'"), cc.FetchInnerHTML(driver, rst.header));
                passFail = 0;
                tglog.AppendLogText(filePath, "Opened Remove Security Template..... OK");

                // Close Delete
                cc.ClickButton(driver, rst.noBtn);
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        //[TestMethod, TestCategory("Smoke_Tests")]
        //public void smoke_Designer_OpenSecurityProfile()
        //{
        //    if (qaDev.ToLower() == dev)
        //    {
        //        instance = 12480609;
        //        testName = "smoke_Designer_OpenSecurityProfile_Dev";
        //    }
        //    else
        //    {
        //        if (qaDev.ToLower() == staging)
        //        {
        //            instance = 12335681;
        //            testName = "smoke_Designer_OpenSecurityProfile_Stage";
        //        }
        //        else
        //        {
        //            instance = 12411513;
        //            testName = "smoke_Designer_OpenSecurityProfile_Prod";
        //        }
        //    }
        //    passFail = 1;  //reset before test
        //    filePath = string.Concat(filePath, testName, ".txt");

        //    CreateLogFile();

        //    try
        //    {
        //        OpenSecurityTemplates();
        //        baseWindowHandle = driver.CurrentWindowHandle;

        //        tst = st.OpenTemplate(driver);
        //        //Thread.Sleep(8000);
        //        tglog.AppendLogText(filePath, "Opened a Security Template ..... OK");

        //        //***** Test
        //        spt=tst.OpenAdvancedCode(driver);
        //        //Thread.Sleep(5000);

        //        StringAssert.Contains(cc.FetchInnerHTML(driver, spt.header), "Security Profile");
        //        tglog.AppendLogText(filePath, "Security Profile Modal Opened ..... OK");

        //        spt.CancelSecurityProfile(driver);
        //        //Thread.Sleep(3000);
        //        passFail = 0;

        //        tst.ExitDesigner(driver);
        //    }
        //    catch (Exception e)
        //    {
        //        passFail = 1;
        //        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
        //    }
        //}

        //[TestMethod, TestCategory("Smoke_Tests")]
        //public void smoke_Designer_OpenTemplateSettings()
        //{
        //    if (qaDev.ToLower() == dev)
        //    {
        //        instance = 12480610;
        //        testName = "smoke_Designer_OpenTemplateSettings_Dev";
        //    }
        //    else
        //    {
        //        if (qaDev.ToLower() == staging)
        //        {
        //            instance = 12335682;
        //            testName = "smoke_Designer_OpenTemplateSettings_Stage";
        //        }
        //        else
        //        {
        //            instance = 12411514;
        //            testName = "smoke_Designer_OpenTemplateSettings_Prod";
        //        }
        //    }
        //    passFail = 1;  //reset before test
        //    filePath = string.Concat(filePath, testName, ".txt");

        //    CreateLogFile();

        //    try
        //    {
        //        OpenSecurityTemplates();
        //        baseWindowHandle = driver.CurrentWindowHandle;

        //        tst = st.OpenTemplate(driver);
        //        //Thread.Sleep(8000);
        //        tglog.AppendLogText(filePath, "Opened a Security Template ..... OK");

        //        //***** Test
        //        tts = tst.OpenTemplateSettings(driver);
        //        //Thread.Sleep(5000);

        //        StringAssert.Contains(cc.FetchInnerHTML(driver, tts.header), "Template Settings");
        //        tglog.AppendLogText(filePath, "Template Settings Modal Opened ..... OK");

        //        tts.CancelTemplateSettings(driver);
        //        //Thread.Sleep(3000);

        //        tst.ExitDesigner(driver);
        //        passFail = 0;
        //    }
        //    catch (Exception e)
        //    {
        //        passFail = 1;
        //        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
        //    }
        //}

        //[TestMethod, TestCategory("Smoke_Tests")]
        //public void smoke_SecurityTemplates_ViewSecuredDocument()
        //{
        //    if (qaDev.ToLower() == dev)
        //    {
        //        instance = 12480607;
        //        testName = "smoke_SecurityTemplates_ViewSecuredDocument_Dev";
        //    }
        //    else
        //    {
        //        if (qaDev.ToLower() == staging)
        //        {
        //            instance = 12335679;
        //            testName = "smoke_SecurityTemplates_ViewSecuredDocument_Stage";
        //        }
        //        else
        //        {
        //            instance = 12411511;
        //            testName = "smoke_SecurityTemplates_ViewSecuredDocument_Prod";
        //        }
        //    }
        //    passFail = 1;  //reset before test
        //    filePath = string.Concat(filePath, testName, ".txt");

        //    CreateLogFile();

        //    try
        //    {
        //        OpenSecurityTemplates();
        //        baseWindowHandle = driver.CurrentWindowHandle;

        //        //***** Show Upload
        //        sst = st.OpenShowUpload(driver);

        //        StringAssert.Contains(cc.FetchInnerText(driver, sst.Header), "Secure Document with ");
        //        tglog.AppendLogText(filePath, "Opened Secure Document popup ..... OK");
        //        //Thread.Sleep(2000);

        //        cc.ClickButton(driver, sst.browseBtn);
        //        //Thread.Sleep(2000);

        //        cc.UploadFile("C:\\Test Files\\", "001-BirthCertificate-Sample1.pdf");
        //        tglog.AppendLogText(filePath, "Uploaded File ..... OK");
        //        //Thread.Sleep(3000);

        //        actd=sst.OpenSecuredDocumentViewer(driver);
        //        //Thread.Sleep(15000);
        //        tglog.AppendLogText(filePath, "Secured Document Shown in Viewer..... OK");

        //        cc.ClickButton(driver, actd.backToDashboard);
        //        //Thread.Sleep(3000);
        //        tglog.AppendLogText(filePath, "Closed Secured Document Viewer and Returned to Dashboard ..... OK");
        //        driver.SwitchTo().Window(baseWindowHandle);
        //        passFail = 0;   // fail until can test designer from this path
        //    }
        //    catch (Exception e)
        //    {
        //        passFail = 1;
        //        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
        //    }
        //}

        #endregion

        #region Settings
        [TestMethod, TestCategory("SD: Smoke_Tests")]
        public void smoke_Authentication_OpenAuthenticationPortal()
        {
            if (qaDev.ToLower() == dev)
            {
                instance = 12480597;
                testName = "smoke_Authentication_OpenAuthenticationPortal_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 12335669;
                    testName = "smoke_Authentication_OpenAuthenticationPortal_Stage";
                }
                else
                {
                    instance = 12411501;
                    testName = "smoke_Authentication_OpenAuthenticationPortal_Prod";
                }
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                auth = mp.OpenAuthPort(driver);
                tglog.AppendLogText(filePath, driver.Title);

                StringAssert.Contains(cc.FetchInnerHTML(driver, auth.header), "Auth Portal settings applies to all AuthMarks.");
                tglog.AppendLogText(filePath, "Opened Authentication Portal ..... OK");
                passFail = 0;
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        [TestMethod, TestCategory("SD: Smoke_Tests")]
        public void smoke_PrintAgent_ClickPrintAgentEdit()
        {
            if (qaDev.ToLower() == dev)
            {
                instance = 12480601;
                testName = "SMOKE_PrintAgent_ClickPrintAgent_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 12335673;
                    testName = "SMOKE_PrintAgent_ClickPrintAgent_Stage";
                }
                else
                {
                    instance = 12411505;
                    testName = "SMOKE_PrintAgent_ClickPrintAgent_Prod";
                }
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                OpenPrintAgents();

                //***** Create Test
                espa = spa.EditPrintAgent(driver);
                StringAssert.Contains(cc.FetchInnerHTML(driver, espa.Header), "Edit Print Agent");
                passFail = 0;
                tglog.AppendLogText(filePath, "Opened Edit Print Agent ..... OK");
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        [TestMethod, TestCategory("SD: Smoke_Tests")]
        public void smoke_PrintAgent_ClickPrintAgentName()
        {
            if (qaDev.ToLower() == dev)
            {
                instance = 12480600;
                testName = "SMOKE_PrintAgent_ClickPrintAgentName_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 12335672;
                    testName = "SMOKE_PrintAgent_ClickPrintAgentName_Stage";
                }
                else
                {
                    instance = 12411504;
                    testName = "SMOKE_PrintAgent_ClickPrintAgentName_Prod";
                }
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                OpenPrintAgents();

                //***** Create Test
                espa = spa.PrintAgentDetail(driver);
                StringAssert.Contains(cc.FetchInnerHTML(driver, espa.Header), "Print Agent");
                passFail = 0;
                //tglog.AppendLogText(filePath, "Opened Print Agent Detail ..... OK");

                //espa = dspa.EditPrintAgent(driver);
                //StringAssert.Contains(cc.FetchInnerHTML(driver, espa.Header), "Edit Print Agent");
                //passFail = 0;
                tglog.AppendLogText(filePath, "Opened Edit Print Agent ..... OK");
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        [TestMethod, TestCategory("SD: Smoke_Tests")]
        public void smoke_PrintAgent_CreatePrintAgent()
        {
            if (qaDev.ToLower() == dev)
            {
                instance = 12480599;
                testName = "SMOKE_PrintAgent_CreatePrintAgent_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 12335671;
                    testName = "SMOKE_PrintAgent_CreatePrintAgent_Stage";
                }
                else
                {
                    instance = 12411503;
                    testName = "SMOKE_PrintAgent_CreatePrintAgent_Prod";
                }
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                OpenPrintAgents();

                //***** Create Test
                cspa = spa.CreatePrintAgent(driver);
                StringAssert.Contains(cc.FetchInnerHTML(driver, cspa.Header), "Create Print Agent");
                passFail = 0;
                tglog.AppendLogText(filePath, "Opened Create Print Agent ..... OK");
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        [TestMethod, TestCategory("SD: Smoke_Tests")]
        public void smoke_PrintAgent_OpenBulkAdd()
        {
            if (qaDev.ToLower() == dev)
            {
                instance = 12547618;
                testName = "smoke_PrintAgent_OpenBulkAdd_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 12519791;
                    testName = "smoke_PrintAgent_OpenBulkAdd_Stage";
                }
                else
                {
                    instance = 12548570;
                    testName = "smoke_PrintAgent_OpenBulkAdd_Prod";
                }
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                OpenPrintAgents();

                //***** Create Test
                bpa = spa.BulkAdd(driver);
                StringAssert.Contains(cc.FetchInnerHTML(driver, bpa.header), "Select a CSV file of new print agents to be imported ");
                passFail = 0;
                tglog.AppendLogText(filePath, "Opened Bulk Add Print Agents ..... OK");
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        [TestMethod, TestCategory("SD: Smoke_Tests")]
        public void smoke_Serialization_OpenSerialization()
        {
            if (qaDev.ToLower() == dev)
            {
                instance = 12480598;
                testName = "smoke_Serialization_OpenSerialization_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 12335670;
                    testName = "smoke_Serialization_OpenSerialization_Stage";
                }
                else
                {
                    instance = 12411502;
                    testName = "smoke_Serialization_OpenSerialization_Prod";
                }
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                ser = mp.OpenSerialization(driver);

                StringAssert.Contains(cc.FetchInnerHTML(driver, ser.Header), "Serialization");
                tglog.AppendLogText(filePath, "Opened Serialization ..... OK");
                passFail = 0;
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        [TestMethod, TestCategory("SD: Smoke_Tests")]
        public void smoke_Settings_OpenGeneralSettings()
        {
            if (qaDev.ToLower() == dev)
            {
                instance = 12480596;
                testName = "smoke_Settings_OpenGeneralSettings_Dev";
            }
            else
            {
                if (qaDev.ToLower() == staging)
                {
                    instance = 12335668;
                    testName = "smoke_Settings_OpenGeneralSettings_Stage";
                }
                else
                {
                    instance = 12411500;
                    testName = "smoke_Settings_OpenGeneralSettings_Prod";
                }
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                stt = mp.OpenSettings(driver);

                tglog.AppendLogText(filePath, "Opened General Settings page ..... OK");

                passFail = 0;
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        #endregion

        #region User
        //[TestMethod, TestCategory("Smoke_Tests")]
        //public void smoke_AcctSet_OpenAccountSettings()
        //{
        //    string company = "";

        //    if (qaDev.ToLower() == dev)
        //    {
        //        instance = 12480602;
        //        testName = "smoke_AcctSet_OpenAccountSettings_Dev";
        //        company = "Thora's Rx Company";
        //    }
        //    else
        //        if (qaDev.ToLower() == staging)
        //        {
        //            instance = 12335674;
        //            testName = "smoke_AcctSet_OpenAccountSettings_Stage";
        //            company = "QA Test Company";
        //    }
        //    else
        //            {
        //                instance = 12411506;
        //                testName = "smoke_AcctSet_OpenAccountSettings_Prod";
        //                company = "QA Test Company";
        //    }
        //    passFail = 1;  //reset before test
        //    filePath = string.Concat(filePath, testName, ".txt");

        //    CreateLogFile();

        //    try
        //    {
        //        asSet = mp.OpenAccountSettings(driver);

        //        StringAssert.Contains(cc.FetchInnerHTML(driver, asSet.header), company);
        //        tglog.AppendLogText(filePath, "Opened Account Settings page ..... OK");

        //        passFail = 0;
        //    }
        //    catch (Exception e)
        //    {
        //        passFail = 1;
        //        tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
        //    }
        //}

        [TestMethod, TestCategory("SD: Smoke_Tests")]
        public void smoke_User_EditUser()
        {
            if (qaDev.ToLower() == dev)
            {
                instance = 12480604;
                testName = "SMOKE_User_EditUser_Dev";
            }
            else
                if (qaDev.ToLower() == staging)
            {
                instance = 12335676;
                testName = "SMOKE_User_EditUser_Stage";
            }
            else
            {
                instance = 12411508;
                testName = "SMOKE_User_EditUser_Prod";
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                cd = mp.OpenCompanyDetail(driver);

                StringAssert.Contains(cc.FetchInnerHTML(driver, cd.header), "Users");
                tglog.AppendLogText(filePath, "Opened Company User page ..... OK");

                eu = cd.OpenEditUser(driver);

                StringAssert.Contains(cc.FetchInnerHTML(driver, eu.header), "Edit User");
                tglog.AppendLogText(filePath, "Opened Edit User page ..... OK");

                passFail = 0;
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        [TestMethod, TestCategory("SD: Smoke_Tests")]
        public void smoke_User_OpenNewUser()
        {
            if (qaDev.ToLower() == dev)
            {
                instance = 12480603;
                testName = "SMOKE_User_OpenNewUser_Dev";
            }
            else
                if (qaDev.ToLower() == staging)
            {
                instance = 12335675;
                testName = "SMOKE_User_OpenNewUser_Stage";
            }
            else
            {
                instance = 12411507;
                testName = "SMOKE_User_OpenNewUser_Prod";
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                cd = mp.OpenCompanyDetail(driver);

                StringAssert.Contains(cc.FetchInnerHTML(driver, cd.header), "Users");
                tglog.AppendLogText(filePath, "Opened Company User page ..... OK");

                nu = cd.OpenNewUser(driver);

                StringAssert.Contains(cc.FetchInnerHTML(driver, nu.header), "Create User");
                tglog.AppendLogText(filePath, "Opened New User page ..... OK");

                passFail = 0;
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        [TestMethod, TestCategory("SD: Smoke_Tests")]
        public void smoke_Users_OpenUserProfile()
        {
            if (qaDev.ToLower() == dev)
            {
                testName = "smoke_Users_OpenUserProfile_Dev";
                instance = 12480593;
            }
            else
                if (qaDev.ToLower() == staging)
            {
                testName = "smoke_Users_OpenUserProfile_Stage";
                instance = 12335665;
            }
            else
            {
                instance = 12411497;
                testName = "smoke_Users_OpenUserProfile_Prod";
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                up = mp.OpenUserProfile(driver);

                StringAssert.Contains(cc.FetchInnerHTML(driver, up.header), "Profile");
                tglog.AppendLogText(filePath, "Opened Profile page ..... OK");
                passFail = 0;
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        [TestMethod, TestCategory("SD: Smoke_Tests")]
        public void smoke_User_ResetPassword()
        {
            if (qaDev.ToLower() == dev)
            {
                instance = 12480605;
                testName = "SMOKE_User_ResetPswd_Dev";
            }
            else
                if (qaDev.ToLower() == staging)
            {
                instance = 12335677;
                testName = "SMOKE_User_ResetPswd_Stage";
            }
            else
            {
                instance = 12411509;
                testName = "SMOKE_User_ResetPswd_Prod";
            }
            passFail = 1;  //reset before test
            filePath = string.Concat(filePath, testName, ".txt");

            CreateLogFile();

            try
            {
                cd = mp.OpenCompanyDetail(driver);

                StringAssert.Contains(cc.FetchInnerHTML(driver, cd.header), "Users");
                tglog.AppendLogText(filePath, "Opened Company User page ..... OK");

                rp = cd.OpenResetPassword(driver);

                StringAssert.Contains(cc.FetchInnerHTML(driver, rp.header), "Reset Password");
                tglog.AppendLogText(filePath, "Opened Reset Password page ..... OK");

                passFail = 0;
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        #endregion

        #region Methods
        private void SetupDriver()
        {
            if ((brow.ToLower() == "ie"))
            {
                this.driver = new InternetExplorerDriver("C:\\Selenium");
            }

            if ((brow.ToLower() == "chrome"))
            {
                this.driver = new ChromeDriver(@"C:\Selenium");
            }

            //if ((brow.ToLower() == "firefox"))
            //{
            //    this.driver = new FirefoxDriver();
            //}

        }

        private void CreateLogFile()
        {
            tglog.CreateLogFile(filePath, string.Concat(Environment.NewLine, "----------- Start testing using ", brow, " -----------", Environment.NewLine));
            tglog.AppendLogText(filePath, string.Concat("Set up ", brow, " ..... OK"));
            tglog.AppendLogText(filePath, string.Concat("URL = ", baseURL));
            tglog.AppendLogText(filePath, string.Concat("Envionment = ", qaDev));
            tglog.AppendLogText(filePath, string.Concat("User = ", user));
            tglog.AppendLogText(filePath, "Opened SecureDocs in browser ..... OK");
            tglog.AppendLogText(filePath, string.Concat("Build Version: ", build));
            tglog.AppendLogText(filePath, "Logged into SecureDocs ..... OK");
            tglog.AppendLogText(filePath, string.Concat("Instance = ", instance));
            tglog.AppendLogText(filePath, string.Concat("FilePath = ", filePath));
            tglog.AppendLogText(filePath, "Opened SecureDocs Dashboard ..... OK");
        }

        private void OpenPrintAgents()
        {
            try
            {
                spa = mp.OpenPrintAgent(driver);

                StringAssert.Contains(cc.FetchInnerHTML(driver, spa.Header), "Print Agents");
                passFail = 0;
                tglog.AppendLogText(filePath, "Opened Secure Print Agents ..... OK");

            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        private void OpenSecurityTemplates()
        {
            try
            {
                st = mp.OpenSecurityTemplates(driver);
                tglog.AppendLogText(filePath, "Opened Security Templates ..... OK");

            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }
        }

        private void RemoveTemplateUnderTest(string text)
        {
            string ele = "";
            string element = "";
            int itemCount = 0;
            string templateName = "";

            itemCount = Convert.ToInt16(cc.FetchChildCount(driver, st.templateGrid));
            for (int x = 1; x <= itemCount; x++)
            {
                element = string.Concat(st.templateGrid, "/div[", x.ToString(), "]/div[2]/a");
                ele = string.Concat(st.templateGrid, "/div[", x.ToString(), "]/div[1]/div/a[3]");
                templateName = cc.FetchInnerHTML(driver, element);

                if (templateName == text)
                {
                    rst = st.RemoveTemplate(driver, ele);

                    StringAssert.Contains(string.Concat("Delete Template '", templateName, "'"), cc.FetchInnerHTML(driver, rst.header));
                    tglog.AppendLogText(filePath, "Opened Remove Security Template..... OK");

                    st = rst.SecurityTemplateYES(driver, "Yes");
                    tglog.AppendLogText(filePath, string.Concat("Removed template ", templateName));
                    break;
                }
            }
        }

        private void SendToPractitest()
        {
            if (qaDev.ToUpper() == "STAGE")
            {
                ptc.PT_Post_Specific_Run(filePath, passFail, resultMsg, instance, project, brow, "Stage");
            }
            else
            {
                ptc.PT_Post_Specific_Run(filePath, passFail, resultMsg, instance, project, brow, "Production");
            }
        }

        private void SetUpEnvironVars()
        {
            if (qaDev.ToLower() == dev)
            {
                filePath = string.Concat(filePath, "Dev\\");
            }
            else
                if (qaDev.ToLower() == staging)
            {
                filePath = string.Concat(filePath, "Stage\\");
            }
            else
            {
                filePath = string.Concat(filePath, "Prod\\");
            }
        }
        #endregion

        //--------------------------------------------------------------------------------------------
        #region Additional test attributes
        [TestInitialize()]
        public void MyTestInitialize()
        {
            cc = new SecureRx.Common.Common_Code(driver);

            brow = ConfigurationManager.AppSettings["browser"];
            baseURL = ConfigurationManager.AppSettings["DocrityURL"];
            project = ConfigurationManager.AppSettings["project"];
            qaDev = ConfigurationManager.AppSettings["testEnv"];
            user = ConfigurationManager.AppSettings["testUser"].ToUpper();
            string pswd = ConfigurationManager.AppSettings["TestPswd"];

            SetupDriver();
            SetUpEnvironVars();

            lp = new SecureRx.Page_Elements.LoginPage(driver, baseURL);
            build = lp.FetchBuildVersion(driver);
            mp = lp.SDLogin(user, pswd, driver);

            //Did Home Menu open
            if (mp == null)
            {
                throw new NoSuchWindowException("SecureDocs home page was not displayed!");
            }
            else
            {
                passFail = 0;
            }
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {

            driver.SwitchTo().Window(baseWindowHandle);
            Thread.Sleep(1000);

            try
            {
                mp.LogoutandCloseBrowser(driver);
                tglog.AppendLogText(filePath, "Logged out and Closed browser..... OK");
            }
            catch (Exception e)
            {
                passFail = 1;
                tglog.AppendLogText(filePath, string.Concat(Environment.NewLine, "Error Message: ", Environment.NewLine, e.Message.ToString(), Environment.NewLine));
            }

            if (passFail != 0)
            {
                resultMsg = string.Concat(testName, " ..... FAILED!!!!! Please refer to log file");
                //driver.Close();
            }
            else
            {
                resultMsg = string.Concat(testName, " ..... PASSED!!!!!");
            }

            SendToPractitest();

            tglog.AppendLogText(filePath, "Close SecureDocs");
            tglog.AppendLogText(filePath, "\n Test Finished \n");
        }

        #endregion

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;
    }
}